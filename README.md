# Urtic-backend #


## Prérequis pour la compilation et éxécution ##

* Une sdk de java 8 d'installé ([Open-jdk8](https://github.com/adoptium/temurin8-binaries/releases))
* Une bd postgres roulant au port 5432 (Voir [Urtic-environment-deployment](https://bitbucket.org/sodanconseil/urtic-environment-deployment/src/master/))
* Un ide pour Java (Vscode ou IntelliJ suggérés )

#### Si utilisation de vscode, les extensions suivantes sont recommandées: ####

* [Java extension pack](https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-pack)
* [Lombok](https://marketplace.visualstudio.com/items?itemName=GabrielBB.vscode-lombok)


## Éxécution du programme à partir de l'IDE ##

* Éxécuter la classe BaseKeycloakStarter.java dans src\main\java\com\base\server

## Éxécution du programme à partir de Maven ##

* Éxécuter la commande suivant dans le dossier contenant le projet 
> mvn spring-boot:run

## Insertion de données dans la bd ##

* Éxécuter la classe AddDataToDB.java dans src\test\unit\java\com\base\server\temp\everything

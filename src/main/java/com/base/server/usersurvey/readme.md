# Class Diagram

The class diagram illustrates relations between entities in the "usersurvey" package.

```mermaid
classDiagram
    UserSurveyEntity --> SurveyVersionEntity : answers
    UserSurveyEntity *-- UserQuestionEntity : contains
    UserQuestionEntity --> QuestionVersionEntity : answers
    UserQuestionEntity *-- UserAnswerEntity : contains
    UserAnswerEntity --> AnswerChoiceEntity : may refers to
```

## Storing user answer to a survey

Each time a user answer a survey, a `UserSurveryEntity` is created with
a link to the related `SurveyVersionEntity` filled.

For each question answered, a `UserQuestionEntity` is created with a link
to the related `QuestionVersionEntity` answered.

The answers provided are stored in the `UserAnswerEntity`. The answered is
stored in the property related to the `QuestionTypeEntity`. For single or
multiple choices answers, the link to the `AnswerChoiceEntity` is placed in
the `answerChoice` properties. 

### UAS7 Example

#### UserSurveyEntity

|Properties|Values|
|---|---|
|id|21|
|survey|7|
|user|42|
|answeredQuestions|[22,23,24]|
|dateFilled|2021-08-16T09:00:00Z|

#### UserQuestionEntity

|Properties|Values|
|---|---|
|id|22|
|questionAnswered|8|
|answers|[24]|
|dateAnswered|2021-08-16T09:02:00Z|

|Properties|Values|
|---|---|
|id|23|
|questionAnswered|9|
|answers|[25]|
|dateAnswered|2021-08-16T09:03:00Z|

|Properties|Values|
|---|---|
|id|23|
|questionAnswered|10|
|answers|[26]|
|dateAnswered|2021-08-16T09:02:10Z|

#### UserAnswerEntity

|Properties|Values|
|---|---|
|id|24|
|answerChoice|14|
|intAnswer||
|intMinAnswer||
|intMaxAnswer||
|floatAnswer||
|floatMinAnswer||
|floatMaxAnswer||
|stringAnswer||
|dateAnswer||
|startDateAnswer||
|endDateAnswer||

|Properties|Values|
|---|---|
|id|25|
|answerChoice|19|
|intAnswer||
|intMinAnswer||
|intMaxAnswer||
|floatAnswer||
|floatMinAnswer||
|floatMaxAnswer||
|stringAnswer||
|dateAnswer||
|startDateAnswer||
|endDateAnswer||

|Properties|Values|
|---|---|
|id|26|
|answerChoice||
|intAnswer|3|
|intMinAnswer||
|intMaxAnswer||
|floatAnswer||
|floatMinAnswer||
|floatMaxAnswer||
|stringAnswer||
|dateAnswer||
|startDateAnswer||
|endDateAnswer||
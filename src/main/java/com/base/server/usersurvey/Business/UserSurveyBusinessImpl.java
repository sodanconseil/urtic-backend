package com.base.server.usersurvey.Business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.usersurvey.Business.interfaces.UserSurveyBusiness;
import com.base.server.usersurvey.factory.UserSurveyFactory;
import com.base.server.usersurvey.persistance.jpa.interfaces.UserSurveyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserSurveyBusinessImpl extends BaseBusinessImpl<UserSurvey, UserSurveyRepository> implements UserSurveyBusiness {

    private UserSurveyRepository userSurveyRepository;
    private UserSurveyFactory userSurveyFactory;

    @Autowired
    public UserSurveyBusinessImpl(UserSurveyRepository userSurveyRepository, UserSurveyFactory factory) {
        super(userSurveyRepository, factory);
        this.userSurveyRepository = userSurveyRepository;
        this.userSurveyFactory = userSurveyFactory;
    }

    @Override
    public List<UserSurvey> getAllUserSurveyByUserId(String UserId, String surveyVersionId) {
        return userSurveyRepository.getAllUserSurveyByUserId(UserId, surveyVersionId);
    }

}

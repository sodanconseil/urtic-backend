package com.base.server.usersurvey.Business;

import com.base.server.Question.Business.Question;
import com.base.server.answerChoice.business.interfaces.AnswerChoiceBusiness;
import com.base.server.answerChoice.dtos.AnswerChoiceRequestDto;
import com.base.server.generic.business.Base;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.business.surveyVersion.Uas7v1SurveyCommand;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import com.base.server.user.persistence.jpa.UserEntity;
import com.base.server.userAnswer.business.interfaces.UserAnswerBusiness;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.Business.interfaces.UserQuestionBusiness;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserSurvey extends Base {

    private String survey_version_Id;
    private String user_id;
//    private List<UserQuestion> userQuestions = new ArrayList<>();
    private Timestamp startDate;
    private Timestamp dateFilled;

    @Builder(setterPrefix = "set")
    public UserSurvey(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String survey_version_Id, String user_id, Timestamp startDate, Timestamp dateFilled) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.survey_version_Id = survey_version_Id;
        this.user_id = user_id;
        this.startDate = startDate;
        this.dateFilled = dateFilled;
    }

    public int calcul_ScoreValue(QuestionType questionType, UserSurvey userSurvey, UserQuestionBusiness userQuestionBusiness, UserAnswerBusiness userAnswerBusiness, int intValue) {
        int uasScoreValue = 0;
        Uas7v1SurveyCommand uas7v1SurveyCommand = new Uas7v1SurveyCommand();
        switch (questionType.getTypeCode()){
            case 0:
                uasScoreValue = uas7v1SurveyCommand.execute(userSurvey, userQuestionBusiness, userAnswerBusiness);
                break;
            case 1:
                uasScoreValue = intValue;
                break;
            default:
                uasScoreValue = 0;

        }
        return uasScoreValue;
    }
}

package com.base.server.usersurvey.Business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.survey.business.Survey;
import com.base.server.usersurvey.Business.UserSurvey;

import java.util.List;

public interface UserSurveyBusiness extends BaseBusiness<UserSurvey> {
    public List<UserSurvey> getAllUserSurveyByUserId(String UserId, String surveyVersionId);
}

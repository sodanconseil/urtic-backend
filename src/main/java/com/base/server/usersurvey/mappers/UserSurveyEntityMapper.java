package com.base.server.usersurvey.mappers;

import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.survey.business.Survey;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserSurveyEntityMapper extends BaseEntityMapper<UserSurveyEntity, UserSurvey> {
    public UserSurvey entityToBusinessObject(UserSurveyEntity entity);
    public UserSurveyEntity businessObjectToEntity(UserSurvey object);
    public List<UserSurvey> listOfEntityToListOfBusinessObjects(List<UserSurveyEntity> entities);

}

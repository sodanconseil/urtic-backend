package com.base.server.usersurvey.mappers;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.survey.business.Survey;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.presentation.dtos.UserSurveyRequestDto;
import com.base.server.usersurvey.presentation.dtos.UserSurveyResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface UserSurveyDtoMapper extends BaseDtoMapper<UserSurvey, UserSurveyRequestDto, UserSurveyResponseDto> {
    UserSurvey requestDtoToBusinessObject(UserSurveyRequestDto dto);
    UserSurveyResponseDto businessObjectToResponseDto(UserSurvey businessObject);
    List<UserSurveyResponseDto> listOfBusinessObjectsToListOfResponseDto(List<UserSurvey> businessObjects);
    Set<UserSurveyResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<UserSurvey> businessObjects);
}

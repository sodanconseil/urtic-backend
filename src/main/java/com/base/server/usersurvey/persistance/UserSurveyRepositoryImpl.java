package com.base.server.usersurvey.persistance;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.mappers.UserSurveyEntityMapper;
import com.base.server.usersurvey.persistance.jpa.interfaces.UserSurveyRepository;
import com.base.server.usersurvey.persistance.jpa.UserSurveyDal;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class UserSurveyRepositoryImpl extends BaseRepositoryImpl<UserSurveyEntity, UserSurvey> implements UserSurveyRepository {
    private UserSurveyDal dal;
    private UserSurveyEntityMapper mapper;

    @Autowired
    public UserSurveyRepositoryImpl(UserSurveyDal dal, UserSurveyEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    public List<UserSurvey> getAllUserSurveyByUserId(String UserId, String surveyVersionId) {
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.getAllUserSurveyByUserId(UserId, surveyVersionId));
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }


}

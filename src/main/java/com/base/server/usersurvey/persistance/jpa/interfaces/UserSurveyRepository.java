package com.base.server.usersurvey.persistance.jpa.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.usersurvey.Business.UserSurvey;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserSurveyRepository extends BaseRepository<UserSurvey> {
    public List<UserSurvey> getAllUserSurveyByUserId(String UserId, String surveyVersionId);
}

package com.base.server.usersurvey.persistance.jpa;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import com.base.server.user.persistence.jpa.UserEntity;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/*
A user survey is a
 */
@Entity
@Table(name = "user_survey")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserSurveyEntity extends BaseEntity<UserSurveyEntity> implements Copyable<UserSurveyEntity> {

    private String survey_version_Id;

    private String user_id;

//    @OneToMany(mappedBy = "userSurveyEntity", cascade = {CascadeType.ALL})
//    private List<UserQuestionEntity> userQuestionEntities = new ArrayList<>();

    private Timestamp startDate;
    private Timestamp dateFilled;

    @Builder(setterPrefix = "set")
    public UserSurveyEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String survey_version_Id, String user_id, Timestamp startDate, Timestamp dateFilled) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.survey_version_Id = survey_version_Id;
        this.user_id = user_id;
        this.startDate = startDate;
        this.dateFilled = dateFilled;
    }

    @Override
    public void copy(UserSurveyEntity userSurveyEntity) {
        super.copy(userSurveyEntity);
        this.survey_version_Id = userSurveyEntity.survey_version_Id;
        this.user_id = userSurveyEntity.user_id;
        this.dateFilled = userSurveyEntity.dateFilled;
    }
}

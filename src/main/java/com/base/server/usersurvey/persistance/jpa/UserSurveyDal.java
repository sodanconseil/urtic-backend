package com.base.server.usersurvey.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.usersurvey.Business.UserSurvey;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserSurveyDal extends BaseDal<UserSurveyEntity> {
    @Query(value = "SELECT * From user_survey WHERE user_id = ?1 AND survey_version_id = ?2", nativeQuery = true)
    public List<UserSurveyEntity> getAllUserSurveyByUserId(String UserId, String surveyVersionId);
}

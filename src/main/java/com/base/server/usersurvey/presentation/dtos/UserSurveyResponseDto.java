package com.base.server.usersurvey.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import com.base.server.user.persistence.jpa.UserEntity;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserSurveyResponseDto extends BaseResponseDto {

    private String survey_version_Id;
    private String user_id;
//    private List<UserQuestion> userQuestions = new ArrayList<>();

    private Timestamp startDate;
    private Timestamp dateFilled;

    @Builder(setterPrefix = "set")
    public UserSurveyResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String survey_version_Id, String user_id, Timestamp startDate, Timestamp dateFilled) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.survey_version_Id = survey_version_Id;
        this.user_id = user_id;
        this.startDate = startDate;
        this.dateFilled = dateFilled;
    }
}

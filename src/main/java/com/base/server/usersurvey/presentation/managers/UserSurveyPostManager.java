package com.base.server.usersurvey.presentation.managers;

import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.survey.business.Survey;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.survey.mappers.SurveyDtoMapper;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.Business.interfaces.UserSurveyBusiness;
import com.base.server.usersurvey.mappers.UserSurveyDtoMapper;
import com.base.server.usersurvey.presentation.dtos.UserSurveyRequestDto;
import com.base.server.usersurvey.presentation.dtos.UserSurveyResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserSurveyPostManager implements RequestManager<UserSurveyRequestDto> {

    private UserSurveyBusiness usersurveyBusiness;
    private UserSurveyDtoMapper usersurveyDtoMapper;

    @Autowired
    public UserSurveyPostManager(UserSurveyBusiness usersurveyBusiness, UserSurveyDtoMapper usersurveyDtoMapper) {
        this.usersurveyBusiness = usersurveyBusiness;
        this.usersurveyDtoMapper = usersurveyDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UserSurveyRequestDto> requestInformation) {

        String userId = requestInformation.getUserId();
        UserSurveyRequestDto usersurveyRequestDto = requestInformation.getDto();
        UserSurvey usersurvey = usersurveyBusiness.save(usersurveyDtoMapper.requestDtoToBusinessObject(usersurveyRequestDto), userId);
        UserSurveyResponseDto usersurveyResponseDto = usersurveyDtoMapper.businessObjectToResponseDto(usersurvey);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(usersurveyDtoMapper.getEtag(usersurvey))
            .body(usersurveyResponseDto);

        return Optional.of(responseEntity);
    }
}

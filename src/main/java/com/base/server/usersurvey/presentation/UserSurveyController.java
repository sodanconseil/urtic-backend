package com.base.server.usersurvey.presentation;

import com.base.server.answerChoice.business.interfaces.AnswerChoiceBusiness;
import com.base.server.answerChoice.dtos.AnswerChoiceRequestDto;
import com.base.server.answerChoice.mappers.AnswerChoiceDtoMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.questionType.Business.interfaces.QuestionTypeBusiness;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.Business.interfaces.QuestionVersionBusiness;
import com.base.server.survey.business.Survey;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.survey.mappers.SurveyDtoMapper;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.business.interfaces.SurveyVersionBusiness;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.business.interfaces.UserAnswerBusiness;
import com.base.server.userAnswer.dtos.UserAnswerRequestDto;
import com.base.server.userAnswer.mappers.UserAnswerDtoMapper;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.Business.interfaces.UserQuestionBusiness;
import com.base.server.userQuestion.factory.UserQuestionFactory;
import com.base.server.userQuestion.mappers.UserQuestionDtoMapper;
import com.base.server.userQuestion.presentation.dtos.UserQuestionRequestDto;
import com.base.server.userQuestion.presentation.dtos.UserQuestionResponseDto;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.Business.interfaces.UserSurveyBusiness;
import com.base.server.usersurvey.factory.UserSurveyFactory;
import com.base.server.usersurvey.mappers.UserSurveyDtoMapper;
import com.base.server.usersurvey.presentation.dtos.UserSurveyRequestDto;
import com.base.server.usersurvey.presentation.dtos.UserSurveyResponseDto;
import com.base.server.usersurvey.presentation.managers.UserSurveyPostManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@RestController
@RequestMapping("/usersurvey")
public class UserSurveyController {

    private UserSurveyPostManager usersurveyPostManager;
    private UserSurveyFactory usersurveyFactory;
    private ValidationManager<UserSurveyRequestDto> validationManager;

    private UserSurveyBusiness userSurveyBusiness;
    private UserQuestionBusiness userQuestionBusiness;
    private UserQuestionDtoMapper userQuestionDtoMapper;
    private UserAnswerBusiness userAnswerBusiness;
    private QuestionVersionBusiness questionVersionBusiness;
    private UserAnswerDtoMapper userAnswerDtoMapper;
    private UserQuestionFactory userQuestionFactory;
    private AnswerChoiceBusiness answerChoiceBusiness;
    private AnswerChoiceDtoMapper answerChoiceDtoMapper;
    private QuestionTypeBusiness questionTypeBusiness;
    private UserSurveyDtoMapper userSurveyDtoMapper;

    private SurveyBusiness surveyBusiness;
    private SurveyDtoMapper surveyDtoMapper;

    private SurveyVersionBusiness surveyVersionBusiness;

    @Autowired
    public UserSurveyController(UserSurveyPostManager usersurveyPostManager, UserSurveyFactory usersurveyFactory, ValidationManager<UserSurveyRequestDto> validationManager, UserSurveyBusiness userSurveyBusiness, UserQuestionBusiness userQuestionBusiness, UserQuestionDtoMapper userQuestionDtoMapper, UserAnswerBusiness userAnswerBusiness, QuestionVersionBusiness questionVersionBusiness, UserAnswerDtoMapper userAnswerDtoMapper, UserQuestionFactory userQuestionFactory, AnswerChoiceBusiness answerChoiceBusiness, AnswerChoiceDtoMapper answerChoiceDtoMapper, QuestionTypeBusiness questionTypeBusiness, UserSurveyDtoMapper userSurveyDtoMapper, SurveyBusiness surveyBusiness, SurveyDtoMapper surveyDtoMapper, SurveyVersionBusiness surveyVersionBusiness) {
        this.usersurveyPostManager = usersurveyPostManager;
        this.usersurveyFactory = usersurveyFactory;
        this.validationManager = validationManager;
        this.userSurveyBusiness = userSurveyBusiness;
        this.userQuestionBusiness = userQuestionBusiness;
        this.userQuestionDtoMapper = userQuestionDtoMapper;
        this.userAnswerBusiness = userAnswerBusiness;
        this.questionVersionBusiness = questionVersionBusiness;
        this.userAnswerDtoMapper = userAnswerDtoMapper;
        this.userQuestionFactory = userQuestionFactory;
        this.answerChoiceBusiness = answerChoiceBusiness;
        this.answerChoiceDtoMapper = answerChoiceDtoMapper;
        this.questionTypeBusiness = questionTypeBusiness;
        this.userSurveyDtoMapper = userSurveyDtoMapper;
        this.surveyBusiness = surveyBusiness;
        this.surveyDtoMapper = surveyDtoMapper;
        this.surveyVersionBusiness = surveyVersionBusiness;
    }

    @RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUserSurveyById(@PathVariable String id, HttpServletRequest request) {
        UserSurvey userSurvey = userSurveyBusiness.get(id);
        UserSurveyResponseDto userSurveyResponseDto = userSurveyDtoMapper.businessObjectToResponseDto(userSurvey);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).
                eTag(userSurveyDtoMapper.getEtag(userSurvey))
                .body(userSurveyResponseDto);
        return responseEntity;
   }

   @RequestMapping(value = "/allForUser/{ShortName}/{UserId}", produces = MediaType.APPLICATION_JSON_VALUE)
   public ResponseEntity<Object> getAllUserSurveyByUserIdAndSurveyName(@PathVariable String ShortName, @PathVariable String UserId, HttpServletRequest request) {
        Survey survey = surveyBusiness.getByShortName(ShortName);
        SurveyVersion surveyVersion = surveyVersionBusiness.getLatestSurveyVersionBySurveyId(survey.getId());

        List<UserSurvey> userSurveys = userSurveyBusiness.getAllUserSurveyByUserId(UserId, surveyVersion.getId());
        List<UserSurveyResponseDto> userSurveyResponseDtos = userSurveyDtoMapper.listOfBusinessObjectsToListOfResponseDto(userSurveys);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(userSurveyResponseDtos);
       return responseEntity;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postUserSurvey(@Valid @RequestBody UserSurveyRequestDto dto, Errors errors, HttpServletRequest request) {
        RequestInformation<UserSurveyRequestDto> requestInformation = usersurveyFactory.newRequestInformation(null, dto, request, errors, null, "toto");
        return usersurveyFactory.newRequestPipeline(validationManager)
                .addManager(usersurveyPostManager)
                .manage(requestInformation);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> editUserSurvey(@PathVariable String id, @Valid @RequestBody UserSurveyRequestDto dto,
                                                 Errors errors, HttpServletRequest request, @RequestHeader(name = "If-Match") String if_match) {

        UserSurvey userSurvey = userSurveyDtoMapper.requestDtoToBusinessObject(dto);
        userSurvey = userSurveyBusiness.modify(id, userSurvey, dto.getUser_id());
        UserSurveyResponseDto userSurveyResponseDto = userSurveyDtoMapper.businessObjectToResponseDto(userSurvey);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .eTag(userSurveyDtoMapper.getEtag(userSurvey))
                .body(userSurveyResponseDto);
        return responseEntity;
    }

    @PostMapping(value = "/{id}/questions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postUserQuestionAndAnswer(@PathVariable String id, @Valid @RequestBody List<AnswerChoiceRequestDto> dtos, Errors errors, HttpServletRequest request) {
        UserSurvey userSurvey = userSurveyBusiness.get(id);
        QuestionVersion questionVersion = questionVersionBusiness.get(dtos.get(0).getQuestionVersion().getId());
        QuestionType questionType = questionTypeBusiness.get(questionVersion.getQuestionType().getId());

        int uasScoreValue =  userSurvey.calcul_ScoreValue(questionType, userSurvey, userQuestionBusiness, userAnswerBusiness, dtos.get(0).getValue());

        SurveyVersion surveyVersion = surveyVersionBusiness.get( userSurvey.getSurvey_version_Id() );
        Survey survey = surveyBusiness.get( surveyVersion.getSurvey().getId() );

        UserQuestionRequestDto userQuestionRequestDto = UserQuestionRequestDto.builder()
                .setName("")
                .setQuestionAnswered( questionVersion.getId() )
                .setUser_survey_Id(id)
                .setDateAnswered( Timestamp.from(Instant.now()) )
                .build();
        UserQuestion unsavedUserQuestion = userQuestionDtoMapper.requestDtoToBusinessObject(userQuestionRequestDto);
        UserQuestion userQuestion = userQuestionBusiness.save(unsavedUserQuestion, "userId");

        int index = 0;
        for (AnswerChoiceRequestDto answer: dtos) {
            UserAnswerRequestDto userAnswerRequestDto = UserAnswerRequestDto.builder()
                    .setName("")
                    .setAnswerChoice_id(answer.getId())
                    .setIntAnswer( uasScoreValue )
                    .setStringAnswer( answer.getText() == null ? null : answer.getText())
                    .setSupplementInputAnswer(answer.getSupplementInputAnswer() == null ? null : answer.getSupplementInputAnswer())
                    .setDateAnswer(Timestamp.from(Instant.now()))
                    .setUserQuestion( userQuestion )
                    .build();
            UserAnswer unsavedUserAnswer = userAnswerDtoMapper.requestDtoToBusinessObject(userAnswerRequestDto);
            UserAnswer savedUserAnswer = userAnswerBusiness.save(unsavedUserAnswer, "userId");

            if( survey.getShortName().equals("UAS7") && questionVersion.getQuestion_order() == 2) {
                List<QuestionType> allQuestionType = questionTypeBusiness.getAll();
                QuestionType questionTypeCalculated = allQuestionType.stream()
                        .filter(qt -> qt.getTypeCode() == 0)
                        .findFirst().get();

                QuestionVersion calculatedQuestionVersion = surveyVersion.getQuestionVersions().stream()
                        .filter(qv -> qv.getQuestion_order() == 3)
                        .findFirst().get();

                UserQuestionRequestDto calculatedUserQuestionRequestDto = UserQuestionRequestDto.builder()
                        .setName("")
                        .setQuestionAnswered( calculatedQuestionVersion.getId() )
                        .setUser_survey_Id(id)
                        .setDateAnswered( Timestamp.from(Instant.now()) )
                        .build();
                UserQuestion unsavedCalculatedUserQuestion = userQuestionDtoMapper.requestDtoToBusinessObject(calculatedUserQuestionRequestDto);
                UserQuestion calculatedUserQuestion = userQuestionBusiness.save(unsavedCalculatedUserQuestion, "userId");


                uasScoreValue =  userSurvey.calcul_ScoreValue(questionTypeCalculated, userSurvey, userQuestionBusiness, userAnswerBusiness, dtos.get(0).getValue());
                UserAnswerRequestDto calculatedUserAnswerRequestDto = UserAnswerRequestDto.builder()
                        .setName("")
                        .setIntAnswer( uasScoreValue )
                        .setDateAnswer(Timestamp.from(Instant.now()))
                        .setUserQuestion( calculatedUserQuestion )
                        .build();
                UserAnswer unsavedCalculatedUserAnswer = userAnswerDtoMapper.requestDtoToBusinessObject(calculatedUserAnswerRequestDto);
                UserAnswer savedCalculatedUserAnswer = userAnswerBusiness.save(unsavedCalculatedUserAnswer, "userId");
            }
            index++;
        }

        UserQuestionResponseDto userQuestionResponseDto = userQuestionDtoMapper.businessObjectToResponseDto(userQuestion);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .eTag(userQuestionDtoMapper.getEtag(userQuestion))
                .body(userQuestionResponseDto);

        return responseEntity;
    }
}

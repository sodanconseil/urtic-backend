package com.base.server.questionVersion.persistance;

import com.base.server.Question.Business.Question;
import com.base.server.Question.mappers.QuestionEntityMapper;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.Question.persistance.jpa.QuestionDal;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.mappers.QuestionVersionEntityMapper;
import com.base.server.questionVersion.persistance.interfaces.QuestionVersionRepository;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionDal;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class QuestionVersionRepositoryImpl extends BaseRepositoryImpl<QuestionVersionEntity, QuestionVersion> implements QuestionVersionRepository {
    private QuestionVersionDal dal;
    private QuestionVersionEntityMapper mapper;

    @Autowired
    public QuestionVersionRepositoryImpl(QuestionVersionDal dal, QuestionVersionEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }


    @Override
    public List<QuestionVersion> getAllQuestionVersionBySurveyVersion(String surveyVersionId){
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.getAllQuestionVersionBySurveyVersion(surveyVersionId));
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }

}

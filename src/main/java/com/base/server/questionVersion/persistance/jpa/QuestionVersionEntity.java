package com.base.server.questionVersion.persistance.jpa;

import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.questionType.persistance.jpa.QuestionTypeEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.survey.business.Uas7DailyScoreCommand;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/*
Version of a question in a survey.
 */
@Entity
@Table(name = "question_version")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class QuestionVersionEntity extends BaseEntity<QuestionVersionEntity> implements Copyable<QuestionVersionEntity> {

    /*
    Order of the question in the survey.
     */
    private int question_order;
    /*
    Question.
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id")
    private QuestionEntity question;
    /*
    Type of the question. E.g. 1 (Single choice question).
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "questionType_id", referencedColumnName = "id")
    private QuestionTypeEntity questionType;

    /*
    The question text. E.g.: Number of wheals.
     */
    @Column(columnDefinition = "text")
    private String text;
    /*
    Question formatted in simple HTML. E.g: Number of <b>wheals</b>.
     */
    @Column(columnDefinition = "text")
    private String formattedText;
    /*
    Contextual information to be displayed helping the user selecting or not the answer. E.g. Mild: <20 wheals over 24 hours.
     */
    @Column(columnDefinition = "text")
    private String info;
    /*
    Information in simple information. E.g. <b>Mild:</b>&nbsp;<20 wheals over 24 hours.
     */
    @Column(columnDefinition = "text")
    private String formattedInfo;
    /*
    Possible answers to the question. Answers are only possible for single choice or multiple choices question.
     */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "surveyVersion_id")
    private SurveyVersionEntity surveyVersionEntity;

    @OneToMany(mappedBy = "questionVersionEntity", cascade = {CascadeType.ALL})
    private List<AnswerChoiceEntity> possibleAnswers;
    /*
    Command class path for calculated question. The class is called by reflection to calculate the answer.
     */
    private String commandClass;

    private boolean isHidden;

    private boolean noAnswerEndSurvey;

    private boolean isSubQuestion;

    private int subQuestionOrder;

    private int verificationOrder;


    @Builder(setterPrefix = "set")
    public QuestionVersionEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, int question_order, QuestionEntity question, QuestionTypeEntity questionType, String text, String formattedText, String info, String formattedInfo, SurveyVersionEntity surveyVersionEntity, List<AnswerChoiceEntity> possibleAnswers, String commandClass, boolean isHidden, boolean noAnswerEndSurvey, boolean isSubQuestion, int subQuestionOrder, int verificationOrder) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.question_order = question_order;
        this.question = question;
        this.questionType = questionType;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.surveyVersionEntity = surveyVersionEntity;
        this.possibleAnswers = possibleAnswers;
        this.commandClass = commandClass;
        this.isHidden = isHidden;
        this.noAnswerEndSurvey = noAnswerEndSurvey;
        this.isSubQuestion = isSubQuestion;
        this.subQuestionOrder = subQuestionOrder;
        this.verificationOrder = verificationOrder;
    }

    @Override
    public void copy(QuestionVersionEntity questionVersionEntity) {
        super.copy(questionVersionEntity);
        this.question_order = question_order;
        this.question = question;
        this.questionType = questionType;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.possibleAnswers = possibleAnswers;
        this.commandClass = commandClass;
        this.isHidden = isHidden;
        this.noAnswerEndSurvey = noAnswerEndSurvey;
        this.isSubQuestion = isSubQuestion;
        this.subQuestionOrder = subQuestionOrder;
        this.verificationOrder = verificationOrder;
    }
}

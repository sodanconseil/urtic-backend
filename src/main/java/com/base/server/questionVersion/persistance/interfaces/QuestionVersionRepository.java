package com.base.server.questionVersion.persistance.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.questionVersion.Business.QuestionVersion;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface QuestionVersionRepository extends BaseRepository<QuestionVersion> {
    public List<QuestionVersion> getAllQuestionVersionBySurveyVersion(String surveyVersionId);
}

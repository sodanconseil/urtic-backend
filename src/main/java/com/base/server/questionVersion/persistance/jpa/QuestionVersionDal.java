package com.base.server.questionVersion.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface QuestionVersionDal extends BaseDal<QuestionVersionEntity> {
    @Query(value = "SELECT * From question_version WHERE survey_version_id = ?1", nativeQuery = true)
    List<QuestionVersionEntity> getAllQuestionVersionBySurveyVersion(String questionVersionId);
}

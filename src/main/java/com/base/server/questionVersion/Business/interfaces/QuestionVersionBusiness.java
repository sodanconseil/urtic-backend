package com.base.server.questionVersion.Business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.questionVersion.Business.QuestionVersion;

import java.util.List;

public interface QuestionVersionBusiness extends BaseBusiness<QuestionVersion> {
    public List<QuestionVersion> getAllQuestionVersionBySurveyVersion(String surveyVersionId);
}

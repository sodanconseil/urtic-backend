package com.base.server.questionVersion.Business;

import com.base.server.Question.Business.Question;
import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.factory.QuestionFactory;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.questionVersion.Business.interfaces.QuestionVersionBusiness;
import com.base.server.questionVersion.factory.QuestionVersionFactory;
import com.base.server.questionVersion.persistance.interfaces.QuestionVersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class QuestionVersionBusinessImpl extends BaseBusinessImpl<QuestionVersion, QuestionVersionRepository> implements QuestionVersionBusiness {

    private QuestionVersionRepository questionVersionRepository;
    private QuestionVersionFactory questionVersionFactory;

    @Autowired
    public QuestionVersionBusinessImpl(QuestionVersionRepository questionVersionRepository, QuestionVersionFactory factory) {
        super(questionVersionRepository, factory);
        this.questionVersionRepository = questionVersionRepository;
        this.questionVersionFactory = factory;
    }

    @Override
    public List<QuestionVersion> getAllQuestionVersionBySurveyVersion(String surveyVersionId){
        return questionVersionRepository.getAllQuestionVersionBySurveyVersion(surveyVersionId);
    }


}

package com.base.server.questionVersion.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionVersionEntityMapper extends BaseEntityMapper<QuestionVersionEntity, QuestionVersion> {
    public QuestionVersion entityToBusinessObject(QuestionVersionEntity entity);
    public QuestionVersionEntity businessObjectToEntity(QuestionVersion object);
    public List<QuestionVersion> listOfEntityToListOfBusinessObjects(List<QuestionVersionEntity> entities);

}

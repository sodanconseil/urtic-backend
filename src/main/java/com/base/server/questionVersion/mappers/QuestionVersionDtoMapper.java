package com.base.server.questionVersion.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.presentation.dtos.QuestionVersionRequestDto;
import com.base.server.questionVersion.presentation.dtos.QuestionVersionResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface QuestionVersionDtoMapper extends BaseDtoMapper<QuestionVersion, QuestionVersionRequestDto, QuestionVersionResponseDto> {
    QuestionVersion requestDtoToBusinessObject(QuestionVersionRequestDto dto);
    QuestionVersionResponseDto businessObjectToResponseDto(QuestionVersion businessObject);
    List<QuestionVersionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<QuestionVersion> businessObjects);
    Set<QuestionVersionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<QuestionVersion> businessObjects);
}

package com.base.server.questionVersion.presentation.dtos;

import com.base.server.Question.Business.Question;
import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.generic.presentation.dtos.BaseResponseDto;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.survey.business.Uas7DailyScoreCommand;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class QuestionVersionResponseDto extends BaseResponseDto {

    private int question_order;
    private Question question;
    private QuestionType questionType;

    @Column(columnDefinition = "text")
    private String text;

    @Column(columnDefinition = "text")
    private String formattedText;

    @Column(columnDefinition = "text")
    private String info;

    @Column(columnDefinition = "text")
    private String formattedInfo;

    private List<QuestionVersion> surveyVersion;
    private List<AnswerChoice> possibleAnswers;
    private String commandClass;
    private boolean isHidden;
    private boolean noAnswerEndSurvey;
    private boolean isSubQuestion;
    private int subQuestionOrder;
    private int verificationOrder;



    @Builder(setterPrefix = "set")
    public QuestionVersionResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, int question_order, Question question, QuestionType questionType, String text, String formattedText, String info, String formattedInfo, List<QuestionVersion> surveyVersion, List<AnswerChoice> possibleAnswers, String commandClass, boolean isHidden, boolean noAnswerEndSurvey, boolean isSubQuestion, int subQuestionOrder, int verificationOrder) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.question_order = question_order;
        this.question = question;
        this.questionType = questionType;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.surveyVersion = surveyVersion;
        this.possibleAnswers = possibleAnswers;
        this.commandClass = commandClass;
        this.isHidden = isHidden;
        this.noAnswerEndSurvey = noAnswerEndSurvey;
        this.isSubQuestion = isSubQuestion;
        this.subQuestionOrder = subQuestionOrder;
        this.verificationOrder = verificationOrder;
    }
}

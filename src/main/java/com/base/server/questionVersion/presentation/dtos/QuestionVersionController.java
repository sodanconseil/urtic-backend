package com.base.server.questionVersion.presentation.dtos;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.interfaces.AnswerChoiceTraductionBusiness;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.Business.interfaces.QuestionVersionBusiness;
import com.base.server.questionVersion.mappers.QuestionVersionDtoMapper;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.interfaces.QuestionVersionTraductionBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/questionversion")
public class QuestionVersionController {

    private QuestionVersionBusiness questionVersionBusiness;
    private QuestionVersionDtoMapper questionVersionDtoMapper;

    //Traduction
    QuestionVersionTraductionBusiness questionVersionTraductionBusiness;
    AnswerChoiceTraductionBusiness answerChoiceTraductionBusiness;


    @Autowired
    public QuestionVersionController(QuestionVersionBusiness questionVersionBusiness, QuestionVersionDtoMapper questionVersionDtoMapper, QuestionVersionTraductionBusiness questionVersionTraductionBusiness, AnswerChoiceTraductionBusiness answerChoiceTraductionBusiness) {
        this.questionVersionBusiness = questionVersionBusiness;
        this.questionVersionDtoMapper = questionVersionDtoMapper;
        this.questionVersionTraductionBusiness = questionVersionTraductionBusiness;
        this.answerChoiceTraductionBusiness = answerChoiceTraductionBusiness;
    }

    @RequestMapping(value = "/{lang}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getQuestionVersionById(@PathVariable String lang, @PathVariable String id, HttpServletRequest request){
        QuestionVersion questionVersion = questionVersionBusiness.get(id);

        QuestionVersionTraduction questionVersionTraduction = questionVersionTraductionBusiness.getByLangAndQuestionVersion(lang, questionVersion.getId());

        if(questionVersionTraduction != null) {
            questionVersion.setText(questionVersionTraduction.getText());
            questionVersion.setFormattedText(questionVersionTraduction.getFormattedText());
            questionVersion.setInfo(questionVersion.getInfo() == null ? null : questionVersionTraduction.getInfo());
            questionVersion.setFormattedInfo(questionVersion.getFormattedInfo() == null ? null : questionVersionTraduction.getFormattedInfo() );

            for (AnswerChoice answer : questionVersion.getPossibleAnswers()) {
                AnswerChoiceTraduction answerChoiceTraduction = answerChoiceTraductionBusiness.getByLangAndAnswerChoice(lang, answer.getId());

                if(answerChoiceTraduction != null) {
                    answer.setText(answerChoiceTraduction.getText());
                    answer.setFormattedText(answerChoiceTraduction.getFormattedText());
                    answer.setInfo(answer.getInfo() == null ? null : answerChoiceTraduction.getInfo());
                    answer.setFormattedInfo(answer.getFormattedInfo() == null ? null : answerChoiceTraduction.getFormattedInfo() );
                    answer.setSupplementInputText(answer.getFormattedInfo() == null ? null : answerChoiceTraduction.getSupplementInputText());
                }
            }
        }

        QuestionVersionResponseDto questionVersionResponseDto = questionVersionDtoMapper.businessObjectToResponseDto(questionVersion);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(questionVersionResponseDto);
        return responseEntity;
    }
}

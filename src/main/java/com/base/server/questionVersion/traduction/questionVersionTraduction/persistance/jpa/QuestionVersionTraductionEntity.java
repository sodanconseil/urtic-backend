package com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "question_version_traduction")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class QuestionVersionTraductionEntity extends BaseEntity<QuestionVersionTraductionEntity> implements Copyable<QuestionVersionTraductionEntity> {

    public String questionVersionId;
    public String language;
    public String text;
    public String formattedText;
    public String info;
    public String formattedInfo;


    @Builder(setterPrefix = "set")
    public QuestionVersionTraductionEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String questionVersionId, String language, String text, String formattedText, String info, String formattedInfo) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.questionVersionId = questionVersionId;
        this.language = language;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
    }

    @Override
    public void copy(QuestionVersionTraductionEntity entity) {
        super.copy(entity);
        this.questionVersionId = questionVersionId;
        this.language = language;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
    }
}

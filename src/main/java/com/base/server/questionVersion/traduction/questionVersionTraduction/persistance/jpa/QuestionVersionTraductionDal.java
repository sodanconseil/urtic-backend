package com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

@Component
public interface QuestionVersionTraductionDal extends BaseDal<QuestionVersionTraductionEntity> {
    @Query(value = "SELECT * From question_version_traduction WHERE language = ?1 AND question_version_id = ?2", nativeQuery = true)
    QuestionVersionTraductionEntity getByLangAndQuestionVersion(String lang, String questionVersionId);
}

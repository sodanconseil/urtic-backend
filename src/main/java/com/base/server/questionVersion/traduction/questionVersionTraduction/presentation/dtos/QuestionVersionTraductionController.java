package com.base.server.questionVersion.traduction.questionVersionTraduction.presentation.dtos;

import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.presentation.dtos.QuestionVersionResponseDto;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.interfaces.QuestionVersionTraductionBusiness;
import com.base.server.questionVersion.traduction.questionVersionTraduction.mappers.QuestionVersionTraductionDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/questionversiontraduction")
public class QuestionVersionTraductionController {

    private QuestionVersionTraductionBusiness questionVersionTraductionBusiness;
    private QuestionVersionTraductionDtoMapper questionVersionTraductionDtoMapper;

    @Autowired
    public QuestionVersionTraductionController(QuestionVersionTraductionBusiness questionVersionTraductionBusiness,
                                               QuestionVersionTraductionDtoMapper questionVersionTraductionDtoMapper) {
    this.questionVersionTraductionBusiness = questionVersionTraductionBusiness;
    this.questionVersionTraductionDtoMapper = questionVersionTraductionDtoMapper;
    }

    @RequestMapping(value = "/{lang}/{question_version_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<Object> getQuestionVersionTraduction(@PathVariable String lang, @PathVariable String question_version_id, HttpServletRequest request){
                QuestionVersionTraduction questionVersionTraduction = questionVersionTraductionBusiness.getByLangAndQuestionVersion(lang, question_version_id);
                QuestionVersionTraductionResponseDto questionVersionTraductionResponseDto = questionVersionTraductionDtoMapper.businessObjectToResponseDto(questionVersionTraduction);
                ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                        .body(questionVersionTraductionResponseDto);
                return responseEntity;


    }
}

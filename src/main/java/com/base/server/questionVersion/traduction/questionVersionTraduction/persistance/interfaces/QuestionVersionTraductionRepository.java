package com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import org.springframework.stereotype.Component;

@Component
public interface QuestionVersionTraductionRepository extends BaseRepository<QuestionVersionTraduction> {
    public QuestionVersionTraduction getByLangAndQuestionVersion(String lang, String questionVersionId);
}

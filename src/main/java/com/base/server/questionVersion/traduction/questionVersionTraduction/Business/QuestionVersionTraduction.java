package com.base.server.questionVersion.traduction.questionVersionTraduction.Business;

import com.base.server.generic.business.Base;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class QuestionVersionTraduction extends Base {

    public String questionVersionId;
    public String language;
    public String text;
    public String formattedText;
    public String info;
    public String formattedInfo;

    @Builder(setterPrefix = "set")
    public QuestionVersionTraduction(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String questionVersionId, String language, String text, String formattedText, String info, String formattedInfo) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.questionVersionId = questionVersionId;
        this.language = language;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
    }
}

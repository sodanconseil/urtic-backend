package com.base.server.questionVersion.traduction.questionVersionTraduction.mappers;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.questionVersion.presentation.dtos.QuestionVersionRequestDto;
import com.base.server.questionVersion.presentation.dtos.QuestionVersionResponseDto;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import com.base.server.questionVersion.traduction.questionVersionTraduction.presentation.dtos.QuestionVersionTraductionRequestDto;
import com.base.server.questionVersion.traduction.questionVersionTraduction.presentation.dtos.QuestionVersionTraductionResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface QuestionVersionTraductionDtoMapper extends BaseDtoMapper<QuestionVersionTraduction, QuestionVersionTraductionRequestDto, QuestionVersionTraductionResponseDto> {
    QuestionVersionTraduction requestDtoToBusinessObject(QuestionVersionRequestDto dto);
    QuestionVersionTraductionResponseDto businessObjectToResponseDto(QuestionVersionTraduction businessObject);
    List<QuestionVersionTraductionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<QuestionVersionTraduction> businessObjects);
    Set<QuestionVersionTraductionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<QuestionVersionTraduction> businessObjects);
}

package com.base.server.questionVersion.traduction.questionVersionTraduction.Business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;

public interface QuestionVersionTraductionBusiness extends BaseBusiness<QuestionVersionTraduction> {
    public QuestionVersionTraduction getByLangAndQuestionVersion(String lang, String questionVersionId);
}

package com.base.server.questionVersion.traduction.questionVersionTraduction.mappers;

import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionVersionTraductionEntityMapper extends BaseEntityMapper<QuestionVersionTraductionEntity, QuestionVersionTraduction> {
    public QuestionVersionTraduction entityToBusinessObject(QuestionVersionTraductionEntity entity);
    public QuestionVersionTraductionEntity businessObjectToEntity(QuestionVersionTraduction object);
    public List<QuestionVersionTraduction> listOfEntityToListOfBusinessObjects(List<QuestionVersionTraductionEntity> entities);
}

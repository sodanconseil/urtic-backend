package com.base.server.questionVersion.traduction.questionVersionTraduction.Business;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.interfaces.QuestionVersionTraductionBusiness;
import com.base.server.questionVersion.traduction.questionVersionTraduction.factory.QuestionVersionTraductionFactory;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.interfaces.QuestionVersionTraductionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class QuestionVersionTraductionBusinessImpl extends BaseBusinessImpl<QuestionVersionTraduction, QuestionVersionTraductionRepository> implements QuestionVersionTraductionBusiness {
    private QuestionVersionTraductionRepository questionVersionTraductionRepository;
    private QuestionVersionTraductionFactory questionVersionTraductionFactory;

    @Autowired
    public QuestionVersionTraductionBusinessImpl(QuestionVersionTraductionRepository questionVersionTraductionRepository, QuestionVersionTraductionFactory factory) {
        super(questionVersionTraductionRepository, factory);
        this.questionVersionTraductionRepository = questionVersionTraductionRepository;
        this.questionVersionTraductionFactory = factory;
    }

    @Override
    public QuestionVersionTraduction getByLangAndQuestionVersion(String lang, String questionVersionId){
        return questionVersionTraductionRepository.getByLangAndQuestionVersion(lang, questionVersionId);
    }
}

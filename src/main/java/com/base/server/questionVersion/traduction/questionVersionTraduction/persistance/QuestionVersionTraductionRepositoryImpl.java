package com.base.server.questionVersion.traduction.questionVersionTraduction.persistance;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.questionVersion.mappers.QuestionVersionEntityMapper;
import com.base.server.questionVersion.persistance.QuestionVersionRepositoryImpl;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import com.base.server.questionVersion.traduction.questionVersionTraduction.mappers.QuestionVersionTraductionEntityMapper;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.interfaces.QuestionVersionTraductionRepository;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionDal;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class QuestionVersionTraductionRepositoryImpl extends BaseRepositoryImpl<QuestionVersionTraductionEntity, QuestionVersionTraduction> implements QuestionVersionTraductionRepository {

    private QuestionVersionTraductionDal dal;
    private QuestionVersionTraductionEntityMapper mapper;

    @Autowired
    public QuestionVersionTraductionRepositoryImpl(QuestionVersionTraductionDal dal, QuestionVersionTraductionEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    @Override
    public QuestionVersionTraduction getByLangAndQuestionVersion(String lang, String questionVersionId){
        try {
            QuestionVersionTraductionEntity questionVersionTraductionEntity = dal.getByLangAndQuestionVersion(lang, questionVersionId);
            return mapper.entityToBusinessObject(questionVersionTraductionEntity);
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

package com.base.server.Question.Business;


import com.base.server.generic.business.Base;
import com.base.server.survey.business.Survey;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Question extends Base {

    private String text;

    @Builder(setterPrefix = "set")
    public Question(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String text) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.text = text;
    }
}

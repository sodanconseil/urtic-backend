package com.base.server.Question.Business;

import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.factory.QuestionFactory;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.frequency.factory.FrequencyFactory;
import com.base.server.frequency.persistence.interfaces.FrequencyRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuestionBusinessImpl extends BaseBusinessImpl<Question, QuestionRepository> implements QuestionBusiness {

    private QuestionRepository questionRepository;
    private QuestionFactory questionFactory;

    @Autowired
    public QuestionBusinessImpl(QuestionRepository questionRepository, QuestionFactory factory) {
        super(questionRepository, factory);
        this.questionRepository = questionRepository;
        this.questionFactory = questionFactory;
    }

}

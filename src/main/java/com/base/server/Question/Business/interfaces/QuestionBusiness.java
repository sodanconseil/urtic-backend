package com.base.server.Question.Business.interfaces;

import com.base.server.Question.Business.Question;
import com.base.server.frequency.business.Frequency;
import com.base.server.generic.business.interfaces.BaseBusiness;

public interface QuestionBusiness extends BaseBusiness<Question> {
}

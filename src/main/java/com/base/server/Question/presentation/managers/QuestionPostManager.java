package com.base.server.Question.presentation.managers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.mappers.QuestionDtoMapper;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.mappers.FrequencyDtoMapper;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.frequency.presentation.dtos.FrequencyResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.survey.business.Survey;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.survey.mappers.SurveyDtoMapper;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class QuestionPostManager implements RequestManager<QuestionRequestDto> {

    private QuestionBusiness questionBusiness;
    private QuestionDtoMapper questionDtoMapper;

    @Autowired
    public QuestionPostManager(QuestionBusiness questionBusiness, QuestionDtoMapper questionDtoMapper) {
        this.questionBusiness = questionBusiness;
        this.questionDtoMapper = questionDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<QuestionRequestDto> requestInformation) {

        String userId = requestInformation.getUserId();
        QuestionRequestDto questionRequestDto = requestInformation.getDto();
        Question question = questionBusiness.save(questionDtoMapper.requestDtoToBusinessObject(questionRequestDto), userId);
        QuestionResponseDto questionResponseDto = questionDtoMapper.businessObjectToResponseDto(question);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(questionDtoMapper.getEtag(question))
            .body(questionResponseDto);

        return Optional.of(responseEntity);
    }
}

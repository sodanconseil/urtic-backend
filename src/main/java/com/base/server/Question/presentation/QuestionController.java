package com.base.server.Question.presentation;

import com.base.server.Question.Business.Question;
import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.factory.QuestionFactory;
import com.base.server.Question.mappers.QuestionDtoMapper;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.Question.presentation.managers.QuestionGetAllManager;
import com.base.server.Question.presentation.managers.QuestionPostManager;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.survey.business.Survey;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import com.base.server.user.presentation.dtos.UserRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    private QuestionFactory questionFactory;
    private QuestionGetAllManager questionGetAllManager;
    private QuestionPostManager questionPostManager;
    private ValidationManager<QuestionRequestDto> questionvalidationManager;

    @Autowired
    private QuestionDtoMapper questionDtoMapper;

    @Autowired
    private QuestionBusiness questionBusiness;

    @Autowired
    public QuestionController(QuestionFactory questionFactory, QuestionGetAllManager questionGetAllManager, ValidationManager<QuestionRequestDto> questionvalidationManager) {
        this.questionFactory = questionFactory;
        this.questionGetAllManager = questionGetAllManager;
        this.questionvalidationManager = questionvalidationManager;
    }



    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        RequestInformation<QuestionRequestDto> requestInformation = questionFactory.newRequestInformation();
        return questionFactory.newRequestPipeline(questionGetAllManager).manage(requestInformation);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postQuestion(@Valid @RequestBody QuestionRequestDto dto, Errors errors, HttpServletRequest request) {
        RequestInformation<QuestionRequestDto> requestInformation = questionFactory.newRequestInformation(null, dto, request, errors, null, "toto");

        return questionFactory.newRequestPipeline(questionvalidationManager)
                .addManager(questionPostManager)
                .manage(requestInformation);
    }
}

package com.base.server.Question.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseRequestDto;
import com.base.server.survey.business.Survey;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class QuestionRequestDto extends BaseRequestDto {

    private String text;

    @Builder(setterPrefix = "set")
    public QuestionRequestDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String text) {
        super(id, name, description, creationUser, creationDate, modificationUser
                , modificationDate);
        this.text = text;
    }
}

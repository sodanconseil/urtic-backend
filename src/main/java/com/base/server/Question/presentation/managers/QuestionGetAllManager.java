package com.base.server.Question.presentation.managers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.mappers.QuestionDtoMapper;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.mappers.FrequencyDtoMapper;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.frequency.presentation.dtos.FrequencyResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class QuestionGetAllManager implements RequestManager<QuestionRequestDto> {

    private QuestionBusiness questionBusiness;
    private QuestionDtoMapper questionDtoMapper;

    @Autowired
    public QuestionGetAllManager(QuestionBusiness questionBusiness, QuestionDtoMapper questionDtoMapper) {
        this.questionBusiness = questionBusiness;
        this.questionDtoMapper = questionDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<QuestionRequestDto> requestInformation) {
        List<Question> questions = questionBusiness.getAll();
        List<QuestionResponseDto> questionResponseDto = questionDtoMapper.listOfBusinessObjectsToListOfResponseDto(questions);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(questionResponseDto);

        return Optional.of(responseEntity);
    }
}

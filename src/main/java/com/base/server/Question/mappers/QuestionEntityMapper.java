package com.base.server.Question.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionEntityMapper extends BaseEntityMapper<QuestionEntity, Question> {
    public Question entityToBusinessObject(QuestionEntity entity);
    public QuestionEntity businessObjectToEntity(Question object);
    public List<Question> listOfEntityToListOfBusinessObjects(List<QuestionEntity> entities);

}

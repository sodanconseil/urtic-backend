package com.base.server.Question.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.frequency.presentation.dtos.FrequencyResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface QuestionDtoMapper extends BaseDtoMapper<Question, QuestionRequestDto, QuestionResponseDto> {
    Question requestDtoToBusinessObject(QuestionRequestDto dto);
    QuestionResponseDto businessObjectToResponseDto(Question businessObject);
    List<QuestionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Question> businessObjects);
    Set<QuestionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<Question> businessObjects);
}

package com.base.server.Question.persistance.jpa;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "question")
@Data
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class QuestionEntity extends BaseEntity<QuestionEntity> implements Copyable<QuestionEntity> {

    /*
    Survey in which this question is asked.
     */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "survey_id")
    private SurveyEntity survey;
    /*
    The question text. E.g.: Number of wheals.
     */

    @OneToOne(mappedBy = "question")
    private QuestionVersionEntity questionVersionEntity;

    private String text;
    @Builder(setterPrefix = "set")
    public QuestionEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, SurveyEntity survey, String text) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.survey = survey;
        this.text = text;
    }

    @Override
    public void copy(QuestionEntity questionEntity) {
        super.copy(questionEntity);
        this.survey = questionEntity.survey;
        this.text = questionEntity.text;

    }

}

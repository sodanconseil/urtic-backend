package com.base.server.Question.persistance;

import com.base.server.Question.Business.Question;
import com.base.server.Question.mappers.QuestionEntityMapper;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.Question.persistance.jpa.QuestionDal;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.mappers.FrequencyEntityMapper;
import com.base.server.frequency.persistence.interfaces.FrequencyRepository;
import com.base.server.frequency.persistence.jpa.FrequencyDal;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuestionRepositoryImpl extends BaseRepositoryImpl<QuestionEntity, Question> implements QuestionRepository {
    private QuestionDal dal;
    private QuestionEntityMapper mapper;

    @Autowired
    public QuestionRepositoryImpl(QuestionDal dal, QuestionEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }
}

package com.base.server.Question.persistance.jpa;

import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.stereotype.Component;

@Component
public interface QuestionDal extends BaseDal<QuestionEntity> {
}

package com.base.server.Question.persistance.interfaces;

import com.base.server.Question.Business.Question;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.survey.business.Survey;
import org.springframework.stereotype.Component;

@Component
public interface QuestionRepository extends BaseRepository<Question> {
}

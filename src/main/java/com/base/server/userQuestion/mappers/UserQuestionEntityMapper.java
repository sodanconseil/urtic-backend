package com.base.server.userQuestion.mappers;

import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserQuestionEntityMapper extends BaseEntityMapper<UserQuestionEntity, UserQuestion> {
    public UserQuestion entityToBusinessObject(UserQuestionEntity entity);
    public UserQuestionEntity businessObjectToEntity(UserQuestion object);
    public List<UserQuestion> listOfEntityToListOfBusinessObjects(List<UserQuestionEntity> entities);

}

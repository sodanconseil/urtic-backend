package com.base.server.userQuestion.mappers;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.presentation.dtos.UserQuestionRequestDto;
import com.base.server.userQuestion.presentation.dtos.UserQuestionResponseDto;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.presentation.dtos.UserSurveyRequestDto;
import com.base.server.usersurvey.presentation.dtos.UserSurveyResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface UserQuestionDtoMapper extends BaseDtoMapper<UserQuestion, UserQuestionRequestDto, UserQuestionResponseDto> {
    UserQuestion requestDtoToBusinessObject(UserQuestionRequestDto dto);
    UserQuestionResponseDto businessObjectToResponseDto(UserQuestion businessObject);
    List<UserQuestionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<UserQuestion> businessObjects);
    Set<UserQuestionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<UserQuestion> businessObjects);
}

package com.base.server.userQuestion.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface UserQuestionDal extends BaseDal<UserQuestionEntity> {
    @Query(value = "SELECT * FROM public.user_question WHERE user_survey_id = ?1", nativeQuery = true)
    public Optional<List<UserQuestionEntity>> getAllUserQuestionFromUserSurvey(String userSurveyId);

    @Query(value = "SELECT * FROM public.user_question WHERE question_answered = ?1", nativeQuery = true)
    public Optional<List<UserQuestionEntity>> getAllFromQuestionVersion(String questionVersionId);
}
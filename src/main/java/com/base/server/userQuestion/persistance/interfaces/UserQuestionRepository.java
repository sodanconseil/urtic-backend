package com.base.server.userQuestion.persistance.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userQuestion.Business.UserQuestion;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserQuestionRepository extends BaseRepository<UserQuestion> {
    List<UserQuestion> getAllUserQuestionFromUserSurvey(String userSurveyId);
    List<UserQuestion> getAllFromQuestionVersion(String questionVersionId);
}

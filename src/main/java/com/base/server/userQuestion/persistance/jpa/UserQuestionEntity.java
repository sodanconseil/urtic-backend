package com.base.server.userQuestion.persistance.jpa;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user_question")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserQuestionEntity extends BaseEntity<UserQuestionEntity> implements Copyable<UserQuestionEntity> {

    private String questionAnswered;

    private String user_survey_Id;

    private Timestamp dateAnswered;

    private boolean hasCompletedSubQuestionCondition;

    @OneToMany(mappedBy="userQuestion", cascade= CascadeType.MERGE)
    private List<UserAnswerEntity> userAnswer;


    @Builder(setterPrefix = "set")
    public UserQuestionEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String questionAnswered, String user_survey_Id, Timestamp dateAnswered, boolean hasCompletedSubQuestionCondition, List<UserAnswerEntity> userAnswer) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.questionAnswered = questionAnswered;
        this.user_survey_Id = user_survey_Id;
        this.dateAnswered = dateAnswered;
        this.hasCompletedSubQuestionCondition = hasCompletedSubQuestionCondition;
        this.userAnswer = userAnswer;
    }


    @Override
    public void copy(UserQuestionEntity userQuestionEntity) {
        super.copy(userQuestionEntity);
        this.questionAnswered = userQuestionEntity.questionAnswered;
        this.user_survey_Id = userQuestionEntity.user_survey_Id;
        this.dateAnswered = userQuestionEntity.dateAnswered;
        this.hasCompletedSubQuestionCondition = userQuestionEntity.hasCompletedSubQuestionCondition;
        this.userAnswer = userAnswer;
    }
}

package com.base.server.userQuestion.persistance;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.mappers.UserQuestionEntityMapper;
import com.base.server.userQuestion.persistance.interfaces.UserQuestionRepository;
import com.base.server.userQuestion.persistance.jpa.UserQuestionDal;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class UserQuestionRepositoryImpl extends BaseRepositoryImpl<UserQuestionEntity, UserQuestion> implements UserQuestionRepository {
    private UserQuestionDal dal;
    private UserQuestionEntityMapper mapper;

    @Autowired
    public UserQuestionRepositoryImpl(UserQuestionDal dal, UserQuestionEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    public List<UserQuestion> getAllUserQuestionFromUserSurvey(String userSurveyId) {
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.getAllUserQuestionFromUserSurvey(userSurveyId).get());
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }

    public List<UserQuestion> getAllFromQuestionVersion(String questionVersionId) {
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.getAllFromQuestionVersion(questionVersionId).get());
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

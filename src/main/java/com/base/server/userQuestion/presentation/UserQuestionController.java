package com.base.server.userQuestion.presentation;

import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.business.interfaces.UserAnswerBusiness;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.Business.interfaces.UserQuestionBusiness;
import com.base.server.userQuestion.mappers.UserQuestionDtoMapper;
import com.base.server.userQuestion.presentation.dtos.UserQuestionResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/userquestion")
public class UserQuestionController {

    private UserQuestionBusiness userQuestionBusiness;
    private UserQuestionDtoMapper userQuestionDtoMapper;
    private UserAnswerBusiness userAnswerBusiness;

    @Autowired
    public UserQuestionController(UserQuestionBusiness userQuestionBusiness, UserQuestionDtoMapper userQuestionDtoMapper, UserAnswerBusiness userAnswerBusiness) {
        this.userQuestionBusiness = userQuestionBusiness;
        this.userQuestionDtoMapper = userQuestionDtoMapper;
        this.userAnswerBusiness = userAnswerBusiness;
    }

    @RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUserQuestionById(@PathVariable String id, HttpServletRequest request){
        UserQuestion userQuestion = userQuestionBusiness.get(id);
        UserQuestionResponseDto userQuestionResponseDto = userQuestionDtoMapper.businessObjectToResponseDto(userQuestion);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(userQuestionResponseDto);
        return responseEntity;
    }

    @RequestMapping(value = "/all/{userSurveyId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllAnsweredQuestionForUserSurvey(@PathVariable String userSurveyId, HttpServletRequest request){
        List<UserQuestion> userQuestionList = userQuestionBusiness.getAllUserQuestionFromUserSurvey(userSurveyId);

        List<UserQuestionResponseDto> userQuestionResponseDto = userQuestionDtoMapper.listOfBusinessObjectsToListOfResponseDto(userQuestionList);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(userQuestionResponseDto);
        return responseEntity;
    }

    @RequestMapping(value = "/all/questionVersion/{questionVersionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllFromQuestionVersion(@PathVariable String questionVersionId, HttpServletRequest request){
        List<UserQuestion> userQuestionList = userQuestionBusiness.getAllFromQuestionVersion(questionVersionId);
        List<UserQuestionResponseDto> userQuestionResponseDto = userQuestionDtoMapper.listOfBusinessObjectsToListOfResponseDto(userQuestionList);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(userQuestionResponseDto);
        return responseEntity;
    }
}

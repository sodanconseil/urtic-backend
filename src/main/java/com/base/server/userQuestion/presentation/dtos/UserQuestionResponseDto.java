package com.base.server.userQuestion.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserQuestionResponseDto extends BaseResponseDto {

    private String questionAnswered;
    private String user_survey_Id;
    private Timestamp dateAnswered;
    private boolean hasCompletedSubQuestionCondition;

    @Builder(setterPrefix = "set")
    public UserQuestionResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String questionAnswered, String user_survey_Id, Timestamp dateAnswered, boolean hasCompletedSubQuestionCondition) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.questionAnswered = questionAnswered;
        this.user_survey_Id = user_survey_Id;
        this.dateAnswered = dateAnswered;
        this.hasCompletedSubQuestionCondition = hasCompletedSubQuestionCondition;
    }
}

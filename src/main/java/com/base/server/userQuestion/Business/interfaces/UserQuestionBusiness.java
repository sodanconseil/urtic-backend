package com.base.server.userQuestion.Business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.userQuestion.Business.UserQuestion;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserQuestionBusiness extends BaseBusiness<UserQuestion> {
    List<UserQuestion> getAllUserQuestionFromUserSurvey(String userSurveyId);
    List<UserQuestion> getAllFromQuestionVersion(String questionVersionId);
}

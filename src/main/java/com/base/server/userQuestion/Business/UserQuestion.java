package com.base.server.userQuestion.Business;

import com.base.server.generic.business.Base;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserQuestion  extends Base {

    private String questionAnswered;
    private String user_survey_Id;
    private Timestamp dateAnswered;
    private boolean hasCompletedSubQuestionCondition;

    @Builder(setterPrefix = "set")
    public UserQuestion(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String questionAnswered, String user_survey_Id, Timestamp dateAnswered, boolean hasCompletedSubQuestionCondition) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.questionAnswered = questionAnswered;
        this.user_survey_Id = user_survey_Id;
        this.dateAnswered = dateAnswered;
        this.hasCompletedSubQuestionCondition = hasCompletedSubQuestionCondition;
    }
}

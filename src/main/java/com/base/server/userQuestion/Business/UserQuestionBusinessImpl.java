package com.base.server.userQuestion.Business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.userQuestion.Business.interfaces.UserQuestionBusiness;
import com.base.server.userQuestion.factory.UserQuestionFactory;
import com.base.server.userQuestion.persistance.interfaces.UserQuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserQuestionBusinessImpl extends BaseBusinessImpl<UserQuestion, UserQuestionRepository> implements UserQuestionBusiness {

    private UserQuestionRepository userQuestionRepository;
    private UserQuestionFactory userQuestionFactory;

    @Autowired
    public UserQuestionBusinessImpl(UserQuestionRepository userQuestionRepository, UserQuestionFactory factory) {
        super(userQuestionRepository, factory);
        this.userQuestionRepository = userQuestionRepository;
        this.userQuestionFactory = userQuestionFactory;
    }

    @Override
    public List<UserQuestion> getAllUserQuestionFromUserSurvey(String userSurveyId) {
        return userQuestionRepository.getAllUserQuestionFromUserSurvey(userSurveyId);
    }

    @Override
    public List<UserQuestion> getAllFromQuestionVersion(String questionVersionId) {
        return userQuestionRepository.getAllFromQuestionVersion(questionVersionId);
    }
}

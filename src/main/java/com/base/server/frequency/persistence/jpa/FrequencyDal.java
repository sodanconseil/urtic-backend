package com.base.server.frequency.persistence.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;

public interface FrequencyDal extends BaseDal<FrequencyEntity> {
    public FrequencyEntity findByName(String name);
}

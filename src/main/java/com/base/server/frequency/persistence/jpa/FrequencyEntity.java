package com.base.server.frequency.persistence.jpa;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "frequency")
@Data
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class FrequencyEntity extends BaseEntity<FrequencyEntity> implements Copyable<FrequencyEntity> {

    private Integer annualized_value;

    private String fr;
    private String en;


    @Builder(setterPrefix = "set")
    public FrequencyEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, Integer annualized_value, String fr, String en) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.annualized_value = annualized_value;
        this.fr = fr;
        this.en = en;
    }

    @Override
    public void copy(FrequencyEntity frequencyEntity) {
        super.copy(frequencyEntity);
        this.annualized_value = frequencyEntity.annualized_value;
        this.fr = fr;
        this.en = en;
    }

}

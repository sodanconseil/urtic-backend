package com.base.server.frequency.persistence;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.mappers.EthnicityEntityMapper;
import com.base.server.ethnicity.persistence.interfaces.EthnicityRepository;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.mappers.FrequencyEntityMapper;
import com.base.server.frequency.persistence.interfaces.FrequencyRepository;
import com.base.server.frequency.persistence.jpa.FrequencyDal;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FrequencyRepositoryImpl extends BaseRepositoryImpl<FrequencyEntity, Frequency> implements FrequencyRepository {
    private FrequencyDal dal;
    private FrequencyEntityMapper mapper;

    @Autowired
    public FrequencyRepositoryImpl(FrequencyDal dal, FrequencyEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }
}

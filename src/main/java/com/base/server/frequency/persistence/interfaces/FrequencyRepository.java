package com.base.server.frequency.persistence.interfaces;

import com.base.server.frequency.business.Frequency;
import com.base.server.generic.persistence.interfaces.BaseRepository;

public interface FrequencyRepository extends BaseRepository<Frequency> {
}

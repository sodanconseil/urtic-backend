package com.base.server.frequency.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FrequencyResponseDto extends BaseResponseDto {

    private Integer annualized_value;

    private String fr;
    private String en;

    @Builder(setterPrefix = "set")
    public FrequencyResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Integer annualized_value, String fr, String en) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.annualized_value = annualized_value;
        this.fr = fr;
        this.en = en;
    }
}

package com.base.server.frequency.presentation;

import com.base.server.dosage.factory.DosageFactory;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.dosage.presentation.managers.*;
import com.base.server.frequency.factory.FrequencyFactory;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.frequency.presentation.managers.*;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/frequences")
public class FrequencyController {

    private FrequencyFactory frequencyFactory;

    private FrequencyExistenceManager frequencyExistenceManager;
    private FrequencyEtagManager frequencyEtagManager;
    private FrequencyPostManager frequencyPostManager;
    private FrequencyGetAllManager frequencyGetAllManager;
    private FrequencyGetByIdManager frequencyGetByIdManager;
    private FrequencyPutManager frequencyPutManager;
    private FrequencyDeleteManager frequencyDeleteManager;

    @Autowired
    public FrequencyController(FrequencyFactory frequencyFactory, FrequencyExistenceManager frequencyExistenceManager, FrequencyEtagManager frequencyEtagManager, FrequencyPostManager frequencyPostManager, FrequencyGetAllManager frequencyGetAllManager, FrequencyGetByIdManager frequencyGetByIdManager, FrequencyPutManager frequencyPutManager, FrequencyDeleteManager frequencyDeleteManager) {
        this.frequencyFactory = frequencyFactory;
        this.frequencyExistenceManager = frequencyExistenceManager;
        this.frequencyEtagManager = frequencyEtagManager;
        this.frequencyPostManager = frequencyPostManager;
        this.frequencyGetAllManager = frequencyGetAllManager;
        this.frequencyGetByIdManager = frequencyGetByIdManager;
        this.frequencyPutManager = frequencyPutManager;
        this.frequencyDeleteManager = frequencyDeleteManager;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        RequestInformation<FrequencyRequestDto> requestInformation = frequencyFactory.newRequestInformation();
        return frequencyFactory.newRequestPipeline(frequencyGetAllManager).manage(requestInformation);
    }

    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getFrequency(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<FrequencyRequestDto> requestInformation = frequencyFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");
        return frequencyFactory.newRequestPipeline(frequencyExistenceManager)
                .addManager(frequencyGetByIdManager)
                .manage(requestInformation);
    }
}

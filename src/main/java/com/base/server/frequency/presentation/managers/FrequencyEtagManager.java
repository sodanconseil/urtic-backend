package com.base.server.frequency.presentation.managers;

import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.mappers.FrequencyDtoMapper;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class FrequencyEtagManager implements RequestManager<FrequencyRequestDto> {

    private FrequencyBusiness frequencyBusiness;
    private FrequencyDtoMapper frequencyDtoMapper;
    private ErrorMapper errorMapper;

    
    @Autowired
    public FrequencyEtagManager(FrequencyBusiness frequencyBusiness, FrequencyDtoMapper frequencyDtoMapper, ErrorMapper errorMapper) {
        this.frequencyBusiness = frequencyBusiness;
        this.frequencyDtoMapper = frequencyDtoMapper;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<FrequencyRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        String receivedEtag = requestInformation.getIf_match();
        String currentEtag = frequencyDtoMapper.getEtag(frequencyBusiness.get(requestInformation.getId()));

        if (!receivedEtag.equals(currentEtag)) {
            HttpStatus status = HttpStatus.PRECONDITION_FAILED;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity was changed, GET the new version";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }    
}

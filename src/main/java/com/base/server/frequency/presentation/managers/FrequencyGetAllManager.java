package com.base.server.frequency.presentation.managers;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.mappers.FrequencyDtoMapper;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;

import com.base.server.frequency.presentation.dtos.FrequencyResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class FrequencyGetAllManager implements RequestManager<FrequencyRequestDto> {

    private FrequencyBusiness frequencyBusiness;
    private FrequencyDtoMapper frequencyDtoMapper;

    @Autowired
    public FrequencyGetAllManager(FrequencyBusiness frequencyBusiness, FrequencyDtoMapper frequencyDtoMapper) {
        this.frequencyBusiness = frequencyBusiness;
        this.frequencyDtoMapper = frequencyDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<FrequencyRequestDto> requestInformation) {
        List<Frequency> frequencys = frequencyBusiness.getAll();
        List<FrequencyResponseDto> ethnicityResponseDto = frequencyDtoMapper.listOfBusinessObjectsToListOfResponseDto(frequencys);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDto);

        return Optional.of(responseEntity);
    }
}

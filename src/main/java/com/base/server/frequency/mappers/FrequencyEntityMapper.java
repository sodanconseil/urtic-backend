package com.base.server.frequency.mappers;


import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FrequencyEntityMapper extends BaseEntityMapper<FrequencyEntity, Frequency> {
    public Frequency entityToBusinessObject(FrequencyEntity entity);
    public FrequencyEntity businessObjectToEntity(Frequency object);
    public List<Frequency> listOfEntityToListOfBusinessObjects(List<FrequencyEntity> entities);
}
package com.base.server.frequency.mappers;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.frequency.presentation.dtos.FrequencyResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface FrequencyDtoMapper extends BaseDtoMapper<Frequency, FrequencyRequestDto, FrequencyResponseDto> {
    Frequency requestDtoToBusinessObject(FrequencyRequestDto dto);
    FrequencyResponseDto businessObjectToResponseDto(Frequency businessObject);
    List<FrequencyResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Frequency> businessObjects);
    Set<FrequencyResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<Frequency> businessObjects);
}

package com.base.server.frequency.business.interfaces;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.frequency.business.Frequency;
import com.base.server.generic.business.interfaces.BaseBusiness;

public interface FrequencyBusiness extends BaseBusiness<Frequency> {
}

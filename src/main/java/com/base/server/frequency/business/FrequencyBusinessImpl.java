package com.base.server.frequency.business;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.factory.EthnicityFactory;
import com.base.server.ethnicity.persistence.interfaces.EthnicityRepository;
import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.factory.FrequencyFactory;
import com.base.server.frequency.persistence.interfaces.FrequencyRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FrequencyBusinessImpl extends BaseBusinessImpl<Frequency, FrequencyRepository> implements FrequencyBusiness {

    private FrequencyRepository frequencyRepository;
    private FrequencyFactory frequencyFactory;

    @Autowired
    public FrequencyBusinessImpl(FrequencyRepository frequencyRepository, FrequencyFactory factory) {
        super(frequencyRepository, factory);
        this.frequencyRepository = frequencyRepository;
        this.frequencyFactory = frequencyFactory;
    }
}

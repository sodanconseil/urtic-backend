package com.base.server.frequency.business;

import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import com.base.server.generic.business.Base;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Frequency extends Base {

    private Integer annualized_value;

    private String fr;
    private String en;

    @Builder(setterPrefix = "set")
    public Frequency(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Integer annualized_value, String fr, String en) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.annualized_value = annualized_value;
        this.fr = fr;
        this.en = en;
    }
}

package com.base.server.unit.mappers;

import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.unit.business.Unit;
import com.base.server.unit.persistence.jpa.UnitEntity;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UnitEntityMapper extends BaseEntityMapper<UnitEntity, Unit>{
    public Unit entityToBusinessObject(UnitEntity entity);
    public UnitEntity businessObjectToEntity(Unit object);
    public List<Unit> listOfEntityToListOfBusinessObjects(List<UnitEntity> entities);
}

package com.base.server.unit.mappers;

import java.util.List;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.unit.business.Unit;
import com.base.server.unit.presentation.dtos.UnitRequestDto;
import com.base.server.unit.presentation.dtos.UnitResponseDto;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UnitDtoMapper extends BaseDtoMapper<Unit, UnitRequestDto, UnitResponseDto> {
    Unit requestDtoToBusinessObject(UnitRequestDto dto);
    UnitResponseDto businessObjectToResponseDto(Unit businessObject);
    List<UnitResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Unit> businessObjects);
}

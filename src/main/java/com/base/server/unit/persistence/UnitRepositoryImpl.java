package com.base.server.unit.persistence;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.unit.business.Unit;
import com.base.server.unit.mappers.UnitEntityMapper;
import com.base.server.unit.persistence.interfaces.UnitRepository;
import com.base.server.unit.persistence.jpa.UnitDal;
import com.base.server.unit.persistence.jpa.UnitEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UnitRepositoryImpl extends BaseRepositoryImpl<UnitEntity, Unit> implements UnitRepository{
    private UnitDal dal;
    private UnitEntityMapper mapper;

    @Autowired
    public UnitRepositoryImpl(UnitDal dal, UnitEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }
}

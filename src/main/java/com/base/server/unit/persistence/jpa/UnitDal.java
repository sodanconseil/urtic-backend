package com.base.server.unit.persistence.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.stereotype.Component;

@Component
public interface UnitDal  extends BaseDal<UnitEntity> {
    public UnitEntity findByName(String abbreviation);
}

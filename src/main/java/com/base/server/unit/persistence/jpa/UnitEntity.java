package com.base.server.unit.persistence.jpa;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "unit")
@Data
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class UnitEntity extends BaseEntity<UnitEntity> implements Copyable<UnitEntity> {
    private String abbreviation;

    @Builder(setterPrefix = "set")
    public UnitEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String abbreviation) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.abbreviation = abbreviation;
    }
    public void copy(UnitEntity unitMeasureEntity){
        super.copy(unitMeasureEntity);
        this.abbreviation = unitMeasureEntity.abbreviation;
    }

}

package com.base.server.unit.persistence.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.unit.business.Unit;

public interface UnitRepository extends BaseRepository<Unit> {
}

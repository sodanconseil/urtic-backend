package com.base.server.unit.business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.unit.business.interfaces.UnitBusiness;
import com.base.server.unit.factory.UnitFactory;
import com.base.server.unit.persistence.interfaces.UnitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UnitBusinessImpl extends BaseBusinessImpl<Unit, UnitRepository> implements UnitBusiness{
    private UnitRepository unitRepository;
    private UnitFactory unitFactory;

    @Autowired
    public UnitBusinessImpl(UnitRepository unitRepository, UnitFactory factory) {
        super(unitRepository, factory);
        this.unitRepository = unitRepository;
        this.unitFactory = unitFactory;
    }
}

package com.base.server.unit.business;

import com.base.server.generic.business.Base;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Unit extends Base {
    private String abbreviation;

    @Builder(setterPrefix = "set")
    public Unit(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String abbreviation) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.abbreviation = abbreviation;
    }
}

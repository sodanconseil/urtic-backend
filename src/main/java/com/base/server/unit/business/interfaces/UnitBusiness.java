package com.base.server.unit.business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.unit.business.Unit;

public interface UnitBusiness extends BaseBusiness<Unit> {
}

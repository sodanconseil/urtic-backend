package com.base.server.unit.presentation.managers;

import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.mappers.FrequencyDtoMapper;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.unit.business.interfaces.UnitBusiness;
import com.base.server.unit.mappers.UnitDtoMapper;
import com.base.server.unit.presentation.dtos.UnitRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UnitEtagManager implements RequestManager<UnitRequestDto> {

    private UnitBusiness unitBusiness;
    private UnitDtoMapper unitDtoMapper;
    private ErrorMapper errorMapper;

    
    @Autowired
    public UnitEtagManager(UnitBusiness unitBusiness, UnitDtoMapper unitDtoMapper, ErrorMapper errorMapper) {
        this.unitBusiness = unitBusiness;
        this.unitDtoMapper = unitDtoMapper;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UnitRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        String receivedEtag = requestInformation.getIf_match();
        String currentEtag = unitDtoMapper.getEtag(unitBusiness.get(requestInformation.getId()));

        if (!receivedEtag.equals(currentEtag)) {
            HttpStatus status = HttpStatus.PRECONDITION_FAILED;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity was changed, GET the new version";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }    
}

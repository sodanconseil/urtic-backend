package com.base.server.unit.presentation.managers;

import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.unit.business.interfaces.UnitBusiness;
import com.base.server.unit.presentation.dtos.UnitRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UnitExistenceManager implements RequestManager<UnitRequestDto> {

    private UnitBusiness unitBusiness;
    private ErrorMapper errorMapper;

    @Autowired
    public UnitExistenceManager(UnitBusiness unitBusiness, ErrorMapper errorMapper) {
        this.unitBusiness = unitBusiness;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UnitRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        if (unitBusiness.get(requestInformation.getId()) == null) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity with the specified id wasn't found";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }
}

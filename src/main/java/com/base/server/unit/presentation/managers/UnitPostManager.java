package com.base.server.unit.presentation.managers;

import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.frequency.mappers.FrequencyDtoMapper;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.frequency.presentation.dtos.FrequencyResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.unit.business.Unit;
import com.base.server.unit.business.interfaces.UnitBusiness;
import com.base.server.unit.mappers.UnitDtoMapper;
import com.base.server.unit.presentation.dtos.UnitRequestDto;
import com.base.server.unit.presentation.dtos.UnitResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UnitPostManager implements RequestManager<UnitRequestDto> {

    private UnitBusiness unitBusiness;
    private UnitDtoMapper unitDtoMapper;

    @Autowired
    public UnitPostManager(UnitBusiness unitBusiness, UnitDtoMapper unitDtoMapper) {
        this.unitBusiness = unitBusiness;
        this.unitDtoMapper = unitDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UnitRequestDto> requestInformation) {

        String userId = requestInformation.getUserId();
        UnitRequestDto unitRequestDto = requestInformation.getDto();
        Unit unit = unitBusiness.save(unitDtoMapper.requestDtoToBusinessObject(unitRequestDto), userId);
        UnitResponseDto unitResponseDto = unitDtoMapper.businessObjectToResponseDto(unit);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(unitDtoMapper.getEtag(unit))
            .body(unitResponseDto);

        return Optional.of(responseEntity);
    }
}

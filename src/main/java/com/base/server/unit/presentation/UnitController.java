package com.base.server.unit.presentation;

import com.base.server.frequency.factory.FrequencyFactory;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.frequency.presentation.managers.*;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.unit.factory.UnitFactory;
import com.base.server.unit.presentation.dtos.UnitRequestDto;
import com.base.server.unit.presentation.managers.*;
import com.base.server.user.presentation.dtos.UserRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/Units")
public class UnitController {

    private UnitFactory unitFactory;

    private UnitExistenceManager unitExistenceManager;
    private UnitEtagManager unitEtagManager;
    private UnitPostManager unitPostManager;
    private UnitGetAllManager unitGetAllManager;
    private UnitGetByIdManager unitGetByIdManager;
    private UnitPutManager unitPutManager;
    private UnitDeleteManager unitDeleteManager;
    private ValidationManager<UnitRequestDto> unitValidationManager;

    @Autowired
    public UnitController(UnitFactory unitFactory, UnitExistenceManager unitExistenceManager, UnitEtagManager unitEtagManager, UnitPostManager unitPostManager, UnitGetAllManager unitGetAllManager, UnitGetByIdManager unitGetByIdManager, UnitPutManager unitPutManager, UnitDeleteManager unitDeleteManager, ValidationManager<UnitRequestDto> unitValidationManager) {
        this.unitFactory = unitFactory;
        this.unitExistenceManager = unitExistenceManager;
        this.unitEtagManager = unitEtagManager;
        this.unitPostManager = unitPostManager;
        this.unitGetAllManager = unitGetAllManager;
        this.unitGetByIdManager = unitGetByIdManager;
        this.unitPutManager = unitPutManager;
        this.unitDeleteManager = unitDeleteManager;
        this.unitValidationManager = unitValidationManager;
    }






    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        RequestInformation<UnitRequestDto> requestInformation = unitFactory.newRequestInformation();
        return unitFactory.newRequestPipeline(unitGetAllManager).manage(requestInformation);
    }

    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUnit(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<UnitRequestDto> requestInformation = unitFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");
        return unitFactory.newRequestPipeline(unitExistenceManager)
                .addManager(unitGetByIdManager)
                .manage(requestInformation);
    }
}

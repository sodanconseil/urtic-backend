package com.base.server.unit.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UnitResponseDto extends BaseResponseDto{

    private String abbreviation;

    @Builder(setterPrefix = "set")
    public UnitResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String abbreviation) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.abbreviation = abbreviation;
    }
}

package com.base.server.dosage.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseRequestDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DosageRequestDto extends BaseRequestDto {

    @Builder(setterPrefix = "set")
    public DosageRequestDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
    }
}

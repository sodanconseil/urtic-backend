package com.base.server.dosage.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DosageResponseDto extends BaseResponseDto {

    @Builder(setterPrefix = "set")
    public DosageResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
    }
}

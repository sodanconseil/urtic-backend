package com.base.server.dosage.presentation;

import com.base.server.dosage.factory.DosageFactory;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.dosage.presentation.managers.*;
import com.base.server.drug.factory.DrugFactory;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.drug.presentation.managers.*;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/dosages")
public class DosageController {

    private DosageFactory dosageFactory;

    private ValidationManager<DosageRequestDto> dosageValidationManager;
    private DosageExistenceManager dosageExistenceManager;
    private DosageEtagManager dosageEtagManager;
    private DosagePostManager dosagePostManager;
    private DosageGetAllManager dosageGetAllManager;
    private DosageGetByIdManager dosageGetByIdManager;
    private DosagePutManager dosagePutManager;
    private DosageDeleteManager dosageDeleteManager;

    @Autowired
    public DosageController(DosageFactory dosageFactory, ValidationManager<DosageRequestDto> dosageValidationManager, DosageExistenceManager dosageExistenceManager, DosageEtagManager dosageEtagManager, DosagePostManager dosagePostManager, DosageGetAllManager dosageGetAllManager, DosageGetByIdManager dosageGetByIdManager, DosagePutManager dosagePutManager, DosageDeleteManager dosageDeleteManager) {
        this.dosageFactory = dosageFactory;
        this.dosageValidationManager = dosageValidationManager;
        this.dosageExistenceManager = dosageExistenceManager;
        this.dosageEtagManager = dosageEtagManager;
        this.dosagePostManager = dosagePostManager;
        this.dosageGetAllManager = dosageGetAllManager;
        this.dosageGetByIdManager = dosageGetByIdManager;
        this.dosagePutManager = dosagePutManager;
        this.dosageDeleteManager = dosageDeleteManager;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        RequestInformation<DosageRequestDto> requestInformation = dosageFactory.newRequestInformation();
        return dosageFactory.newRequestPipeline(dosageGetAllManager).manage(requestInformation);
    }

    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getDosage(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<DosageRequestDto> requestInformation = dosageFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");
        return dosageFactory.newRequestPipeline(dosageExistenceManager)
                .addManager(dosageGetByIdManager)
                .manage(requestInformation);
    }



}

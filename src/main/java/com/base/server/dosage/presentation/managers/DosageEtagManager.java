package com.base.server.dosage.presentation.managers;

import com.base.server.dosage.business.interfaces.DosageBusiness;
import com.base.server.dosage.mappers.DosageDtoMapper;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DosageEtagManager implements RequestManager<DosageRequestDto> {

    private DosageBusiness dosageBusiness;
    private DosageDtoMapper dosageDtoMapper;
    private ErrorMapper errorMapper;

    
    @Autowired
    public DosageEtagManager(DosageBusiness dosageBusiness, DosageDtoMapper dosageDtoMapper, ErrorMapper errorMapper) {
        this.dosageBusiness = dosageBusiness;
        this.dosageDtoMapper = dosageDtoMapper;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DosageRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        String receivedEtag = requestInformation.getIf_match();
        String currentEtag = dosageDtoMapper.getEtag(dosageBusiness.get(requestInformation.getId()));

        if (!receivedEtag.equals(currentEtag)) {
            HttpStatus status = HttpStatus.PRECONDITION_FAILED;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity was changed, GET the new version";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }    
}

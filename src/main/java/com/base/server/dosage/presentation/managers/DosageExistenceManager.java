package com.base.server.dosage.presentation.managers;

import com.base.server.dosage.business.interfaces.DosageBusiness;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DosageExistenceManager implements RequestManager<DosageRequestDto> {

    private DosageBusiness dosageBusiness;
    private ErrorMapper errorMapper;

    @Autowired
    public DosageExistenceManager(DosageBusiness dosageBusiness, ErrorMapper errorMapper) {
        this.dosageBusiness = dosageBusiness;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DosageRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        if (dosageBusiness.get(requestInformation.getId()) == null) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity with the specified id wasn't found";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }
}

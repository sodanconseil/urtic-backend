package com.base.server.dosage.presentation.managers;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.business.interfaces.DosageBusiness;
import com.base.server.dosage.mappers.DosageDtoMapper;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.dosage.presentation.dtos.DosageResponseDto;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DosagePutManager implements RequestManager<DosageRequestDto> {

    private DosageBusiness dosageBusiness;
    private DosageDtoMapper dosageDtoMapper;

    @Autowired
    public DosagePutManager(DosageBusiness dosageBusiness, DosageDtoMapper dosageDtoMapper) {
        this.dosageBusiness = dosageBusiness;
        this.dosageDtoMapper = dosageDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DosageRequestDto> requestInformation) {

        String id = requestInformation.getId();
        String userId = requestInformation.getUserId();
        DosageRequestDto ethnicityRequestDto = requestInformation.getDto();
        Dosage dosage = dosageBusiness.modify(id, dosageDtoMapper.requestDtoToBusinessObject(ethnicityRequestDto), userId);
        DosageResponseDto ethnicityResponseDto = dosageDtoMapper.businessObjectToResponseDto(dosage);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(dosageDtoMapper.getEtag(dosage))
            .body(ethnicityResponseDto);

        return Optional.of(responseEntity);
    }
}

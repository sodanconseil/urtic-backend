package com.base.server.dosage.presentation.managers;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.business.interfaces.DosageBusiness;
import com.base.server.dosage.mappers.DosageDtoMapper;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.dosage.presentation.dtos.DosageResponseDto;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class DosageGetAllManager implements RequestManager<DosageRequestDto> {

    private DosageBusiness dosageBusiness;
    private DosageDtoMapper dosageDtoMapper;

    @Autowired
    public DosageGetAllManager(DosageBusiness dosageBusiness, DosageDtoMapper dosageDtoMapper) {
        this.dosageBusiness = dosageBusiness;
        this.dosageDtoMapper = dosageDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DosageRequestDto> requestInformation) {
        List<Dosage> dosages = dosageBusiness.getAll();
        List<DosageResponseDto> DosageResponseDto = dosageDtoMapper.listOfBusinessObjectsToListOfResponseDto(dosages);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(DosageResponseDto);

        return Optional.of(responseEntity);
    }
}

package com.base.server.dosage.business;

import com.base.server.dosage.business.interfaces.DosageBusiness;
import com.base.server.dosage.factory.DosageFactory;
import com.base.server.dosage.persistence.interfaces.DosageRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.generic.factory.BaseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DosageBusinessImpl extends BaseBusinessImpl<Dosage, DosageRepository> implements DosageBusiness {
    private DosageRepository dosageRepository;
    private DosageFactory dosageFactory;

    @Autowired
    public DosageBusinessImpl(DosageRepository dosageRepository, DosageFactory factory) {
        super(dosageRepository, factory);
        this.dosageRepository = dosageRepository;
    }
}

package com.base.server.dosage.business.interfaces;

import com.base.server.dosage.business.Dosage;
import com.base.server.generic.business.interfaces.BaseBusiness;

public interface DosageBusiness  extends BaseBusiness<Dosage> {
}

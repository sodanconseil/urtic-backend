package com.base.server.dosage.business;

import com.base.server.generic.business.Base;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Dosage  extends Base {

    @Builder(setterPrefix = "set")
    public Dosage(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
    }
}

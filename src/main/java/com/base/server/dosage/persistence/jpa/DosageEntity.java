package com.base.server.dosage.persistence.jpa;

import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.prescription.persistence.jpa.PrescriptionEntity;
import com.base.server.unit.persistence.jpa.UnitEntity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "dosage")
@Data
@NoArgsConstructor
public class DosageEntity extends BaseEntity<DosageEntity> implements Copyable<DosageEntity> {


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "unit_id", nullable = false)
    private UnitEntity unitMeasureEntity;

    // TODO confirmer
    //@ManyToOne(fetch = FetchType.LAZY, optional = false)
    //@JoinColumn(name = "frequency_id", nullable = false)
    //private FrequencyEntity frequencyEntity;


    @ManyToMany(mappedBy = "dosages")
    private Set<DrugEntity> drugEntities;

    @Builder(setterPrefix = "set")
    public DosageEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, UnitEntity unitMeasureEntity, FrequencyEntity frequencyEntity, Set<DrugEntity> drugEntities) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.unitMeasureEntity = unitMeasureEntity;
        //this.frequencyEntity = frequencyEntity;
        this.drugEntities = drugEntities == null? new HashSet<>() : drugEntities;
    }

    @Override
    public void copy(DosageEntity dosageEntity) {
        super.copy(dosageEntity);
        this.unitMeasureEntity = dosageEntity.unitMeasureEntity;
        //this.frequencyEntity = dosageEntity.frequencyEntity;
        this.drugEntities = dosageEntity.drugEntities;
    }
}

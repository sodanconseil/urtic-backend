package com.base.server.dosage.persistence.interfaces;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.stereotype.Component;


public interface DosageRepository extends BaseRepository<Dosage> {
}

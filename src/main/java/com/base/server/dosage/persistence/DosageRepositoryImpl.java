package com.base.server.dosage.persistence;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.mappers.DosageEntityMapper;
import com.base.server.dosage.persistence.interfaces.DosageRepository;
import com.base.server.dosage.persistence.jpa.DosageDal;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.persistence.interfaces.EthnicityRepository;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DosageRepositoryImpl extends BaseRepositoryImpl<DosageEntity, Dosage> implements DosageRepository {
    private DosageDal dal;
    private DosageEntityMapper mapper;

    @Autowired
    public DosageRepositoryImpl(DosageDal dal, DosageEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }
}

package com.base.server.dosage.persistence.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.stereotype.Component;

@Component
public interface DosageDal extends BaseDal<DosageEntity> {
}

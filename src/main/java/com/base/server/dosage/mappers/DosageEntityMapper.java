package com.base.server.dosage.mappers;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DosageEntityMapper  extends BaseEntityMapper<DosageEntity, Dosage> {
    public Dosage entityToBusinessObject(DosageEntity entity);
    public DosageEntity businessObjectToEntity(Dosage object);
    public List<Dosage> listOfEntityToListOfBusinessObjects(List<DosageEntity> entities);
}

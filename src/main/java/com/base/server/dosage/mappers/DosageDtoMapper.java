package com.base.server.dosage.mappers;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.dosage.presentation.dtos.DosageResponseDto;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface DosageDtoMapper extends BaseDtoMapper<Dosage, DosageRequestDto, DosageResponseDto> {
    Dosage requestDtoToBusinessObject(DosageRequestDto dto);
    DosageResponseDto businessObjectToResponseDto(Dosage businessObject);
    List<DosageResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Dosage> businessObjects);
    Set<DosageResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<Dosage> businessObjects);
}

package com.base.server.answerChoice.mappers;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.dtos.AnswerChoiceRequestDto;
import com.base.server.answerChoice.dtos.AnswerChoiceResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface AnswerChoiceDtoMapper extends BaseDtoMapper<AnswerChoice, AnswerChoiceRequestDto, AnswerChoiceResponseDto> {
    AnswerChoice requestDtoToBusinessObject(AnswerChoiceRequestDto dto);
    AnswerChoiceResponseDto businessObjectToResponseDto(AnswerChoice businessObject);
    List<AnswerChoiceResponseDto> listOfBusinessObjectsToListOfResponseDto(List<AnswerChoice> businessObjects);
    Set<AnswerChoiceResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<AnswerChoice> businessObjects);
}

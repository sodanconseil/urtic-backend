package com.base.server.answerChoice.mappers;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AnswerChoiceEntityMapper extends BaseEntityMapper<AnswerChoiceEntity, AnswerChoice> {
    public AnswerChoice entityToBusinessObject(AnswerChoiceEntity entity);
    public AnswerChoiceEntity businessObjectToEntity(AnswerChoice object);
    public List<AnswerChoice> listOfEntityToListOfBusinessObjects(List<AnswerChoiceEntity> entities);

}

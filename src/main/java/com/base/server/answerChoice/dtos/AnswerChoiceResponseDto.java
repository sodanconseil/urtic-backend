package com.base.server.answerChoice.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AnswerChoiceResponseDto extends BaseResponseDto {

    private int code;
    private int answer_order;
    private int value;

    @Column(columnDefinition = "text")
    private String text;

    @Column(columnDefinition = "text")
    private String formattedText;

    @Column(columnDefinition = "text")
    private String info;

    @Column(columnDefinition = "text")
    private String formattedInfo;
    private QuestionVersion questionVersion;
    private boolean supplementInput;

    @Column(columnDefinition = "text")
    private String supplementInputText;

    private String subQuestionCondition;

    private boolean endSurvey;


    @Builder(setterPrefix = "set")
    public AnswerChoiceResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, int code, int answer_order, int value, String text, String formattedText, String info, String formattedInfo, QuestionVersion questionVersion, boolean supplementInput, String supplementInputText, String subQuestionCondition, boolean endSurvey) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.code = code;
        this.answer_order = answer_order;
        this.value = value;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.questionVersion = questionVersion;
        this.supplementInput = supplementInput;
        this.supplementInputText = supplementInputText;
        this.subQuestionCondition = subQuestionCondition;
        this.endSurvey = endSurvey;
    }
}

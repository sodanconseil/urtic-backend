package com.base.server.answerChoice.business.interfaces;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AnswerChoiceBusiness extends BaseBusiness<AnswerChoice> {
    public List<AnswerChoice> getAllFromSubQuestionCondition(String subQuestionCondition);
}

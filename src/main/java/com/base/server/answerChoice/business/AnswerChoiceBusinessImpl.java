package com.base.server.answerChoice.business;

import com.base.server.answerChoice.business.interfaces.AnswerChoiceBusiness;
import com.base.server.answerChoice.factory.AnswerChoiceFactory;
import com.base.server.answerChoice.persistance.interfaces.AnswerChoiceRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AnswerChoiceBusinessImpl extends BaseBusinessImpl<AnswerChoice, AnswerChoiceRepository> implements AnswerChoiceBusiness {

    private AnswerChoiceRepository answerChoiceRepository;
    private AnswerChoiceFactory answerChoiceFactory;

    @Autowired
    public AnswerChoiceBusinessImpl(AnswerChoiceRepository answerChoiceRepository, AnswerChoiceFactory factory) {
        super(answerChoiceRepository, factory);
        this.answerChoiceRepository = answerChoiceRepository;
        this.answerChoiceFactory = answerChoiceFactory;
    }

    @Override
    public List<AnswerChoice> getAllFromSubQuestionCondition(String subQuestionCondition){
        return answerChoiceRepository.getAllFromSubQuestionCondition(subQuestionCondition);
    }

}

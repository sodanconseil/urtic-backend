package com.base.server.answerChoice.business;

import com.base.server.generic.business.Base;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.w3c.dom.Text;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AnswerChoice extends Base {

    private int code;
    private int answer_order;

    @Column(columnDefinition = "text")
    private String text;

    @Column(columnDefinition = "text")
    private String formattedText;

    @Column(columnDefinition = "text")
    private String info;

    @Column(columnDefinition = "text")
    private String formattedInfo;
    private QuestionVersion questionVersion;
    private boolean supplementInput;

    @Column(columnDefinition = "text")
    private String supplementInputText;

    private String subQuestionCondition;

    private boolean endSurvey;


    @Builder(setterPrefix = "set")
    public AnswerChoice(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, int code, int answer_order, String text, String formattedText, String info, String formattedInfo, QuestionVersion questionVersion, boolean supplementInput, String supplementInputText, String subQuestionCondition, boolean endSurvey) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.code = code;
        this.answer_order = answer_order;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.questionVersion = questionVersion;
        this.supplementInput = supplementInput;
        this.supplementInputText = supplementInputText;
        this.subQuestionCondition = subQuestionCondition;
        this.endSurvey = endSurvey;
    }
}

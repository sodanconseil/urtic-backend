package com.base.server.answerChoice.traduction.answerChoiceTraduction.presentation.dtos;

import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.interfaces.AnswerChoiceTraductionBusiness;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.mappers.AnswerChoiceTraductionDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;

@RestController

@RequestMapping("answerchoicetraduction")
public class AnswerChoiceTraductionController {
    private AnswerChoiceTraductionBusiness answerChoiceTraductionBusiness;
    private AnswerChoiceTraductionDtoMapper answerChoiceTraductionDtoMapper;

    @Autowired

    public AnswerChoiceTraductionController(AnswerChoiceTraductionBusiness answerChoiceTraductionBusiness, AnswerChoiceTraductionDtoMapper answerChoiceTraductionDtoMapper) {
        this.answerChoiceTraductionBusiness = answerChoiceTraductionBusiness;
        this.answerChoiceTraductionDtoMapper = answerChoiceTraductionDtoMapper;
    }

    @RequestMapping(value = "/{lang}/{answer_choice_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAnswerChoiceTraduction(@PathVariable String lang, @PathVariable String answer_choice_id, HttpServletRequest request){
        AnswerChoiceTraduction answerChoiceTraduction = answerChoiceTraductionBusiness.getByLangAndAnswerChoice(lang, answer_choice_id);
        AnswerChoiceTraductionResponseDto answerChoiceTraductionResponseDto = answerChoiceTraductionDtoMapper.businessObjectToResponseDto(answerChoiceTraduction);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(answerChoiceTraductionResponseDto);
        return responseEntity;
    }
}

package com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance;

import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.mappers.AnswerChoiceTraductionEntityMapper;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.interfaces.AnswerChoiceTraductionRepository;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa.AnswerChoiceTraductionDal;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa.AnswerChoiceTraductionEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.NoSuchElementException;

@Component
public class AnswerChoiceTraductionRepositoryImpl extends BaseRepositoryImpl<AnswerChoiceTraductionEntity, AnswerChoiceTraduction> implements AnswerChoiceTraductionRepository {
    private AnswerChoiceTraductionDal dal;
    private AnswerChoiceTraductionEntityMapper mapper;

    @Autowired
    public AnswerChoiceTraductionRepositoryImpl(AnswerChoiceTraductionDal dal, AnswerChoiceTraductionEntityMapper mapper){
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    @Override
    public AnswerChoiceTraduction getByLangAndAnswerChoice(String lang, String answer_choice_id){
        try {
            return mapper.entityToBusinessObject(dal.getByLangAndAnswerChoice(lang, answer_choice_id));
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

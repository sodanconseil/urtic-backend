package com.base.server.answerChoice.traduction.answerChoiceTraduction.mappers;

import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.presentation.dtos.AnswerChoiceTraductionRequestDto;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.presentation.dtos.AnswerChoiceTraductionResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface AnswerChoiceTraductionDtoMapper extends BaseDtoMapper<AnswerChoiceTraduction, AnswerChoiceTraductionRequestDto, AnswerChoiceTraductionResponseDto> {
    AnswerChoiceTraduction requestDtoToBusinessObject(AnswerChoiceTraductionRequestDto dto);
    AnswerChoiceTraductionResponseDto businessObjectToResponseDto(AnswerChoiceTraduction businessObject);
    List<AnswerChoiceTraductionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<AnswerChoiceTraduction> businessObjects);
    Set<AnswerChoiceTraductionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<AnswerChoiceTraduction> businessObjects);
}

package com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AnswerChoiceTraductionDal extends BaseDal<AnswerChoiceTraductionEntity> {

    @Query(value = "SELECT * FROM answer_choice_traduction WHERE language = ?1 and answer_choice_id = ?2", nativeQuery = true)
    public AnswerChoiceTraductionEntity getByLangAndAnswerChoice(String lang, String answer_choice_id);
}

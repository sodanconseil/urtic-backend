package com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.interfaces;

import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import org.springframework.stereotype.Component;

@Component
public interface AnswerChoiceTraductionRepository extends BaseRepository<AnswerChoiceTraduction> {
    public AnswerChoiceTraduction getByLangAndAnswerChoice(String lang, String answer_choice_id);
}

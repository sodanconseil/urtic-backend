package com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa;

import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "answer_choice_traduction")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class AnswerChoiceTraductionEntity
        extends BaseEntity<AnswerChoiceTraductionEntity>
        implements Copyable<AnswerChoiceTraductionEntity> {

    private String answerChoiceId;
    private String language;

    @Column(columnDefinition = "text")
    private String text;

    @Column(columnDefinition = "text")
    private String formattedText;

    @Column(columnDefinition = "text")
    private String info;

    @Column(columnDefinition = "text")
    private String formattedInfo;

    @Column(columnDefinition = "text")
    private String supplementInputText;


    @Builder(setterPrefix = "set")
    public AnswerChoiceTraductionEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String answerChoiceId, String language, String text, String formattedText, String info, String formattedInfo, String supplementInputText) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.answerChoiceId = answerChoiceId;
        this.language = language;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.supplementInputText = supplementInputText;
    }

    @Override
    public void copy(AnswerChoiceTraductionEntity entity) {
        super.copy(entity);
        this.answerChoiceId = answerChoiceId;
        this.language = language;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.supplementInputText = supplementInputText;
    }
}

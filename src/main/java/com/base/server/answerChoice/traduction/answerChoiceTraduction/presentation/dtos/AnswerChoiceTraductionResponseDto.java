package com.base.server.answerChoice.traduction.answerChoiceTraduction.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AnswerChoiceTraductionResponseDto extends BaseResponseDto {

    private String answerChoiceId;
    private String language;

    @Column(columnDefinition = "text")
    private String text;

    @Column(columnDefinition = "text")
    private String formattedText;

    @Column(columnDefinition = "text")
    private String info;

    @Column(columnDefinition = "text")
    private String formattedInfo;

    @Column(columnDefinition = "text")
    private String supplementInputText;



    @Builder(setterPrefix = "set")
    public AnswerChoiceTraductionResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String answerChoiceId, String language, String text, String formattedText, String info, String formattedInfo, String supplementInputText) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.answerChoiceId = answerChoiceId;
        this.language = language;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.supplementInputText = supplementInputText;
    }
}

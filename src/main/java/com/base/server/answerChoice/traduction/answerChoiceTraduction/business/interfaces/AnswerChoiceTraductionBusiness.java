package com.base.server.answerChoice.traduction.answerChoiceTraduction.business.interfaces;

import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.generic.business.interfaces.BaseBusiness;
import org.springframework.stereotype.Component;

@Component
public interface AnswerChoiceTraductionBusiness extends BaseBusiness<AnswerChoiceTraduction> {
    public AnswerChoiceTraduction getByLangAndAnswerChoice(String lang, String answer_choice_id);
}

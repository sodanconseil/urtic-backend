package com.base.server.answerChoice.traduction.answerChoiceTraduction.mappers;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa.AnswerChoiceTraductionEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AnswerChoiceTraductionEntityMapper extends BaseEntityMapper<AnswerChoiceTraductionEntity, AnswerChoiceTraduction> {
    public AnswerChoiceTraduction entityToBusinessObject(AnswerChoiceTraductionEntity entity);
    public AnswerChoiceTraductionEntity businessObjectToEntity(AnswerChoiceTraduction object);
    public List<AnswerChoiceTraduction> listOfEntityToListOfBusinessObjects(List<AnswerChoiceTraductionEntity> entities);
}

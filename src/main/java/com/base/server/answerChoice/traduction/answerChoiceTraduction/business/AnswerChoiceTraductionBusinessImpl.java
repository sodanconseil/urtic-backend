package com.base.server.answerChoice.traduction.answerChoiceTraduction.business;

import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.interfaces.AnswerChoiceTraductionBusiness;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.factory.AnswerChoiceTraductionFactory;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.interfaces.AnswerChoiceTraductionRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AnswerChoiceTraductionBusinessImpl extends BaseBusinessImpl<AnswerChoiceTraduction, AnswerChoiceTraductionRepository> implements AnswerChoiceTraductionBusiness {
    private AnswerChoiceTraductionRepository answerChoiceTraductionRepository;
    private AnswerChoiceTraductionFactory answerChoiceTraductionFactory;

    @Autowired
    public AnswerChoiceTraductionBusinessImpl(AnswerChoiceTraductionRepository answerChoiceTraductionRepository, AnswerChoiceTraductionFactory factory){
        super(answerChoiceTraductionRepository, factory);
        this.answerChoiceTraductionRepository = answerChoiceTraductionRepository;
        this.answerChoiceTraductionFactory = factory;
    }

    @Override
    public AnswerChoiceTraduction getByLangAndAnswerChoice(String lang, String answer_choice_id){
        return answerChoiceTraductionRepository.getByLangAndAnswerChoice(lang, answer_choice_id);
    }
}

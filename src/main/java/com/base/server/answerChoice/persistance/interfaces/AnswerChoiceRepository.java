package com.base.server.answerChoice.persistance.interfaces;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.userAnswer.business.UserAnswer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AnswerChoiceRepository extends BaseRepository<AnswerChoice> {
    public List<AnswerChoice> getAllFromSubQuestionCondition(String subQuestionCondition);
}

package com.base.server.answerChoice.persistance.jpa;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AnswerChoiceDal extends BaseDal<AnswerChoiceEntity> {
    @Query(value = "SELECT * From answer_choice WHERE sub_question_condition = ?1", nativeQuery = true)
    public List<AnswerChoiceEntity> getAllFromSubQuestionCondition(String subQuestionCondition);

    @Query(value = "SELECT * From answer_choice WHERE question_version_id = ?1", nativeQuery = true)
    public List<AnswerChoiceEntity> findAllByQuestionId(String questionId);
}

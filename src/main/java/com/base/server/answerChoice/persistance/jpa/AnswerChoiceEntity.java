package com.base.server.answerChoice.persistance.jpa;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.w3c.dom.Text;

import javax.persistence.*;
import java.sql.Timestamp;

/*
Choices available for single choice or multiple choices question.

Examples are related to @see <a href="https://compass.clinic/wp-content/uploads/2020/05/UAS7-1.pdf">Urticaria Activty Score (UAS7)</a> form.
 */
@Entity
@Table(name = "answer_choice")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class AnswerChoiceEntity extends BaseEntity<AnswerChoiceEntity> implements Copyable<AnswerChoiceEntity> {

    /*
    Code identifying an answer choice amount others. E.g. 1
     */
    private int code;
    /*
    Order in which display the answer choice. E.g. 2.
     */
    private int answer_order;
    /*
    Value of the answer choice. E.g. 1.
     */
    private int value;
    /*
    Text to be displayed. E.g. 1
     */
    @Column(columnDefinition = "text")
    private String text;
    /*
    Text formatted in simple HTML. E.g. <b>1</b>.
     */
    @Column(columnDefinition = "text")
    private String formattedText;
    /*
    Contextual information to be displayed helping the user selecting or not the answer. E.g. Mild: <20 wheals over 24 hours.
     */
    @Column(columnDefinition = "text")
    private String info;
    /*
    Information in simple information. E.g. <b>Mild:</b>&nbsp;<20 wheals over 24 hours.
     */
    @Column(columnDefinition = "text")
    private String formattedInfo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "questionVersion_id")
    private QuestionVersionEntity questionVersionEntity;

//    @OneToOne( mappedBy = "answerChoice")
//    private UserAnswerEntity userAnswerEntity;

    private boolean supplementInput;

    @Column(columnDefinition = "text")
    private String supplementInputText;

    private String subQuestionCondition;

    private boolean endSurvey;



    @Builder(setterPrefix = "set")
    public AnswerChoiceEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, int code, int answer_order, int value, String text, String formattedText, String info, String formattedInfo, QuestionVersionEntity questionVersionEntity, boolean supplementInput, String supplementInputText, String subQuestionCondition, boolean endSurvey) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.code = code;
        this.answer_order = answer_order;
        this.value = value;
        this.text = text;
        this.formattedText = formattedText;
        this.info = info;
        this.formattedInfo = formattedInfo;
        this.questionVersionEntity = questionVersionEntity;
        this.supplementInput = supplementInput;
        this.supplementInputText = supplementInputText;
        this.subQuestionCondition = subQuestionCondition;
        this.endSurvey = endSurvey;
    }


    @Override
    public void copy(AnswerChoiceEntity answerChoiceEntity) {
        super.copy(answerChoiceEntity);
        this.code = answerChoiceEntity.code;
        this.answer_order = answerChoiceEntity.answer_order;
        this.value = answerChoiceEntity.value;
        this.text = answerChoiceEntity.text;
        this.formattedText = answerChoiceEntity.formattedText;
        this.info = answerChoiceEntity.info;
        this.formattedInfo = answerChoiceEntity.formattedInfo;
        this.questionVersionEntity = answerChoiceEntity.questionVersionEntity;
        this.supplementInput = answerChoiceEntity.supplementInput;
        this.supplementInputText = answerChoiceEntity.supplementInputText;
        this.subQuestionCondition = answerChoiceEntity.subQuestionCondition;
        this.endSurvey = answerChoiceEntity.endSurvey;
    }
}

package com.base.server.answerChoice.persistance;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.mappers.AnswerChoiceEntityMapper;
import com.base.server.answerChoice.persistance.interfaces.AnswerChoiceRepository;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceDal;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.mappers.UserQuestionEntityMapper;
import com.base.server.userQuestion.persistance.interfaces.UserQuestionRepository;
import com.base.server.userQuestion.persistance.jpa.UserQuestionDal;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class AnswerChoiceRepositoryImpl extends BaseRepositoryImpl<AnswerChoiceEntity, AnswerChoice> implements AnswerChoiceRepository {
    private AnswerChoiceDal dal;
    private AnswerChoiceEntityMapper mapper;

    @Autowired
    public AnswerChoiceRepositoryImpl(AnswerChoiceDal dal, AnswerChoiceEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    @Override
    public List<AnswerChoice> getAllFromSubQuestionCondition(String subQuestionCondition){
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.getAllFromSubQuestionCondition(subQuestionCondition));
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

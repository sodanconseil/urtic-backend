package com.base.server.surveyVersion.business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.survey.business.Survey;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.survey.factory.SurveyFactory;
import com.base.server.survey.persistance.interfaces.SurveyRepository;
import com.base.server.surveyVersion.business.interfaces.SurveyVersionBusiness;
import com.base.server.surveyVersion.factory.SurveyVersionFactory;
import com.base.server.surveyVersion.persistance.interfaces.SurveyVersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SurveyVersionBusinessImpl extends BaseBusinessImpl<SurveyVersion, SurveyVersionRepository> implements SurveyVersionBusiness {

    private SurveyVersionRepository surveyVersionRepository;
    private SurveyVersionFactory surveyVersionFactory;

    @Autowired
    public SurveyVersionBusinessImpl(SurveyVersionRepository surveyVersionRepository, SurveyVersionFactory factory) {
        super(surveyVersionRepository, factory);
        this.surveyVersionRepository = surveyVersionRepository;
        this.surveyVersionFactory = surveyVersionFactory;
    }

    @Override
    public SurveyVersion getLatestSurveyVersionBySurveyId(String id) {
        return surveyVersionRepository.getLatestSurveyVersionBySurveyId(id);
    }
}

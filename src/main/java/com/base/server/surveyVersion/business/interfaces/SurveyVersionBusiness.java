package com.base.server.surveyVersion.business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.surveyVersion.business.SurveyVersion;

import java.util.List;

public interface SurveyVersionBusiness extends BaseBusiness<SurveyVersion> {
    public SurveyVersion getLatestSurveyVersionBySurveyId(String id);

}

package com.base.server.surveyVersion.business.surveyVersion;

import com.base.server.survey.business.Uas7DailyScoreCommand;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.business.interfaces.UserAnswerBusiness;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.Business.interfaces.UserQuestionBusiness;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.Business.interfaces.UserSurveyBusiness;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class Uas7v1SurveyCommand extends Uas7DailyScoreCommand {
    public Uas7v1SurveyCommand() {
    }

    @Override
    public int execute(UserSurvey userSurvey, UserQuestionBusiness userQuestionBusiness, UserAnswerBusiness userAnswerBusiness) {
        List<UserQuestion> userQuestionList = userQuestionBusiness.getAllUserQuestionFromUserSurvey( userSurvey.getId() );
        List<UserAnswer> userAnswerList = new ArrayList<>();
        for (UserQuestion question: userQuestionList) {
            List<UserAnswer> userAnswerListFromUserQuestion = userAnswerBusiness.getFromUserQuestion( question.getId() );
            if(userAnswerListFromUserQuestion != null){
                for (UserAnswer answer: userAnswerListFromUserQuestion) {
                    userAnswerList.add(answer);
                }
            }
        }

        int uasCalculatedScore = 0;
        for (UserAnswer answer: userAnswerList) {
            uasCalculatedScore = uasCalculatedScore + answer.getIntAnswer();
        }
        return uasCalculatedScore;
    }
}
package com.base.server.surveyVersion.persistance.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.survey.business.Survey;
import com.base.server.surveyVersion.business.SurveyVersion;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SurveyVersionRepository extends BaseRepository<SurveyVersion> {
    public SurveyVersion getLatestSurveyVersionBySurveyId(String id);

}

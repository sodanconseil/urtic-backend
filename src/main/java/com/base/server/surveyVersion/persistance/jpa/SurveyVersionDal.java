package com.base.server.surveyVersion.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.prescription.persistence.jpa.PrescriptionEntity;
import com.base.server.surveyVersion.business.SurveyVersion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface SurveyVersionDal extends BaseDal<SurveyVersionEntity> {
    @Query(value="select * from survey_version sv where sv.survey_id = ?1 ORDER BY date DESC FETCH FIRST 1 ROWS ONLY", nativeQuery = true)
    public Optional<SurveyVersionEntity> getLatestSurveyVersionBySurveyId(String survey_id);
}

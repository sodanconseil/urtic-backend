package com.base.server.surveyVersion.persistance;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.prescription.business.Prescription;
import com.base.server.survey.business.Survey;
import com.base.server.survey.mappers.SurveyEntityMapper;
import com.base.server.survey.persistance.interfaces.SurveyRepository;
import com.base.server.survey.persistance.jpa.SurveyDal;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.mappers.SurveyVersionEntityMapper;
import com.base.server.surveyVersion.persistance.interfaces.SurveyVersionRepository;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionDal;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class SurveyVersionRepositoryImpl extends BaseRepositoryImpl<SurveyVersionEntity, SurveyVersion> implements SurveyVersionRepository {
    private SurveyVersionDal dal;
    private SurveyVersionEntityMapper mapper;

    @Autowired
    public SurveyVersionRepositoryImpl(SurveyVersionDal dal, SurveyVersionEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    public SurveyVersion getLatestSurveyVersionBySurveyId(String id) {
        try {
            return mapper.entityToBusinessObject(dal.getLatestSurveyVersionBySurveyId(id).get());
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

package com.base.server.surveyVersion.persistance.jpa;

import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/*
Version of a survey. The latest version of a survey is the one to be used.
 */
@Entity
@Table(name = "survey_version")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SurveyVersionEntity extends BaseEntity<SurveyVersionEntity> implements Copyable<SurveyVersionEntity> {

    /*
    Survey.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "survey_id")
    private SurveyEntity survey;
    /*
    Version of the survey.
     */
    private int survey_version;
    /*
    Date of the survey version.
     */
    private Timestamp date;
    /*
    Survey questions.
     */
    @OneToMany(mappedBy = "surveyVersionEntity", cascade = {CascadeType.ALL})
    private List<QuestionVersionEntity> questionVersions = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "frequency_id", nullable = false)
    private FrequencyEntity frequency;

    private int delais;

    private String en;

    private String fr;

    private String shortName;


    @Builder(setterPrefix = "set")
    public SurveyVersionEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, SurveyEntity survey, int survey_version, Timestamp date, List<QuestionVersionEntity> questionVersions, FrequencyEntity frequency, int delais, String en, String fr, String shortName) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.survey = survey;
        this.survey_version = survey_version;
        this.date = date;
        this.questionVersions = questionVersions;
        this.frequency = frequency;
        this.delais = delais;
        this.en = en;
        this.fr = fr;
        this.shortName = shortName;
    }



    @Override
    public void copy(SurveyVersionEntity surveyVersionEntity) {
        super.copy(surveyVersionEntity);
        this.survey = surveyVersionEntity.survey;
        this.survey_version = surveyVersionEntity.survey_version;
        this.date = surveyVersionEntity.date;
        this.frequency = surveyVersionEntity.frequency;
        this.delais = surveyVersionEntity.delais;
        this.en = surveyVersionEntity.en;
        this.fr = surveyVersionEntity.fr;
        this.shortName = surveyVersionEntity.shortName;
    }
}

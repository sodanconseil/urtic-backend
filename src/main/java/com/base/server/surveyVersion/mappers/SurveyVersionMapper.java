package com.base.server.surveyVersion.mappers;


import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import com.base.server.surveyVersion.presentation.dtos.SurveyVersionRequestDto;
import com.base.server.surveyVersion.presentation.dtos.SurveyVersionResponseDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SurveyVersionMapper {

    SurveyVersionResponseDto requestDtoToResponseDto(SurveyVersionRequestDto dto);
    List<SurveyVersionResponseDto> listofrequestDtoToResponseDto(List<SurveyVersionRequestDto> dtos);

}

package com.base.server.surveyVersion.mappers;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.survey.business.Survey;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.presentation.dtos.SurveyVersionRequestDto;
import com.base.server.surveyVersion.presentation.dtos.SurveyVersionResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface SurveyVersionDtoMapper extends BaseDtoMapper<SurveyVersion, SurveyVersionRequestDto, SurveyVersionResponseDto> {
    SurveyVersion requestDtoToBusinessObject(SurveyVersionRequestDto dto);
    SurveyVersionResponseDto businessObjectToResponseDto(SurveyVersion businessObject);
    List<SurveyVersionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<SurveyVersion> businessObjects);
    Set<SurveyVersionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<SurveyVersion> businessObjects);
}

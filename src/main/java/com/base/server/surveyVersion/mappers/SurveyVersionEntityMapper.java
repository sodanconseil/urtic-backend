package com.base.server.surveyVersion.mappers;

import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.survey.business.Survey;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SurveyVersionEntityMapper extends BaseEntityMapper<SurveyVersionEntity, SurveyVersion> {
    public SurveyVersion entityToBusinessObject(SurveyVersionEntity entity);
    public SurveyVersionEntity businessObjectToEntity(SurveyVersion object);
    public List<SurveyVersion> listOfEntityToListOfBusinessObjects(List<SurveyVersionEntity> entities);

}

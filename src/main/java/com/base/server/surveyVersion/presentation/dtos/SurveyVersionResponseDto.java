package com.base.server.surveyVersion.presentation.dtos;

import com.base.server.Question.Business.Question;
import com.base.server.frequency.business.Frequency;
import com.base.server.generic.presentation.dtos.BaseResponseDto;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.survey.business.Survey;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.surveyVersion.business.SurveyVersion;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SurveyVersionResponseDto extends BaseResponseDto {

    /*
    Survey.
     */
    private Survey survey;
    /*
    Version of the survey.
     */
    private int survey_version;
    /*
    Date of the survey version.
     */
    private Timestamp date;
    /*
    Survey questions.
     */
    private List<QuestionVersion> questionVersions = new ArrayList<>();

    private Frequency frequency;

    private int delais;

    private String en;

    private String fr;

    private String shortName;


    @Builder(setterPrefix = "set")
    public SurveyVersionResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Survey survey, int survey_version, Timestamp date, List<QuestionVersion> questionVersions, Frequency frequency, int delais, String en, String fr, String shortName) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.survey = survey;
        this.survey_version = survey_version;
        this.date = date;
        this.questionVersions = questionVersions;
        this.frequency = frequency;
        this.delais = delais;
        this.en = en;
        this.fr = fr;
        this.shortName = shortName;
    }
}

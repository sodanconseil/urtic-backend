package com.base.server.userAnswer.mappers;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.dtos.UserAnswerRequestDto;
import com.base.server.userAnswer.dtos.UserAnswerResponseDto;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.presentation.dtos.UserQuestionRequestDto;
import com.base.server.userQuestion.presentation.dtos.UserQuestionResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface UserAnswerDtoMapper extends BaseDtoMapper<UserAnswer, UserAnswerRequestDto, UserAnswerResponseDto> {
    UserAnswer requestDtoToBusinessObject(UserAnswerRequestDto dto);
    UserAnswerResponseDto businessObjectToResponseDto(UserAnswer businessObject);
    List<UserAnswerResponseDto> listOfBusinessObjectsToListOfResponseDto(List<UserAnswer> businessObjects);
    Set<UserAnswerResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<UserAnswer> businessObjects);
}

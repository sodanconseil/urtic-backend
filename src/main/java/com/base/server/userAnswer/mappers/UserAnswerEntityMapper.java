package com.base.server.userAnswer.mappers;

import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserAnswerEntityMapper extends BaseEntityMapper<UserAnswerEntity, UserAnswer> {
    public UserAnswer entityToBusinessObject(UserAnswerEntity entity);
    public UserAnswerEntity businessObjectToEntity(UserAnswer object);
    public List<UserAnswer> listOfEntityToListOfBusinessObjects(List<UserAnswerEntity> entities);

}

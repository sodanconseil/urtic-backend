package com.base.server.userAnswer.business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.user.business.User;
import com.base.server.userAnswer.business.UserAnswer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserAnswerBusiness  extends BaseBusiness<UserAnswer> {
    public List<UserAnswer> getFromUserQuestion(String id);
}

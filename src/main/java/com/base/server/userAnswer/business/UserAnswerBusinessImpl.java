package com.base.server.userAnswer.business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.userAnswer.business.interfaces.UserAnswerBusiness;
import com.base.server.userAnswer.factory.UserAnswerFactory;
import com.base.server.userAnswer.persistance.interfaces.UserAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserAnswerBusinessImpl extends BaseBusinessImpl<UserAnswer, UserAnswerRepository> implements UserAnswerBusiness {

    private UserAnswerRepository userAnswerRepository;
    private UserAnswerFactory userAnswerFactory;

    @Autowired
    public UserAnswerBusinessImpl(UserAnswerRepository userAnswerRepository, UserAnswerFactory factory) {
        super(userAnswerRepository, factory);
        this.userAnswerRepository = userAnswerRepository;
        this.userAnswerFactory = userAnswerFactory;
    }

    @Override
    public List<UserAnswer> getFromUserQuestion(String id) {
        return userAnswerRepository.findFromUserQuestion(id);
    }
}

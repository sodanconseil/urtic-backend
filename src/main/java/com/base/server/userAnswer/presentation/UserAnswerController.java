package com.base.server.userAnswer.presentation;

import com.base.server.SubQuestionCondition.business.SubQuestionCondition;
import com.base.server.SubQuestionCondition.business.interfaces.SubQuestionConditionBusiness;
import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.business.interfaces.AnswerChoiceBusiness;
import com.base.server.answerChoice.dtos.AnswerChoiceRequestDto;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.questionType.Business.interfaces.QuestionTypeBusiness;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.Business.interfaces.QuestionVersionBusiness;
import com.base.server.survey.business.Survey;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.business.interfaces.SurveyVersionBusiness;
import com.base.server.user.persistence.jpa.UserEntity;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.business.interfaces.UserAnswerBusiness;
import com.base.server.userAnswer.dtos.UserAnswerRequestDto;
import com.base.server.userAnswer.dtos.UserAnswerResponseDto;
import com.base.server.userAnswer.mappers.UserAnswerDtoMapper;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.Business.interfaces.UserQuestionBusiness;
import com.base.server.userQuestion.mappers.UserQuestionDtoMapper;
import com.base.server.userQuestion.presentation.dtos.UserQuestionResponseDto;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.Business.interfaces.UserSurveyBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/useranswer")
public class UserAnswerController {

    private UserAnswerDtoMapper userAnswerDtoMapper;
    private UserAnswerBusiness userAnswerBusiness;

    private AnswerChoiceBusiness answerChoiceBusiness;

    private QuestionVersionBusiness questionVersionBusiness;
    private QuestionTypeBusiness questionTypeBusiness;

    private UserQuestionBusiness userQuestionBusiness;
    private UserQuestionDtoMapper userQuestionDtoMapper;

    private UserSurveyBusiness userSurveyBusiness;

    private SurveyVersionBusiness surveyVersionBusiness;

    private SurveyBusiness surveyBusiness;

    private SubQuestionConditionBusiness subQuestionConditionBusiness;


    @Autowired
    public UserAnswerController(UserAnswerDtoMapper userAnswerDtoMapper, UserAnswerBusiness userAnswerBusiness, AnswerChoiceBusiness answerChoiceBusiness, QuestionVersionBusiness questionVersionBusiness, QuestionTypeBusiness questionTypeBusiness, UserQuestionBusiness userQuestionBusiness, UserQuestionDtoMapper userQuestionDtoMapper, UserSurveyBusiness userSurveyBusiness, SurveyVersionBusiness surveyVersionBusiness, SurveyBusiness surveyBusiness, SubQuestionConditionBusiness subQuestionConditionBusiness) {
        this.userAnswerDtoMapper = userAnswerDtoMapper;
        this.userAnswerBusiness = userAnswerBusiness;
        this.answerChoiceBusiness = answerChoiceBusiness;
        this.questionVersionBusiness = questionVersionBusiness;
        this.questionTypeBusiness = questionTypeBusiness;
        this.userQuestionBusiness = userQuestionBusiness;
        this.userQuestionDtoMapper = userQuestionDtoMapper;
        this.userSurveyBusiness = userSurveyBusiness;
        this.surveyVersionBusiness = surveyVersionBusiness;
        this.surveyBusiness = surveyBusiness;
        this.subQuestionConditionBusiness = subQuestionConditionBusiness;
    }



    @RequestMapping(value = "/all/{userQuestionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllAnswerByUserQuestionId(@PathVariable String userQuestionId, HttpServletRequest request){
        List<UserAnswer> userAnswerList = userAnswerBusiness.getFromUserQuestion(userQuestionId);
        List<UserAnswerResponseDto> userAnswerResponseDtoList = userAnswerDtoMapper.listOfBusinessObjectsToListOfResponseDto(userAnswerList);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(userAnswerResponseDtoList);
        return responseEntity;
    }

    @RequestMapping(value = "/{userAnswerId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getOneById(@PathVariable String userAnswerId, HttpServletRequest request){
        UserAnswer userAnswer = userAnswerBusiness.get(userAnswerId);
        UserAnswerResponseDto userAnswerResponseDto = userAnswerDtoMapper.businessObjectToResponseDto(userAnswer);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(userAnswerResponseDto);
        return responseEntity;
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> editUserAnswer(@PathVariable String id, @Valid @RequestBody List<UserAnswerRequestDto> dtos,
                                                 Errors errors, HttpServletRequest request) {
        //Delete old answer
        List<UserAnswer> allAnswerFromUserQuestion = userAnswerBusiness.getFromUserQuestion(dtos.get(0).getUserQuestion().getId());
        for (UserAnswer existingAnswer: allAnswerFromUserQuestion ) {
            userAnswerBusiness.delete(existingAnswer.getId());
        }

        //Creating the new answers
        UserAnswerResponseDto newUserAnswerResponse = null;
        for (UserAnswerRequestDto dto: dtos) {
            UserQuestion userQuestion = userQuestionBusiness.get(dto.getUserQuestion().getId());
            dto.setUserQuestion(userQuestion);

            //Delete subQuestionAnswer if condition is not met
            SubQuestionCondition currentSubQuestionCondition = subQuestionConditionBusiness.getByQuestionVersion(userQuestion.getQuestionAnswered());
            QuestionVersion currentQuestionVersion = questionVersionBusiness.get(userQuestion.getQuestionAnswered());

            if(dto.isSubConditionIsMet() == false && currentSubQuestionCondition != null){

                UserSurvey currentUserSurvey = userSurveyBusiness.get(userQuestion.getUser_survey_Id());
                List<UserQuestion> userQuestionList = userQuestionBusiness.getAllUserQuestionFromUserSurvey(currentUserSurvey.getId());
                UserQuestion currentUserSubQuestion = userQuestionList.stream().filter(uq -> uq.getQuestionAnswered().equals(currentSubQuestionCondition.getSubQuestionVersion()))
                        .findFirst().orElse(null);

                if(currentUserSubQuestion != null){
                    List<UserAnswer> allSubQuestionAnswers = userAnswerBusiness.getFromUserQuestion(currentUserSubQuestion.getId());
                    for (UserAnswer existingAnswer: allSubQuestionAnswers ) {
                        userAnswerBusiness.delete(existingAnswer.getId());
                    }
                    userQuestionBusiness.delete(currentUserSubQuestion.getId());
                }
            }

            UserAnswer unsavedUserAnswer = userAnswerDtoMapper.requestDtoToBusinessObject(dto);
            UserAnswer newAnswer = userAnswerBusiness.save( unsavedUserAnswer, "userId" );
            newUserAnswerResponse  = userAnswerDtoMapper.businessObjectToResponseDto(newAnswer);
        }

        //Checking if survey is UAS7
        UserSurvey userSurvey = userSurveyBusiness.get( dtos.get(0).getUserQuestion().getUser_survey_Id() );
        SurveyVersion surveyVersion = surveyVersionBusiness.get( userSurvey.getSurvey_version_Id() );
        Survey survey = surveyBusiness.get( surveyVersion.getSurvey().getId() );

        if( survey.getShortName().equals("UAS7")){
            List<QuestionType> allQuestionType = questionTypeBusiness.getAll();
            QuestionType questionTypeCalculalted = allQuestionType.stream()
                    .filter(qt -> qt.getTypeCode() == 0)
                    .findFirst().get();

            QuestionVersion calculatedQuestion = surveyVersion.getQuestionVersions().stream()
                .filter(qv -> qv.getQuestionType().getId() == questionTypeCalculalted.getId())
                .findFirst().get();

            List<UserQuestion> allUserQuestionFromUserSurvey = userQuestionBusiness.getAllUserQuestionFromUserSurvey( dtos.get(0).getUserQuestion().getUser_survey_Id() );

            UserQuestion calculatedUserQuestion = allUserQuestionFromUserSurvey.stream()
                    .filter(q -> q.getQuestionAnswered().equals(calculatedQuestion.getId()))
                    .findFirst().get();

            List<UserAnswer> calculatedUserAnswer = userAnswerBusiness.getFromUserQuestion( calculatedUserQuestion.getId() );

            //Recalculate the uas score
            userAnswerBusiness.delete( calculatedUserAnswer.get(0).getId() );

            int uasScoreValue =  userSurvey.calcul_ScoreValue(questionTypeCalculalted, userSurvey, userQuestionBusiness, userAnswerBusiness, 0);

            UserAnswerRequestDto userAnswerRequestDto = UserAnswerRequestDto.builder()
                    .setName("")
                    .setIntAnswer( uasScoreValue )
                    .setDateAnswer(Timestamp.from(Instant.now()))
                    .setUserQuestion( calculatedUserQuestion )
                    .build();
            UserAnswer unsavedNewCalculatedAnswer = userAnswerDtoMapper.requestDtoToBusinessObject(userAnswerRequestDto);
            userAnswerBusiness.save( unsavedNewCalculatedAnswer, "userId" );
        }

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(newUserAnswerResponse);

        return responseEntity;
    }
}

package com.base.server.userAnswer.dtos;

import com.base.server.generic.presentation.dtos.BaseRequestDto;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserAnswerRequestDto extends BaseRequestDto {

    private String answerChoice_id;

    private int intAnswer;

    private int intMinAnswer;

    private int intMaxAnswer;

    private float floatAnswer;

    private float floatMinAnswer;

    private float floatMaxAnswer;

    private String stringAnswer;

    private String supplementInputAnswer;

    private Timestamp dateAnswer;

    private Timestamp startDateAnswer;

    private Timestamp endDateAnswer;

    private UserQuestion userQuestion;

    private boolean subConditionIsMet;

    @Builder(setterPrefix = "set")
    public UserAnswerRequestDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String answerChoice_id, int intAnswer, int intMinAnswer, int intMaxAnswer, float floatAnswer, float floatMinAnswer, float floatMaxAnswer, String stringAnswer, String supplementInputAnswer, Timestamp dateAnswer, Timestamp startDateAnswer, Timestamp endDateAnswer, UserQuestion userQuestion, boolean subConditionIsMet) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.answerChoice_id = answerChoice_id;
        this.intAnswer = intAnswer;
        this.intMinAnswer = intMinAnswer;
        this.intMaxAnswer = intMaxAnswer;
        this.floatAnswer = floatAnswer;
        this.floatMinAnswer = floatMinAnswer;
        this.floatMaxAnswer = floatMaxAnswer;
        this.stringAnswer = stringAnswer;
        this.supplementInputAnswer = supplementInputAnswer;
        this.dateAnswer = dateAnswer;
        this.startDateAnswer = startDateAnswer;
        this.endDateAnswer = endDateAnswer;
        this.userQuestion = userQuestion;
        this.subConditionIsMet = subConditionIsMet;
    }
}

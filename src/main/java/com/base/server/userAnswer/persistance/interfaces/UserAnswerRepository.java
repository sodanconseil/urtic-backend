package com.base.server.userAnswer.persistance.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.survey.business.Survey;
import com.base.server.userAnswer.business.UserAnswer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserAnswerRepository extends BaseRepository<UserAnswer> {
    List<UserAnswer> findFromUserQuestion(String id);
}

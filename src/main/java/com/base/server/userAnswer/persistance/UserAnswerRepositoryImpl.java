package com.base.server.userAnswer.persistance;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.mappers.UserAnswerEntityMapper;
import com.base.server.userAnswer.persistance.interfaces.UserAnswerRepository;
import com.base.server.userAnswer.persistance.jpa.UserAnswerDal;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.mappers.UserQuestionEntityMapper;
import com.base.server.userQuestion.persistance.interfaces.UserQuestionRepository;
import com.base.server.userQuestion.persistance.jpa.UserQuestionDal;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class UserAnswerRepositoryImpl extends BaseRepositoryImpl<UserAnswerEntity, UserAnswer> implements UserAnswerRepository {
    private UserAnswerDal dal;
    private UserAnswerEntityMapper mapper;

    @Autowired
    public UserAnswerRepositoryImpl(UserAnswerDal dal, UserAnswerEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    public List<UserAnswer> findFromUserQuestion(String user_question_id){
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.findAllByUserQuestion(user_question_id).get());
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

package com.base.server.userAnswer.persistance.jpa;

import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/*
Answer provided by an user to a survey question. The question type defined the properties used to store the answer.
 */
@Entity
@Table(name = "user_answer")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class UserAnswerEntity extends BaseEntity<UserAnswerEntity> implements Copyable<UserAnswerEntity> {

    private String answerChoice_id;

    private int intAnswer;

    private int intMinAnswer;

    private int intMaxAnswer;

    private float floatAnswer;

    private float floatMinAnswer;

    private float floatMaxAnswer;

    private String stringAnswer;

    private String supplementInputAnswer;

    private Timestamp dateAnswer;

    private Timestamp startDateAnswer;

    private Timestamp endDateAnswer;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "userQuestion_id")
    private UserQuestionEntity userQuestion;



    @Builder(setterPrefix = "set")
    public UserAnswerEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String answerChoice_id, int intAnswer, int intMinAnswer, int intMaxAnswer, float floatAnswer, float floatMinAnswer, float floatMaxAnswer, String stringAnswer, String supplementInputAnswer, Timestamp dateAnswer, Timestamp startDateAnswer, Timestamp endDateAnswer, UserQuestionEntity userQuestion) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.answerChoice_id = answerChoice_id;
        this.intAnswer = intAnswer;
        this.intMinAnswer = intMinAnswer;
        this.intMaxAnswer = intMaxAnswer;
        this.floatAnswer = floatAnswer;
        this.floatMinAnswer = floatMinAnswer;
        this.floatMaxAnswer = floatMaxAnswer;
        this.stringAnswer = stringAnswer;
        this.supplementInputAnswer = supplementInputAnswer;
        this.dateAnswer = dateAnswer;
        this.startDateAnswer = startDateAnswer;
        this.endDateAnswer = endDateAnswer;
        this.userQuestion = userQuestion;
    }



    @Override
    public void copy(UserAnswerEntity userAnswerEntity) {
        super.copy(userAnswerEntity);
        this.answerChoice_id = userAnswerEntity.answerChoice_id;
        this.intAnswer = userAnswerEntity.intAnswer;
        this.intMinAnswer = userAnswerEntity.intMinAnswer;
        this.intMaxAnswer = userAnswerEntity.intMaxAnswer;
        this.floatAnswer = userAnswerEntity.floatAnswer;
        this.floatMinAnswer = userAnswerEntity.floatMinAnswer;
        this.floatMaxAnswer = userAnswerEntity.floatMaxAnswer;
        this.stringAnswer = userAnswerEntity.stringAnswer;
        this.dateAnswer = userAnswerEntity.dateAnswer;
        this.startDateAnswer = userAnswerEntity.startDateAnswer;
        this.endDateAnswer = userAnswerEntity.endDateAnswer;
        this.userQuestion = userAnswerEntity.userQuestion;
        this.supplementInputAnswer = userAnswerEntity.supplementInputAnswer;
    }
}

package com.base.server.userAnswer.persistance.jpa;


import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public interface UserAnswerDal  extends BaseDal<UserAnswerEntity> {
    @Query(value = "SELECT * FROM public.user_answer WHERE user_question_id = ?1", nativeQuery = true)
    public Optional<List<UserAnswerEntity>> findAllByUserQuestion(String user_question_id);
}

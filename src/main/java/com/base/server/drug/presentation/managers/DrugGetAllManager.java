package com.base.server.drug.presentation.managers;

import java.util.List;
import java.util.Optional;

import com.base.server.drug.business.Drug;
import com.base.server.drug.business.interfaces.DrugBusiness;
import com.base.server.drug.mappers.DrugDtoMapper;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.drug.presentation.dtos.DrugResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DrugGetAllManager implements RequestManager<DrugRequestDto> {

    private DrugBusiness drugBusiness;
    private DrugDtoMapper drugDtoMapper;

    @Autowired
    public DrugGetAllManager(DrugBusiness drugBusiness, DrugDtoMapper drugDtoMapper) {
        this.drugBusiness = drugBusiness;
        this.drugDtoMapper = drugDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DrugRequestDto> requestInformation) {
        List<Drug> drugs = drugBusiness.getAll();
        List<DrugResponseDto> drugResponseDto = drugDtoMapper.listOfBusinessObjectsToListOfResponseDto(drugs);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(drugResponseDto);

        return Optional.of(responseEntity);
    }
}

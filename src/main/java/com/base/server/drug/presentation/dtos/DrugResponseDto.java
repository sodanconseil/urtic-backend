package com.base.server.drug.presentation.dtos;

import com.base.server.dosage.business.Dosage;
import com.base.server.generic.presentation.dtos.BaseResponseDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DrugResponseDto extends BaseResponseDto {

    private String commercial_name;
    private List<Dosage> dosages;

    @Builder(setterPrefix = "set")
    public DrugResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate,
                           String commercial_name, List<Dosage> dosages) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.commercial_name = commercial_name;
        this.dosages = dosages;
    }
}

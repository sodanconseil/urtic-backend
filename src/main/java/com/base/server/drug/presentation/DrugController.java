package com.base.server.drug.presentation;

import com.base.server.drug.business.Drug;
import com.base.server.drug.factory.DrugFactory;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.drug.presentation.managers.*;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.user.factory.UserFactory;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.managers.*;
import com.base.server.user.presentation.managers.ethnicity.UserDeleteEthnicityManager;
import com.base.server.user.presentation.managers.ethnicity.UserGetAllEthnicitiesManager;
import com.base.server.user.presentation.managers.ethnicity.UserPutEthnicityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/drugs")
public class DrugController {

    private DrugFactory drugFactory;

    private ValidationManager<DrugRequestDto> drugValidationManager;
    private DrugExistenceManager drugExistenceManager;
    private DrugEtagManager drugEtagManager;
    private DrugPostManager drugPostManager;
    private DrugGetAllManager drugGetAllManager;
    private DrugGetByIdManager drugGetByIdManager;
    private DrugPutManager drugPutManager;
    private DrugDeleteManager drugDeleteManager;

  //  private DrugGetAllEthnicitiesManager drugGetAllEthnicitiesManager;
   // private DrugPutEthnicityManager drugPutEthnicityManager;
  //  private DrugDeleteEthnicityManager drugDeleteEthnicityManager;
    @Autowired

    public DrugController(DrugFactory drugFactory, ValidationManager<DrugRequestDto> drugValidationManager, DrugExistenceManager drugExistenceManager, DrugEtagManager drugEtagManager, DrugPostManager drugPostManager, DrugGetAllManager drugGetAllManager, DrugGetByIdManager drugGetByIdManager, DrugPutManager drugPutManager, DrugDeleteManager drugDeleteManager) {
        this.drugFactory = drugFactory;
        this.drugValidationManager = drugValidationManager;
        this.drugExistenceManager = drugExistenceManager;
        this.drugEtagManager = drugEtagManager;
        this.drugPostManager = drugPostManager;
        this.drugGetAllManager = drugGetAllManager;
        this.drugGetByIdManager = drugGetByIdManager;
        this.drugPutManager = drugPutManager;
        this.drugDeleteManager = drugDeleteManager;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        RequestInformation<DrugRequestDto> requestInformation = drugFactory.newRequestInformation();
        return drugFactory.newRequestPipeline(drugGetAllManager).manage(requestInformation);
    }

    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getDrug(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<DrugRequestDto> requestInformation = drugFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");
        return drugFactory.newRequestPipeline(drugExistenceManager)
                .addManager(drugGetByIdManager)
                .manage(requestInformation);
    }





}

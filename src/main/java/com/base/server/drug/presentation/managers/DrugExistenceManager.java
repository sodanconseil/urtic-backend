package com.base.server.drug.presentation.managers;

import com.base.server.drug.business.interfaces.DrugBusiness;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DrugExistenceManager implements RequestManager<DrugRequestDto> {

    private DrugBusiness drugBusiness;
    private ErrorMapper errorMapper;

    @Autowired
    public DrugExistenceManager(DrugBusiness drugBusiness, ErrorMapper errorMapper) {
        this.drugBusiness = drugBusiness;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DrugRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        if (drugBusiness.get(requestInformation.getId()) == null) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The drug with the specified id wasn't found";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }
}

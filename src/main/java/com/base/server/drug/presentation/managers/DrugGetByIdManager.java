package com.base.server.drug.presentation.managers;

import java.util.Optional;

import com.base.server.drug.business.Drug;
import com.base.server.drug.business.interfaces.DrugBusiness;
import com.base.server.drug.mappers.DrugDtoMapper;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.drug.presentation.dtos.DrugResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DrugGetByIdManager implements RequestManager<DrugRequestDto> {

    private DrugBusiness drugBusiness;
    private DrugDtoMapper drugDtoMapper;

    @Autowired
    public DrugGetByIdManager(DrugBusiness drugBusiness, DrugDtoMapper drugDtoMapper) {
        this.drugBusiness = drugBusiness;
        this.drugDtoMapper = drugDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DrugRequestDto> requestInformation) {
        Drug drug = drugBusiness.get(requestInformation.getId());
        DrugResponseDto drugResponseDto = drugDtoMapper.businessObjectToResponseDto(drug);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(drugDtoMapper.getEtag(drug))
            .body(drugResponseDto);

        return Optional.of(responseEntity);
    }
}

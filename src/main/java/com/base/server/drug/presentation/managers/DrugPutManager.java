package com.base.server.drug.presentation.managers;

import com.base.server.drug.business.Drug;
import com.base.server.drug.business.interfaces.DrugBusiness;
import com.base.server.drug.mappers.DrugDtoMapper;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.drug.presentation.dtos.DrugResponseDto;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DrugPutManager implements RequestManager<DrugRequestDto> {

    private DrugBusiness drugBusiness;
    private DrugDtoMapper drugDtoMapper;

    @Autowired
    public DrugPutManager(DrugBusiness drugBusiness, DrugDtoMapper drugDtoMapper) {
        this.drugBusiness = drugBusiness;
        this.drugDtoMapper = drugDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DrugRequestDto> requestInformation) {

        String id = requestInformation.getId();
        String userId = requestInformation.getUserId();
        DrugRequestDto drugRequestDto = requestInformation.getDto();
        Drug drug = drugBusiness.modify(id, drugDtoMapper.requestDtoToBusinessObject(drugRequestDto), userId);
        DrugResponseDto drugResponseDto = drugDtoMapper.businessObjectToResponseDto(drug);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(drugDtoMapper.getEtag(drug))
            .body(drugResponseDto);

        return Optional.of(responseEntity);
    }
}

package com.base.server.drug.presentation.managers;

import java.util.Optional;

import com.base.server.drug.business.interfaces.DrugBusiness;
import com.base.server.drug.mappers.DrugDtoMapper;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DrugEtagManager implements RequestManager<DrugRequestDto> {

    private DrugBusiness drugBusiness;
    private DrugDtoMapper drugDtoMapper;
    private ErrorMapper errorMapper;

    
    @Autowired
    public DrugEtagManager(DrugBusiness drugBusiness, DrugDtoMapper drugDtoMapper, ErrorMapper errorMapper) {
        this.drugBusiness = drugBusiness;
        this.drugDtoMapper = drugDtoMapper;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<DrugRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        String receivedEtag = requestInformation.getIf_match();
        String currentEtag = drugDtoMapper.getEtag(drugBusiness.get(requestInformation.getId()));

        if (!receivedEtag.equals(currentEtag)) {
            HttpStatus status = HttpStatus.PRECONDITION_FAILED;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity was changed, GET the new version";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }    
}

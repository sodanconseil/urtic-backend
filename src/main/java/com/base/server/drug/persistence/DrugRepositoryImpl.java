package com.base.server.drug.persistence;

import com.base.server.drug.business.Drug;
import com.base.server.drug.mappers.DrugEntityMapper;
import com.base.server.drug.persistence.interfaces.DrugRepository;
import com.base.server.drug.persistence.jpa.DrugDal;
import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DrugRepositoryImpl extends BaseRepositoryImpl<DrugEntity, Drug> implements DrugRepository {
    private DrugDal dal;
    private DrugEntityMapper mapper;

    @Autowired
    public DrugRepositoryImpl(DrugDal dal, DrugEntityMapper mapper){
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }
}

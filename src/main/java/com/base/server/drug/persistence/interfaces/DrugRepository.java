package com.base.server.drug.persistence.interfaces;

import com.base.server.drug.business.Drug;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.user.business.User;
import org.springframework.stereotype.Component;

@Component
public interface DrugRepository extends BaseRepository<Drug> {
}

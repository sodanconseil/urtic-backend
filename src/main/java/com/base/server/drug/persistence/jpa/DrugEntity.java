package com.base.server.drug.persistence.jpa;


import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Vector;

@Entity
@Table(name = "drug")
@Data
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class DrugEntity extends BaseEntity<DrugEntity> implements Copyable<DrugEntity> {

    // Mis en ManyToMany mais les medicaments ne partagent pas les dosages dans les faits, ils vont avoir chacun
    // le leur
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.ALL
            })
    @JoinTable(name = "drug_dosage",
            joinColumns = { @JoinColumn(name = "drug_id", referencedColumnName = "id")},
            inverseJoinColumns = { @JoinColumn(name = "dosage_id", referencedColumnName = "id") })
    private List<DosageEntity> dosages;
    private String commercial_name;

    @Builder(setterPrefix = "set")
    public DrugEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version,
                      List<DosageEntity> dosageEntities, String commercial_name) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        //this.dosages = dosageEntities;
        this.commercial_name = commercial_name;
        this.dosages = dosageEntities == null? new Vector<DosageEntity>() : dosageEntities;
    }

    @Override
    public void copy(DrugEntity drugEntity) {
        super.copy(drugEntity);
        this.dosages = drugEntity.dosages;
        this.commercial_name = drugEntity.commercial_name;
    }

    @Override
    public String toString() {
        return commercial_name;
    }
}

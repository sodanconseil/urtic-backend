package com.base.server.drug.persistence.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.stereotype.Component;


@Component
public interface DrugDal extends BaseDal<DrugEntity> {
}

package com.base.server.drug.business.interfaces;

import com.base.server.drug.business.Drug;
import com.base.server.generic.business.interfaces.BaseBusiness;

public interface DrugBusiness  extends BaseBusiness<Drug> {
}

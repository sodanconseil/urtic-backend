package com.base.server.drug.business;

import com.base.server.dosage.business.Dosage;
import com.base.server.generic.business.Base;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Drug extends Base {

    private String commercial_name;
    private List<Dosage> dosages;

    @Builder(setterPrefix = "set")
    public Drug(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate,
                String commercial_name, List<Dosage> dosages) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.commercial_name = commercial_name;
        this.dosages = dosages;
    }
}

package com.base.server.drug.business;

import com.base.server.drug.business.interfaces.DrugBusiness;
import com.base.server.drug.factory.DrugFactory;
import com.base.server.drug.persistence.interfaces.DrugRepository;
import com.base.server.generic.business.BaseBusinessImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DrugBusinessImpl extends BaseBusinessImpl<Drug, DrugRepository> implements DrugBusiness {
    private DrugRepository drugRepository;
    private DrugFactory drugFactory;

    @Autowired
    public DrugBusinessImpl(DrugRepository drugRepository, DrugFactory factory) {
        super(drugRepository, factory);
        this.drugRepository = drugRepository;
        this.drugFactory = drugFactory;
    }
}

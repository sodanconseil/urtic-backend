package com.base.server.drug.mappers;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.presentation.dtos.DosageRequestDto;
import com.base.server.dosage.presentation.dtos.DosageResponseDto;
import com.base.server.drug.business.Drug;
import com.base.server.drug.presentation.dtos.DrugRequestDto;
import com.base.server.drug.presentation.dtos.DrugResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface DrugDtoMapper extends BaseDtoMapper<Drug, DrugRequestDto, DrugResponseDto> {
    Drug requestDtoToBusinessObject(DrugRequestDto dto);
    DrugResponseDto businessObjectToResponseDto(Drug businessObject);
    List<DrugResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Drug> businessObjects);
    Set<DrugResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<Drug> businessObjects);
}

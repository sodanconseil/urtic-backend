package com.base.server.drug.mappers;

import com.base.server.drug.business.Drug;
import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DrugEntityMapper extends BaseEntityMapper<DrugEntity, Drug> {
    public Drug entityToBusinessObject(DrugEntity entity);
    public DrugEntity businessObjectToEntity(Drug object);
    public List<Drug> listOfEntityToListOfBusinessObjects(List<DrugEntity> entities);
}

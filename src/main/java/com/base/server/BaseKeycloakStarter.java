package com.base.server;

import com.base.server.config.model.Cors;
import com.base.server.generic.presentation.UserServiceInterceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@EntityScan(basePackageClasses = { BaseKeycloakStarter.class })
public class BaseKeycloakStarter extends SpringBootServletInitializer implements WebMvcConfigurer {

	@Autowired
	private UserServiceInterceptor userServiceInterceptor;

	@Autowired
	private Cors cors;

	public static void main(String[] args) {
		SpringApplication.run(BaseKeycloakStarter.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BaseKeycloakStarter.class);
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(userServiceInterceptor);
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping(cors.getMapping())
				.allowedMethods(cors.getAllowedMethods().toArray(new String[cors.getAllowedMethods().size()]))
				.exposedHeaders(cors.getExposedHeaders().toArray(new String[cors.getExposedHeaders().size()]));
	}

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
}

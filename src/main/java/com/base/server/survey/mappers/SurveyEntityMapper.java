package com.base.server.survey.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.survey.business.Survey;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SurveyEntityMapper extends BaseEntityMapper<SurveyEntity, Survey> {
    public Survey entityToBusinessObject(SurveyEntity entity);
    public SurveyEntity businessObjectToEntity(Survey object);
    public List<Survey> listOfEntityToListOfBusinessObjects(List<SurveyEntity> entities);

}

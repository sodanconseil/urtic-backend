package com.base.server.survey.mappers;


import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SurveyMapper {

    SurveyResponseDto requestDtoToResponseDto(SurveyRequestDto dto);
    List<SurveyResponseDto> listofrequestDtoToResponseDto(List<SurveyRequestDto> dtos);

}

package com.base.server.survey.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.survey.business.Survey;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface SurveyDtoMapper extends BaseDtoMapper<Survey, SurveyRequestDto, SurveyResponseDto> {
    Survey requestDtoToBusinessObject(SurveyRequestDto dto);
    SurveyRequestDto surveyToSurveyRequestDto(Survey survey);
    List<SurveyRequestDto> listofsurveyTolistofSurveyRequestDto(List<Survey> survey);
    SurveyResponseDto requestDtoToResponseDto(SurveyRequestDto dto);
    List<SurveyResponseDto> listofrequestDtoToResponseDto(List<SurveyRequestDto> dtos);
    SurveyResponseDto businessObjectToResponseDto(Survey businessObject);
    List<SurveyResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Survey> businessObjects);
    Set<SurveyResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<Survey> businessObjects);
}

package com.base.server.survey.presentation.dtos;

import com.base.server.Question.Business.Question;
import com.base.server.frequency.business.Frequency;
import com.base.server.generic.presentation.dtos.BaseRequestDto;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.surveyVersion.business.SurveyVersion;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SurveyRequestDto extends BaseRequestDto {

    private String shortName;


    private Set<Question> questions;

    private Frequency frequency;

    private int delais;

    @Builder(setterPrefix = "set")
    public SurveyRequestDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String shortName, Set<Question> questions, Frequency frequency, int delais) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.shortName = shortName;
        this.questions = questions;
        this.frequency = frequency;
        this.delais = delais;
    }
}

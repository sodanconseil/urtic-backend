package com.base.server.survey.presentation;

import com.base.server.Question.presentation.managers.QuestionPostManager;
import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.AnswerChoiceTraduction;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.business.interfaces.AnswerChoiceTraductionBusiness;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.QuestionVersionTraduction;
import com.base.server.questionVersion.traduction.questionVersionTraduction.Business.interfaces.QuestionVersionTraductionBusiness;
import com.base.server.survey.business.Survey;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.survey.factory.SurveyFactory;
import com.base.server.survey.mappers.SurveyDtoMapper;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import com.base.server.survey.presentation.managers.SurveyGetAllManager;
import com.base.server.survey.presentation.managers.SurveyPostManager;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.business.interfaces.SurveyVersionBusiness;
import com.base.server.surveyVersion.mappers.SurveyVersionDtoMapper;
import com.base.server.surveyVersion.presentation.dtos.SurveyVersionResponseDto;
import com.base.server.user.business.User;
import com.base.server.user.presentation.AuthentifiedUserController;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;
import io.swagger.v3.core.util.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/survey")
public class SurveyController {

    private SurveyFactory surveyFactory;
    private SurveyGetAllManager surveyGetAllManager;
    private SurveyPostManager surveyPostManager;
    private ValidationManager<SurveyRequestDto> validationManager;
    private SurveyBusiness surveyBusiness;
    private SurveyDtoMapper surveyDtoMapper;
    private SurveyVersionDtoMapper surveyVersionDtoMapper;
    private QuestionVersionTraductionBusiness questionVersionTraductionBusiness;
    private SurveyVersionBusiness surveyVersionBusiness;
    private AnswerChoiceTraductionBusiness answerChoiceTraductionBusiness;


    @Autowired
    public SurveyController(SurveyFactory surveyFactory, SurveyGetAllManager surveyGetAllManager, SurveyPostManager surveyPostManager, ValidationManager<SurveyRequestDto> validationManager, SurveyBusiness surveyBusiness, SurveyDtoMapper surveyDtoMapper, SurveyVersionDtoMapper surveyVersionDtoMapper, QuestionVersionTraductionBusiness questionVersionTraductionBusiness, SurveyVersionBusiness surveyVersionBusiness, AnswerChoiceTraductionBusiness answerChoiceTraductionBusiness) {
        this.surveyFactory = surveyFactory;
        this.surveyGetAllManager = surveyGetAllManager;
        this.surveyPostManager = surveyPostManager;
        this.validationManager = validationManager;
        this.surveyBusiness = surveyBusiness;
        this.surveyDtoMapper = surveyDtoMapper;
        this.surveyVersionDtoMapper = surveyVersionDtoMapper;
        this.questionVersionTraductionBusiness = questionVersionTraductionBusiness;
        this.surveyVersionBusiness = surveyVersionBusiness;
        this.answerChoiceTraductionBusiness = answerChoiceTraductionBusiness;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        List<Survey> surveys = surveyBusiness.getAll();
        List<SurveyRequestDto> surveyRequestDtos = surveyDtoMapper.listofsurveyTolistofSurveyRequestDto(surveys);
        List<SurveyResponseDto> surveyResponseDto = surveyDtoMapper.listofrequestDtoToResponseDto(surveyRequestDtos);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(surveyResponseDto);
        return responseEntity;
    }


    @RequestMapping(value = "/{lang}/{id}/Latest", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getSurveyLatest(@PathVariable String lang, @PathVariable String id, HttpServletRequest request) {

        //Get the surveyVersion
        SurveyVersion surveyVersion = surveyVersionBusiness.getLatestSurveyVersionBySurveyId(id);

        //Set text according to language
        for (QuestionVersion questionVersion: surveyVersion.getQuestionVersions()) {
            QuestionVersionTraduction questionVersionTraduction = questionVersionTraductionBusiness.getByLangAndQuestionVersion(lang, questionVersion.getId());

            if(questionVersionTraduction != null) {
                questionVersion.setText(questionVersionTraduction.getText());
                questionVersion.setFormattedText(questionVersionTraduction.getFormattedText());
                questionVersion.setInfo(questionVersion.getInfo() == null ? null : questionVersionTraduction.getInfo());
                questionVersion.setFormattedInfo(questionVersion.getFormattedInfo() == null ? null : questionVersionTraduction.getFormattedInfo() );

                for (AnswerChoice answer : questionVersion.getPossibleAnswers()) {
                    AnswerChoiceTraduction answerChoiceTraduction = answerChoiceTraductionBusiness.getByLangAndAnswerChoice(lang, answer.getId());

                    if(answerChoiceTraduction != null) {
                        answer.setText(answerChoiceTraduction.getText());
                        answer.setFormattedText(answerChoiceTraduction.getFormattedText());
                        answer.setInfo(answer.getInfo() == null ? null : answerChoiceTraduction.getInfo());
                        answer.setFormattedInfo(answer.getFormattedInfo() == null ? null : answerChoiceTraduction.getFormattedInfo() );
                        answer.setSupplementInputText(answer.getSupplementInputText() == null ? null : answerChoiceTraduction.getSupplementInputText());
                    }
                }
            }
        }
        SurveyVersionResponseDto surveyVersionResponseDto = surveyVersionDtoMapper.businessObjectToResponseDto(surveyVersion);

        System.out.println("Dto   "+ surveyVersionResponseDto);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .eTag(surveyVersionDtoMapper.getEtag(surveyVersion))
                .body(surveyVersionResponseDto);

        return responseEntity;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postSurvey(@Valid @RequestBody SurveyRequestDto dto, Errors errors, HttpServletRequest request) {
        RequestInformation<SurveyRequestDto> requestInformation = surveyFactory.newRequestInformation(null, dto, request, errors, null, "toto");
        return surveyFactory.newRequestPipeline(validationManager)
                .addManager(surveyPostManager)
                .manage(requestInformation);
    }


}

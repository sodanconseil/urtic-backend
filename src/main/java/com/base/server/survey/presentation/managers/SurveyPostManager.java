package com.base.server.survey.presentation.managers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.mappers.QuestionDtoMapper;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.survey.business.Survey;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.survey.mappers.SurveyDtoMapper;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class SurveyPostManager implements RequestManager<SurveyRequestDto> {

    private SurveyBusiness surveyBusiness;
    private SurveyDtoMapper surveyDtoMapper;

    @Autowired
    public SurveyPostManager(SurveyBusiness surveyBusiness, SurveyDtoMapper surveyDtoMapper) {
        this.surveyBusiness = surveyBusiness;
        this.surveyDtoMapper = surveyDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<SurveyRequestDto> requestInformation) {

        String userId = requestInformation.getUserId();
        SurveyRequestDto surveyRequestDto = requestInformation.getDto();
      //  Survey serveyDto = surveyDtoMapper.requestDtoToBusinessObject(surveyRequestDto);

      //  Survey survey = surveyBusiness.save(serveyDto, userId);
        Survey survey = surveyBusiness.save(surveyDtoMapper.requestDtoToBusinessObject(surveyRequestDto), userId);
        SurveyResponseDto surveyResponseDto = surveyDtoMapper.businessObjectToResponseDto(survey);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(surveyDtoMapper.getEtag(survey))
            .body(surveyResponseDto);

        return Optional.of(responseEntity);
    }
}

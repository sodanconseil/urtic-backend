package com.base.server.survey.persistance.interfaces;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.survey.business.Survey;
import org.springframework.stereotype.Component;

@Component
public interface SurveyRepository extends BaseRepository<Survey> {
    public Survey getByShortName(String shortName);
}

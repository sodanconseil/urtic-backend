# Class Diagram

The class diagram illustrates relations between entities in the "survey" package.

```mermaid
classDiagram
    SurveyEntity *-- QuestionEntity : contains
    SurveyVersionEntity --> SurveyEntity : related to
    SurveyVersionEntity *-- QuestionVersionEntity : contains
    QuestionVersionEntity --> QuestionEntity : related to
    QuestionVersionEntity --> QuestionTypeEntity : is of
    QuestionVersionEntity *-- AnswerChoiceEntity: offers
```

## Building a survey

First step in building a survey is to create a `SurveyEntity` for the survey 
and create `QuestionEntity` for each survey question. This creates the base 
of the survey.

Next step is to create the first version of a survey adding a 
`SurveyVersionEntity`. Surveys may change overtime, keeping track of the 
previous version is important for analysing historical data. 
Then, for each `QuestionEntity`, create a `QuestionVersionEntity`.

For each `QuestionVersionEntity` that are single or multiple choices question,
create the appropriate `AnswerChoiceEntity`.

### USA7 Example

#### QuestionTypeEntity

|Properties|Values|
|---|---|
|id|1|
|typeCode|0|
|name|Calculated|

|Properties|Values|
|---|---|
|id|2|
|typeCode|1|
|name|Single choice|

#### SurveyEntity

|Properties|Value|
|---|---|
|id|3|
|shortName|USA7|
|name|Urticaria Activity Score|
|questions|[4,5,6]|

#### QuestionEntity

|Properties|Value|
|---|---|
|id|4|
|survey|3|
|text|Number of wheals|

|Properties|Value|
|---|---|
|id|5|
|survey|3|
|text|Itch (pruritus) intensity|

|Properties|Value|
|---|---|
|id|5|
|survey|3|
|text|Daily USA score|

#### SurveyVersionEntity

|Properties|Value|
|---|---|
|id|7|
|survey|3|
|version|1|
|date|2021-08-15T08:00:00Z|
|questions|[6,7,8]|

#### QuestionVersionEntity

|Properties|Value|
|---|---|
|id|8|
|order|1|
|questionTypeEntity|2|
|text|Number of wheals|
|formattedText|Number of wheals|
|info||
|formattedInfo||
|possibleAnswers|[13,14,15,16]|
|commandClass||

|Properties|Value|
|---|---|
|id|10|
|order|2|
|questionTypeEntity|2|
|text|Itch (pruritus) intensity|
|formattedText|Itch (pruritus) intensity|
|info||
|formattedInfo||
|possibleAnswers|[17,18,19,20]|
|commandClass||

|Properties|Value|
|---|---|
|id|12|
|order|3|
|questionTypeEntity|1|
|text|Daily UAS score|
|formattedText|Daily UAS score|
|info|The sum of the daily number of wheals and daily intensity of pruritus|
|formattedInfo|The sum of the daily number of wheals and daily intensity of pruritus|
|possibleAnswers||
|commandClass|com.base.server.survey.business.uas7DailyScoreCommand|

#### AnswerChoiceEntity

|Properties|Values|
|---|---|
|id|13|
|code|1|
|order|1|
|value|0|
|text|0|
|formattedText|1|
|info|None|
|formattedInfo|None|

|Properties|Values|
|---|---|
|id|14|
|code|2|
|order|2|
|value|1|
|text|1|
|formattedText|1|
|info|Mild: <20 wheals over 24 hours|
|formattedInfo|Mild: <20 wheals over 24 hours|

|Properties|Values|
|---|---|
|id|15|
|code|2|
|order|3|
|value|2|
|text|2|
|formattedText|2|
|info|Moderate: 20-50 wheals over 24 hours|
|formattedInfo|Moderate: 20-50 wheals over 24 hours|

|Properties|Values|
|---|---|
|id|16|
|code|2|
|order|4|
|value|3|
|text|3|
|formattedText|3|
|info|Intense: 50 wheals over 24 hour or large areas of wheals that blend into one|
|formattedInfo|Intense: 50 wheals over 24 hour or large areas of wheals that blend into one|

|Properties|Values|
|---|---|
|id|17|
|code|1|
|order|1|
|value|0|
|text|0|
|formattedText|1|
|info|None|
|formattedInfo|None|

|Properties|Values|
|---|---|
|id|18|
|code|2|
|order|2|
|value|1|
|text|1|
|formattedText|1|
|info|Mild: present but not annoying or troublesome|
|formattedInfo|Mild: present but not annoying or troublesome|

|Properties|Values|
|---|---|
|id|19|
|code|2|
|order|3|
|value|2|
|text|2|
|formattedText|2|
|info|Moderate: troublesome but does not interfere with normal daily activity or sleep|
|formattedInfo|Moderate: troublesome but does not interfere with normal daily activity or sleep|

|Properties|Values|
|---|---|
|id|20|
|code|2|
|order|4|
|value|3|
|text|3|
|formattedText|3|
|info|Intense: severe itch (pruritus), which is sufficient troublesome to interfere with normal daily activity or sleep|
|formattedInfo|Intense: severe itch (pruritus), which is sufficient troublesome to interfere with normal daily activity or sleep|



package com.base.server.survey.persistance.jpa;


import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
A survey is a questionnaire used to evaluation patient's condition.
 */

@Entity
@Table(name = "survey")
@Data
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
public class SurveyEntity extends BaseEntity<SurveyEntity> implements Copyable<SurveyEntity> {

    /*
    Short name of the survey. E.g.: UAS7.
     */
    private String shortName;
    /*
    Name of the survey. E.g.: Urticaria Activity Score.
     */
  //  private String name;
    /*
    Survey questions.
     */
   // @OneToMany(mappedBy = "survey")
 //   private Set<QuestionEntity> questions;

    @OneToMany(mappedBy = "survey", cascade = {CascadeType.ALL})
    private List<SurveyVersionEntity> surveyVersionEntities = new ArrayList<>();

    @OneToMany(mappedBy = "survey", cascade = {CascadeType.ALL})
    private List<QuestionEntity> questions =  new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "frequency_id", nullable = false)
    private FrequencyEntity frequency;

    private int delais;

    @Builder(setterPrefix = "set")
    public SurveyEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String shortName, List<SurveyVersionEntity> surveyVersionEntities, List<QuestionEntity> questions, FrequencyEntity frequency, int delais) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.shortName = shortName;
        this.surveyVersionEntities = surveyVersionEntities;
        this.questions = questions == null ? new ArrayList<>() : questions;
        this.frequency = frequency;
        this.delais = delais;
    }

    @Override
    public void copy(SurveyEntity surveyEntity) {
        super.copy(surveyEntity);
        this.shortName = surveyEntity.shortName;
        this.surveyVersionEntities = surveyEntity.surveyVersionEntities;
        this.questions = surveyEntity.questions;
        this.frequency = surveyEntity.frequency;
        this.delais = surveyEntity.delais;
    }



}

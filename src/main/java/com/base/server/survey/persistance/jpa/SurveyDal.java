package com.base.server.survey.persistance.jpa;

import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.survey.business.Survey;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

@Component
public interface SurveyDal extends BaseDal<SurveyEntity> {
    @Query(value = "SELECT * From survey WHERE short_name = ?1", nativeQuery = true)
    public SurveyEntity getByShortName(String shortName);
}

package com.base.server.survey.persistance;

import com.base.server.Question.Business.Question;
import com.base.server.Question.mappers.QuestionEntityMapper;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.Question.persistance.jpa.QuestionDal;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.survey.business.Survey;
import com.base.server.survey.mappers.SurveyEntityMapper;
import com.base.server.survey.persistance.interfaces.SurveyRepository;
import com.base.server.survey.persistance.jpa.SurveyDal;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.NoSuchElementException;

@Component
public class SurveyRepositoryImpl extends BaseRepositoryImpl<SurveyEntity, Survey> implements SurveyRepository {
    private SurveyDal dal;
    private SurveyEntityMapper mapper;

    @Autowired
    public SurveyRepositoryImpl(SurveyDal dal, SurveyEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    @Override
    public Survey getByShortName(String shortName) {
        try {
            return mapper.entityToBusinessObject(dal.getByShortName(shortName));
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

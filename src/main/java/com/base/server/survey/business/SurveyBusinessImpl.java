package com.base.server.survey.business;

import com.base.server.Question.Business.Question;
import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.factory.QuestionFactory;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.survey.business.interfaces.SurveyBusiness;
import com.base.server.survey.factory.SurveyFactory;
import com.base.server.survey.persistance.interfaces.SurveyRepository;
import com.base.server.surveyVersion.business.SurveyVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SurveyBusinessImpl extends BaseBusinessImpl<Survey, SurveyRepository> implements SurveyBusiness {

    private SurveyRepository surveyRepository;
    private SurveyFactory surveyFactory;

    @Autowired
    public SurveyBusinessImpl(SurveyRepository surveyRepository, SurveyFactory factory) {
        super(surveyRepository, factory);
        this.surveyRepository = surveyRepository;
        this.surveyFactory = surveyFactory;
    }

    @Override
    public Survey getByShortName(String shortName) {
        return surveyRepository.getByShortName(shortName);
    }
}

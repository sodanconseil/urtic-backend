package com.base.server.survey.business.interfaces;

import com.base.server.Question.Business.Question;
import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.survey.business.Survey;

public interface SurveyBusiness extends BaseBusiness<Survey> {
    public Survey getByShortName(String shortName);
}

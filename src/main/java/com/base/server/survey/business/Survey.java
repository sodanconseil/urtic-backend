package com.base.server.survey.business;

import com.base.server.Question.Business.Question;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.frequency.business.Frequency;
import com.base.server.generic.business.Base;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.surveyVersion.business.SurveyVersion;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Survey extends Base {

    private String shortName;

    private List<Question> questions = new ArrayList<>();

    private Frequency frequency;

    private int delais;

    @Builder(setterPrefix = "set")
    public Survey(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String shortName, List<Question> questions, Frequency frequency, int delais) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.shortName = shortName;
        this.questions = questions;
        this.frequency = frequency;
        this.delais = delais;
    }
}

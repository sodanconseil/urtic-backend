package com.base.server.survey.business;

import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.business.interfaces.UserAnswerBusiness;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.Business.interfaces.UserQuestionBusiness;
import com.base.server.usersurvey.Business.UserSurvey;
import com.base.server.usersurvey.Business.interfaces.UserSurveyBusiness;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public abstract class Uas7DailyScoreCommand {

    public Uas7DailyScoreCommand() {

    }

    public int execute(UserSurvey userSurvey, UserQuestionBusiness userQuestionBusiness, UserAnswerBusiness userAnswerBusiness){
        return 0;
    }
}
package com.base.server.generic.mapper;

import java.sql.Timestamp;
import java.util.List;

import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.dtos.ResponseErrorDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;

@Component
public class ErrorMapperImpl implements ErrorMapper{

    @Autowired
    public ErrorMapperImpl() {
    }

    @Override
    public String listOfErrorsToString(List<ObjectError> errors) {
        StringBuilder builder = new StringBuilder();

        if (errors != null && errors.size() > 0) {

            for (int i = 0; i < errors.size() - 1; i++) {
                builder.append(errors.get(i).getDefaultMessage() + ", ");
            }

            builder.append(errors.get(errors.size() - 1).getDefaultMessage());
        }

        return builder.toString();
    }

    @Override
    public ResponseErrorDto errorToResponseErrorDto(HttpStatus status, String message, String requestURI) {
        ResponseErrorDto dto = ResponseErrorDto.builder()
        .setError(status.getReasonPhrase())
        .setMessage(message)
        .setPath(requestURI)
        .setStatus(status.value())
        .setTimestamp(new Timestamp(System.currentTimeMillis()))
        .build();

        return dto;
    }
}

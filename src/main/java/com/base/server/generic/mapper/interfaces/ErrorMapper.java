package com.base.server.generic.mapper.interfaces;

import java.util.List;

import com.base.server.generic.presentation.dtos.ResponseErrorDto;

import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;

public interface ErrorMapper {
    
    public String listOfErrorsToString(List<ObjectError> errors);
    public ResponseErrorDto errorToResponseErrorDto(HttpStatus status, String message, String requestURI);
}

package com.base.server.generic.mapper.interfaces;

import java.util.List;

import com.base.server.generic.business.Base;
import com.base.server.generic.persistence.jpa.BaseEntity;

public interface BaseEntityMapper<Entity extends BaseEntity<Entity>, BusinessObject extends Base> {
    public BusinessObject entityToBusinessObject(Entity entity);
    public Entity businessObjectToEntity(BusinessObject object);
    public List<BusinessObject> listOfEntityToListOfBusinessObjects(List<Entity> entities);
}

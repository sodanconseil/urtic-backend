package com.base.server.generic.mapper.interfaces;

import java.util.List;
import java.util.Set;

import com.base.server.generic.business.Base;

public interface BaseDtoMapper<BusinessObject extends Base, RequestDto, ResponseDto> {
    BusinessObject requestDtoToBusinessObject(RequestDto dto);
    ResponseDto businessObjectToResponseDto(BusinessObject businessObject);
    List<ResponseDto> listOfBusinessObjectsToListOfResponseDto(List<BusinessObject> businessObjects);
    Set<ResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<BusinessObject> businessObjects);
    
    default String getEtag(BusinessObject businessObject) {
        return String.valueOf(businessObject.getVersion());
    }
}

package com.base.server.generic.services;

import java.util.List;

import com.base.server.generic.presentation.JwtTokenParser;
import com.base.server.generic.presentation.JwtTokenParser.JwtToken;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;

@Component
public class UserService {

    private JwtTokenParser jwtTokenParser;

    @Autowired
    public UserService(JwtTokenParser jwtTokenParser) {
        this.jwtTokenParser = jwtTokenParser;
    }

    @Getter
    private String userId;
    @Getter
    private String userName;
    @Getter
    private String userFamilyName;
    @Getter 
    private String email;
    @Getter
    private List<String> userRoles;

    public void changeCurrentUser(String authorization) { 
        JwtToken token = this.jwtTokenParser.parseJwtToken(authorization);

        this.userId = token.getUserId();
        this.userName = token.getName();
        this.userFamilyName = token.getFamilyName();
        this.email = token.getEmail();
        this.userRoles = token.getRoles();
    }

    public void removeCurrentUser() {
        this.userId = null;
        this.userName = null;
        this.userFamilyName = null;
        this.email = null;
        this.userRoles = null;
    }

}

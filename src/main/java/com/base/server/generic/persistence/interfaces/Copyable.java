package com.base.server.generic.persistence.interfaces;

public interface Copyable<T> {
    public void copy(T toCopy);
}

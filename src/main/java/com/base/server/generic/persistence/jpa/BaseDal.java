package com.base.server.generic.persistence.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseDal<T extends BaseEntity<T>> extends JpaRepository<T, String> {
    public T getOne(String id);
    public List<T> findAll();
    public void deleteById(String id);
    public Optional<T> findById(String id);
}

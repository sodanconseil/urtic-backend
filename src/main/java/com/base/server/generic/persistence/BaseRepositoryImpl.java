package com.base.server.generic.persistence;

import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import com.base.server.generic.business.Base;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.generic.persistence.jpa.BaseEntity;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;

public class BaseRepositoryImpl<Entity extends BaseEntity<Entity>, BusinessObject extends Base> implements BaseRepository<BusinessObject> {
    private BaseDal<Entity> dal;
    private BaseEntityMapper<Entity, BusinessObject> mapper;

    public BaseRepositoryImpl(BaseDal<Entity> dal, BaseEntityMapper<Entity, BusinessObject> mapper) {
        this.dal = dal;
        this.mapper = mapper;
    }

    @Override
    public BusinessObject getOne(String id) {
        try {
            return mapper.entityToBusinessObject(dal.findById(id).get());
        } catch (EntityNotFoundException ex) { 
            return null;
        } catch (NoSuchElementException ex) {
            return null;
        }
    }

    @Override
    public List<BusinessObject> findAll() {
        return mapper.listOfEntityToListOfBusinessObjects(dal.findAll());
    }

    @Override
    public BusinessObject delete(String id) {
        if (id != null) {

            try {
                BusinessObject businessObject = getOne(id);
                dal.deleteById(id);
                return businessObject;

            } catch (EmptyResultDataAccessException ex) {
                return null;
            }
            
        } else {
            throw new InvalidDataAccessApiUsageException("Impossible to delete businessObject with null id");
        }
    }

    @Override
    public BusinessObject modify(String id, BusinessObject businessObject) {
        Entity entity = null;

        try {

            if (businessObject != null) {

                entity = dal.findById(id).get();
                Entity toSave = mapper.businessObjectToEntity(businessObject);
                // TODO add an abstract method for copying
                entity.copy(toSave);
                Entity response = dal.save(entity);

                return mapper.entityToBusinessObject(response);
            } else {
                throw new InvalidDataAccessApiUsageException("Impossible to modify a null businessObject");
            }
            
        } catch (NoSuchElementException ex) {
            return null;
        } catch (ObjectOptimisticLockingFailureException ex) {
            // Ne devrait pas arriver à cause des ETAGS
            throw ex;
        }
    }

    @Override
    public BusinessObject save(BusinessObject businessObject) {
        if (businessObject != null) {

            Entity entity = mapper.businessObjectToEntity(businessObject);
            Entity response = (Entity) dal.save(entity);

            return mapper.entityToBusinessObject(response);
        } else {
            throw new InvalidDataAccessApiUsageException("Impossible to save a null businessObject");
        }
    }
}

package com.base.server.generic.persistence.jpa;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.base.server.generic.persistence.interfaces.Copyable;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@MappedSuperclass
@Data
public abstract class BaseEntity<T extends BaseEntity<T>> implements Copyable<T> {
    @Version
    private Long version;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;

    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "Description")
    private String description;

    @Column(name = "CreationUser")
    private String creationUser;

    @Column(name = "CreationDate")
    private Timestamp creationDate;

    @Column(name = "ModificationUser")
    private String modificationUser;

    @Column(name = "ModificationDate")
    private Timestamp modificationDate;

    public BaseEntity() { }

    public BaseEntity(String id, String name, String description, String creationUser,
            Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creationUser = creationUser;
        this.creationDate = creationDate;
        this.modificationUser = modificationUser;
        this.modificationDate = modificationDate;
        this.version = version;
    }

    @Override
    public void copy(T entity) {
        this.name = entity.getName();
        this.description = entity.getDescription();
        this.creationUser = entity.getCreationUser();
        this.creationDate = entity.getCreationDate();
        this.modificationUser = entity.getModificationUser();
        this.modificationDate = entity.getModificationDate();
        this.version = entity.getVersion();
    }
}

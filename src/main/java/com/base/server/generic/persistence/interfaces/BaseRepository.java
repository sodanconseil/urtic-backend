package com.base.server.generic.persistence.interfaces;

import java.util.List;

import com.base.server.generic.business.Base;

public interface BaseRepository<T extends Base>{
    public T getOne(String id);
    public List<T> findAll();
    public T delete(String id);
    public T modify(String id, T project);
    public T save(T project);
}

package com.base.server.generic.business.interfaces;

import java.util.List;

import com.base.server.generic.business.Base;

public interface BaseBusiness <T extends Base> { 
    public T get(String id);
	public List<T> getAll();
	public T delete(String id);
	public T modify(String id, T t, String userId);
	public T save(T t, String userId);
}
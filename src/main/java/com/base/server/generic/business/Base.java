package com.base.server.generic.business;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor            
public class Base implements Serializable {
    private static final long serialVersionUID = -8810530160081980349L;

    private String id;

    private Long version;

    @NotNull
    private String name;

    private String description;

    private String creationUser;

    private Timestamp creationDate;

    private String modificationUser;

    private Timestamp modificationDate;
}

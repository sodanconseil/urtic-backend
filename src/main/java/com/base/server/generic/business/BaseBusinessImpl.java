package com.base.server.generic.business;

import java.sql.Timestamp;
import java.util.List;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.generic.factory.BaseFactory;
import com.base.server.generic.persistence.interfaces.BaseRepository;

public abstract class BaseBusinessImpl<BusinessObject extends Base, Repository extends BaseRepository<BusinessObject>> implements BaseBusiness<BusinessObject> {
    private Repository businessObjectRepository;
    private BaseFactory businessObjectFactory;

    public BaseBusinessImpl(Repository businessObjectRepository, BaseFactory factory) {
        this.businessObjectRepository = businessObjectRepository;
        this.businessObjectFactory = factory;
    }

    @Override
    public BusinessObject get(String id) {
        return businessObjectRepository.getOne(id);
    }

    @Override
    public List<BusinessObject> getAll() {
        return businessObjectRepository.findAll();
    }

    @Override
    public BusinessObject delete(String id) {
        return businessObjectRepository.delete(id);
    }

    @Override
    public BusinessObject modify(String id, BusinessObject businessObject, String userId) {
        Timestamp now = businessObjectFactory.newCurrentTimeStamp();
        businessObject.setModificationDate(now);
        businessObject.setModificationUser(userId);
        return businessObjectRepository.modify(id, businessObject);
    }
    
    @Override
    public BusinessObject save(BusinessObject businessObject, String userId) {
        Timestamp now = businessObjectFactory.newCurrentTimeStamp();
        businessObject.setCreationDate(businessObject.getCreationDate() == null ? now : businessObject.getCreationDate());
        businessObject.setModificationDate(now);
        businessObject.setCreationUser(businessObject.getCreationDate() == null ? userId : businessObject.getCreationUser());
        businessObject.setModificationUser(userId);
        return businessObjectRepository.save(businessObject);
    }
}

package com.base.server.generic.presentation.managers;

import com.base.server.generic.builders.ResponseEntityBuilder;
import com.base.server.generic.factory.BaseFactory;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.RequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ErrorRequestManager {

    private ErrorMapper projectErrorMapper;
    private RequestValidator requestValidator;
    private BaseFactory baseFactory;

    @Autowired
    public ErrorRequestManager(ErrorMapper projectErrorMapper, RequestValidator requestValidator, BaseFactory baseFactory) {
        this.projectErrorMapper = projectErrorMapper;
        this.requestValidator = requestValidator;
        this.baseFactory = baseFactory;
    }

    public ResponseEntity<Object> manageNotMatchingEtags(String requestURI) { 
		ResponseEntityBuilder responseEntityBuilder = baseFactory.newResponseEntityBuilder();
        
        String message = "The project was modified, GET the new version";
		HttpStatus status = HttpStatus.PRECONDITION_FAILED;

		return responseEntityBuilder
		.setStatus(status)
		.setBody(projectErrorMapper.errorToResponseErrorDto(status, message, requestURI))
        .build();
	}

	public ResponseEntity<Object> manageUnexistingRequest(String requestURI) {
		ResponseEntityBuilder responseEntityBuilder = baseFactory.newResponseEntityBuilder();

        String message = "The project with the specified id wasn't found";
		HttpStatus status = HttpStatus.NOT_FOUND;

		return responseEntityBuilder
		.setStatus(status)
		.setBody(projectErrorMapper.errorToResponseErrorDto(status, message, requestURI))
        .build();

	}

	public ResponseEntity<Object> manageInvalidRequest(String requestURI) {
        ResponseEntityBuilder responseEntityBuilder = baseFactory.newResponseEntityBuilder();

		HttpStatus status = HttpStatus.BAD_REQUEST;

		return responseEntityBuilder
        .setStatus(status)
		.setBody(projectErrorMapper.errorToResponseErrorDto(status, requestValidator.getErrorMessage(), requestURI))
        .build();
	}
}

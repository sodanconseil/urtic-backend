package com.base.server.generic.presentation.pipeline;

import java.util.Optional;

import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.RequestValidator;
import com.base.server.generic.presentation.dtos.BaseRequestDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ValidationManager<RequestDto extends BaseRequestDto> implements RequestManager<RequestDto> {

    private RequestValidator requestValidator;
    private ErrorMapper errorMapper;

    @Autowired
    public ValidationManager(RequestValidator requestValidator, ErrorMapper errorMapper) {
        this.requestValidator = requestValidator;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<RequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        boolean validRequestDto = requestValidator.isValid(requestInformation.getRequestToValidate());

        if (!validRequestDto) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = requestValidator.getErrorMessage();

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
            
            toReturn = Optional.of(response);
        }

        return toReturn;
    }
}

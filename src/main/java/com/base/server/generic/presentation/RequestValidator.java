package com.base.server.generic.presentation;

import java.util.List;

import com.base.server.generic.mapper.interfaces.ErrorMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

@Component
public class RequestValidator {

    private ErrorMapper mapper;
    private String message;

    @Autowired
    public RequestValidator(ErrorMapper mapper) {
        this.mapper = mapper;
        this.message = "";
    }

    public boolean isValid(Errors errors) {
        List<ObjectError> errorList = errors.getAllErrors();

        message = mapper.listOfErrorsToString(errorList);
        
        return errorList.size() == 0;
    }

    public String getErrorMessage() { 
        return message;
    }
}

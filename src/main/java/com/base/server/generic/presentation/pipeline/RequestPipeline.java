package com.base.server.generic.presentation.pipeline;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.base.server.generic.presentation.dtos.BaseRequestDto;
import org.springframework.http.ResponseEntity;

public class RequestPipeline<RequestDto extends BaseRequestDto> {
    List<RequestManager<RequestDto>> managers;

    public RequestPipeline(RequestManager<RequestDto> firstManager) {
        this.managers = new ArrayList<>();
        this.managers.add(firstManager);
    }

    public RequestPipeline<RequestDto> addManager(RequestManager<RequestDto> manager) {
        this.managers.add(manager);
        return this;
    }

    public ResponseEntity<Object> manage(RequestInformation<RequestDto> requestInformation) {
        boolean requestManaged = false;
        Optional<ResponseEntity<Object>> response = null;

        for (int i = 0; i < managers.size() && !requestManaged; i++) {
            response = managers.get(i).process(requestInformation);
            requestManaged = response.isPresent();
        }

        return response.get();
    }
}

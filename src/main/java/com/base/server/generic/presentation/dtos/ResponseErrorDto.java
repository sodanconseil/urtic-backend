package com.base.server.generic.presentation.dtos;

import java.sql.Timestamp;

import lombok.Builder;
import lombok.Data;

@Data
public class ResponseErrorDto {
    private Timestamp timestamp;
    private int status;
    private String error;
    private String message;
    private String path;

    public ResponseErrorDto() {
    }

    @Builder(setterPrefix = "set")
    public ResponseErrorDto(Timestamp timestamp, int status, String error, String message, String path) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.message = message;
        this.path = path;
    }
}

package com.base.server.generic.presentation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.base.server.generic.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class UserServiceInterceptor implements HandlerInterceptor {

    private UserService userService;

    @Autowired
    public UserServiceInterceptor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String authorization = request.getHeader("Authorization");
        boolean validAuthorization = authorization != null && authorization.contains("Bearer ");

        if (validAuthorization) {
            this.userService.changeCurrentUser(authorization);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        this.userService.removeCurrentUser();
    }
}

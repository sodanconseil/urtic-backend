package com.base.server.generic.presentation.pipeline;

import java.util.Optional;

import org.springframework.http.ResponseEntity;

public interface RequestManager<RequestDto> {
    public Optional<ResponseEntity<Object>> process(RequestInformation<RequestDto> requestInformation);
}

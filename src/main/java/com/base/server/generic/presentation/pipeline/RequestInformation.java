package com.base.server.generic.presentation.pipeline;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.Errors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestInformation<RequestDto> {
    private Map<String, String> pathVariables;
    private RequestDto dto;
    private HttpServletRequest request;  
	private Errors requestToValidate; 
    private String if_match;
    private String userId;

    public String getId() { 
        return pathVariables.get("id");
    }
}

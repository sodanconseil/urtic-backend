package com.base.server.generic.presentation;

import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;

@Component
public class JwtTokenParser {

    @Data
    @AllArgsConstructor
    public static class JwtToken {
        private final String userId;
        private final String name;
        private final String familyName;
        private final String email;
        private final List<String> roles;
    }
    
    public JwtTokenParser() {
    }

    public JwtToken parseJwtToken(String authorization) {
        // TODO try and catch correctement les erreurs qui peuvent arriver lors du decodage
        DecodedJWT decodedJWT = JWT.decode(authorization.replace("Bearer", "").replaceAll(" ", ""));
        // Fonctionne seulement avec le plugin keycloak de angular
        //@SuppressWarnings("unchecked")
        //Map<String, List<String>> accountMap = (Map<String, List<String>>)decodedJWT.getClaim("resource_access").asMap().get("account");
        //List<String> roles = accountMap.get("roles");
        String userId = decodedJWT.getSubject();
        String name = decodedJWT.getClaim("given_name").asString();
        String familyName = decodedJWT.getClaim("family_name").asString();
        String email = decodedJWT.getClaim("email").asString();

        return new JwtToken(userId, name, familyName, email, null);
    }
}

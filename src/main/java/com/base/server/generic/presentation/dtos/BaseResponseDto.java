package com.base.server.generic.presentation.dtos;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor            
public class BaseResponseDto implements Serializable {
    private static final long serialVersionUID = -2175562482834148723L;

    private String id;

    @NotNull
    private String name;

    private String description;

    private String creationUser;

    private Timestamp creationDate;

    private String modificationUser;

    private Timestamp modificationDate;
}

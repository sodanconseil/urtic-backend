package com.base.server.generic.factory;

import java.sql.Timestamp;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.base.server.generic.builders.ResponseEntityBuilder;
import com.base.server.generic.presentation.dtos.BaseRequestDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.generic.presentation.pipeline.RequestPipeline;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class BaseFactory {
    public Timestamp newCurrentTimeStamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    public <T extends BaseRequestDto> RequestPipeline<T> newRequestPipeline(RequestManager<T> firstManager) {
        return new RequestPipeline<>(firstManager);
    }

    public <T extends BaseRequestDto> RequestInformation<T> newRequestInformation() {
        return new RequestInformation<>();
    }

    public <T extends BaseRequestDto> RequestInformation<T> newRequestInformation(Map<String, String> pathVariables, T dto,
            HttpServletRequest request, Errors requestToValidate, String if_match, String userId) {
        return new RequestInformation<T>(pathVariables, dto, request, requestToValidate, if_match, userId);
    }

    public ResponseEntityBuilder newResponseEntityBuilder() {
        return new ResponseEntityBuilder();
    }
}

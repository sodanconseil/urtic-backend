package com.base.server.generic.builders;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.stereotype.Component;

@Component
public class ResponseEntityBuilder {

    public ResponseEntityBuilder() {
    }

    private HttpStatus status;
    private String etag;
    private Object body;

    public ResponseEntityBuilder setStatus(HttpStatus status) {
        this.status = status;
        return this;
    }

    public ResponseEntityBuilder setEtag(String etag) {
        this.etag = etag;
        return this;
    }

    public ResponseEntityBuilder setBody(Object body) {
        this.body = body;
        return this;
    }

    public ResponseEntity<Object> build() {

        BodyBuilder builder = ResponseEntity.status(status);
        ResponseEntity<Object> responseEntity;

        if (etag != null)
            builder.eTag(etag);

        if (body != null) {
            responseEntity = builder.body(body);
        } else {
            responseEntity = builder.build();
        }

        return responseEntity;
    }
}

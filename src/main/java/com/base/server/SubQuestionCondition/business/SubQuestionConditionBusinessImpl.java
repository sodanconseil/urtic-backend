package com.base.server.SubQuestionCondition.business;

import com.base.server.SubQuestionCondition.business.interfaces.SubQuestionConditionBusiness;
import com.base.server.SubQuestionCondition.factory.SubQuestionConditionFactory;
import com.base.server.SubQuestionCondition.persistance.interfaces.SubQuestionConditionRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.survey.business.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SubQuestionConditionBusinessImpl extends BaseBusinessImpl<SubQuestionCondition, SubQuestionConditionRepository> implements SubQuestionConditionBusiness {
    private SubQuestionConditionRepository subQuestionConditionRepository;
    private SubQuestionConditionFactory subQuestionConditionFactory;

    @Autowired
    public SubQuestionConditionBusinessImpl(SubQuestionConditionRepository subQuestionConditionRepository, SubQuestionConditionFactory factory) {
        super(subQuestionConditionRepository, factory);
        this.subQuestionConditionRepository = subQuestionConditionRepository;
    }


    @Override
    public List<SubQuestionCondition> getAllBySurveyVersion(String surveyVersionId) {
        return subQuestionConditionRepository.getAllBySurveyVersion(surveyVersionId);
    }

    @Override
    public SubQuestionCondition getByQuestionVersion(String questionVersionId){
        return subQuestionConditionRepository.getByQuestionVersion(questionVersionId);
    };

}

package com.base.server.SubQuestionCondition.business;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.generic.business.Base;
import com.base.server.questionVersion.Business.QuestionVersion;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SubQuestionCondition extends Base {

    private String questionVersion;

    private String surveyVersion;

    private int subQuestionOrder;

    private String subQuestionVersion;

    @Builder(setterPrefix = "set")
    public SubQuestionCondition(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String questionVersion, String surveyVersion, int subQuestionOrder, String subQuestionVersion) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.questionVersion = questionVersion;
        this.surveyVersion = surveyVersion;
        this.subQuestionOrder = subQuestionOrder;
        this.subQuestionVersion = subQuestionVersion;
    }
}

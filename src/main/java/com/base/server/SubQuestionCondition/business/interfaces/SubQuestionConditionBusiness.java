package com.base.server.SubQuestionCondition.business.interfaces;

import com.base.server.SubQuestionCondition.business.SubQuestionCondition;
import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.survey.business.Survey;

import java.util.List;

public interface SubQuestionConditionBusiness extends BaseBusiness<SubQuestionCondition> {
    public List<SubQuestionCondition> getAllBySurveyVersion(String SurveyVersionId);

    public SubQuestionCondition getByQuestionVersion(String questionVersionId);
}

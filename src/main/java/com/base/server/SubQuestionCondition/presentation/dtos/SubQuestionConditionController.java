package com.base.server.SubQuestionCondition.presentation.dtos;

import com.base.server.SubQuestionCondition.business.SubQuestionCondition;
import com.base.server.SubQuestionCondition.business.interfaces.SubQuestionConditionBusiness;
import com.base.server.SubQuestionCondition.mappers.SubQuestionConditionDtoMapper;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionDal;
import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.business.interfaces.AnswerChoiceBusiness;
import com.base.server.survey.business.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/subquestioncondition")
public class SubQuestionConditionController {
    private SubQuestionConditionDtoMapper subQuestionConditionDtoMapper;
    private SubQuestionConditionBusiness subQuestionConditionBusiness;
    private AnswerChoiceBusiness answerChoiceBusiness;

    @Autowired
    public SubQuestionConditionController(SubQuestionConditionDtoMapper subQuestionConditionDtoMapper, SubQuestionConditionBusiness subQuestionConditionBusiness, AnswerChoiceBusiness answerChoiceBusiness) {
        this.subQuestionConditionDtoMapper = subQuestionConditionDtoMapper;
        this.subQuestionConditionBusiness = subQuestionConditionBusiness;
        this.answerChoiceBusiness = answerChoiceBusiness;
    }

    @RequestMapping(value = "/{surveyVersionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllBySurveyVersion(@PathVariable String surveyVersionId, HttpServletRequest request){
        List<SubQuestionCondition> subQuestionConditionList = subQuestionConditionBusiness.getAllBySurveyVersion(surveyVersionId);
        List<SubQuestionConditionResponseDto> subQuestionConditionResponseDtos = subQuestionConditionDtoMapper.listOfBusinessObjectsToListOfResponseDto(subQuestionConditionList);

        for (SubQuestionConditionResponseDto dto: subQuestionConditionResponseDtos) {
            List<AnswerChoice> answerChoiceList = answerChoiceBusiness.getAllFromSubQuestionCondition(dto.getId());
            dto.setAnswerChoices(answerChoiceList);
        }


        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(subQuestionConditionResponseDtos);
        return responseEntity;
    }

    @RequestMapping(value = "/questionVersion/{questionVersionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getByQuestionVersion(@PathVariable String questionVersionId, HttpServletRequest request){
        SubQuestionCondition subQuestionCondition = subQuestionConditionBusiness.getByQuestionVersion(questionVersionId);
        SubQuestionConditionResponseDto subQuestionConditionResponseDto = subQuestionConditionDtoMapper.businessObjectToResponseDto(subQuestionCondition);

        List<AnswerChoice> answerChoiceList = answerChoiceBusiness.getAllFromSubQuestionCondition(subQuestionConditionResponseDto.getId());
        subQuestionConditionResponseDto.setAnswerChoices(answerChoiceList);


        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .body(subQuestionConditionResponseDto);
        return responseEntity;
    }
}

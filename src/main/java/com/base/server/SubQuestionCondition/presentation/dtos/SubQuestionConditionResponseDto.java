package com.base.server.SubQuestionCondition.presentation.dtos;

import com.base.server.answerChoice.business.AnswerChoice;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.generic.presentation.dtos.BaseResponseDto;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SubQuestionConditionResponseDto extends BaseResponseDto {
    private String questionVersion;

    private List<AnswerChoice> answerChoices;

    private String surveyVersion;

    private int subQuestionOrder;

    private String subQuestionVersion;


    @Builder(setterPrefix = "set")
    public SubQuestionConditionResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, String questionVersion, List<AnswerChoice> answerChoices, String surveyVersion, int subQuestionOrder, String subQuestionVersion) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.questionVersion = questionVersion;
        this.answerChoices = answerChoices;
        this.surveyVersion = surveyVersion;
        this.subQuestionOrder = subQuestionOrder;
        this.subQuestionVersion = subQuestionVersion;
    }
}

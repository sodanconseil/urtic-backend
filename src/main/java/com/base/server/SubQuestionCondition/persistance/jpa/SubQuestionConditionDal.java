package com.base.server.SubQuestionCondition.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SubQuestionConditionDal extends BaseDal<SubQuestionConditionEntity> {
    @Query(value = "SELECT * FROM public.sub_question_condition WHERE survey_version = ?1", nativeQuery = true)
    public List<SubQuestionConditionEntity> getAllBySurveyVersion(String surveyVersionId);

    @Query(value = "SELECT * FROM public.sub_question_condition WHERE question_version = ?1", nativeQuery = true)
    public SubQuestionConditionEntity getByQuestionVersion(String questionVersionId);
}

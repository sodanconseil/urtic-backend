package com.base.server.SubQuestionCondition.persistance.jpa;

import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "sub_question_condition")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SubQuestionConditionEntity extends BaseEntity<SubQuestionConditionEntity> implements Copyable<SubQuestionConditionEntity> {

    private String surveyVersion;

    private int subQuestionOrder;

    private String questionVersion;

    private String subQuestionVersion;

    @Builder(setterPrefix = "set")
    public SubQuestionConditionEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, String surveyVersion, int subQuestionOrder, String questionVersion, String subQuestionVersion) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.surveyVersion = surveyVersion;
        this.subQuestionOrder = subQuestionOrder;
        this.questionVersion = questionVersion;
        this.subQuestionVersion = subQuestionVersion;
    }

    @Override
    public void copy(SubQuestionConditionEntity subQuestionConditionEntity) {
        super.copy(subQuestionConditionEntity);
        this.questionVersion = subQuestionConditionEntity.questionVersion;
        this.surveyVersion = subQuestionConditionEntity.surveyVersion;
        this.subQuestionOrder = subQuestionConditionEntity.subQuestionOrder;
        this.subQuestionVersion = subQuestionConditionEntity.subQuestionVersion;

    }
}

package com.base.server.SubQuestionCondition.persistance;

import com.base.server.SubQuestionCondition.business.SubQuestionCondition;
import com.base.server.SubQuestionCondition.mappers.SubQuestionConditionEntityMapper;
import com.base.server.SubQuestionCondition.persistance.interfaces.SubQuestionConditionRepository;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionDal;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.survey.business.Survey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class SubQuestionConditionRepositoryImpl extends BaseRepositoryImpl<SubQuestionConditionEntity, SubQuestionCondition> implements SubQuestionConditionRepository {
    private SubQuestionConditionDal dal;
    private SubQuestionConditionEntityMapper mapper;

    @Autowired
    public SubQuestionConditionRepositoryImpl(SubQuestionConditionDal dal, SubQuestionConditionEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    @Override
    public List<SubQuestionCondition> getAllBySurveyVersion(String surveyVersionId) {
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.getAllBySurveyVersion(surveyVersionId));
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }

    @Override
    public SubQuestionCondition getByQuestionVersion(String surveyVersionId) {
        try {
            return mapper.entityToBusinessObject(dal.getByQuestionVersion(surveyVersionId));
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

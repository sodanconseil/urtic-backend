package com.base.server.SubQuestionCondition.persistance.interfaces;

import com.base.server.SubQuestionCondition.business.SubQuestionCondition;
import com.base.server.generic.persistence.interfaces.BaseRepository;

import java.util.List;

public interface SubQuestionConditionRepository extends BaseRepository<SubQuestionCondition> {
    public List<SubQuestionCondition> getAllBySurveyVersion(String surveyVersionId);

    public SubQuestionCondition getByQuestionVersion(String questionVersionId);
}

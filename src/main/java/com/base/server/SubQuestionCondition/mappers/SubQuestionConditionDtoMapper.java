package com.base.server.SubQuestionCondition.mappers;

import com.base.server.SubQuestionCondition.business.SubQuestionCondition;
import com.base.server.SubQuestionCondition.presentation.dtos.SubQuestionConditionRequestDto;
import com.base.server.SubQuestionCondition.presentation.dtos.SubQuestionConditionResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface SubQuestionConditionDtoMapper {
    SubQuestionCondition requestDtoToBusinessObject(SubQuestionConditionRequestDto dto);
    SubQuestionConditionResponseDto businessObjectToResponseDto(SubQuestionCondition businessObject);
    List<SubQuestionConditionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<SubQuestionCondition> businessObject);
    Set<SubQuestionConditionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<SubQuestionCondition> businessObject);
}


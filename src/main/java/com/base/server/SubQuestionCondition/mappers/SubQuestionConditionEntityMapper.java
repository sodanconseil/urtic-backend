package com.base.server.SubQuestionCondition.mappers;

import com.base.server.SubQuestionCondition.business.SubQuestionCondition;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SubQuestionConditionEntityMapper extends BaseEntityMapper<SubQuestionConditionEntity, SubQuestionCondition> {
    public SubQuestionCondition entityToBusinessObject(SubQuestionConditionEntity entity);
    public SubQuestionConditionEntity businessObjectToEntity(SubQuestionCondition object);
    public List<SubQuestionCondition> listOfEntityToListOfBusinessObjects(List<SubQuestionConditionEntity> entities);
}

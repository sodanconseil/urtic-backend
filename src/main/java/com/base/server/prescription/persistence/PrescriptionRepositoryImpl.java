package com.base.server.prescription.persistence;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.mappers.PrescriptionEntityMapper;
import com.base.server.prescription.persistence.interfaces.PrescriptionRepository;
import com.base.server.prescription.persistence.jpa.PrescriptionDal;
import com.base.server.prescription.persistence.jpa.PrescriptionEntity;

import com.base.server.user.business.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;

@Component
public class PrescriptionRepositoryImpl extends BaseRepositoryImpl<PrescriptionEntity, Prescription> implements PrescriptionRepository {
    private PrescriptionDal dal;
    private PrescriptionEntityMapper mapper;

    @Autowired
    public PrescriptionRepositoryImpl(PrescriptionDal dal, PrescriptionEntityMapper mapper){
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    public List<Prescription> findAllForUser(String userId) {
        try {
            return mapper.listOfEntityToListOfBusinessObjects(dal.findAllByUserId(userId).get());
        } catch (EntityNotFoundException ex) {
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }

}

package com.base.server.prescription.persistence.jpa;


import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.unit.persistence.jpa.UnitEntity;
import com.base.server.user.persistence.jpa.UserEntity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "prescription")
@Data
@NoArgsConstructor
public class PrescriptionEntity extends BaseEntity<PrescriptionEntity> implements Copyable<PrescriptionEntity> {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "drug_id", nullable = false)
    private DrugEntity drug;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "frequency_id", nullable = false)
    private FrequencyEntity frequency;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "dosage_id", nullable = false)
    private DosageEntity dosage;

    private int quantity = 0;
    private int frequencyQuantity = 0;

    
    @Builder(setterPrefix = "set")
    public PrescriptionEntity(String id, String name, String description, String creationUser, Timestamp creationDate,
            String modificationUser, Timestamp modificationDate, Long version, DrugEntity drug, UserEntity user,
            FrequencyEntity frequency, DosageEntity dosage, int quantity, int frequencyQuantity) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.drug = drug;
        this.user = user;
        this.frequency = frequency;
        this.dosage = dosage;
        this.quantity = quantity;
        this.frequencyQuantity = frequencyQuantity;
    }

    @Override
    public void copy(PrescriptionEntity drugPatientEntity) {
        super.copy(drugPatientEntity);
        this.drug = drugPatientEntity.drug;
        this.user = drugPatientEntity.user;
        this.dosage = drugPatientEntity.dosage;
        if ( drugPatientEntity.frequency != null ) this.frequency = drugPatientEntity.frequency;
        this.quantity = drugPatientEntity.quantity;
        this.frequencyQuantity = drugPatientEntity.getFrequencyQuantity();
    }

    @Override
    public String toString() {
        return this.getId();
    }
}

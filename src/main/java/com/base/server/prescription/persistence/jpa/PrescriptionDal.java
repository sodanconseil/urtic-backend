package com.base.server.prescription.persistence.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import com.base.server.user.business.User;
import com.base.server.user.persistence.jpa.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;


@Component
public interface PrescriptionDal extends BaseDal<PrescriptionEntity> {
    @Query(value="select * from prescription p where p.user_id = ?1", nativeQuery = true)
    public Optional<List<PrescriptionEntity>> findAllByUserId(String userId);
}

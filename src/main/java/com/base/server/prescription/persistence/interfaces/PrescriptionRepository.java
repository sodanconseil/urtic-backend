package com.base.server.prescription.persistence.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.prescription.business.Prescription;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface PrescriptionRepository extends BaseRepository<Prescription> {
    public List<Prescription> findAllForUser(String id);
}

package com.base.server.prescription.presentation.dtos;

import com.base.server.frequency.presentation.dtos.FrequencyResponseDto;
import com.base.server.generic.presentation.dtos.BaseResponseDto;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PrescriptionResponseDto extends BaseResponseDto {

    private String drugId;
    private String userId;
    private String frequencyId;
    private String dosageId;
    private int quantity;
    private int frequencyQuantity;
}

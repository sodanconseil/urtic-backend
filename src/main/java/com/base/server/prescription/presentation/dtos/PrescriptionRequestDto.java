package com.base.server.prescription.presentation.dtos;

import com.base.server.dosage.business.Dosage;
import com.base.server.drug.business.Drug;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.presentation.dtos.FrequencyRequestDto;
import com.base.server.generic.presentation.dtos.BaseRequestDto;
import com.base.server.user.business.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PrescriptionRequestDto extends BaseRequestDto {

    private String drugId;
    private String userId;
    private String frequencyId;
    private String dosageId;
    private int quantity;
    private int frequencyQuantity;
}

package com.base.server.prescription.presentation;

import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.business.interfaces.FrequencyBusiness;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.business.interfaces.PrescriptionBusiness;
import com.base.server.prescription.factory.PrescriptionFactory;
import com.base.server.prescription.mappers.PrescriptionDtoMapper;
import com.base.server.prescription.presentation.dtos.PrescriptionRequestDto;
import com.base.server.prescription.presentation.dtos.PrescriptionResponseDto;
import com.base.server.user.business.User;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/prescriptions")
@NoArgsConstructor
public class PrescriptionController {

    private PrescriptionFactory prescriptionFactory;
    private ValidationManager<PrescriptionRequestDto> prescriptionValidationManager;
    private PrescriptionBusiness prescriptionBusiness;
    private PrescriptionDtoMapper prescriptionDtoMapper;
    private FrequencyBusiness frequencyBusiness;

    @Autowired
    public PrescriptionController(PrescriptionFactory prescriptionFactory, ValidationManager<PrescriptionRequestDto> prescriptionValidationManager,
                                  PrescriptionBusiness prescriptionBusiness, PrescriptionDtoMapper prescriptionDtoMapper,
                                  FrequencyBusiness frequencyBusiness) {
        this.prescriptionFactory = prescriptionFactory;
        this.prescriptionValidationManager = prescriptionValidationManager;
        this.prescriptionBusiness = prescriptionBusiness;
        this.prescriptionDtoMapper = prescriptionDtoMapper;
        this.frequencyBusiness = frequencyBusiness;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postPrescription(@Valid @RequestBody PrescriptionRequestDto dto, Errors errors, HttpServletRequest request) {

        // TOOD: need to validate dosage est bien pour la bonne drug
        Prescription unsavedPrescription = prescriptionDtoMapper.requestDtoToBusinessObject(dto);
        unsavedPrescription.setFrequency(frequencyBusiness.save(unsavedPrescription.getFrequency(), "userId"));
        Prescription prescription = prescriptionBusiness.save(unsavedPrescription, "userId");
        PrescriptionResponseDto prescriptionResponseDto = prescriptionDtoMapper.businessObjectToResponseDto(prescription);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .eTag(prescriptionDtoMapper.getEtag(prescription))
                .body(prescriptionResponseDto);

        return responseEntity;
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deletePrescription(@PathVariable String id, HttpServletRequest request) {

        Prescription prescription = prescriptionBusiness.delete(id);
        PrescriptionResponseDto prescriptionResponseDto = prescriptionDtoMapper.businessObjectToResponseDto(prescription);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .eTag(prescriptionDtoMapper.getEtag(prescription))
                .body(prescriptionResponseDto);

        return responseEntity;
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> putPrescription(@PathVariable String id, @Valid @RequestBody PrescriptionRequestDto dto,
                                   Errors errors, HttpServletRequest request, @RequestHeader(name = "If-Match") String if_match) {

        Prescription prescription = prescriptionDtoMapper.requestDtoToBusinessObject(dto);
        //Frequency frequency =  prescription.getFrequency();
        //frequencyBusiness.modify(frequency.getId(), frequency, "userId");
        prescription = prescriptionBusiness.modify(id, prescription, "userId");
        PrescriptionResponseDto prescriptionResponseDto = prescriptionDtoMapper.businessObjectToResponseDto(prescription);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
                .eTag(prescriptionDtoMapper.getEtag(prescription))
                .body(prescriptionResponseDto);

        return responseEntity;
    }
}

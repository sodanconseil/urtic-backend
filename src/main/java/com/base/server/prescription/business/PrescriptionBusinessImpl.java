package com.base.server.prescription.business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.prescription.business.interfaces.PrescriptionBusiness;
import com.base.server.prescription.factory.PrescriptionFactory;
import com.base.server.prescription.persistence.interfaces.PrescriptionRepository;

import com.base.server.user.business.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PrescriptionBusinessImpl extends BaseBusinessImpl<Prescription, PrescriptionRepository> implements PrescriptionBusiness {
    private PrescriptionRepository prescriptionRepository;
    private PrescriptionFactory prescriptionFactory;

    @Autowired
    public PrescriptionBusinessImpl(PrescriptionRepository prescriptionRepository, PrescriptionFactory prescriptionFactory) {
        super(prescriptionRepository, prescriptionFactory);
        this.prescriptionRepository = prescriptionRepository;
        this.prescriptionFactory = prescriptionFactory;
    }

    @Override
    public List<Prescription> getPrescriptionsForUser(String id) {
        return prescriptionRepository.findAllForUser(id);
    }
}

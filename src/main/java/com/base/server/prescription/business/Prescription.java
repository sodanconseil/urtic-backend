package com.base.server.prescription.business;


import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.drug.business.Drug;
import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.business.Base;
import com.base.server.user.business.User;
import com.base.server.user.persistence.jpa.UserEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;


@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Prescription extends Base {

    private Drug drug;
    private User user;
    private Frequency frequency;
    private Dosage dosage;
    private int quantity;
    private int frequencyQuantity;

    @Builder(setterPrefix = "set")
    public Prescription(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser,
                        Timestamp modificationDate, Drug drug, User user, Frequency frequency, Dosage dosage, int quantity,
                        int frequencyQuantity) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.drug = drug;
        this.user = user;
        this.frequency = frequency;
        this.dosage = dosage;
        this.quantity = quantity;
        this.frequencyQuantity = frequencyQuantity;
    }
}

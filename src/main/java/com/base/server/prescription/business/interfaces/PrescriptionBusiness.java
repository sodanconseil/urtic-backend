package com.base.server.prescription.business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.prescription.business.Prescription;

import java.util.List;

public interface PrescriptionBusiness extends BaseBusiness<Prescription> {
    public List<Prescription> getPrescriptionsForUser(String id);
}

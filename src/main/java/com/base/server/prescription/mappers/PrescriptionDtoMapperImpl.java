package com.base.server.prescription.mappers;

import com.base.server.dosage.business.Dosage;
import com.base.server.dosage.persistence.interfaces.DosageRepository;
import com.base.server.drug.business.Drug;
import com.base.server.drug.mappers.DrugDtoMapper;
import com.base.server.drug.persistence.interfaces.DrugRepository;
import com.base.server.frequency.business.Frequency;
import com.base.server.frequency.mappers.FrequencyDtoMapper;
import com.base.server.frequency.persistence.interfaces.FrequencyRepository;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.presentation.dtos.PrescriptionRequestDto;
import com.base.server.prescription.presentation.dtos.PrescriptionResponseDto;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;

import com.base.server.user.business.User;
import com.base.server.user.persistence.interfaces.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
 *  Mapper genere par MapStruct mais modifie a la main pour faire le mapping de id vers leurs entites.
 *  Il faudrait trouver la facon de le faire avec MapStruct.
 */
@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-08-04T09:51:25-0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_282 (AdoptOpenJDK)"
)
@Component
public class PrescriptionDtoMapperImpl implements PrescriptionDtoMapper {

    @Autowired
    private DrugDtoMapper drugDtoMapper;
    @Autowired
    private DrugRepository drugRepository;
    @Autowired
    private DosageRepository dosageRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FrequencyRepository frequencyRepository;
    @Autowired
    FrequencyDtoMapper frequencyDtoMapper;

    @Override
    public Set<PrescriptionResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<Prescription> businessObjects) {
        if ( businessObjects == null ) {
            return null;
        }

        Set<PrescriptionResponseDto> set = new HashSet<PrescriptionResponseDto>( Math.max( (int) ( businessObjects.size() / .75f ) + 1, 16 ) );
        for ( Prescription prescription : businessObjects ) {
            set.add( businessObjectToResponseDto( prescription ) );
        }

        return set;
    }

    @Override
    public Prescription requestDtoToBusinessObject(PrescriptionRequestDto dto) {
        if ( dto == null ) {
            return null;
        }

        Prescription prescription = new Prescription();
        // Manual modification to MapStruct generated class
        prescription.setDrug( mapDrug( dto.getDrugId() ) );
        prescription.setUser( mapUser( dto.getUserId() ));
        prescription.setId( dto.getId() );
        prescription.setName( dto.getName() );
        prescription.setDescription( dto.getDescription() );
        prescription.setCreationUser( dto.getCreationUser() );
        prescription.setCreationDate( dto.getCreationDate() );
        prescription.setModificationUser( dto.getModificationUser() );
        prescription.setModificationDate( dto.getModificationDate() );
        prescription.setFrequency( mapFrequency( dto.getFrequencyId()));
        prescription.setDosage( mapDosage(dto.getDosageId()) );
        prescription.setQuantity(dto.getQuantity());
        prescription.setFrequencyQuantity(dto.getFrequencyQuantity());

        return prescription;
    }

    @Override
    public PrescriptionResponseDto businessObjectToResponseDto(Prescription businessObject) {
        if ( businessObject == null ) {
            return null;
        }

        PrescriptionResponseDto prescriptionResponseDto = new PrescriptionResponseDto();

        prescriptionResponseDto.setId( businessObject.getId() );
        prescriptionResponseDto.setName( businessObject.getName() );
        prescriptionResponseDto.setDescription( businessObject.getDescription() );
        prescriptionResponseDto.setCreationUser( businessObject.getCreationUser() );
        prescriptionResponseDto.setCreationDate( businessObject.getCreationDate() );
        prescriptionResponseDto.setModificationUser( businessObject.getModificationUser() );
        prescriptionResponseDto.setModificationDate( businessObject.getModificationDate() );
        prescriptionResponseDto.setDrugId( businessObject.getDrug().getId() );
        prescriptionResponseDto.setUserId( businessObject.getUser().getId() );
        prescriptionResponseDto.setFrequencyId( businessObject.getFrequency().getId() );
        prescriptionResponseDto.setDosageId( businessObject.getDosage().getId() );
        prescriptionResponseDto.setQuantity(businessObject.getQuantity());
        prescriptionResponseDto.setFrequencyQuantity(businessObject.getFrequencyQuantity());

        return prescriptionResponseDto;
    }

    @Override
    public List<PrescriptionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Prescription> businessObjects) {
        if ( businessObjects == null ) {
            return null;
        }

        List<PrescriptionResponseDto> list = new ArrayList<PrescriptionResponseDto>( businessObjects.size() );
        for ( Prescription prescription : businessObjects ) {
            list.add( businessObjectToResponseDto( prescription ) );
        }

        return list;
    }

    // Manual modification to MapStruct generated class
    public Drug mapDrug(String id) {
        return drugRepository.getOne(id);
    }

    public User mapUser(String id) {
        return userRepository.getOne(id);
    }

    public Dosage mapDosage(String id) {
        return dosageRepository.getOne(id);
    }

    public Frequency mapFrequency(String id) { return frequencyRepository.getOne(id); }
}

package com.base.server.prescription.mappers;

import java.util.List;

import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.persistence.jpa.PrescriptionEntity;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PrescriptionEntityMapper extends BaseEntityMapper<PrescriptionEntity, Prescription> {
    public Prescription entityToBusinessObject(PrescriptionEntity entity);
    public PrescriptionEntity businessObjectToEntity(Prescription object);
    public List<Prescription> listOfEntityToListOfBusinessObjects(List<PrescriptionEntity> entities);
}

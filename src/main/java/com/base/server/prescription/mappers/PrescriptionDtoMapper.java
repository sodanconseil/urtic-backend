package com.base.server.prescription.mappers;

import com.base.server.dosage.business.Dosage;
import com.base.server.drug.business.Drug;
import com.base.server.frequency.business.Frequency;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.presentation.dtos.PrescriptionRequestDto;
import com.base.server.prescription.presentation.dtos.PrescriptionResponseDto;
import com.base.server.user.business.User;

import java.util.List;


// Essais de faire le mapping id vers entite avec MapStruct.  Pas le temps donc le mapper a ete modifie a la main
//@Mapper(componentModel = "spring", uses = { PrescriptionMapperUtils.class })//DrugDtoMapper.class })
public interface PrescriptionDtoMapper extends BaseDtoMapper<Prescription, PrescriptionRequestDto, PrescriptionResponseDto>{

    // Essais de faire le mapping id vers entite avec MapStruct.  Pas le temps donc le mapper a ete modifie a la main
    //@Mapping(source = "drugId", target = "drug")
    Prescription requestDtoToBusinessObject(PrescriptionRequestDto dto);
    PrescriptionResponseDto businessObjectToResponseDto(Prescription businessObject);
    List<PrescriptionResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Prescription> businessObjects);

    Drug mapDrug(String id);
    User mapUser(String id);
    Dosage mapDosage(String id);
}

package com.base.server.ethnicity.presentation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.ethnicity.factory.EthnicityFactory;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.managers.EthnicityDeleteManager;
import com.base.server.ethnicity.presentation.managers.EthnicityEtagManager;
import com.base.server.ethnicity.presentation.managers.EthnicityExistenceManager;
import com.base.server.ethnicity.presentation.managers.EthnicityGetAllManager;
import com.base.server.ethnicity.presentation.managers.EthnicityGetByIdManager;
import com.base.server.ethnicity.presentation.managers.EthnicityPostManager;
import com.base.server.ethnicity.presentation.managers.EthnicityPutManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ethnicities")
public class EthnicityController {

    private EthnicityFactory ethnicityFactory;

    private ValidationManager<EthnicityRequestDto> validationManager;

    private EthnicityExistenceManager ethnicityExistenceManager;
    private EthnicityEtagManager ethnicityEtagManager;
    private EthnicityPostManager ethnicityPostManager;
    private EthnicityGetAllManager ethnicityGetAllManager;
    private EthnicityGetByIdManager ethnicityGetByIdManager;
    private EthnicityPutManager ethnicityPutManager;
    private EthnicityDeleteManager ethnicityDeleteManager;

    @Autowired
    public EthnicityController(EthnicityFactory ethnicityFactory, ValidationManager<EthnicityRequestDto> validationManager, EthnicityExistenceManager ethnicityExistenceManage, EthnicityEtagManager ethnicityEtagManager, EthnicityGetAllManager ethnicityGetAllManager, EthnicityPostManager ethnicityPostManager, EthnicityGetByIdManager ethnicityGetByIdManager, EthnicityPutManager ethnicityPutManager, EthnicityDeleteManager ethnicityDeleteManager) {
        this.validationManager = validationManager;
        this.ethnicityFactory = ethnicityFactory;
        this.ethnicityExistenceManager = ethnicityExistenceManage;
        this.ethnicityEtagManager = ethnicityEtagManager;
        this.ethnicityGetAllManager = ethnicityGetAllManager;
        this.ethnicityPostManager = ethnicityPostManager;
        this.ethnicityGetByIdManager = ethnicityGetByIdManager;
        this.ethnicityPutManager = ethnicityPutManager;
        this.ethnicityDeleteManager = ethnicityDeleteManager;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        RequestInformation<EthnicityRequestDto> requestInformation = ethnicityFactory.newRequestInformation();
        return ethnicityFactory.newRequestPipeline(ethnicityGetAllManager).manage(requestInformation);
    }

    @GetMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getEthnicity(@PathVariable String id, HttpServletRequest request) {
        
        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);
        RequestInformation<EthnicityRequestDto> requestInformation = ethnicityFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");
        return ethnicityFactory.newRequestPipeline(ethnicityExistenceManager)
            .addManager(ethnicityGetByIdManager)
            .manage(requestInformation);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postEthnicity(@Valid @RequestBody EthnicityRequestDto dto, Errors errors, HttpServletRequest request) {
        RequestInformation<EthnicityRequestDto> requestInformation = ethnicityFactory.newRequestInformation(null, dto, request, errors, null, "toto");

        return ethnicityFactory.newRequestPipeline(validationManager)
            .addManager(ethnicityPostManager)
            .manage(requestInformation);
    }

    @PutMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> putEthnicity(@PathVariable String id, @Valid @RequestBody EthnicityRequestDto dto,
			Errors errors, HttpServletRequest request, @RequestHeader(name = "If-Match") String if_match) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<EthnicityRequestDto> requestInformation = ethnicityFactory.newRequestInformation(pathVariables, dto, request, errors, if_match, "toto");

        return ethnicityFactory.newRequestPipeline(validationManager)
            .addManager(ethnicityExistenceManager)
            .addManager(ethnicityEtagManager)
            .addManager(ethnicityPutManager)
            .manage(requestInformation);
    }

    @DeleteMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteEthnicity(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<EthnicityRequestDto> requestInformation = ethnicityFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");

        return ethnicityFactory.newRequestPipeline(ethnicityExistenceManager)
            .addManager(ethnicityDeleteManager)
            .manage(requestInformation);
    }
}

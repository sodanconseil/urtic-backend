package com.base.server.ethnicity.presentation.dtos;

import java.sql.Timestamp;

import com.base.server.generic.presentation.dtos.BaseResponseDto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EthnicityResponseDto extends BaseResponseDto {

    private String en;
    private String fr;
    
    @Builder(setterPrefix = "set")
    public EthnicityResponseDto(String id, String name, String description, String creationUser, Timestamp creationDate,
            String modificationUser, Timestamp modificationDate, String en, String fr) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.fr = fr;
        this.en = en;
    }
}

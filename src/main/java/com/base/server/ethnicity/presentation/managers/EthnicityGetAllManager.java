package com.base.server.ethnicity.presentation.managers;

import java.util.List;
import java.util.Optional;

import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class EthnicityGetAllManager implements RequestManager<EthnicityRequestDto> {

    private EthnicityBusiness ethnicityBusiness;
    private EthnicityDtoMapper ethnicityDtoMapper;

    @Autowired
    public EthnicityGetAllManager(EthnicityBusiness ethnicityBusiness, EthnicityDtoMapper ethnicityDtoMapper) {
        this.ethnicityBusiness = ethnicityBusiness;
        this.ethnicityDtoMapper = ethnicityDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<EthnicityRequestDto> requestInformation) {
        List<Ethnicity> ethnicitys = ethnicityBusiness.getAll();
        List<EthnicityResponseDto> ethnicityResponseDto = ethnicityDtoMapper.listOfBusinessObjectsToListOfResponseDto(ethnicitys);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDto);

        return Optional.of(responseEntity);
    }
}

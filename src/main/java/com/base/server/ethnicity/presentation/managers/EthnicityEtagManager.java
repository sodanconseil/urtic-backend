package com.base.server.ethnicity.presentation.managers;

import java.util.Optional;

import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class EthnicityEtagManager implements RequestManager<EthnicityRequestDto> {

    private EthnicityBusiness ethnicityBusiness;
    private EthnicityDtoMapper ethnicityDtoMapper;
    private ErrorMapper errorMapper;

    
    @Autowired
    public EthnicityEtagManager(EthnicityBusiness ethnicityBusiness, EthnicityDtoMapper ethnicityDtoMapper, ErrorMapper errorMapper) {
        this.ethnicityBusiness = ethnicityBusiness;
        this.ethnicityDtoMapper = ethnicityDtoMapper;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<EthnicityRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        String receivedEtag = requestInformation.getIf_match();
        String currentEtag = ethnicityDtoMapper.getEtag(ethnicityBusiness.get(requestInformation.getId()));

        if (!receivedEtag.equals(currentEtag)) {
            HttpStatus status = HttpStatus.PRECONDITION_FAILED;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity was changed, GET the new version";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }    
}

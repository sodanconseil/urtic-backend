package com.base.server.ethnicity.business;

import java.sql.Timestamp;

import com.base.server.generic.business.Base;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Ethnicity extends Base {

    private String en;
    private String fr;

    @Builder(setterPrefix = "set")
    public Ethnicity(String id, Long version, String name, String description, String creationUser,
            Timestamp creationDate, String modificationUser, Timestamp modificationDate, String en, String fr) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.fr = fr;
        this.en = en;
    }
}

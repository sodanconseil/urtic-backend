package com.base.server.ethnicity.business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.ethnicity.business.Ethnicity;

public interface EthnicityBusiness extends BaseBusiness<Ethnicity> {
}

package com.base.server.ethnicity.business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.factory.EthnicityFactory;
import com.base.server.ethnicity.persistence.interfaces.EthnicityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EthnicityBusinessImpl extends BaseBusinessImpl<Ethnicity, EthnicityRepository> implements EthnicityBusiness{

    private EthnicityRepository ethnicityRepository;
    private EthnicityFactory ethnicityFactory;

    @Autowired
    public EthnicityBusinessImpl(EthnicityRepository ethnicityRepository, EthnicityFactory factory) {
        super(ethnicityRepository, factory);
        this.ethnicityRepository = ethnicityRepository;
    }
}

package com.base.server.ethnicity.persistence.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.ethnicity.business.Ethnicity;

public interface EthnicityRepository extends BaseRepository<Ethnicity> 
{  
}
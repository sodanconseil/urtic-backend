package com.base.server.ethnicity.persistence.jpa;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.user.persistence.jpa.UserEntity;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ethnicity")
@Data
@EqualsAndHashCode(callSuper=true, exclude = "users")
@NoArgsConstructor
public class EthnicityEntity extends BaseEntity<EthnicityEntity> implements Copyable<EthnicityEntity> {

    @ManyToMany(mappedBy = "ethnicities")
    private Set<UserEntity> users;

    private String en;
    private String fr;

    @Builder(setterPrefix = "set")
    public EthnicityEntity(String id, String name, String description, String creationUser, Timestamp creationDate,
            String modificationUser, Timestamp modificationDate, Long version, Set<UserEntity> users, String en, String fr) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.users = users == null ? new HashSet<>() : users;
        this.fr = fr;
        this.en = en;
    }

    @Override
    public void copy(EthnicityEntity ethnicityEntity) {
        super.copy(ethnicityEntity);
        this.users = ethnicityEntity.users;
        this.fr = ethnicityEntity.fr;
        this.en = ethnicityEntity.en;
    }

    @Override
    public String toString() {
        // TODO: a ameliorer, le toString() par defaut de lombok cause des stackoverflow sur le toString()
        return "EthnicityEntity";
    }
}

package com.base.server.ethnicity.persistence;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.mappers.EthnicityEntityMapper;
import com.base.server.ethnicity.persistence.interfaces.EthnicityRepository;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EthnicityRepositoryImpl extends BaseRepositoryImpl<EthnicityEntity, Ethnicity> implements EthnicityRepository {
    private EthnicityDal dal;
    private EthnicityEntityMapper mapper;

    @Autowired
    public EthnicityRepositoryImpl(EthnicityDal dal, EthnicityEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }
}

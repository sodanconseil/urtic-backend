package com.base.server.ethnicity.persistence.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;

import org.springframework.stereotype.Component;

@Component
public interface EthnicityDal extends BaseDal<EthnicityEntity>  {
} 

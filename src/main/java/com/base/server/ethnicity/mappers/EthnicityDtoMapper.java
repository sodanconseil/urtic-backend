package com.base.server.ethnicity.mappers;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;

import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface EthnicityDtoMapper extends BaseDtoMapper<Ethnicity, EthnicityRequestDto, EthnicityResponseDto> {
    Ethnicity requestDtoToBusinessObject(EthnicityRequestDto dto);
    EthnicityResponseDto businessObjectToResponseDto(Ethnicity businessObject);
    List<EthnicityResponseDto> listOfBusinessObjectsToListOfResponseDto(List<Ethnicity> businessObjects);
    Set<EthnicityResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<Ethnicity> businessObjects);
}

package com.base.server.ethnicity.mappers;


import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EthnicityEntityMapper extends BaseEntityMapper<EthnicityEntity, Ethnicity> {
    public Ethnicity entityToBusinessObject(EthnicityEntity entity);
    public EthnicityEntity businessObjectToEntity(Ethnicity object);
    public List<Ethnicity> listOfEntityToListOfBusinessObjects(List<EthnicityEntity> entities);
}
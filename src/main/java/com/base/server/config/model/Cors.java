package com.base.server.config.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "cross-origin-resource-sharing")
public class Cors {

	private List<String> allowedOrigin = new ArrayList<String>(Arrays.asList("Localhost:8083"));
	private List<String> allowedHeaders = new ArrayList<String>();
	private List<String> exposedHeaders = new ArrayList<String>(Arrays.asList("ETag"));
	private List<String> allowedMethods = new ArrayList<String>(Arrays.asList("*"));
	private String mapping = "/**";
	private long maxAge = 600;

	public List<String> getAllowedOrigin() {
		return this.allowedOrigin;
	}

	public List<String> getAllowedHeaders() {
		return this.allowedHeaders;
	}

	public List<String> getExposedHeaders() {
		return this.exposedHeaders;
	}

	public List<String> getAllowedMethods() {
		return this.allowedMethods;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}

	public long getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(long maxAge) {
		this.maxAge = maxAge;
	}
}

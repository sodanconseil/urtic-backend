package com.base.server.config.roles;

public class Roles {

	public static final String POC_ROLE_NAME = "poc";
	public static final String ALL_ROLE_NAME = "all";
	public static final String ADMIN_ROLE_NAME = "admin";
	
	public static final String POC = "hasRole('" + POC_ROLE_NAME + "') or hasRole('" + ADMIN_ROLE_NAME + "')";
	public static final String ALL = "hasRole('" + ALL_ROLE_NAME + "') or hasRole('" + ADMIN_ROLE_NAME + "')";
	public static final String ADMIN = "hasRole('" + ADMIN_ROLE_NAME + "')";
}

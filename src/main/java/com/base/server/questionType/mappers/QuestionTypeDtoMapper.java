package com.base.server.questionType.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.presentation.dtos.QuestionRequestDto;
import com.base.server.Question.presentation.dtos.QuestionResponseDto;
import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.questionType.presentation.dtos.QuestionTypeRequestDto;
import com.base.server.questionType.presentation.dtos.QuestionTypeResponseDto;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface QuestionTypeDtoMapper extends BaseDtoMapper<QuestionType, QuestionTypeRequestDto, QuestionTypeResponseDto> {
    QuestionType requestDtoToBusinessObject(QuestionTypeRequestDto dto);
    QuestionTypeResponseDto businessObjectToResponseDto(QuestionType businessObject);
    List<QuestionTypeResponseDto> listOfBusinessObjectsToListOfResponseDto(List<QuestionType> businessObjects);
    Set<QuestionTypeResponseDto> setOfBusinessObjectsToSetOfResponseDto(Set<QuestionType> businessObjects);
}

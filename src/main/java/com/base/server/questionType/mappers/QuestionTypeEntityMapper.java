package com.base.server.questionType.mappers;

import com.base.server.Question.Business.Question;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.questionType.persistance.jpa.QuestionTypeEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface QuestionTypeEntityMapper extends BaseEntityMapper<QuestionTypeEntity, QuestionType> {
    public QuestionType entityToBusinessObject(QuestionTypeEntity entity);
    public QuestionTypeEntity businessObjectToEntity(QuestionType object);
    public List<QuestionType> listOfEntityToListOfBusinessObjects(List<QuestionTypeEntity> entities);

}

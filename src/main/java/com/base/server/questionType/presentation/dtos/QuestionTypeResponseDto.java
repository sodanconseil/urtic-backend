package com.base.server.questionType.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseResponseDto;
import com.base.server.questionVersion.Business.QuestionVersion;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class QuestionTypeResponseDto extends BaseResponseDto {
    private int typeCode;

    @Builder(setterPrefix = "set")
    public QuestionTypeResponseDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, int typeCode) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.typeCode = typeCode;
    }
}

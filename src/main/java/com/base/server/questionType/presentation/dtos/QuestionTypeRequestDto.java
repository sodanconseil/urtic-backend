package com.base.server.questionType.presentation.dtos;

import com.base.server.generic.presentation.dtos.BaseRequestDto;
import com.base.server.questionVersion.Business.QuestionVersion;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class QuestionTypeRequestDto extends BaseRequestDto {

    private int typeCode;

    @Builder(setterPrefix = "set")
    public QuestionTypeRequestDto(String id, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, int typeCode) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.typeCode = typeCode;
    }
}

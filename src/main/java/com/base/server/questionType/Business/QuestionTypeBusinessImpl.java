package com.base.server.questionType.Business;

import com.base.server.Question.Business.Question;
import com.base.server.Question.Business.interfaces.QuestionBusiness;
import com.base.server.Question.factory.QuestionFactory;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.questionType.Business.interfaces.QuestionTypeBusiness;
import com.base.server.questionType.factory.QuestionTypeFactory;
import com.base.server.questionType.persistance.interfaces.QuestionTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuestionTypeBusinessImpl extends BaseBusinessImpl<QuestionType, QuestionTypeRepository> implements QuestionTypeBusiness {

    private QuestionTypeRepository questionTypeRepository;
    private QuestionTypeFactory questionTypeFactory;

    @Autowired
    public QuestionTypeBusinessImpl(QuestionTypeRepository questionTypeRepository, QuestionTypeFactory factory) {
        super(questionTypeRepository, factory);
        this.questionTypeRepository = questionTypeRepository;
        this.questionTypeFactory = questionTypeFactory;
    }

}

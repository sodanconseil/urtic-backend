package com.base.server.questionType.Business;


import com.base.server.generic.business.Base;
import com.base.server.questionVersion.Business.QuestionVersion;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class QuestionType extends Base {
    private int typeCode;

    @Builder(setterPrefix = "set")
    public QuestionType(String id, Long version, @NotNull String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, int typeCode) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.typeCode = typeCode;
    }
}

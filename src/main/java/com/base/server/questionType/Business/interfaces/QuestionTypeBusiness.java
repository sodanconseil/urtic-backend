package com.base.server.questionType.Business.interfaces;

import com.base.server.questionType.Business.QuestionType;
import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.questionVersion.Business.QuestionVersion;
import lombok.Builder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Component
public interface QuestionTypeBusiness extends BaseBusiness<QuestionType> {
}

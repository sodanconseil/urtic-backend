package com.base.server.questionType.persistance;

import com.base.server.Question.Business.Question;
import com.base.server.Question.mappers.QuestionEntityMapper;
import com.base.server.Question.persistance.interfaces.QuestionRepository;
import com.base.server.Question.persistance.jpa.QuestionDal;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.questionType.Business.QuestionType;
import com.base.server.questionType.mappers.QuestionTypeEntityMapper;
import com.base.server.questionType.persistance.interfaces.QuestionTypeRepository;
import com.base.server.questionType.persistance.jpa.QuestionTypeDal;
import com.base.server.questionType.persistance.jpa.QuestionTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class QuestionTypeRepositoryImpl extends BaseRepositoryImpl<QuestionTypeEntity, QuestionType> implements QuestionTypeRepository {
    private QuestionTypeDal dal;
    private QuestionTypeEntityMapper mapper;

    @Autowired
    public QuestionTypeRepositoryImpl(QuestionTypeDal dal, QuestionTypeEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }
}

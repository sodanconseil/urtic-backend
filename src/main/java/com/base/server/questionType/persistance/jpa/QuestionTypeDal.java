package com.base.server.questionType.persistance.jpa;

import com.base.server.generic.persistence.jpa.BaseDal;
import org.springframework.stereotype.Component;

@Component
public interface QuestionTypeDal extends BaseDal<QuestionTypeEntity> {
    public QuestionTypeEntity findByTypeCode(int typeCode);

    public QuestionTypeEntity findByName(String name);

}

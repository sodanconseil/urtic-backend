package com.base.server.questionType.persistance.interfaces;

import com.base.server.questionType.Business.QuestionType;
import com.base.server.generic.persistence.interfaces.BaseRepository;
import org.springframework.stereotype.Component;

@Component
public interface QuestionTypeRepository extends BaseRepository<QuestionType> {
}

package com.base.server.questionType.persistance.jpa;

import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/*
Type of questions. Question types are:
0. Calculated;
1. Single choice;
2. Multiple choices;
3. Int value (E.g. Hear rate);
4. Int range value (E.g. Blood pressure with diastolic and systolic values, respiration rate per minute);
5. Float value (E.g. Temperature);
6. Float range value;
7. String value;
8. Date value;
9. Date range value.
 */

@Entity
@Table(name = "question_type")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class QuestionTypeEntity extends BaseEntity<QuestionTypeEntity> implements Copyable<QuestionTypeEntity> {

    /*
    Code used to identify the question type. E.g.: 1.
     */
    private int typeCode;
    /*
    Name of the question type. E.g. Single choice question.
     */
   // private String name;

    @OneToMany(mappedBy = "questionType", cascade = {CascadeType.ALL})
    private List<QuestionVersionEntity> questionVersionEntity = new ArrayList<>();

    @Builder(setterPrefix = "set")
    public QuestionTypeEntity(String id, String name, String description, String creationUser, Timestamp creationDate, String modificationUser, Timestamp modificationDate, Long version, int typeCode, String name1, List<QuestionVersionEntity> questionVersionEntity) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.typeCode = typeCode;
        this.questionVersionEntity = questionVersionEntity;
    }



    @Override
    public void copy(QuestionTypeEntity questionTypeEntity) {
        super.copy(questionTypeEntity);
        this.typeCode = questionTypeEntity.typeCode;
        this.questionVersionEntity = questionTypeEntity.questionVersionEntity;
    }
}

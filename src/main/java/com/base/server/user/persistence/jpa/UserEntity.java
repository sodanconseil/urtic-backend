package com.base.server.user.persistence.jpa;

import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.generic.persistence.interfaces.Copyable;
import com.base.server.generic.persistence.jpa.BaseEntity;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.persistence.jpa.PrescriptionEntity;
import com.base.server.user.business.Gender;
import com.base.server.user.business.UserType;
import com.base.server.usersurvey.persistance.jpa.UserSurveyEntity;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public class UserEntity extends BaseEntity<UserEntity> implements Copyable<UserEntity> {
    private String keycloakId;
    private String firstName;
    private String middleName;
    private String email;
    private Timestamp birthday;
    private String phoneNumber;
    private Gender gender;
    private UserType type;
    private boolean emailValid;
    private boolean phoneNumberValid;
    private Timestamp lastAuthentication;
    private String prefferedLanguage;

    // On a enleve les prescriptions du user car elles causent un stackoverflow dans les Mappers.
    // Il est possible d'aller les chercher avec la route: /users/id/prescriptions
    //@OneToMany(mappedBy = "user")
    //private List<PrescriptionEntity> prescriptions = new Vector<PrescriptionEntity>();

  //  @OneToOne(mappedBy = "user")
  //  private UserSurveyEntity userSurveyEntity;


    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "users_ethnicity", 
               joinColumns = @JoinColumn(name = "users_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "ethnicity_id", referencedColumnName = "id"))
    private Set<EthnicityEntity> ethnicities;

    @Builder(setterPrefix = "set")
    public UserEntity(String keycloakId, String id, String name, String description, String creationUser, Timestamp creationDate,
            String modificationUser, Timestamp modificationDate, Long version, String firstName, String middleName,
            String email, Timestamp birthday, String phoneNumber, Gender gender, UserType type, boolean emailValid,
            boolean phoneNumberValid, Timestamp lastAuthentication,  Set<EthnicityEntity> ethnicities,
                      List<PrescriptionEntity> prescriptions, String prefferedLanguage) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate, version);
        this.keycloakId = keycloakId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.email = email;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.type = type;
        this.emailValid = emailValid;
        this.phoneNumberValid = phoneNumberValid;
        this.ethnicities = ethnicities == null ? new HashSet<>() : ethnicities;
        this.prefferedLanguage = prefferedLanguage;
        //this.prescriptions = prescriptions;
    }


    @Override
    public void copy(UserEntity userEntity) {
        super.copy(userEntity);
        this.keycloakId = userEntity.keycloakId;
        this.firstName = userEntity.firstName;
        this.middleName = userEntity.middleName;
        this.email = userEntity.email;
        this.birthday = userEntity.birthday;
        this.phoneNumber = userEntity.phoneNumber;
        this.gender = userEntity.gender;
        this.type = userEntity.type;
        this.emailValid = userEntity.emailValid;
        this.phoneNumberValid = userEntity.phoneNumberValid;
        this.lastAuthentication = userEntity.lastAuthentication;
        this.ethnicities = userEntity.ethnicities;
        this.prefferedLanguage = prefferedLanguage;
        //this.prescriptions = userEntity.prescriptions;
    }

    @Override
    public String toString() {
        // TODO: a ameliorer, le toString() par defaut de lombok cause des stackoverflow sur le toString()
        return "UserEntity";
    }
}

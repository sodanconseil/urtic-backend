package com.base.server.user.persistence.interfaces;

import com.base.server.generic.persistence.interfaces.BaseRepository;
import com.base.server.user.business.User;

public interface UserRepository extends BaseRepository<User> 
{  
    User getByKeycloakId(String keycloakId);
}
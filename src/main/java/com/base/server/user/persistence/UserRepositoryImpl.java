package com.base.server.user.persistence;

import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityNotFoundException;

import com.base.server.generic.persistence.BaseRepositoryImpl;
import com.base.server.user.business.User;
import com.base.server.user.mappers.UserEntityMapper;
import com.base.server.user.persistence.interfaces.UserRepository;
import com.base.server.user.persistence.jpa.UserDal;
import com.base.server.user.persistence.jpa.UserEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class UserRepositoryImpl extends BaseRepositoryImpl<UserEntity, User> implements UserRepository {
    private UserDal dal;
    private UserEntityMapper mapper;

    @Autowired
    public UserRepositoryImpl(UserDal dal, UserEntityMapper mapper) {
        super(dal, mapper);
        this.dal = dal;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public User getOne(String id) { 
        return super.getOne(id);
    }

    @Override
    @Transactional
    public List<User> findAll() {
        return super.findAll();
    }

    @Override
    public User getByKeycloakId(String keycloakId) {
        try {
            return mapper.entityToBusinessObject(dal.findByKeycloakId(keycloakId).get());
        } catch (EntityNotFoundException ex) { 
            return null;
        } catch(NoSuchElementException ex) {
            return null;
        }
    }
}

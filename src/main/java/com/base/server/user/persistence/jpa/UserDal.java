package com.base.server.user.persistence.jpa;

import java.util.Optional;

import com.base.server.generic.persistence.jpa.BaseDal;

import org.springframework.stereotype.Component;

@Component
public interface UserDal extends BaseDal<UserEntity>  {
    public Optional<UserEntity> findByKeycloakId(String keycloakId);
} 

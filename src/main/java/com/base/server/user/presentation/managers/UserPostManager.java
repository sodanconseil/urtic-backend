package com.base.server.user.presentation.managers;

import java.util.Optional;

import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.mappers.UserDtoMapper;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserPostManager implements RequestManager<UserRequestDto> {

    private UserBusiness userBusiness;
    private UserDtoMapper userDtoMapper;

    @Autowired
    public UserPostManager(UserBusiness userBusiness, UserDtoMapper userDtoMapper) {
        this.userBusiness = userBusiness;
        this.userDtoMapper = userDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UserRequestDto> requestInformation) {

        String userId = requestInformation.getUserId();
        UserRequestDto userRequestDto = requestInformation.getDto();
        User user = userBusiness.save(userDtoMapper.requestDtoToBusinessObject(userRequestDto), userId);
        UserResponseDto userResponseDto = userDtoMapper.businessObjectToResponseDto(user);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK)
            .eTag(userDtoMapper.getEtag(user))
            .body(userResponseDto);

        return Optional.of(responseEntity);
    }
}

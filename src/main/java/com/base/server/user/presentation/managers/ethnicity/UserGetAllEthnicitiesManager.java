package com.base.server.user.presentation.managers.ethnicity;


import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.mappers.UserDtoMapper;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserGetAllEthnicitiesManager implements RequestManager<EthnicityRequestDto> {
    
    private UserBusiness userBusiness;
    private EthnicityDtoMapper ethnicityDtoMapper;

    @Autowired
    public UserGetAllEthnicitiesManager(UserBusiness userBusiness, EthnicityDtoMapper ethnicityDtoMapper) { 
        this.userBusiness = userBusiness;
        this.ethnicityDtoMapper = ethnicityDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<EthnicityRequestDto> requestInformation) {
        User user = userBusiness.get(requestInformation.getId());
        Set<Ethnicity> ethnicities = user.getEthnicities();
        Set<EthnicityResponseDto> ethnicityResponseDtos = ethnicityDtoMapper.setOfBusinessObjectsToSetOfResponseDto(ethnicities);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDtos);
        return Optional.of(responseEntity);
    }
}

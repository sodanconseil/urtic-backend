package com.base.server.user.presentation.managers;

import java.util.List;
import java.util.Optional;

import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.mappers.UserDtoMapper;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserGetAllManager implements RequestManager<UserRequestDto> {

    private UserBusiness userBusiness;
    private UserDtoMapper userDtoMapper;

    @Autowired
    public UserGetAllManager(UserBusiness userBusiness, UserDtoMapper userDtoMapper) {
        this.userBusiness = userBusiness;
        this.userDtoMapper = userDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UserRequestDto> requestInformation) {
        List<User> users = userBusiness.getAll();
        List<UserResponseDto> userResponseDto = userDtoMapper.listOfBusinessObjectsToListOfResponseDto(users);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(userResponseDto);

        return Optional.of(responseEntity);
    }
}

package com.base.server.user.presentation.managers.ethnicity;

import java.util.Optional;

import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class UserVerifyEthnicityExistenceManager implements RequestManager<EthnicityRequestDto> {
    private EthnicityBusiness ethnicityBusiness;
    private ErrorMapper errorMapper;

    @Autowired
    public UserVerifyEthnicityExistenceManager(EthnicityBusiness ethnicityBusiness, ErrorMapper errorMapper) {
        this.ethnicityBusiness = ethnicityBusiness;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<EthnicityRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        if (ethnicityBusiness.get(requestInformation.getPathVariables().get("ethnicityId")) == null) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The ethnicity with the specified id wasn't found";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }
}

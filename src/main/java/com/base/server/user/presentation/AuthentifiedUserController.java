package com.base.server.user.presentation;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.business.interfaces.PrescriptionBusiness;
import com.base.server.prescription.mappers.PrescriptionDtoMapper;
import com.base.server.prescription.presentation.dtos.PrescriptionResponseDto;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.generic.services.UserService;
import com.base.server.prescription.business.interfaces.PrescriptionBusiness;
import com.base.server.prescription.mappers.PrescriptionDtoMapper;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.factory.UserFactory;
import com.base.server.user.mappers.UserDtoMapper;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;
import com.base.server.user.presentation.managers.UserDeleteManager;
import com.base.server.user.presentation.managers.UserEtagManager;
import com.base.server.user.presentation.managers.UserExistenceManager;
import com.base.server.user.presentation.managers.UserGetAllManager;
import com.base.server.user.presentation.managers.UserGetByIdManager;
import com.base.server.user.presentation.managers.UserPostManager;
import com.base.server.user.presentation.managers.UserPutManager;
import com.base.server.user.presentation.managers.ethnicity.UserDeleteEthnicityManager;
import com.base.server.user.presentation.managers.ethnicity.UserGetAllEthnicitiesManager;
import com.base.server.user.presentation.managers.ethnicity.UserPutEthnicityManager;
import com.base.server.user.presentation.managers.ethnicity.UserVerifyEthnicityExistenceManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/me")
public class AuthentifiedUserController {

    private UserBusiness userBusiness;
    private UserService userService;
    private UserDtoMapper userDtoMapper;

    private EthnicityBusiness ethnicityBusiness;
    private EthnicityDtoMapper ethnicityDtoMapper;

    private PrescriptionDtoMapper prescriptionDtoMapper;
    private PrescriptionBusiness prescriptionBusiness;

    private ErrorMapper errorMapper;

    @Autowired
    public AuthentifiedUserController(UserBusiness userBusiness, UserService userService, UserDtoMapper userDtoMapper,
            EthnicityBusiness ethnicityBusiness, EthnicityDtoMapper ethnicityDtoMapper,
            PrescriptionDtoMapper prescriptionDtoMapper, PrescriptionBusiness prescriptionBusiness,
            ErrorMapper errorMapper) {
        this.userBusiness = userBusiness;
        this.userService = userService;
        this.userDtoMapper = userDtoMapper;
        this.ethnicityBusiness = ethnicityBusiness;
        this.ethnicityDtoMapper = ethnicityDtoMapper;
        this.prescriptionDtoMapper = prescriptionDtoMapper;
        this.prescriptionBusiness = prescriptionBusiness;
        this.errorMapper = errorMapper;
    }

    // Todo vérifier que le token est présent et valide
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getKeycloakUser(HttpServletRequest request) {

        ResponseEntity<Object> toReturn;

        String userId = userService.getUserId();

        if (userId == null) {

            HttpStatus status = HttpStatus.FORBIDDEN;
            String uri = request.getRequestURI();
            String errorString = "Missing authorization (bearer) token";

            toReturn = ResponseEntity.status(status)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
        } else {

            if (userBusiness.getByKeycloakId(userId) == null) {
                HttpStatus status = HttpStatus.NOT_FOUND;
                String uri = request.getRequestURI();
                String errorString = "The user with associated with this token wasn't found, please create one";

                toReturn = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
            } else {
                User user = userBusiness.getByKeycloakId(userService.getUserId());
                UserResponseDto userResponseDto = userDtoMapper.businessObjectToResponseDto(user);

                toReturn = ResponseEntity.status(HttpStatus.OK).eTag(userDtoMapper.getEtag(user)).body(userResponseDto);
            }
        }
        return toReturn;
    }

    // TODO vérifier que le token est présent et valide
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postKeycloakUser(@Valid @RequestBody UserRequestDto dto, Errors errors,
            HttpServletRequest request) {

        ResponseEntity<Object> toReturn;

        String userId = userService.getUserId();

        if (userId == null) {

            HttpStatus status = HttpStatus.FORBIDDEN;
            String uri = request.getRequestURI();
            String errorString = "Missing authorization (bearer) token";

            toReturn = ResponseEntity.status(status)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
        } else {

            if (userBusiness.getByKeycloakId(userId) != null) {
                HttpStatus status = HttpStatus.CONFLICT;
                String uri = request.getRequestURI();
                String errorString = "The user associated with this token already exists";

                toReturn = ResponseEntity.status(status)
                        .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
            } else {
                User user = userDtoMapper.requestDtoToBusinessObject(dto);
                user.setKeycloakId(userService.getUserId());
                user.setFirstName(userService.getUserName());
                user.setName(userService.getUserFamilyName());
                user.setEmail(userService.getEmail());
                user = userBusiness.save(user, userId);
                UserResponseDto userResponseDto = userDtoMapper.businessObjectToResponseDto(user);

                toReturn = ResponseEntity.status(HttpStatus.OK).eTag(userDtoMapper.getEtag(user)).body(userResponseDto);
            }
        }

        return toReturn;
    }

    //
    // Mappings pour l'ethnicite
    //

    // TODO vérifier qu'un user a été créé pour le token donné en param
    @GetMapping(path = "/ethnicities", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllEthnicities(HttpServletRequest request) {
        ResponseEntity<Object> toReturn;

        String userId = userService.getUserId();

        if (userId == null) {

            HttpStatus status = HttpStatus.FORBIDDEN;
            String uri = request.getRequestURI();
            String errorString = "Missing authorization (bearer) token";

            toReturn = ResponseEntity.status(status)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
        } else {

            if (userBusiness.getByKeycloakId(userId) == null) {
                HttpStatus status = HttpStatus.NOT_FOUND;
                String uri = request.getRequestURI();
                String errorString = "The user with associated with this token wasn't found, please create one";

                toReturn = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
            } else {

                User user = userBusiness.getByKeycloakId(userService.getUserId());
                Set<Ethnicity> ethnicities = user.getEthnicities();
                Set<EthnicityResponseDto> ethnicityResponseDtos = ethnicityDtoMapper
                        .setOfBusinessObjectsToSetOfResponseDto(ethnicities);
                toReturn = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDtos);
            }
        }

        return toReturn;
    }

    // TODO vérifier qu'un user a été créé pour le token donné en param
    @PutMapping(path = "/ethnicities/{ethnicityId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postUserEthnicity(@PathVariable String ethnicityId, HttpServletRequest request) {
        ResponseEntity<Object> toReturn;

        if (ethnicityBusiness.get(ethnicityId) == null) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            String uri = request.getRequestURI();
            String errorString = "The ethnicity with the specified id wasn't found";

            toReturn = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
        } else {

            User user = userBusiness.getByKeycloakId(userService.getUserId());
            Ethnicity ethnicity = ethnicityBusiness.get(ethnicityId);
            user.getEthnicities().add(ethnicity);
            userBusiness.modify(user.getId(), user, user.getId());
            EthnicityResponseDto ethnicityResponseDto = ethnicityDtoMapper.businessObjectToResponseDto(ethnicity);

            toReturn = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDto);
        }

        return toReturn;
    }

    // TODO vérifier qu'un user a été créé pour le token donné en param
    @DeleteMapping(path = "/ethnicities/{ethnicityId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteUserEthnicity(@PathVariable String ethnicityId, HttpServletRequest request) {

        ResponseEntity<Object> toReturn;

        if (ethnicityBusiness.get(ethnicityId) == null) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            String uri = request.getRequestURI();
            String errorString = "The ethnicity with the specified id wasn't found";

            toReturn = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));
        } else {
            User user = userBusiness.get(userService.getUserId());
            Set<Ethnicity> ethnicities = user.getEthnicities();
            Set<Ethnicity> filteredEthnicities = ethnicities.stream()
                    .filter(ethnicity -> !ethnicity.getId().equals(ethnicityId)).collect(Collectors.toSet());
            user.setEthnicities(filteredEthnicities);
            userBusiness.modify(userService.getUserId(), user, userService.getUserId());

            Set<Ethnicity> difference = new HashSet<>(ethnicities);
            difference.removeAll(filteredEthnicities);
            Ethnicity deletedEthnicity = difference.toArray(new Ethnicity[1])[0];
            EthnicityResponseDto ethnicityResponseDto = ethnicityDtoMapper
                    .businessObjectToResponseDto(deletedEthnicity);

            toReturn = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDto);
        }

        return toReturn;
    }

    //
    // Mappings pour prescriptions
    //

    @GetMapping(path = "/prescriptions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUserPrescriptions(HttpServletRequest request) {
        User user = userBusiness.getByKeycloakId(userService.getUserId());
        List<Prescription> prescriptions = prescriptionBusiness.getPrescriptionsForUser(user.getId());
        List<PrescriptionResponseDto> prescriptionResponseDtos = prescriptionDtoMapper
                .listOfBusinessObjectsToListOfResponseDto(prescriptions);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(prescriptionResponseDtos);

        return responseEntity;
    }
}
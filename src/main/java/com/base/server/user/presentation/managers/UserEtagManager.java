package com.base.server.user.presentation.managers;

import java.util.Optional;

import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.mappers.UserDtoMapper;
import com.base.server.user.presentation.dtos.UserRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserEtagManager implements RequestManager<UserRequestDto> {

    private UserBusiness userBusiness;
    private UserDtoMapper userDtoMapper;
    private ErrorMapper errorMapper;

    
    @Autowired
    public UserEtagManager(UserBusiness userBusiness, UserDtoMapper userDtoMapper, ErrorMapper errorMapper) {
        this.userBusiness = userBusiness;
        this.userDtoMapper = userDtoMapper;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UserRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        String receivedEtag = requestInformation.getIf_match();
        String currentEtag = userDtoMapper.getEtag(userBusiness.get(requestInformation.getId()));

        if (!receivedEtag.equals(currentEtag)) {
            HttpStatus status = HttpStatus.PRECONDITION_FAILED;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The user was changed, GET the new version";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }    
}

package com.base.server.user.presentation.managers.ethnicity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.mappers.UserDtoMapper;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserDeleteEthnicityManager implements RequestManager<EthnicityRequestDto>{

    private UserBusiness userBusiness;
    private EthnicityDtoMapper ethnicityDtoMapper;

    @Autowired
    public UserDeleteEthnicityManager(UserBusiness userBusiness, EthnicityDtoMapper ethnicityDtoMapper) {
        this.userBusiness = userBusiness;
        this.ethnicityDtoMapper = ethnicityDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<EthnicityRequestDto> requestInformation) {
        User user = userBusiness.get(requestInformation.getId());
        Set<Ethnicity> ethnicities = user.getEthnicities();
        Set<Ethnicity> filteredEthnicities = ethnicities.stream().filter(ethnicity -> !ethnicity.getId().equals(requestInformation.getDto().getId())).collect(Collectors.toSet());
        user.setEthnicities(filteredEthnicities);
        userBusiness.modify(requestInformation.getId(), user, requestInformation.getUserId());

        Set<Ethnicity> difference = new HashSet<>(ethnicities);
        difference.removeAll(filteredEthnicities);
        Ethnicity deletedEthnicity = difference.toArray(new Ethnicity[1])[0];
        EthnicityResponseDto ethnicityResponseDto = ethnicityDtoMapper.businessObjectToResponseDto(deletedEthnicity);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDto);

        return Optional.of(responseEntity);
    }
}

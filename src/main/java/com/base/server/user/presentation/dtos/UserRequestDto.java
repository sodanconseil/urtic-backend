package com.base.server.user.presentation.dtos;

import java.sql.Timestamp;
import java.util.List;

import com.base.server.generic.presentation.dtos.BaseRequestDto;
import com.base.server.prescription.business.Prescription;
import com.base.server.user.business.Gender;
import com.base.server.user.business.UserType;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserRequestDto extends BaseRequestDto {

    private String firstName;
    private String middleName;
    private String email;
    private Timestamp birthday;
    private String phoneNumber;
    private Gender gender;
    private UserType type; 
    private boolean emailValid;
    private boolean phoneNumberValid;
    private Timestamp lastAuthentication;
    private List<Prescription> prescriptions;
    private String prefferedLanguage;

    @Builder(setterPrefix = "set")
    public UserRequestDto(String id, String name, String description, String creationUser, Timestamp creationDate,
            String modificationUser, Timestamp modificationDate, String firstName,
            String middleName, String email, Timestamp birthday, String phoneNumber, Gender gender, UserType type,
            boolean emailValid, boolean phoneNumberValid, Timestamp lastAuthentication, List<Prescription> prescriptions,
            String prefferedLanguage) {
        super(id, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.firstName = firstName;
        this.middleName = middleName;
        this.email = email;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.type = type;
        this.emailValid = emailValid;
        this.phoneNumberValid = phoneNumberValid;
        this.lastAuthentication = lastAuthentication;
        this.prescriptions = prescriptions;
        this.prefferedLanguage = prefferedLanguage;
    }
}

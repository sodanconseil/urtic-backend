package com.base.server.user.presentation.managers.ethnicity;

import java.util.Optional;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserPutEthnicityManager implements RequestManager<EthnicityRequestDto>{
    private UserBusiness userBusiness;
    private EthnicityBusiness ethnicityBusiness;
    private EthnicityDtoMapper ethnicityDtoMapper;


    @Autowired
    public UserPutEthnicityManager(UserBusiness userBusiness, EthnicityBusiness ethnicityBusiness, EthnicityDtoMapper ethnicityDtoMapper) {
        this.userBusiness = userBusiness;
        this.ethnicityBusiness = ethnicityBusiness;
        this.ethnicityDtoMapper = ethnicityDtoMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<EthnicityRequestDto> requestInformation) {
        User user = userBusiness.get(requestInformation.getId());
        

        Ethnicity ethnicity = ethnicityDtoMapper.requestDtoToBusinessObject(requestInformation.getDto());
        user.getEthnicities().add(ethnicity);
        userBusiness.save(user, requestInformation.getUserId());
        // TODO renvoyer l'entite saved par la bd plutôt que lui envoyé
        EthnicityResponseDto ethnicityResponseDto = ethnicityDtoMapper.businessObjectToResponseDto(ethnicity);

        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(ethnicityResponseDto);

        return Optional.of(responseEntity);
    }
}

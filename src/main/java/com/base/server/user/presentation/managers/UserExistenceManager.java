package com.base.server.user.presentation.managers;

import java.util.Optional;

import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.RequestManager;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.presentation.dtos.UserRequestDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class UserExistenceManager implements RequestManager<UserRequestDto> {

    private UserBusiness userBusiness;
    private ErrorMapper errorMapper;

    @Autowired
    public UserExistenceManager(UserBusiness userBusiness, ErrorMapper errorMapper) {
        this.userBusiness = userBusiness;
        this.errorMapper = errorMapper;
    }

    @Override
    public Optional<ResponseEntity<Object>> process(RequestInformation<UserRequestDto> requestInformation) {
        Optional<ResponseEntity<Object>> toReturn = Optional.empty();

        if (userBusiness.get(requestInformation.getId()) == null) {
            HttpStatus status = HttpStatus.NOT_FOUND;
            String uri = requestInformation.getRequest().getRequestURI();
            String errorString = "The user with the specified id wasn't found";

            ResponseEntity<Object> response = ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(errorMapper.errorToResponseErrorDto(status, errorString, uri));

            toReturn = Optional.of(response);
        }

        return toReturn;
    }
}

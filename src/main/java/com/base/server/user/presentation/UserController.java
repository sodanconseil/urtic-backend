package com.base.server.user.presentation;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.base.server.ethnicity.presentation.dtos.EthnicityRequestDto;
import com.base.server.ethnicity.presentation.dtos.EthnicityResponseDto;
import com.base.server.generic.presentation.pipeline.RequestInformation;
import com.base.server.generic.presentation.pipeline.ValidationManager;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.business.interfaces.PrescriptionBusiness;
import com.base.server.prescription.mappers.PrescriptionDtoMapper;
import com.base.server.prescription.presentation.dtos.PrescriptionResponseDto;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.factory.UserFactory;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.managers.UserDeleteManager;
import com.base.server.user.presentation.managers.UserEtagManager;
import com.base.server.user.presentation.managers.UserExistenceManager;
import com.base.server.user.presentation.managers.UserGetAllManager;
import com.base.server.user.presentation.managers.UserGetByIdManager;
import com.base.server.user.presentation.managers.UserPostManager;
import com.base.server.user.presentation.managers.UserPutManager;
import com.base.server.user.presentation.managers.ethnicity.UserDeleteEthnicityManager;
import com.base.server.user.presentation.managers.ethnicity.UserGetAllEthnicitiesManager;
import com.base.server.user.presentation.managers.ethnicity.UserPutEthnicityManager;
import com.base.server.user.presentation.managers.ethnicity.UserVerifyEthnicityExistenceManager;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserFactory userFactory;
    private UserBusiness userBusiness;

    private ValidationManager<UserRequestDto> userValidationManager;
    private UserExistenceManager userExistenceManager;
    private UserEtagManager userEtagManager;
    private UserPostManager userPostManager;
    private UserGetAllManager userGetAllManager;
    private UserGetByIdManager userGetByIdManager;
    private UserPutManager userPutManager;
    private UserDeleteManager userDeleteManager;

    private UserGetAllEthnicitiesManager userGetAllEthnicitiesManager;
    private UserPutEthnicityManager userPutEthnicityManager;
    private UserDeleteEthnicityManager userDeleteEthnicityManager;
    private UserVerifyEthnicityExistenceManager userVerifyEthnicityExistenceManager;

    private PrescriptionBusiness prescriptionBusiness;
    private PrescriptionDtoMapper prescriptionDtoMapper;

    @Autowired
    public UserController(UserFactory userFactory, ValidationManager<UserRequestDto> userValidationManager,
                          UserExistenceManager userExistenceManager, UserEtagManager userEtagManager, UserPostManager userPostManager,
                          UserGetAllManager userGetAllManager, UserGetByIdManager userGetByIdManager, UserPutManager userPutManager,
                          UserDeleteManager userDeleteManager, ValidationManager<EthnicityRequestDto> ethnicityValidationManager,
                          UserGetAllEthnicitiesManager userGetAllEthnicitiesManager, UserPutEthnicityManager userPutEthnicityManager,
                          UserDeleteEthnicityManager userDeleteEthnicityManager, PrescriptionBusiness prescriptionBusiness,
                          PrescriptionDtoMapper prescriptionDtoMapper, UserBusiness userBusiness) {
        this.userFactory = userFactory;
        this.userValidationManager = userValidationManager;
        this.userExistenceManager = userExistenceManager;
        this.userEtagManager = userEtagManager;
        this.userPostManager = userPostManager;
        this.userGetAllManager = userGetAllManager;
        this.userGetByIdManager = userGetByIdManager;
        this.userPutManager = userPutManager;
        this.userDeleteManager = userDeleteManager;
        this.userGetAllEthnicitiesManager = userGetAllEthnicitiesManager;
        this.userPutEthnicityManager = userPutEthnicityManager;
        this.userDeleteEthnicityManager = userDeleteEthnicityManager;
        this.prescriptionBusiness = prescriptionBusiness;
        this.prescriptionDtoMapper = prescriptionDtoMapper;
        this.userBusiness = userBusiness;
    }


    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAll() {
        RequestInformation<UserRequestDto> requestInformation = userFactory.newRequestInformation();
        return userFactory.newRequestPipeline(userGetAllManager).manage(requestInformation);
    }


    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUser(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<UserRequestDto> requestInformation = userFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");
        return userFactory.newRequestPipeline(userExistenceManager)
                .addManager(userGetByIdManager)
                .manage(requestInformation);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postUser(@Valid @RequestBody UserRequestDto dto, Errors errors, HttpServletRequest request) {
        RequestInformation<UserRequestDto> requestInformation = userFactory.newRequestInformation(null, dto, request, errors, null, "toto");

        return userFactory.newRequestPipeline(userValidationManager)
                .addManager(userPostManager)
                .manage(requestInformation);
    }

    @PutMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Object> putUser(@PathVariable String id, @Valid @RequestBody UserRequestDto dto,
                                   Errors errors, HttpServletRequest request) {

        User oldUser = userBusiness.get(id);
 
        oldUser.setName(dto.getName() == null ? oldUser.getName() : dto.getName());
        oldUser.setFirstName(dto.getFirstName() == null ? oldUser.getFirstName() : dto.getFirstName());
        oldUser.setPrefferedLanguage(dto.getPrefferedLanguage() == null ? oldUser.getPrefferedLanguage() : dto.getPrefferedLanguage());
        oldUser.setEmail(dto.getEmail() == null ? oldUser.getEmail() : dto.getEmail());
        oldUser.setBirthday(dto.getBirthday() == null ? oldUser.getBirthday() : dto.getBirthday());
        oldUser.setGender(dto.getGender() == null ? oldUser.getGender() : dto.getGender());
        oldUser.setPhoneNumber(dto.getPhoneNumber() == null ? oldUser.getPhoneNumber() : dto.getPhoneNumber());

        User savedUser = userBusiness.save(oldUser,id);
        return ResponseEntity.ok(savedUser);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteUser(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<UserRequestDto> requestInformation = userFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");

        return userFactory.newRequestPipeline(userExistenceManager)
                .addManager(userDeleteManager)
                .manage(requestInformation);
    }

    //
    // Mappings pour l'ethnicite
    //

    @GetMapping(path = "/{id}/ethnicities", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllEthnicities(@PathVariable String id, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);

        RequestInformation<EthnicityRequestDto> requestInformation = new RequestInformation<EthnicityRequestDto>(pathVariables, null, request, null, null, "toto");
        return userFactory.newRequestPipeline(userGetAllEthnicitiesManager).manage(requestInformation);
    }

    @PutMapping(path = "/{id}/ethnicities/{ethnicityId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postUserEthnicity(@PathVariable String id, @PathVariable String ethnicityId, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);
        pathVariables.put("ethnicityId", ethnicityId);

        RequestInformation<EthnicityRequestDto> requestInformation = userFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");

        return userFactory.newRequestPipeline(userVerifyEthnicityExistenceManager)
                .addManager(userPutEthnicityManager)
                .manage(requestInformation);
    }

    @DeleteMapping(path = "/{id}/ethnicities/{ethnicityId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteUserEthnicity(@PathVariable String id, @PathVariable String ethnicityId, HttpServletRequest request) {

        Map<String, String> pathVariables = new HashMap<>();
        pathVariables.put("id", id);
        pathVariables.put("ethnicityId", ethnicityId);

        RequestInformation<EthnicityRequestDto> requestInformation = userFactory.newRequestInformation(pathVariables, null, request, null, null, "toto");

        return userFactory.newRequestPipeline(userDeleteEthnicityManager).manage(requestInformation);
    }

    @GetMapping(path = "/{id}/prescriptions", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUserPrescriptions(@PathVariable String id, HttpServletRequest request) {
        List<Prescription> prescriptions = prescriptionBusiness.getPrescriptionsForUser(id);
        List<PrescriptionResponseDto> prescriptionResponseDtos = prescriptionDtoMapper.listOfBusinessObjectsToListOfResponseDto(prescriptions);
        ResponseEntity<Object> responseEntity = ResponseEntity.status(HttpStatus.OK).body(prescriptionResponseDtos);

        return responseEntity;
    }
}

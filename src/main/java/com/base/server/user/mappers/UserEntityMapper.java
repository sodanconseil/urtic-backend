package com.base.server.user.mappers;


import com.base.server.generic.mapper.interfaces.BaseEntityMapper;
import com.base.server.user.business.User;
import com.base.server.user.persistence.jpa.UserEntity;

import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserEntityMapper extends BaseEntityMapper<UserEntity, User> {
    public User entityToBusinessObject(UserEntity entity);
    public UserEntity businessObjectToEntity(User object);
    public List<User> listOfEntityToListOfBusinessObjects(List<UserEntity> entities);
}
package com.base.server.user.mappers;

import java.util.List;

import com.base.server.generic.mapper.interfaces.BaseDtoMapper;
import com.base.server.user.business.User;
import com.base.server.user.presentation.dtos.UserRequestDto;
import com.base.server.user.presentation.dtos.UserResponseDto;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDtoMapper extends BaseDtoMapper<User, UserRequestDto, UserResponseDto> {
    User requestDtoToBusinessObject(UserRequestDto dto);
    UserResponseDto businessObjectToResponseDto(User businessObject);
    List<UserResponseDto> listOfBusinessObjectsToListOfResponseDto(List<User> businessObjects);
}

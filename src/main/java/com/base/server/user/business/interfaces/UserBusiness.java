package com.base.server.user.business.interfaces;

import com.base.server.generic.business.interfaces.BaseBusiness;
import com.base.server.user.business.User;

public interface UserBusiness extends BaseBusiness<User> {
    public User getByKeycloakId(String keycloakId);
}

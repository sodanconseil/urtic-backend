package com.base.server.user.business;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.generic.business.Base;

import com.base.server.prescription.business.Prescription;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends Base {
    private String keycloakId;
    private String firstName;
    private String middleName;
    private String email;
    private Timestamp birthday;
    private String phoneNumber;
    private Gender gender; 
    private UserType type;
    private boolean emailValid;
    private boolean phoneNumberValid;
    private Timestamp lastAuthentication;
    private Set<Ethnicity> ethnicities;
    private String prefferedLanguage;
    //private List<Prescription> prescriptions;

    @Builder(setterPrefix = "set")
    public User(String id, Long version, String name, String description, String creationUser, Timestamp creationDate,
            String modificationUser, Timestamp modificationDate, String firstName, String keycloakId,
            String middleName, String email, Timestamp birthday, String phoneNumber, Gender gender, UserType type,
            boolean emailValid, boolean phoneNumberValid, Timestamp lastAuthentication,  Set<Ethnicity> ethnicities,
                List<Prescription> prescriptions, String prefferedLanguage) {
        super(id, version, name, description, creationUser, creationDate, modificationUser, modificationDate);
        this.keycloakId = keycloakId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.email = email;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.gender = gender;
        this.type = type;
        this.emailValid = emailValid;
        this.phoneNumberValid = phoneNumberValid;
        this.lastAuthentication = lastAuthentication;
        this.ethnicities = ethnicities == null ? new HashSet<>() : ethnicities;
        this.prefferedLanguage = prefferedLanguage;
        //this.prescriptions = prescriptions;
    }
}

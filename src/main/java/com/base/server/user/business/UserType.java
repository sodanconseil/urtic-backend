package com.base.server.user.business;

public enum UserType {
    PATIENT,
    PERSONNEL_MEDICAL,
    PHARMACEUTIQUE
}

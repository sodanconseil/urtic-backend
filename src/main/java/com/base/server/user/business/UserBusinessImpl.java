package com.base.server.user.business;

import com.base.server.generic.business.BaseBusinessImpl;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.factory.UserFactory;
import com.base.server.user.persistence.interfaces.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserBusinessImpl extends BaseBusinessImpl<User, UserRepository> implements UserBusiness{

    private UserRepository userRepository;
    private UserFactory userFactory;

    @Autowired
    public UserBusinessImpl(UserRepository userRepository, UserFactory factory) {
        super(userRepository, factory);
        this.userRepository = userRepository;
        this.userFactory = userFactory;
    }

    @Override
    public User getByKeycloakId(String keycloakId) {
        return userRepository.getByKeycloakId(keycloakId);
    }
}

package unit.java.com.base.server.generic.presentation.managers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.base.server.generic.builders.ResponseEntityBuilder;
import com.base.server.generic.factory.BaseFactory;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.RequestValidator;
import com.base.server.generic.presentation.dtos.ResponseErrorDto;
import com.base.server.generic.presentation.managers.ErrorRequestManager;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ErrorRequestManagerTest {
    
    private ErrorMapper projectErrorMapper = mock(ErrorMapper.class);
    private RequestValidator requestValidator = mock(RequestValidator.class);
    private BaseFactory projectFactory = mock(BaseFactory.class);
    private ErrorRequestManager errorRequestManager = new ErrorRequestManager(projectErrorMapper, requestValidator, projectFactory);

    private String requestURI = "this string";
    private ResponseEntity<Object> responseEntity = mock(ResponseEntity.class);
    private ResponseEntityBuilder mock = mock(ResponseEntityBuilder.class);

    private ErrorRequestManagerTest() {
        when(mock.setStatus(any())).thenReturn(mock);
        when(mock.setBody(any())).thenReturn(mock);
        when(mock.build()).thenReturn(responseEntity);
        when(projectFactory.newResponseEntityBuilder()).thenReturn(mock);
    }

    //
    // Tests of the manageNotMatchingEtags(String requestURI) method
    //

    @Test
    public void Should_return_non_matching_etag_response() {
        String message = "The project was modified, GET the new version";
        ResponseErrorDto response = mock(ResponseErrorDto.class);
        HttpStatus status = HttpStatus.PRECONDITION_FAILED;
        when(projectErrorMapper.errorToResponseErrorDto(status, message, requestURI)).thenReturn(response);
        
        assertEquals(errorRequestManager.manageNotMatchingEtags(requestURI),responseEntity);
        verify(mock).setStatus(status);
        verify(mock).setBody(response);
        verify(mock).build();
    }

    //
    // Tests of the manageUnexistingRequest(String requestURI) method
    //

    @Test
    public void Should_return_non_unexisting_request_response() {
        String message = "The project with the specified id wasn't found";
        ResponseErrorDto response = mock(ResponseErrorDto.class);
        HttpStatus status = HttpStatus.NOT_FOUND;

        when(projectErrorMapper.errorToResponseErrorDto(status, message, requestURI)).thenReturn(response);
        
        assertEquals(errorRequestManager.manageUnexistingRequest(requestURI),responseEntity);
        verify(mock).setStatus(status);
        verify(mock).setBody(response);
        verify(mock).build();
    }

    //
    // Tests of the manageInvalidRequest(String requestURI) method
    //

    @Test
    public void Should_return_invalid_request_response() {
        String errorMessage = "This is an error message";
        ResponseErrorDto response = mock(ResponseErrorDto.class);
        HttpStatus status = HttpStatus.BAD_REQUEST;

        when(requestValidator.getErrorMessage()).thenReturn(errorMessage);
        when(projectErrorMapper.errorToResponseErrorDto(status, requestValidator.getErrorMessage(), requestURI)).thenReturn(response);
        
        assertEquals(errorRequestManager.manageInvalidRequest(requestURI),responseEntity);
        verify(mock).setStatus(status);
        verify(mock).setBody(response);
        verify(mock).build();
    }
}

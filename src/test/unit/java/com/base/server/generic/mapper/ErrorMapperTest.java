package unit.java.com.base.server.generic.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import com.base.server.generic.mapper.ErrorMapperImpl;
import com.base.server.generic.mapper.interfaces.ErrorMapper;
import com.base.server.generic.presentation.dtos.ResponseErrorDto;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;

public class ErrorMapperTest {

    private ResponseErrorDto.ResponseErrorDtoBuilder projectResponseDtoBuilder = mock(ResponseErrorDto.ResponseErrorDtoBuilder.class);
    private ErrorMapper projectErrorMapper = new ErrorMapperImpl();

    //
    // Tests of the listOfErrorsToString(List<ObjectError> errors) method
    //

    @Test
    public void Should_return_empty_string_errors_is_null() {
        List<ObjectError> errors = null;
        assertEquals(projectErrorMapper.listOfErrorsToString(errors), "");
    }

    @Test
    public void Should_return_string_representing_errors() {
        List<ObjectError> errors = (List<ObjectError>) mock(List.class);
        ObjectError error = mock(ObjectError.class);
        when(error.getDefaultMessage()).thenReturn("This is a default message");
        when(errors.size()).thenReturn(4);
        when(errors.get(anyInt())).thenReturn(error);

        String output = "This is a default message, This is a default message, This is a default message, This is a default message";
        assertEquals(projectErrorMapper.listOfErrorsToString(errors), output);
    }

    //
    // Tests of the errorToProjectResponseErrorDto(HttpStatus status, String
    // message, String requestURI) method
    //

    @Test
    public void Should_set_the_ProjectResponseErrorDto() {
        String message = "This should be the message";
        String requestURI = "This should be the request";
        HttpStatus status = mock(HttpStatus.class);
        ResponseErrorDto projectResponseErrorDto = mock(ResponseErrorDto.class);
        when(status.getReasonPhrase()).thenReturn("This should be the reason");
        when(status.value()).thenReturn(123);

        try (MockedStatic<ResponseErrorDto> theMock = Mockito.mockStatic(ResponseErrorDto.class)) {
            theMock.when(() -> ResponseErrorDto.builder()).thenReturn(projectResponseDtoBuilder);

            when(projectResponseDtoBuilder.setError(any())).thenReturn(projectResponseDtoBuilder);
            when(projectResponseDtoBuilder.setMessage(any())).thenReturn(projectResponseDtoBuilder);
            when(projectResponseDtoBuilder.setPath(any())).thenReturn(projectResponseDtoBuilder);
            when(projectResponseDtoBuilder.setStatus(anyInt())).thenReturn(projectResponseDtoBuilder);
            when(projectResponseDtoBuilder.setTimestamp(any())).thenReturn(projectResponseDtoBuilder);
            when(projectResponseDtoBuilder.build()).thenReturn(projectResponseErrorDto);

            projectErrorMapper.errorToResponseErrorDto(status, message, requestURI);
            verify(projectResponseDtoBuilder).setError(status.getReasonPhrase());
            verify(projectResponseDtoBuilder).setMessage(message);
            verify(projectResponseDtoBuilder).setPath(requestURI);
            verify(projectResponseDtoBuilder).setStatus(status.value());
            verify(projectResponseDtoBuilder).setTimestamp(any());
            verify(projectResponseDtoBuilder).build();
        }
    }
}

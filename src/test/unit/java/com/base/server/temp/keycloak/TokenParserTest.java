package unit.java.com.base.server.temp.keycloak;

import java.util.List;

import javax.persistence.EntityManager;

import com.base.server.BaseKeycloakStarter;
import com.base.server.dosage.persistence.jpa.DosageDal;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.drug.persistence.jpa.DrugDal;
import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.frequency.persistence.jpa.FrequencyDal;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.presentation.JwtTokenParser;
import com.base.server.generic.presentation.JwtTokenParser.JwtToken;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.persistence.jpa.PrescriptionDal;
import com.base.server.prescription.persistence.jpa.PrescriptionEntity;
import com.base.server.unit.persistence.jpa.UnitDal;
import com.base.server.unit.persistence.jpa.UnitEntity;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.persistence.jpa.UserDal;
import com.base.server.user.persistence.jpa.UserEntity;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest
public class TokenParserTest {

    @Autowired
    JwtTokenParser jwtTokenParser;

    @Test
    public void testParser() {
        String token = "Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJLS1JQamNBVm9LTWQ0c3JZS3F1Mmx1eThsOVI2RUxNV1JFeDJRQVJYVF9jIn0.eyJleHAiOjE2MjgwMDkzODcsImlhdCI6MTYyODAwOTA4NywiYXV0aF90aW1lIjoxNjI4MDA5MDg3LCJqdGkiOiJlYzQ3N2EzYi03MGMzLTRhZTMtOTYzNy1kYWYxZjMyZmE0MjciLCJpc3MiOiJodHRwOi8vNTIuMjM1LjIzLjQ2OjgwODAvYXV0aC9yZWFsbXMvVXJ0aWMiLCJhdWQiOiJVcnRpY1VJIiwic3ViIjoiZjdkYTQwNzItMGEzMS00OTkzLThkMGMtZjVmN2Y5MWY2Y2NjIiwidHlwIjoiSUQiLCJhenAiOiJVcnRpY1VJIiwic2Vzc2lvbl9zdGF0ZSI6ImMzNmM4ZjFiLTdmNDItNGNmNS05MTI5LTY1YzcyYmI4YzljYyIsImF0X2hhc2giOiIzMXFyLUo4cWFjaEx2Zk53S3R5Uk5BIiwiYWNyIjoiMSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6ImZlbGl4IGRhbGxhaXJlIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiZmVsaXgiLCJnaXZlbl9uYW1lIjoiZmVsaXgiLCJmYW1pbHlfbmFtZSI6ImRhbGxhaXJlIiwiZW1haWwiOiJmQGYuY29tIn0.WWCb_D0y1aQ3e0E8mGCuxHQJFrxoDWewKlMxHwd5QPUdD2ESKEF1J03VeuOUHIFgmxy2S7H9_bkJuhksgsKuzpZ27bSmrlJBsdrZFAurZxSAKx-tUOPs11USTtWGamBTEN3LreF_PYRy3SNpkV5j_IecWk4NdyTowNVviNcHUpMbcUrw8DIAkaVDrttRFh_azLXr5EK5lSLRe72lq-u5NmSF5sW7LCQDRU2hQBz2xKQa7BENXPqGoLdOXDph39PAPlOdGFiMfemht_spqqpxjvF6Hw-ROfQ1Egz010OkdrPM_gI2wgriMQ23M1NNWfmI0IdQcEGG_cZx1Vs6PVwV0A";
        JwtToken jwtToken = jwtTokenParser.parseJwtToken(token);
        System.out.println();
    }
}

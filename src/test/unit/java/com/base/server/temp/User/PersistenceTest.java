package unit.java.com.base.server.temp.User;

import java.util.List;

import javax.persistence.EntityManager;

import com.base.server.BaseKeycloakStarter;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.persistence.jpa.UserDal;
import com.base.server.user.persistence.jpa.UserEntity;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest
public class PersistenceTest {

    @Autowired
    UserDal userDal;

    @Test
    public void testSaveEthnicity() { 
        EthnicityEntity caucasian = EthnicityEntity.builder().setName("caucasian").build();
        UserEntity jonathan = UserEntity.builder().setName("Jonathan").build();

        jonathan.getEthnicities().add(caucasian);

        UserEntity jonathanSaved = userDal.save(jonathan);

        System.out.println();
    }
}

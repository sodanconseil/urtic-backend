package unit.java.com.base.server.temp.User;

import java.util.List;

import javax.persistence.EntityManager;

import com.base.server.BaseKeycloakStarter;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.mappers.UserEntityMapper;
import com.base.server.user.persistence.jpa.UserDal;
import com.base.server.user.persistence.jpa.UserEntity;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest
public class MappersTest {
    

    @Autowired 
    UserEntityMapper userEntityMapper;


    @Test
    public void testEntityMapper() {
        Ethnicity caucasian = Ethnicity.builder().setName("caucasian").build();
        User jonathan = User.builder().setName("Jonathan").build();

        jonathan.getEthnicities().add(caucasian);
    
        UserEntity jonathanEntity = userEntityMapper.businessObjectToEntity(jonathan);
    
        System.out.println();
    }
}

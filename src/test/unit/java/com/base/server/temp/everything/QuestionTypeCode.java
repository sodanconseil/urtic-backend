package unit.java.com.base.server.temp.everything;

public enum QuestionTypeCode {
    SINGLE_CHOICE(1);

    private int code;

    QuestionTypeCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

package unit.java.com.base.server.temp.everything;

import com.base.server.BaseKeycloakStarter;
import com.base.server.Question.persistance.jpa.QuestionDal;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionDal;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionEntity;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceDal;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa.AnswerChoiceTraductionDal;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa.AnswerChoiceTraductionEntity;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.frequency.persistence.jpa.FrequencyDal;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.questionType.persistance.jpa.QuestionTypeDal;
import com.base.server.questionType.persistance.jpa.QuestionTypeEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionDal;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionDal;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionEntity;
import com.base.server.survey.persistance.jpa.SurveyDal;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionDal;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest(properties = { "spring.jpa.hibernate.ddl-auto=update", "server.port=8084" })
class AddAshtmaDataToDB {

    private static final String EN = "en";
    private static final String NO_INFO = "";
    private static final List<String> YES_NO_DONT_KNOW_CHOICES_FR = Arrays.asList("Oui", "Non", "Je ne sais pas");
    private static final List<String> YES_NO_DONT_KNOW_CHOICES_EN = Arrays.asList("Yes", "No", "I don't know");
    private static final List<String> SYMPTOM_FREQUENCY_CHOICES_FR = Arrays.asList("Jamais", "Moins de 2 jours par semaine", "Plus de 2 jours par semaine", "Presque tous les jours");
    private static final List<String> SYMPTOM_FREQUENCY_CHOICES_EN = Arrays.asList("Never", "Up to 2 days per week", "More than 2 days per week", "Almost every day");

    @Autowired
    AnswerChoiceDal answerChoiceDal;
    @Autowired
    FrequencyDal frequencyDal;
    @Autowired
    QuestionDal questionDal;
    @Autowired
    QuestionTypeDal questionTypeDal;
    @Autowired
    QuestionVersionDal questionVersionDal;
    @Autowired
    QuestionVersionTraductionDal questionVersionTraductionDal;
    @Autowired
    SurveyDal surveyDal;
    @Autowired
    SurveyVersionDal surveyVersionDal;
    @Autowired
    AnswerChoiceTraductionDal answerChoiceTraductionDal;
    @Autowired
    EthnicityDal ethnicityDal;
    @Autowired
    SubQuestionConditionDal subQuestionConditionDal;

    @Test
    void addActSurveyToDatabase() {
        addFrequenciesToDatabase();
        addQuestionTypesToDatabase();

        // Survey & Questions
        SurveyEntity ACT = SurveyEntity.builder().setShortName("ACT")
                .setName("Test de Contrôle de l’Asthme")
                .setFrequency(frequencyDal.findByName("jour"))
                .setDelais(14)
                .build();
        ACT = surveyDal.save(ACT);

        QuestionEntity question1 = QuestionEntity.builder().setSurvey(ACT)
                .setText("Dans l'ensemble, à quel point vos symptômes allergiques vous dérangent-ils aujourd'hui?")
                .setName("Dans l'ensemble, à quel point vos symptômes allergiques vous dérangent-ils aujourd'hui?").build();
        QuestionEntity question2 = QuestionEntity.builder().setSurvey(ACT)
                .setText("À quel point vos symptômes nasaux vous dérangent-ils aujourd'hui?")
                .setName("À quel point vos symptômes nasaux vous dérangent-ils aujourd'hui?").build();
        QuestionEntity question3 = QuestionEntity.builder().setSurvey(ACT)
                .setText("À quel point vos symptômes oculaires vous dérangent-ils aujourd'hui?")
                .setName("À quel point vos symptômes oculaires vous dérangent-ils aujourd'hui?").build();
        QuestionEntity question4 = QuestionEntity.builder().setSurvey(ACT)
                .setText("À quel point vos symptômes d'asthme vous dérangent-ils aujourd'hui?")
                .setName("À quel point vos symptômes d'asthme vous dérangent-ils aujourd'hui?").build();
        QuestionEntity question5 = QuestionEntity.builder().setSurvey(ACT)
                .setText("Travaillez-vous aujourd'hui?")
                .setName("Travaillez-vous aujourd'hui?").build();
        QuestionEntity question6 = QuestionEntity.builder().setSurvey(ACT)
                .setText("Dans quelle mesure vos symptômes allergiques affectent-ils votre travail aujourd'hui?")
                .setName("Dans quelle mesure vos symptômes allergiques affectent-ils votre travail aujourd'hui?").build();
        QuestionEntity question7 = QuestionEntity.builder().setSurvey(ACT)
                .setText("Suivez-vous actuellement des cours dans un cadre académique?")
                .setName("Suivez-vous actuellement des cours dans un cadre académique?").build();
        QuestionEntity question8 = QuestionEntity.builder().setSurvey(ACT)
                .setText("Dans quelle mesure les allergies ont-elles affecté votre productivité à l'école ou en cours dans un cadre académique?")
                .setName("Dans quelle mesure les allergies ont-elles affecté votre productivité à l'école ou en cours dans un cadre académique?").build();
        QuestionEntity question9 = QuestionEntity.builder().setSurvey(ACT)
                .setText("Avez-vous utilisé un traitement aujourd'hui? Si oui inscrivez les nom des médicaments.")
                .setName("Avez-vous utilisé un traitement aujourd'hui? Si oui inscrivez les nom des médicaments.").build();

        QuestionEntity[] allQuestion = new QuestionEntity[]{
                question1, question2, question3, question4, question5,
                question6, question7, question8, question9,
        };

        for (QuestionEntity question : allQuestion) {
            questionDal.save(question);
        }

        ACT.setQuestions(Arrays.asList(allQuestion));

        // Survey Version
        Timestamp yesterday = Timestamp.from(new Date().toInstant().minus(1, ChronoUnit.DAYS));
        Timestamp today = Timestamp.from(new Date().toInstant());

        SurveyVersionEntity surveyVersionOld = SurveyVersionEntity.builder().setSurvey(ACT)
                .setSurvey_version(1).setDate(yesterday)
                .setFrequency(frequencyDal.findByName("jour"))
                .setName(ACT.getName()).setShortName(ACT.getShortName())
                .setFr(ACT.getName()).setEn("Asthma Control Test")
                .setDelais(14)
                .build();
        SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(ACT)
                .setSurvey_version(2).setDate(today)
                .setFrequency(frequencyDal.findByName("jour"))
                .setDelais(14)
                .setName(ACT.getName()).setShortName(ACT.getShortName())
                .setFr(ACT.getName()).setEn("Asthma Control Test")
                .build();

        surveyVersionDal.save(surveyVersionOld);
        surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);

        // Question Version
        QuestionVersionEntity questionVersion1 = QuestionVersionEntity.builder()
                .setQuestion_order(1)
                .setVerificationOrder(1)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("Dans l'ensemble, à quel point vos symptômes allergiques vous dérangent-ils aujourd'hui?")
                .setFormattedText("Dans l'ensemble, à quel point vos symptômes allergiques vous dérangent-ils aujourd'hui?").setName("")
                .setInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question1).build();

        QuestionVersionEntity questionVersion2 = QuestionVersionEntity.builder()
                .setQuestion_order(2)
                .setVerificationOrder(2)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("À quel point vos symptômes nasaux vous dérangent-ils aujourd'hui?")
                .setFormattedText("À quel point vos symptômes nasaux vous dérangent-ils aujourd'hui?").setName("")
                .setInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question2).build();

        QuestionVersionEntity questionVersion3 = QuestionVersionEntity.builder()
                .setQuestion_order(3)
                .setVerificationOrder(3)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("À quel point vos symptômes oculaires vous dérangent-ils aujourd'hui?")
                .setFormattedText("À quel point vos symptômes oculaires vous dérangent-ils aujourd'hui?").setName("")
                .setInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question3).build();

        QuestionVersionEntity questionVersion4 = QuestionVersionEntity.builder()
                .setQuestion_order(4)
                .setVerificationOrder(4)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("À quel point vos symptômes d'asthme vous dérangent-ils aujourd'hui?")
                .setFormattedText("À quel point vos symptômes d'asthme vous dérangent-ils aujourd'hui?").setName("")
                .setInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question4).build();

        QuestionVersionEntity questionVersion5 = QuestionVersionEntity.builder()
                .setQuestion_order(5)
                .setVerificationOrder(5)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("Travaillez-vous aujourd'hui?")
                .setFormattedText("Travaillez-vous aujourd'hui?").setName("")
                .setInfo("Appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question5).build();

        QuestionVersionEntity questionVersion6 = QuestionVersionEntity.builder()
                .setQuestion_order(0)
                .setVerificationOrder(6)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("Dans quelle mesure vos symptômes allergiques affectent-ils votre travail aujourd'hui?")
                .setFormattedText("Dans quelle mesure vos symptômes allergiques affectent-ils votre travail aujourd'hui?").setName("")
                .setInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setIsSubQuestion(true)
                .setSubQuestionOrder(0)
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question6).build();

        QuestionVersionEntity questionVersion7 = QuestionVersionEntity.builder()
                .setQuestion_order(6)
                .setVerificationOrder(7)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("Suivez-vous actuellement des cours dans un cadre académique?")
                .setFormattedText("Suivez-vous actuellement des cours dans un cadre académique?").setName("")
                .setInfo("Appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question7).build();

        QuestionVersionEntity questionVersion8 = QuestionVersionEntity.builder()
                .setQuestion_order(0)
                .setVerificationOrder(8)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("Dans quelle mesure les allergies ont-elles affecté votre productivité à l'école ou en cours dans un cadre académique?")
                .setFormattedText("Dans quelle mesure les allergies ont-elles affecté votre productivité à l'école ou en cours dans un cadre académique?").setName("")
                .setInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setFormattedInfo("Sur une échelle de 0 à 4 (0 étant aucunement dérangeant et 4 étant extrèmement dérengeant) appuyez sur la case appropriée, qui définit le mieux votre situation.")
                .setIsSubQuestion(true)
                .setSubQuestionOrder(0)
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question8).build();

        QuestionVersionEntity questionVersion9 = QuestionVersionEntity.builder()
                .setQuestion_order(7)
                .setVerificationOrder(9)
                .setQuestionType(questionTypeDal.findByTypeCode(1))
                .setText("Avez-vous utilisé un traitement aujourd'hui? Si oui inscrivez les nom des médicaments.")
                .setFormattedText("Avez-vous utilisé un traitement aujourd'hui? Si oui inscrivez les nom des médicaments.").setName("")
                .setInfo("Appuyez sur la case appropriée, qui définit le mieux votre situation et inscrivez les médicaments au besoin.")
                .setFormattedInfo("Appuyez sur la case appropriée, qui définit le mieux votre situation et inscrivez les médicaments au besoin.")
                .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question9).build();

        questionVersion1 = questionVersionDal.save(questionVersion1);
        questionVersion2 = questionVersionDal.save(questionVersion2);
        questionVersion3 = questionVersionDal.save(questionVersion3);
        questionVersion4 = questionVersionDal.save(questionVersion4);
        questionVersion5 = questionVersionDal.save(questionVersion5);
        questionVersion6 = questionVersionDal.save(questionVersion6);
        questionVersion7 = questionVersionDal.save(questionVersion7);
        questionVersion8 = questionVersionDal.save(questionVersion8);
        questionVersion9 = questionVersionDal.save(questionVersion9);

        // Translate Version
        QuestionVersionTraductionEntity questionVersionTraduction1 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion1.getId())
                .setLanguage("en")
                .setText("Overall how much are your allergic symptoms bothering you today?")
                .setFormattedText("Overall how much are your allergic symptoms bothering you today?")
                .setInfo("On a scale of 0 to 4 (0 being none bothersome and 4 being extremely bothersome) press the appropriate box, which best defines your situation.")
                .setFormattedInfo("On a scale of 0 to 4 (0 being not at all annoying and 4 being extremely annoying) press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction2 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion2.getId())
                .setLanguage("en")
                .setText("How much are your nose symptoms bothering you today?")
                .setFormattedText("How much are your nose symptoms bothering you today?")
                .setInfo("On a scale of 0 to 4 (0 being none bothersome and 4 being extremely bothersome) press the appropriate box, which best defines your situation.")
                .setFormattedInfo("On a scale of 0 to 4 (0 being not at all annoying and 4 being extremely annoying) press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction3 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion3.getId())
                .setLanguage("en")
                .setText("How much are your eyes symptoms bothering you today?")
                .setFormattedText("How much are your eyes symptoms bothering you today?")
                .setInfo("On a scale of 0 to 4 (0 being none bothersome and 4 being extremely bothersome) press the appropriate box, which best defines your situation.")
                .setFormattedInfo("On a scale of 0 to 4 (0 being not at all annoying and 4 being extremely annoying) press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction4 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion4.getId())
                .setLanguage("en")
                .setText("How much are your asthma symptoms bothering you today?")
                .setFormattedText("How much are your asthma symptoms bothering you today?")
                .setInfo("On a scale of 0 to 4 (0 being none bothersome and 4 being extremely bothersome) press the appropriate box, which best defines your situation.")
                .setFormattedInfo("On a scale of 0 to 4 (0 being not at all annoying and 4 being extremely annoying) press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction5 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion5.getId())
                .setLanguage("en")
                .setText("Are you working today?")
                .setFormattedText("Are you working today?")
                .setInfo("Press the appropriate box, which best defines your situation.")
                .setFormattedInfo("Press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction6 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion6.getId())
                .setLanguage("en")
                .setText("How much are your allergic symptoms affecting your work today?")
                .setFormattedText("How much are your allergic symptoms affecting your work today?")
                .setInfo("On a scale of 0 to 4 (0 being none bothersome and 4 being extremely bothersome) press the appropriate box, which best defines your situation.")
                .setFormattedInfo("On a scale of 0 to 4 (0 being not at all annoying and 4 being extremely annoying) press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction7 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion7.getId())
                .setLanguage("en")
                .setText("Do you currently attend classes in an academic setting?")
                .setFormattedText("Do you currently attend classes in an academic setting?")
                .setInfo("Press the appropriate box, which best defines your situation.")
                .setFormattedInfo("Press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction8 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion8.getId())
                .setLanguage("en")
                .setText("How much did your allergies affect your productivity while in school or attending classes in an academic setting?")
                .setFormattedText("How much did your allergies affect your productivity while in school or attending classes in an academic setting?")
                .setInfo("On a scale of 0 to 4 (0 being none bothersome and 4 being extremely bothersome) press the appropriate box, which best defines your situation.")
                .setFormattedInfo("On a scale of 0 to 4 (0 being not at all annoying and 4 being extremely annoying) press the appropriate box, which best defines your situation.")
                .setName("")
                .build();

        QuestionVersionTraductionEntity questionVersionTraduction9 = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(questionVersion9.getId())
                .setLanguage("en")
                .setText("Have you used any treatment today? If yes, list the names of the medications.")
                .setFormattedText("Have you used any treatment today? If yes, list the names of the medications.")
                .setInfo("Press the appropriate box that best defines your situation and enter medications as needed.")
                .setFormattedInfo("Press the appropriate box that best defines your situation and enter medications as needed.")
                .setName("")
                .build();

        questionVersionTraductionDal.save(questionVersionTraduction1);
        questionVersionTraductionDal.save(questionVersionTraduction2);
        questionVersionTraductionDal.save(questionVersionTraduction3);
        questionVersionTraductionDal.save(questionVersionTraduction4);
        questionVersionTraductionDal.save(questionVersionTraduction5);
        questionVersionTraductionDal.save(questionVersionTraduction6);
        questionVersionTraductionDal.save(questionVersionTraduction7);
        questionVersionTraductionDal.save(questionVersionTraduction8);
        questionVersionTraductionDal.save(questionVersionTraduction9);

        // Sub Question
        SubQuestionConditionEntity subQuestionConditionEntityQ5 = SubQuestionConditionEntity.builder()
                .setName("")
                .setSurveyVersion(surveyVersionLatest.getId())
                .setQuestionVersion(questionVersion5.getId())
                .setSubQuestionVersion(questionVersion6.getId())
                .setSubQuestionOrder(1)
                .build();
        subQuestionConditionEntityQ5 = subQuestionConditionDal.save(subQuestionConditionEntityQ5);

        SubQuestionConditionEntity subQuestionConditionEntityQ7 = SubQuestionConditionEntity.builder()
                .setName("")
                .setSurveyVersion(surveyVersionLatest.getId())
                .setQuestionVersion(questionVersion7.getId())
                .setSubQuestionVersion(questionVersion8.getId())
                .setSubQuestionOrder(1)
                .build();
        subQuestionConditionEntityQ7 = subQuestionConditionDal.save(subQuestionConditionEntityQ7);

        // Answer choice

        //Question1
        AnswerChoiceEntity answerChoiceQ1A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                .setValue(0).setText("0 - Pas du tout").setFormattedText("0 - Pas du tout")
                .setName("")
                .setQuestionVersionEntity(questionVersion1).build();

        AnswerChoiceEntity answerChoiceQ1A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("1 - Un peu").setFormattedText("2 - Un peu")
                .setName("")
                .setQuestionVersionEntity(questionVersion1).build();

        AnswerChoiceEntity answerChoiceQ1A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                .setValue(2).setText("2 - Moyennement").setFormattedText("2 - Moyennement")
                .setName("")
                .setQuestionVersionEntity(questionVersion1).build();

        AnswerChoiceEntity answerChoiceQ1A4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                .setValue(3).setText("3 - Beaucoup").setFormattedText("3 - Beaucoup")
                .setName("")
                .setQuestionVersionEntity(questionVersion1).build();

        AnswerChoiceEntity answerChoiceQ1A5 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(5)
                .setValue(4).setText("4 - Énormément").setFormattedText("4 - Énormément")
                .setName("")
                .setQuestionVersionEntity(questionVersion1).build();

        answerChoiceQ1A1 = answerChoiceDal.save(answerChoiceQ1A1);
        answerChoiceQ1A2 = answerChoiceDal.save(answerChoiceQ1A2);
        answerChoiceQ1A3 = answerChoiceDal.save(answerChoiceQ1A3);
        answerChoiceQ1A4 = answerChoiceDal.save(answerChoiceQ1A4);
        answerChoiceQ1A5 = answerChoiceDal.save(answerChoiceQ1A5);

        //Question2
        AnswerChoiceEntity answerChoiceQ2A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                .setValue(0).setText("0 - Pas du tout").setFormattedText("0 - Pas du tout")
                .setName("")
                .setQuestionVersionEntity(questionVersion2).build();

        AnswerChoiceEntity answerChoiceQ2A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("1 - Un peu").setFormattedText("2 - Un peu")
                .setName("")
                .setQuestionVersionEntity(questionVersion2).build();

        AnswerChoiceEntity answerChoiceQ2A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                .setValue(2).setText("2 - Moyennement").setFormattedText("2 - Moyennement")
                .setName("")
                .setQuestionVersionEntity(questionVersion2).build();

        AnswerChoiceEntity answerChoiceQ2A4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                .setValue(3).setText("3 - Beaucoup").setFormattedText("3 - Beaucoup")
                .setName("")
                .setQuestionVersionEntity(questionVersion2).build();

        AnswerChoiceEntity answerChoiceQ2A5 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(5)
                .setValue(4).setText("4 - Énormément").setFormattedText("4 - Énormément")
                .setName("")
                .setQuestionVersionEntity(questionVersion2).build();

        answerChoiceQ2A1 = answerChoiceDal.save(answerChoiceQ2A1);
        answerChoiceQ2A2 = answerChoiceDal.save(answerChoiceQ2A2);
        answerChoiceQ2A3 = answerChoiceDal.save(answerChoiceQ2A3);
        answerChoiceQ2A4 = answerChoiceDal.save(answerChoiceQ2A4);
        answerChoiceQ2A5 = answerChoiceDal.save(answerChoiceQ2A5);

        //Question3
        AnswerChoiceEntity answerChoiceQ3A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                .setValue(0).setText("0 - Pas du tout").setFormattedText("0 - Pas du tout")
                .setName("")
                .setQuestionVersionEntity(questionVersion3).build();

        AnswerChoiceEntity answerChoiceQ3A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("1 - Un peu").setFormattedText("2 - Un peu")
                .setName("")
                .setQuestionVersionEntity(questionVersion3).build();

        AnswerChoiceEntity answerChoiceQ3A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                .setValue(2).setText("2 - Moyennement").setFormattedText("2 - Moyennement")
                .setName("")
                .setQuestionVersionEntity(questionVersion3).build();

        AnswerChoiceEntity answerChoiceQ3A4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                .setValue(3).setText("3 - Beaucoup").setFormattedText("3 - Beaucoup")
                .setName("")
                .setQuestionVersionEntity(questionVersion3).build();

        AnswerChoiceEntity answerChoiceQ3A5 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(5)
                .setValue(4).setText("4 - Énormément").setFormattedText("4 - Énormément")
                .setName("")
                .setQuestionVersionEntity(questionVersion3).build();

        answerChoiceQ3A1 = answerChoiceDal.save(answerChoiceQ3A1);
        answerChoiceQ3A2 = answerChoiceDal.save(answerChoiceQ3A2);
        answerChoiceQ3A3 = answerChoiceDal.save(answerChoiceQ3A3);
        answerChoiceQ3A4 = answerChoiceDal.save(answerChoiceQ3A4);
        answerChoiceQ3A5 = answerChoiceDal.save(answerChoiceQ3A5);

        //Question4
        AnswerChoiceEntity answerChoiceQ4A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                .setValue(0).setText("0 - Pas du tout").setFormattedText("0 - Pas du tout")
                .setName("")
                .setQuestionVersionEntity(questionVersion4).build();

        AnswerChoiceEntity answerChoiceQ4A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("1 - Un peu").setFormattedText("2 - Un peu")
                .setName("")
                .setQuestionVersionEntity(questionVersion4).build();

        AnswerChoiceEntity answerChoiceQ4A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                .setValue(2).setText("2 - Moyennement").setFormattedText("2 - Moyennement")
                .setName("")
                .setQuestionVersionEntity(questionVersion4).build();

        AnswerChoiceEntity answerChoiceQ4A4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                .setValue(3).setText("3 - Beaucoup").setFormattedText("3 - Beaucoup")
                .setName("")
                .setQuestionVersionEntity(questionVersion4).build();

        AnswerChoiceEntity answerChoiceQ4A5 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(5)
                .setValue(4).setText("4 - Énormément").setFormattedText("4 - Énormément")
                .setName("")
                .setQuestionVersionEntity(questionVersion4).build();

        answerChoiceQ4A1 = answerChoiceDal.save(answerChoiceQ4A1);
        answerChoiceQ4A2 = answerChoiceDal.save(answerChoiceQ4A2);
        answerChoiceQ4A3 = answerChoiceDal.save(answerChoiceQ4A3);
        answerChoiceQ4A4 = answerChoiceDal.save(answerChoiceQ4A4);
        answerChoiceQ4A5 = answerChoiceDal.save(answerChoiceQ4A5);

        //Question5
        AnswerChoiceEntity answerChoiceQ5A1 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(1)
                .setValue(0).setText("Non").setFormattedText("Non")
                .setName("")
                .setQuestionVersionEntity(questionVersion5).build();

        AnswerChoiceEntity answerChoiceQ5A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("Oui").setFormattedText("Oui")
                .setSubQuestionCondition(subQuestionConditionEntityQ5.getId())
                .setName("")
                .setQuestionVersionEntity(questionVersion5).build();

        answerChoiceQ5A1 = answerChoiceDal.save(answerChoiceQ5A1);
        answerChoiceQ5A2 = answerChoiceDal.save(answerChoiceQ5A2);

        //Question6
        AnswerChoiceEntity answerChoiceQ6A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                .setValue(0).setText("0 - Pas du tout").setFormattedText("0 - Pas du tout")
                .setName("")
                .setQuestionVersionEntity(questionVersion6).build();

        AnswerChoiceEntity answerChoiceQ6A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("1 - Un peu").setFormattedText("2 - Un peu")
                .setName("")
                .setQuestionVersionEntity(questionVersion6).build();

        AnswerChoiceEntity answerChoiceQ6A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                .setValue(2).setText("2 - Moyennement").setFormattedText("2 - Moyennement")
                .setName("")
                .setQuestionVersionEntity(questionVersion6).build();

        AnswerChoiceEntity answerChoiceQ6A4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                .setValue(3).setText("3 - Beaucoup").setFormattedText("3 - Beaucoup")
                .setName("")
                .setQuestionVersionEntity(questionVersion6).build();

        AnswerChoiceEntity answerChoiceQ6A5 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(5)
                .setValue(4).setText("4 - Énormément").setFormattedText("4 - Énormément")
                .setName("")
                .setQuestionVersionEntity(questionVersion6).build();

        answerChoiceQ6A1 = answerChoiceDal.save(answerChoiceQ6A1);
        answerChoiceQ6A2 = answerChoiceDal.save(answerChoiceQ6A2);
        answerChoiceQ6A3 = answerChoiceDal.save(answerChoiceQ6A3);
        answerChoiceQ6A4 = answerChoiceDal.save(answerChoiceQ6A4);
        answerChoiceQ6A5 = answerChoiceDal.save(answerChoiceQ6A5);

        //Question7
        AnswerChoiceEntity answerChoiceQ7A1 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(1)
                .setValue(0).setText("Non").setFormattedText("Non")
                .setName("")
                .setQuestionVersionEntity(questionVersion7).build();

        AnswerChoiceEntity answerChoiceQ7A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("Oui").setFormattedText("Oui")
                .setSubQuestionCondition(subQuestionConditionEntityQ7.getId())
                .setName("")
                .setQuestionVersionEntity(questionVersion7).build();

        answerChoiceQ7A1 = answerChoiceDal.save(answerChoiceQ7A1);
        answerChoiceQ7A2 = answerChoiceDal.save(answerChoiceQ7A2);

        //Question8
        AnswerChoiceEntity answerChoiceQ8A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                .setValue(0).setText("0 - Pas du tout").setFormattedText("0 - Pas du tout")
                .setName("")
                .setQuestionVersionEntity(questionVersion8).build();

        AnswerChoiceEntity answerChoiceQ8A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("1 - Un peu").setFormattedText("2 - Un peu")
                .setName("")
                .setQuestionVersionEntity(questionVersion8).build();

        AnswerChoiceEntity answerChoiceQ8A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                .setValue(2).setText("2 - Moyennement").setFormattedText("2 - Moyennement")
                .setName("")
                .setQuestionVersionEntity(questionVersion8).build();

        AnswerChoiceEntity answerChoiceQ8A4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                .setValue(3).setText("3 - Beaucoup").setFormattedText("3 - Beaucoup")
                .setName("")
                .setQuestionVersionEntity(questionVersion8).build();

        AnswerChoiceEntity answerChoiceQ8A5 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(5)
                .setValue(4).setText("4 - Énormément").setFormattedText("4 - Énormément")
                .setName("")
                .setQuestionVersionEntity(questionVersion8).build();

        answerChoiceQ8A1 = answerChoiceDal.save(answerChoiceQ8A1);
        answerChoiceQ8A2 = answerChoiceDal.save(answerChoiceQ8A2);
        answerChoiceQ8A3 = answerChoiceDal.save(answerChoiceQ8A3);
        answerChoiceQ8A4 = answerChoiceDal.save(answerChoiceQ8A4);
        answerChoiceQ8A5 = answerChoiceDal.save(answerChoiceQ8A5);

        //Question9
        AnswerChoiceEntity answerChoiceQ9A1 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(1)
                .setValue(0).setText("Non").setFormattedText("Non")
                .setName("")
                .setQuestionVersionEntity(questionVersion9).build();

        AnswerChoiceEntity answerChoiceQ9A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                .setValue(1).setText("Oui").setFormattedText("Oui")
                .setName("")
                .setSupplementInput(true)
                .setSupplementInputText("Quel(s) est (sont), dans ce cas, le(s) médicament(s)?")
                .setQuestionVersionEntity(questionVersion9).build();

        answerChoiceQ9A1 = answerChoiceDal.save(answerChoiceQ9A1);
        answerChoiceQ9A2 = answerChoiceDal.save(answerChoiceQ9A2);

        //Traduction Question1
        AnswerChoiceTraductionEntity answerChoiceTraductionQ1A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("1 - Not at all")
                .setFormattedText("0 - Not at all")
                .setAnswerChoiceId(answerChoiceQ1A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ1A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("2 - A little")
                .setFormattedText("0 - A little")
                .setAnswerChoiceId(answerChoiceQ1A2.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ1A3 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("3 - Moderately")
                .setFormattedText("0 - Moderately")
                .setAnswerChoiceId(answerChoiceQ1A3.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ1A4 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("4 - A lot")
                .setFormattedText("0 - A lot")
                .setAnswerChoiceId(answerChoiceQ1A4.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ1A5 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("5 - Enormously")
                .setFormattedText("0 - Enormously")
                .setAnswerChoiceId(answerChoiceQ1A5.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ1A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ1A2);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ1A3);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ1A4);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ1A5);

        //Traduction Question2
        AnswerChoiceTraductionEntity answerChoiceTraductionQ2A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("1 - Not at all")
                .setFormattedText("0 - Not at all")
                .setAnswerChoiceId(answerChoiceQ2A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ2A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("2 - A little")
                .setFormattedText("0 - A little")
                .setAnswerChoiceId(answerChoiceQ2A2.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ2A3 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("3 - Moderately")
                .setFormattedText("0 - Moderately")
                .setAnswerChoiceId(answerChoiceQ2A3.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ2A4 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("4 - A lot")
                .setFormattedText("0 - A lot")
                .setAnswerChoiceId(answerChoiceQ2A4.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ2A5 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("5 - Enormously")
                .setFormattedText("0 - Enormously")
                .setAnswerChoiceId(answerChoiceQ2A5.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ2A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ2A2);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ2A3);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ2A4);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ2A5);

        //Traduction Question3
        AnswerChoiceTraductionEntity answerChoiceTraductionQ3A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("1 - Not at all")
                .setFormattedText("0 - Not at all")
                .setAnswerChoiceId(answerChoiceQ3A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ3A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("2 - A little")
                .setFormattedText("0 - A little")
                .setAnswerChoiceId(answerChoiceQ3A2.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ3A3 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("3 - Moderately")
                .setFormattedText("0 - Moderately")
                .setAnswerChoiceId(answerChoiceQ3A3.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ3A4 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("4 - A lot")
                .setFormattedText("0 - A lot")
                .setAnswerChoiceId(answerChoiceQ3A4.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ3A5 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("5 - Enormously")
                .setFormattedText("0 - Enormously")
                .setAnswerChoiceId(answerChoiceQ3A5.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ3A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ3A2);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ3A3);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ3A4);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ3A5);

        //Traduction Question4
        AnswerChoiceTraductionEntity answerChoiceTraductionQ4A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("1 - Not at all")
                .setFormattedText("0 - Not at all")
                .setAnswerChoiceId(answerChoiceQ4A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ4A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("2 - A little")
                .setFormattedText("0 - A little")
                .setAnswerChoiceId(answerChoiceQ4A2.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ4A3 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("3 - Moderately")
                .setFormattedText("0 - Moderately")
                .setAnswerChoiceId(answerChoiceQ4A3.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ4A4 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("4 - A lot")
                .setFormattedText("0 - A lot")
                .setAnswerChoiceId(answerChoiceQ4A4.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ4A5 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("5 - Enormously")
                .setFormattedText("0 - Enormously")
                .setAnswerChoiceId(answerChoiceQ4A5.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ4A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ4A2);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ4A3);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ4A4);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ4A5);

        //Traduction question 5
        AnswerChoiceTraductionEntity answerChoiceTraductionQ5A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("No")
                .setFormattedText("No")
                .setAnswerChoiceId(answerChoiceQ5A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ5A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("Yes")
                .setFormattedText("Yes")
                .setAnswerChoiceId(answerChoiceQ5A2.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ5A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ5A2);

        //Traduction Question6
        AnswerChoiceTraductionEntity answerChoiceTraductionQ6A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("1 - Not at all")
                .setFormattedText("0 - Not at all")
                .setAnswerChoiceId(answerChoiceQ6A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ6A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("2 - A little")
                .setFormattedText("0 - A little")
                .setAnswerChoiceId(answerChoiceQ6A2.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ6A3 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("3 - Moderately")
                .setFormattedText("0 - Moderately")
                .setAnswerChoiceId(answerChoiceQ6A3.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ6A4 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("4 - A lot")
                .setFormattedText("0 - A lot")
                .setAnswerChoiceId(answerChoiceQ6A4.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ6A5 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("5 - Enormously")
                .setFormattedText("0 - Enormously")
                .setAnswerChoiceId(answerChoiceQ6A5.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ6A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ6A2);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ6A3);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ6A4);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ6A5);

        //Traduction question 7
        AnswerChoiceTraductionEntity answerChoiceTraductionQ7A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("No")
                .setFormattedText("No")
                .setAnswerChoiceId(answerChoiceQ7A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ7A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("Yes")
                .setFormattedText("Yes")
                .setAnswerChoiceId(answerChoiceQ7A2.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ7A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ7A2);

        //Traduction Question8
        AnswerChoiceTraductionEntity answerChoiceTraductionQ8A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("1 - Not at all")
                .setFormattedText("0 - Not at all")
                .setAnswerChoiceId(answerChoiceQ8A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ8A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("2 - A little")
                .setFormattedText("0 - A little")
                .setAnswerChoiceId(answerChoiceQ8A2.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ8A3 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("3 - Moderately")
                .setFormattedText("0 - Moderately")
                .setAnswerChoiceId(answerChoiceQ8A3.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ8A4 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("4 - A lot")
                .setFormattedText("0 - A lot")
                .setAnswerChoiceId(answerChoiceQ8A4.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ8A5 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("5 - Enormously")
                .setFormattedText("0 - Enormously")
                .setAnswerChoiceId(answerChoiceQ8A5.getId())
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ8A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ8A2);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ8A3);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ8A4);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ8A5);

        //Traduction question 9
        AnswerChoiceTraductionEntity answerChoiceTraductionQ9A1 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("No")
                .setFormattedText("No")
                .setAnswerChoiceId(answerChoiceQ9A1.getId())
                .setName("")
                .build();

        AnswerChoiceTraductionEntity answerChoiceTraductionQ9A2 = AnswerChoiceTraductionEntity.builder()
                .setLanguage("en")
                .setText("Yes")
                .setFormattedText("Yes")
                .setAnswerChoiceId(answerChoiceQ9A2.getId())
                .setSupplementInputText("* Which, in this case, are the drugs?")
                .setName("")
                .build();

        answerChoiceTraductionDal.save(answerChoiceTraductionQ9A1);
        answerChoiceTraductionDal.save(answerChoiceTraductionQ9A2);

        //Link answer to question
        List<AnswerChoiceEntity> possibleAnswers1 = new ArrayList<>();
        possibleAnswers1.add(answerChoiceQ1A1);
        possibleAnswers1.add(answerChoiceQ1A2);
        possibleAnswers1.add(answerChoiceQ1A3);
        possibleAnswers1.add(answerChoiceQ1A4);
        possibleAnswers1.add(answerChoiceQ1A5);

        List<AnswerChoiceEntity> possibleAnswers2 = new ArrayList<>();
        possibleAnswers2.add(answerChoiceQ2A1);
        possibleAnswers2.add(answerChoiceQ2A2);
        possibleAnswers2.add(answerChoiceQ2A3);
        possibleAnswers2.add(answerChoiceQ2A4);
        possibleAnswers2.add(answerChoiceQ2A5);

        List<AnswerChoiceEntity> possibleAnswers3 = new ArrayList<>();
        possibleAnswers3.add(answerChoiceQ3A1);
        possibleAnswers3.add(answerChoiceQ3A2);
        possibleAnswers3.add(answerChoiceQ3A3);
        possibleAnswers3.add(answerChoiceQ3A4);
        possibleAnswers3.add(answerChoiceQ3A5);

        List<AnswerChoiceEntity> possibleAnswers4 = new ArrayList<>();
        possibleAnswers4.add(answerChoiceQ4A1);
        possibleAnswers4.add(answerChoiceQ4A2);
        possibleAnswers4.add(answerChoiceQ4A3);
        possibleAnswers4.add(answerChoiceQ4A4);
        possibleAnswers4.add(answerChoiceQ4A5);

        List<AnswerChoiceEntity> possibleAnswers5 = new ArrayList<>();
        possibleAnswers5.add(answerChoiceQ5A1);
        possibleAnswers5.add(answerChoiceQ5A2);

        List<AnswerChoiceEntity> possibleAnswers6 = new ArrayList<>();
        possibleAnswers6.add(answerChoiceQ6A1);
        possibleAnswers6.add(answerChoiceQ6A2);
        possibleAnswers6.add(answerChoiceQ6A3);
        possibleAnswers6.add(answerChoiceQ6A4);
        possibleAnswers6.add(answerChoiceQ6A5);

        List<AnswerChoiceEntity> possibleAnswers7 = new ArrayList<>();
        possibleAnswers7.add(answerChoiceQ7A1);
        possibleAnswers7.add(answerChoiceQ7A2);

        List<AnswerChoiceEntity> possibleAnswers8 = new ArrayList<>();
        possibleAnswers8.add(answerChoiceQ8A1);
        possibleAnswers8.add(answerChoiceQ8A2);
        possibleAnswers8.add(answerChoiceQ8A3);
        possibleAnswers8.add(answerChoiceQ8A4);
        possibleAnswers8.add(answerChoiceQ8A5);

        List<AnswerChoiceEntity> possibleAnswers9 = new ArrayList<>();
        possibleAnswers9.add(answerChoiceQ9A1);
        possibleAnswers9.add(answerChoiceQ9A2);

        questionVersion1.setPossibleAnswers(possibleAnswers1);
        questionVersion2.setPossibleAnswers(possibleAnswers2);
        questionVersion3.setPossibleAnswers(possibleAnswers3);
        questionVersion4.setPossibleAnswers(possibleAnswers4);
        questionVersion5.setPossibleAnswers(possibleAnswers5);
        questionVersion6.setPossibleAnswers(possibleAnswers6);
        questionVersion7.setPossibleAnswers(possibleAnswers7);
        questionVersion8.setPossibleAnswers(possibleAnswers8);
        questionVersion9.setPossibleAnswers(possibleAnswers9);
    }

    @Test
    void addMedicalHistoryToDatabase() {
        addEthnicitiesToDatabase();
        addFrequenciesToDatabase();
        addQuestionTypesToDatabase();
        addQuestionTypesToDatabase();

        SurveyEntity hmSurvey = SurveyEntity.builder()
                .setShortName("HM")
                .setName("Historique médical")
                .setFrequency(frequencyDal.findByName("unique"))
                .setDelais(1)
                .build();
        surveyDal.save(hmSurvey);

        SurveyVersionEntity hmSurveyVersion = SurveyVersionEntity.builder()
                .setSurvey(hmSurvey)
                .setSurvey_version(1)
                .setFrequency(hmSurvey.getFrequency())
                .setDelais(1)
                .setDate(getInstantTimestamp())
                .setName(hmSurvey.getName())
                .setShortName(hmSurvey.getShortName())
                .setFr(hmSurvey.getName())
                .setEn("Medical History")
                .build();

        surveyVersionDal.save(hmSurveyVersion);

        QuestionData question1Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(1)
                .setVerificationOrder(1)
                .setLabelFr("Souffrez-vous de rhinite allergique?")
                .setLabelEn("Do you have allergic rhinitis?")
                .build();
        addYesNoOrDontKnowQuestion(question1Data);

        QuestionData question2Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(2)
                .setVerificationOrder(2)
                .setLabelFr("Avez-vous de l'asthme?")
                .setLabelEn("Do you have asthma?")
                .setSupplementInputLabelsFr(Arrays.asList("Depuis quand?", null, null))
                .setSupplementInputLabelsEn(Arrays.asList("Since when?", null, null))
                .build();
        addYesNoOrDontKnowQuestion(question2Data);

        QuestionData question3Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(3)
                .setVerificationOrder(3)
                .setLabelFr("Avez-vous le nez bouché?")
                .setLabelEn("Do you have a blocked nose?")
                .build();
        addSymptomFrequencyQuestion(question3Data);

        QuestionData question4Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(4)
                .setVerificationOrder(4)
                .setLabelFr("Eternuez-vous?")
                .setLabelEn("Do you sneeze?")
                .build();
        addSymptomFrequencyQuestion(question4Data);

        QuestionData question5Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(5)
                .setVerificationOrder(5)
                .setLabelFr("Avez-vous le nez qui démange?")
                .setLabelEn("Do you have a itchy nose?")
                .build();
        addSymptomFrequencyQuestion(question5Data);

        QuestionData question6Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(6)
                .setVerificationOrder(6)
                .setLabelFr("Avez-vous le nez qui coule?")
                .setLabelEn("Do you have a runny nose?")
                .build();
        addSymptomFrequencyQuestion(question6Data);

        QuestionData question7Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(7)
                .setVerificationOrder(7)
                .setLabelFr("Avez-vous des difficultés respiratoires/dyspnée?")
                .setLabelEn("Do you have shortness of breath/dyspnoea?")
                .build();
        addSymptomFrequencyQuestion(question7Data);

        QuestionData question8Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(8)
                .setVerificationOrder(8)
                .setLabelFr("Entendez-vous des sifflements dans la poitrine?")
                .setLabelEn("Do you have wheezing in the chest?")
                .build();
        addSymptomFrequencyQuestion(question8Data);

        QuestionData question9Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(9)
                .setVerificationOrder(9)
                .setLabelFr("Ressentez-vous des douleurs à la poitrine pendant l'exercice physique?")
                .setLabelEn("Do you have chest tightness during physical exercise?")
                .build();
        addSymptomFrequencyQuestion(question9Data);

        QuestionData question10Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(10)
                .setVerificationOrder(10)
                .setLabelFr("Ressentez-vous de la fatigue ou des limitations dans les tâches quotidiennes?")
                .setLabelEn("Do you feel tired / limitations in doing daily tasks?")
                .build();
        addSymptomFrequencyQuestion(question10Data);

        QuestionData question11Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(11)
                .setVerificationOrder(11)
                .setLabelFr("Vous-réveillez vous la nuit?")
                .setLabelEn("Do you wake up in the night?")
                .build();
        addSymptomFrequencyQuestion(question11Data);

        QuestionData question12Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(12)
                .setVerificationOrder(12)
                .setLabelFr("Avez-vous les yeux qui démangent?")
                .setLabelEn("Do you have itchy eyes?")
                .build();
        addSymptomFrequencyQuestion(question12Data);

        QuestionData question13Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(13)
                .setVerificationOrder(13)
                .setLabelFr("Avez-vous les yeux rouges ou larmoyants?")
                .setLabelEn("Do you have red or watery eyes?")
                .build();
        addSymptomFrequencyQuestion(question13Data);

        QuestionData question14Data = QuestionData.builder()
                .setSurvey(hmSurvey)
                .setSurveyVersion(hmSurveyVersion)
                .setOrder(14)
                .setVerificationOrder(14)
                .setLabelFr("Avez-vous l'odorat altéré?")
                .setLabelEn("Do you have an impaired smell?")
                .build();
        addSymptomFrequencyQuestion(question14Data);
    }

    private void addYesNoOrDontKnowQuestion(QuestionData questionData) {

        QuestionEntity question = QuestionEntity.builder()
                .setSurvey(questionData.getSurvey())
                .setText(questionData.getLabelFr())
                .setName("")
                .build();
        questionDal.save(question);
        questionData.setInfoFr(NO_INFO);
        questionData.setInfoEn(NO_INFO);
        questionData.setChoiceLabelsFr(YES_NO_DONT_KNOW_CHOICES_FR);
        questionData.setChoiceLabelsEn(YES_NO_DONT_KNOW_CHOICES_EN);

        createQuestionVersion(question, questionData);
        questionData.getSurvey().getQuestions().add(question);
    }

    private void addSymptomFrequencyQuestion(QuestionData questionData) {
        QuestionEntity question = QuestionEntity.builder()
                .setSurvey(questionData.getSurvey())
                .setText(questionData.getLabelFr())
                .setName("")
                .build();
        questionDal.save(question);
        questionData.setInfoFr(NO_INFO);
        questionData.setInfoEn(NO_INFO);
        questionData.setChoiceLabelsFr(SYMPTOM_FREQUENCY_CHOICES_FR);
        questionData.setChoiceLabelsEn(SYMPTOM_FREQUENCY_CHOICES_EN);

        createQuestionVersion(question, questionData);
        questionData.getSurvey().getQuestions().add(question);
    }

    private void createQuestionVersion(QuestionEntity question, QuestionData questionData){
        QuestionVersionEntity questionVersion = QuestionVersionEntity.builder()
                .setQuestion_order(questionData.getOrder())
                .setVerificationOrder(questionData.getVerificationOrder())
                .setQuestionType(questionTypeDal.findByTypeCode(QuestionTypeCode.SINGLE_CHOICE.getCode()))
                .setText(question.getText())
                .setFormattedText(question.getText())
                .setInfo(questionData.getInfoFr())
                .setFormattedInfo(questionData.getInfoFr())
                .setName("")
                .setSurveyVersionEntity(questionData.getSurveyVersion())
                .setQuestion(question)
                .build();

        questionVersionDal.save(questionVersion);
        questionVersion.setPossibleAnswers(createAnswersChoices(questionVersion, questionData.getChoiceLabelsFr(), questionData.getSupplementInputLabelsFr()));
        questionVersionDal.save(questionVersion);

        createQuestionVersionTranslation(questionVersion, EN, questionData.getLabelEn(), questionData.getChoiceLabelsEn(), questionData.getSupplementInputLabelsEn());
    }

    private List<AnswerChoiceEntity> createAnswersChoices(QuestionVersionEntity questionVersion, List<String> answerChoiceLabels, List<String> supplementInputLabels){
        List<AnswerChoiceEntity> possibleAnswers = new ArrayList<>();
        for (int i = 0; i < answerChoiceLabels.size(); i ++) {
            String answerChoiceLabel = answerChoiceLabels.get(i);
            AnswerChoiceEntity answerChoice = AnswerChoiceEntity.builder()
                    .setAnswer_order(i + 1)
                    .setValue(0)
                    .setText(answerChoiceLabel)
                    .setFormattedText(answerChoiceLabel)
                    .setInfo("")
                    .setFormattedInfo("")
                    .setName("")
                    .setQuestionVersionEntity(questionVersion)
                    .build();
            if (supplementInputLabels != null && supplementInputLabels.size() > i && !StringUtils.isEmpty(supplementInputLabels.get(i))) {
                answerChoice.setSupplementInput(true);
                answerChoice.setSupplementInputText(supplementInputLabels.get(i));
            }
            answerChoice = answerChoiceDal.save(answerChoice);
            possibleAnswers.add(answerChoice);
        }
        return possibleAnswers;
    }

    private void createQuestionVersionTranslation(QuestionVersionEntity question, String language, String label, List<String> answerChoiceLabels, List<String> supplementInputLabels){
        QuestionVersionTraductionEntity questionVersionTranslation = QuestionVersionTraductionEntity.builder()
                .setQuestionVersionId(question.getId())
                .setLanguage(language)
                .setText(label)
                .setFormattedText(label)
                .setInfo("")
                .setFormattedInfo("")
                .setName("")
                .build();
        questionVersionTraductionDal.save(questionVersionTranslation);

        List<AnswerChoiceEntity> answerChoices = answerChoiceDal.findAllByQuestionId(question.getId());
        for (int i = 0; i < answerChoiceLabels.size(); i ++) {

            int finalAnswerIndex = i + 1;
            String answerChoiceLabel = answerChoiceLabels.get(i);
            AnswerChoiceEntity currentAnswer = answerChoices.stream().filter(a -> a.getAnswer_order() == finalAnswerIndex).findFirst().get();

            AnswerChoiceTraductionEntity answerTranslation = AnswerChoiceTraductionEntity.builder()
                    .setLanguage(language)
                    .setText(answerChoiceLabel)
                    .setFormattedText(answerChoiceLabel)
                    .setAnswerChoiceId(currentAnswer.getId())
                    .setName("")
                    .build();
            if (supplementInputLabels != null && supplementInputLabels.size() > i && !StringUtils.isEmpty(supplementInputLabels.get(i))) {
                answerTranslation.setSupplementInputText(supplementInputLabels.get(i));
            }
            answerChoiceTraductionDal.save(answerTranslation);
        }
    }

    private void addFrequenciesToDatabase(){
        FrequencyEntity frequencyJour = FrequencyEntity.builder()
                .setName("jour").setAnnualized_value(1)
                .setFr("jour")
                .setEn("day")
                .build();

        FrequencyEntity frequencySemaine = FrequencyEntity.builder()
                .setName("semaine").setAnnualized_value(7)
                .setFr("semaine")
                .setEn("week")
                .build();

        FrequencyEntity frequencyMois = FrequencyEntity.builder()
                .setName("mois").setAnnualized_value(30)
                .setFr("mois")
                .setEn("month")
                .build();

        FrequencyEntity frequencyYear = FrequencyEntity.builder()
                .setName("année").setAnnualized_value(365)
                .setFr("année")
                .setEn("year")
                .build();

        FrequencyEntity frequencyUnique = FrequencyEntity.builder()
                .setName("unique").setAnnualized_value(0)
                .setFr("unique")
                .setEn("unique")
                .build();

        if(frequencyDal.findByName(frequencyJour.getName()) == null) {
            frequencyDal.save(frequencyJour);
        }
        if(frequencyDal.findByName(frequencySemaine.getName()) == null) {
            frequencyDal.save(frequencySemaine);
        }
        if(frequencyDal.findByName(frequencyMois.getName()) == null) {
            frequencyDal.save(frequencyMois);
        }
        if(frequencyDal.findByName(frequencyYear.getName()) == null) {
            frequencyDal.save(frequencyYear);
        }
        if(frequencyDal.findByName(frequencyUnique.getName()) == null) {
            frequencyDal.save(frequencyUnique);
        }
    }

    private void addEthnicitiesToDatabase() {
        ethnicityDal.save(EthnicityEntity.builder()
                .setName("")
                .setFr("Causasien(ne)")
                .setEn("Causasian")
                .build());

        ethnicityDal.save(EthnicityEntity.builder()
                .setName("")
                .setFr("Noir(e)")
                .setEn("Black")
                .build());

        ethnicityDal.save(EthnicityEntity.builder()
                .setName("")
                .setDescription("")
                .setFr("Asiatique")
                .setEn("Asian")
                .build());
    }

    private void addQuestionTypesToDatabase() {
        QuestionTypeEntity calculated = QuestionTypeEntity.builder()
                .setName("Calculated")
                .setTypeCode(0)
                .build();
        QuestionTypeEntity singleChoice = QuestionTypeEntity.builder()
                .setName("Single choice")
                .setTypeCode(1)
                .build();
        QuestionTypeEntity multipleChoices = QuestionTypeEntity.builder()
                .setName("Multiple choices")
                .setTypeCode(2)
                .build();
        QuestionTypeEntity inValue = QuestionTypeEntity.builder()
                .setName("Int value (E.g. Hear rate)")
                .setTypeCode(3)
                .build();
        QuestionTypeEntity intRange = QuestionTypeEntity.builder()
                .setName("Int range value (E.g. Blood pressure with diastolic and systolic values, respiration rate per minute)")
                .setTypeCode(4)
                .build();
        QuestionTypeEntity floatValue = QuestionTypeEntity.builder()
                .setName("Float value (E.g. Temperature)")
                .setTypeCode(5)
                .build();
        QuestionTypeEntity floatRange = QuestionTypeEntity.builder()
                .setName("Float range value")
                .setTypeCode(6)
                .build();
        QuestionTypeEntity stringValue = QuestionTypeEntity.builder()
                .setName("String value")
                .setTypeCode(7)
                .build();
        QuestionTypeEntity dateValue = QuestionTypeEntity.builder()
                .setName("Date value")
                .setTypeCode(8)
                .build();
        QuestionTypeEntity dateRange = QuestionTypeEntity.builder()
                .setName("Date range value")
                .setTypeCode(9)
                .build();

        if (questionTypeDal.findByName(calculated.getName()) == null) {
            questionTypeDal.save(calculated);
        }
        if (questionTypeDal.findByName(singleChoice.getName()) == null) {
            questionTypeDal.save(singleChoice);
        }
        if (questionTypeDal.findByName(multipleChoices.getName()) == null) {
            questionTypeDal.save(multipleChoices);
        }
        if (questionTypeDal.findByName(inValue.getName()) == null) {
            questionTypeDal.save(inValue);
        }
        if (questionTypeDal.findByName(intRange.getName()) == null) {
            questionTypeDal.save(intRange);
        }
        if (questionTypeDal.findByName(floatValue.getName()) == null) {
            questionTypeDal.save(floatValue);
        }
        if(questionTypeDal.findByName(floatRange.getName()) == null) {
            questionTypeDal.save(floatRange);
        }
        if(questionTypeDal.findByName(stringValue.getName()) == null) {
            questionTypeDal.save(stringValue);
        }
        if(questionTypeDal.findByName(dateValue.getName()) == null) {
            questionTypeDal.save(dateValue);
        }
        if(questionTypeDal.findByName(dateRange.getName()) == null) {
            questionTypeDal.save(dateRange);
        }
    }

    private Timestamp getInstantTimestamp() {
        return Timestamp.from(Instant.now());
    }
}

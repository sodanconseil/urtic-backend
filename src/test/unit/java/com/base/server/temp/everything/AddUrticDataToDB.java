package unit.java.com.base.server.temp.everything;

import com.base.server.BaseKeycloakStarter;
import com.base.server.Question.persistance.jpa.QuestionDal;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionDal;
import com.base.server.SubQuestionCondition.persistance.jpa.SubQuestionConditionEntity;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceDal;
import com.base.server.answerChoice.persistance.jpa.AnswerChoiceEntity;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa.AnswerChoiceTraductionDal;
import com.base.server.answerChoice.traduction.answerChoiceTraduction.persistance.jpa.AnswerChoiceTraductionEntity;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.drug.persistence.jpa.DrugDal;
import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.frequency.persistence.jpa.FrequencyDal;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.questionType.persistance.jpa.QuestionTypeDal;
import com.base.server.questionType.persistance.jpa.QuestionTypeEntity;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionDal;
import com.base.server.questionVersion.persistance.jpa.QuestionVersionEntity;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionDal;
import com.base.server.questionVersion.traduction.questionVersionTraduction.persistance.jpa.QuestionVersionTraductionEntity;
import com.base.server.survey.persistance.jpa.SurveyDal;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionDal;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import com.base.server.unit.persistence.jpa.UnitDal;
import com.base.server.unit.persistence.jpa.UnitEntity;
import com.base.server.user.persistence.jpa.UserDal;
import com.base.server.user.persistence.jpa.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.*;

@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest(properties = { "spring.jpa.hibernate.ddl-auto=update", "server.port=8084" })
public class AddUrticDataToDB {

//        TODO: Add verificationOrder in every survey

        @Autowired
        AnswerChoiceDal answerChoiceDal;
        @Autowired
        DrugDal drugDal;
        @Autowired
        UnitDal unitDal;
        @Autowired
        FrequencyDal frequencyDal;
        @Autowired
        UserDal userDal;
        @Autowired
        EthnicityDal ethnicityDal;
        @Autowired
        QuestionTypeDal questionTypeDal;
        @Autowired
        QuestionDal questionDal;
        @Autowired
        QuestionVersionDal questionVersionDal;
        @Autowired
        QuestionVersionTraductionDal questionVersionTraductionDal;
        @Autowired
        SurveyDal surveyDal;
        @Autowired
        SurveyVersionDal surveyVersionDal;
        @Autowired
        SubQuestionConditionDal subQuestionConditionDal;
        @Autowired
        AnswerChoiceTraductionDal answerChoiceTraductionDal;

        @Test
        public void AddFrequencyToDatabase(){
                FrequencyEntity frequencyJour = FrequencyEntity.builder()
                        .setName("jour").setAnnualized_value(1)
                        .setFr("jour")
                        .setEn("day")
                        .build();

                FrequencyEntity frequencySemaine = FrequencyEntity.builder()
                        .setName("semaine").setAnnualized_value(7)
                        .setFr("semaine")
                        .setEn("week")
                        .build();

                FrequencyEntity frequencyMois = FrequencyEntity.builder()
                        .setName("mois").setAnnualized_value(30)
                        .setFr("mois")
                        .setEn("month")
                        .build();

                FrequencyEntity frequencyYear = FrequencyEntity.builder()
                        .setName("année").setAnnualized_value(365)
                        .setFr("année")
                        .setEn("year")
                        .build();

                FrequencyEntity frequencyUnique = FrequencyEntity.builder()
                        .setName("unique").setAnnualized_value(0)
                        .setFr("unique")
                        .setEn("unique")
                        .build();


                if(frequencyDal.findByName(frequencyJour.getName()) == null) {frequencyDal.save(frequencyJour); }
                if(frequencyDal.findByName(frequencySemaine.getName()) == null) {frequencyDal.save(frequencySemaine); }
                if(frequencyDal.findByName(frequencyMois.getName()) == null) {frequencyDal.save(frequencyMois); }
                if(frequencyDal.findByName(frequencyYear.getName()) == null) {frequencyDal.save(frequencyYear); }
                if(frequencyDal.findByName(frequencyUnique.getName()) == null) {frequencyDal.save(frequencyUnique); }
        }

        @Test
        public void TestInsertData() {

                DrugEntity drug;
                DosageEntity dosage;
                UnitEntity unit_mg = UnitEntity.builder().setName("mg").build();
                unit_mg = unitDal.save(unit_mg);
                UnitEntity unit_ui_ml = UnitEntity.builder().setName("ui/ml").build();
                unitDal.save(unit_ui_ml);
                UnitEntity unit_percent = UnitEntity.builder().setName("%").build();
                unitDal.save(unit_percent);

                drug = DrugEntity.builder()
                        .setName("cetirizine").setDescription("Reactine").setCommercial_name("Reactine")
                                .build();

                dosage = DosageEntity.builder()
                                .setName("5").setUnitMeasureEntity(unit_mg).setDescription("5 mg").build();
                drug.getDosages().add(dosage);

                dosage = DosageEntity.builder()
                                .setName("10").setUnitMeasureEntity(unit_mg).setDescription("10 mg").build();
                drug.getDosages().add(dosage);

                dosage = DosageEntity.builder()
                                .setName("20").setUnitMeasureEntity(unit_mg).setDescription("20 mg").build();
                drug.getDosages().add(dosage);
                drugDal.save(drug);

                UserEntity johnDoe = UserEntity.builder()
                                .setName("John Doe").setDescription("John Doe")
                        .setPrefferedLanguage("fr")
                        .setCreationDate(Timestamp.from(new Date().toInstant()))
                        .setKeycloakId("9921db69-5f0f-4774-9c49-61686c488db9").build();

                userDal.save(johnDoe);
        }

        @Test
        public void AddEthnicitiesToDatabase() {
                ethnicityDal.save(EthnicityEntity.builder()
                        .setName("")
                        .setFr("Causasien(ne)")
                        .setEn("Causasian")
                        .build());

                ethnicityDal.save(EthnicityEntity.builder()
                        .setName("")
                        .setFr("Noir(e)")
                        .setEn("Black")
                        .build());

                ethnicityDal.save(EthnicityEntity.builder()
                        .setName("")
                        .setDescription("")
                        .setFr("Asiatique")
                        .setEn("Asian")
                        .build());
        }

        @Test
        public void AddDrugsAndDosagesToDatabase() {
                AddFrequencyToDatabase();
                UnitEntity unit_mg = unitDal.findByName("mg");
                UnitEntity unit_ui_ml = unitDal.findByName("ui/ml");
                UnitEntity unit_percent = unitDal.findByName("%");

                /*
                 * Les noms commerciaux doivent toujours commencer par une majuscule, PAS le nom
                 * generique. Celui-ci va dependre de l'affiche.
                 *
                 * https://www.btb.termiumplus.gc.ca/tpv2guides/guides/clefsfp/index-fra.html?
                 * lang=fra&lettr=indx_catlog_m&page=90ugM5Q8qMNo.html
                 */
                DrugEntity bilastine = DrugEntity.builder().setName("bilastine").setCommercial_name("Blexten").build();
                DrugEntity cetirizine = DrugEntity.builder().setName("cetirizine").setCommercial_name("Reactine")
                                .build();
                DrugEntity chlorpheniramine = DrugEntity.builder().setName("chlorpheniramine ")
                                .setCommercial_name("Chlor tripolon").build();
                DrugEntity cyproheptadine = DrugEntity.builder().setName("cyproheptadine")
                                .setCommercial_name("Periactin").build();
                DrugEntity desloratadine = DrugEntity.builder().setName("desloratadine").setCommercial_name("Aerius")
                                .build();
                DrugEntity diphenhydramine = DrugEntity.builder().setName("diphenhydramine")
                                .setCommercial_name("Benadryl").build();
                DrugEntity fexofenadine = DrugEntity.builder().setName("fexofenadine").setCommercial_name("Allegra")
                                .build();
                DrugEntity hydroxyzine = DrugEntity.builder().setName("hydroxyzine ").setCommercial_name("Atarax")
                                .build();
                DrugEntity loratadine = DrugEntity.builder().setName("loratadine").setCommercial_name("Claritin")
                                .build();
                DrugEntity nortriptyline = DrugEntity.builder().setName("nortriptyline").setCommercial_name("Aventyl")
                                .build();
                DrugEntity panectyl = DrugEntity.builder().setName("panectyl").setCommercial_name("Triméprazine")
                                .build();
                DrugEntity promethazine = DrugEntity.builder().setName("promethazine").setCommercial_name("Phenergan")
                                .build();
                DrugEntity rupatadine = DrugEntity.builder().setName("rupatadine").setCommercial_name("Rupall").build();
                DrugEntity doxepine = DrugEntity.builder().setName("doxépine").setCommercial_name("Sinequan").build();
                DrugEntity cimetidine = DrugEntity.builder().setName("cimetidine").setCommercial_name("Tagamet")
                                .build();
                DrugEntity famotidine = DrugEntity.builder().setName("famotidine").setCommercial_name("pepcid").build();
                DrugEntity ranitidine = DrugEntity.builder().setName("ranitidine").setCommercial_name("Zantac").build();
                DrugEntity colchicine = DrugEntity.builder().setName("colchicine").setCommercial_name("").build();
                DrugEntity dapsone = DrugEntity.builder().setName("dapsone").setCommercial_name("Avlosulfon").build();
                DrugEntity dexamethasone = DrugEntity.builder().setName("dexaméthasone").setCommercial_name("Decadron")
                                .build();
                DrugEntity fludrocortisone = DrugEntity.builder().setName("fludrocortisone")
                                .setCommercial_name("Florinef").build();
                DrugEntity hydroxychloroquine = DrugEntity.builder().setName("hydroxychloroquine")
                                .setCommercial_name("Plaquenil").build();
                DrugEntity methylprednisolonMedrol = DrugEntity.builder().setName("methylprednisolone")
                                .setCommercial_name("Medrol").build();
                DrugEntity prednisolone = DrugEntity.builder().setName("prednisolone").setCommercial_name("Pediapred")
                                .build();
                DrugEntity prednisone = DrugEntity.builder().setName("prednisone").setCommercial_name("Deltasone")
                                .build();
                DrugEntity sulfasalazine = DrugEntity.builder().setName("sulfasalazine")
                                .setCommercial_name("Salazopyrin").build();
                DrugEntity omalizumab = DrugEntity.builder().setName("omalizumab").setCommercial_name("Xolair").build();
                DrugEntity adalimumab = DrugEntity.builder().setName("Adalimumab").setCommercial_name("Humira").build();
                DrugEntity cromoglycatesodique = DrugEntity.builder().setName("Cromoglycate sodique")
                                .setCommercial_name("Intal").build();
                DrugEntity cyclophosphamide = DrugEntity.builder().setName("Cyclophosphamide")
                                .setCommercial_name("Procytox").build();
                DrugEntity cyclosporine = DrugEntity.builder().setName("Cyclosporine").setCommercial_name("Neoral")
                                .build();
                DrugEntity etanercept = DrugEntity.builder().setName("Étanercept").setCommercial_name("Enbrel").build();
                DrugEntity Inflixumab = DrugEntity.builder().setName("Inflixumab").setCommercial_name("Remicade")
                                .build();
                DrugEntity methotrexate = DrugEntity.builder().setName("Methotrexate").setCommercial_name("").build();
                DrugEntity mofetilmycophenolate = DrugEntity.builder().setName("Mofétil mycophénolate")
                                .setCommercial_name("CellCept").build();
                DrugEntity sirolimus = DrugEntity.builder().setName("Sirolimus").setCommercial_name("Rapamune").build();
                DrugEntity tacrolimus = DrugEntity.builder().setName("Tacrolimus").setCommercial_name("Prograf")
                                .build();
                DrugEntity montelukast = DrugEntity.builder().setName("Montélukast").setCommercial_name("Singulair")
                                .build();
                DrugEntity zafirlukast = DrugEntity.builder().setName("Zafirlukast").setCommercial_name("Accolate")
                                .build();
                DrugEntity acidetranexamique = DrugEntity.builder().setName("Acide tranexamique")
                                .setCommercial_name("Cyklokapron").build();
                DrugEntity epinephrine = DrugEntity.builder().setName("Epinephrine").setCommercial_name("").build();
                DrugEntity heparine = DrugEntity.builder().setName("Héparine").setCommercial_name("Hepalean").build();
                DrugEntity nifedipine = DrugEntity.builder().setName("Nifédipine").setCommercial_name("Adalat").build();
                DrugEntity olopatadine = DrugEntity.builder().setName("Olopatadine").setCommercial_name("Patanol")
                                .build();
                DrugEntity theophylline = DrugEntity.builder().setName("Théophylline").setCommercial_name("Theo dur")
                                .build();
                DrugEntity warfarine = DrugEntity.builder().setName("Warfarine").setCommercial_name("Coumadin").build();

                /* bilastine */
                DosageEntity dosage_bilastine_20 = DosageEntity.builder().setName("20").setUnitMeasureEntity(unit_mg)
                                .setDescription("20 mg").build();
                bilastine.getDosages().add(dosage_bilastine_20);

                /* cetirizine */
                DosageEntity dosage_cetirizine_5 = DosageEntity.builder().setName("5").setUnitMeasureEntity(unit_mg)
                                .setDescription("5 mg").build();

                DosageEntity dosage_cetirizine_10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();

                DosageEntity dosage_cetirizine_20 = DosageEntity.builder().setName("20").setUnitMeasureEntity(unit_mg)
                                .setDescription("20 mg").build();
                cetirizine.getDosages().add(dosage_cetirizine_5);
                cetirizine.getDosages().add(dosage_cetirizine_10);
                cetirizine.getDosages().add(dosage_cetirizine_20);

                /* chlorpheniramine */
                DosageEntity dosage_chlorpheniramine_4 = DosageEntity.builder().setName("4")
                                .setUnitMeasureEntity(unit_mg).setDescription("4 mg").build();
                chlorpheniramine.getDosages().add(dosage_chlorpheniramine_4);

                /* cyproheptadine */
                DosageEntity dosage_cyproheptadine_4 = DosageEntity.builder().setName("4").setUnitMeasureEntity(unit_mg)
                                .setDescription("4 mg").build();
                cyproheptadine.getDosages().add(dosage_cyproheptadine_4);

                /* desloratadine */
                DosageEntity dosage_desloratadine_5 = DosageEntity.builder().setName("5").setUnitMeasureEntity(unit_mg)
                                .setDescription("5 mg").build();
                desloratadine.getDosages().add(dosage_desloratadine_5);

                /* diphenhydramine */
                DosageEntity dosage_diphenhydramine_25 = DosageEntity.builder().setName("25")
                                .setUnitMeasureEntity(unit_mg).setDescription("25 mg").build();
                DosageEntity dosage_diphenhydramine_50 = DosageEntity.builder().setName("50")
                                .setUnitMeasureEntity(unit_mg).setDescription("25 mg").build();
                diphenhydramine.getDosages().add(dosage_diphenhydramine_25);
                diphenhydramine.getDosages().add(dosage_diphenhydramine_50);

                /* fexofenadine */
                DosageEntity dosage_fexofenadine_60 = DosageEntity.builder().setName("60").setUnitMeasureEntity(unit_mg)
                                .setDescription("60 mg").build();
                DosageEntity dosage_fexofenadine_120 = DosageEntity.builder().setName("120")
                                .setUnitMeasureEntity(unit_mg).setDescription("120 mg").build();
                fexofenadine.getDosages().add(dosage_fexofenadine_60);
                fexofenadine.getDosages().add(dosage_fexofenadine_120);

                /* hydroxyzine */
                DosageEntity dosage_hydroxyzine_10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                DosageEntity dosage_hydroxyzine_25 = DosageEntity.builder().setName("25").setUnitMeasureEntity(unit_mg)
                                .setDescription("25 mg").build();
                DosageEntity dosage_hydroxyzine_50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_mg)
                                .setDescription("50 mg").build();
                hydroxyzine.getDosages().add(dosage_hydroxyzine_10);
                hydroxyzine.getDosages().add(dosage_hydroxyzine_25);
                hydroxyzine.getDosages().add(dosage_hydroxyzine_50);

                /* loratadine */
                DosageEntity dosage_loratadine_10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                loratadine.getDosages().add(dosage_loratadine_10);

                /* promethazine */
                DosageEntity dosage_promethazine_50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_mg)
                                .setDescription("50 mg").build();
                promethazine.getDosages().add(dosage_promethazine_50);

                /* rupatadine */
                DosageEntity dosage_rupatadine_10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                DosageEntity dosage_rupatadine_25 = DosageEntity.builder().setName("25").setUnitMeasureEntity(unit_mg)
                                .setDescription("25 mg").build();
                DosageEntity dosage_rupatadine_50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_mg)
                                .setDescription("50 mg").build();
                DosageEntity dosage_rupatadine_75 = DosageEntity.builder().setName("75").setUnitMeasureEntity(unit_mg)
                                .setDescription("75 mg").build();
                DosageEntity dosage_rupatadine_100 = DosageEntity.builder().setName("100").setUnitMeasureEntity(unit_mg)
                                .setDescription("100 mg").build();

                rupatadine.getDosages().add(dosage_rupatadine_10);
                rupatadine.getDosages().add(dosage_rupatadine_25);
                rupatadine.getDosages().add(dosage_rupatadine_50);
                rupatadine.getDosages().add(dosage_rupatadine_75);
                rupatadine.getDosages().add(dosage_rupatadine_100);

                /* doxépine */
                DosageEntity dosage_doxepine_10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                DosageEntity dosage_doxepine_25 = DosageEntity.builder().setName("25").setUnitMeasureEntity(unit_mg)
                                .setDescription("25 mg").build();
                DosageEntity dosage_doxepine_50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_mg)
                                .setDescription("50 mg").build();
                DosageEntity dosage_doxepine_75 = DosageEntity.builder().setName("75").setUnitMeasureEntity(unit_mg)
                                .setDescription("75 mg").build();
                DosageEntity dosage_doxepine_100 = DosageEntity.builder().setName("100").setUnitMeasureEntity(unit_mg)
                                .setDescription("100 mg").build();
                doxepine.getDosages().add(dosage_doxepine_10);
                doxepine.getDosages().add(dosage_doxepine_25);
                doxepine.getDosages().add(dosage_doxepine_50);
                doxepine.getDosages().add(dosage_doxepine_75);
                doxepine.getDosages().add(dosage_doxepine_100);

                /* cimetidine */
                DosageEntity dosage_cimetidine_200 = DosageEntity.builder().setName("200").setUnitMeasureEntity(unit_mg)
                                .setDescription("200 mg").build();
                DosageEntity dosage_cimetidine_300 = DosageEntity.builder().setName("300").setUnitMeasureEntity(unit_mg)
                                .setDescription("300 mg").build();
                DosageEntity dosage_cimetidine_400 = DosageEntity.builder().setName("400").setUnitMeasureEntity(unit_mg)
                                .setDescription("400 mg").build();
                DosageEntity dosage_cimetidine_600 = DosageEntity.builder().setName("600").setUnitMeasureEntity(unit_mg)
                                .setDescription("600 mg").build();
                DosageEntity dosage_cimetidine_800 = DosageEntity.builder().setName("800").setUnitMeasureEntity(unit_mg)
                                .setDescription("800 mg").build();
                cimetidine.getDosages().add(dosage_cimetidine_200);
                cimetidine.getDosages().add(dosage_cimetidine_300);
                cimetidine.getDosages().add(dosage_cimetidine_400);
                cimetidine.getDosages().add(dosage_cimetidine_600);
                cimetidine.getDosages().add(dosage_cimetidine_800);

                /* famotidine */
                DosageEntity dosage_famotidine_10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                DosageEntity dosage_famotidine_20 = DosageEntity.builder().setName("20").setUnitMeasureEntity(unit_mg)
                                .setDescription("20 mg").build();
                DosageEntity dosage_famotidine_40 = DosageEntity.builder().setName("40").setUnitMeasureEntity(unit_mg)
                                .setDescription("40 mg").build();
                famotidine.getDosages().add(dosage_famotidine_10);
                famotidine.getDosages().add(dosage_famotidine_20);
                famotidine.getDosages().add(dosage_famotidine_40);

                /* ranitidine */
                DosageEntity dosage_ranitidine_75 = DosageEntity.builder().setName("75").setUnitMeasureEntity(unit_mg)
                                .setDescription("75 mg").build();
                DosageEntity dosage_ranitidine_150 = DosageEntity.builder().setName("150").setUnitMeasureEntity(unit_mg)
                                .setDescription("150 mg").build();
                DosageEntity dosage_ranitidine_300 = DosageEntity.builder().setName("300").setUnitMeasureEntity(unit_mg)
                                .setDescription("300 mg").build();
                ranitidine.getDosages().add(dosage_ranitidine_75);
                ranitidine.getDosages().add(dosage_ranitidine_150);
                ranitidine.getDosages().add(dosage_ranitidine_300);

                /* colchicine */

                DosageEntity dosage_colchicine_06 = DosageEntity.builder().setName("0.6").setUnitMeasureEntity(unit_mg)
                                .setDescription("0.6 mg").build();
                colchicine.getDosages().add(dosage_colchicine_06);

                /* dapsone */
                DosageEntity dosage_dapsone_06 = DosageEntity.builder().setName("100").setUnitMeasureEntity(unit_mg)
                                .setDescription("100 mg").build();
                dapsone.getDosages().add(dosage_dapsone_06);

                /* dexaméthasone */
                DosageEntity dosage_dexamethasone_05 = DosageEntity.builder().setName("0.5")
                                .setUnitMeasureEntity(unit_mg).setDescription("0.5 mg").build();
                DosageEntity dosage_dexamethasone_24 = DosageEntity.builder().setName("2.4")
                                .setUnitMeasureEntity(unit_mg).setDescription("2.4 mg").build();
                dexamethasone.getDosages().add(dosage_dexamethasone_05);
                dexamethasone.getDosages().add(dosage_dexamethasone_24);

                /* fludrocortisone */
                DosageEntity dosage_fludrocortisone = DosageEntity.builder().setName("null")
                                .setUnitMeasureEntity(unit_mg).setDescription(" ").build();
                fludrocortisone.getDosages().add(dosage_fludrocortisone);

                /* hydroxychloroquine */
                DosageEntity dosage_hydroxychloroquine = DosageEntity.builder().setName("200")
                                .setUnitMeasureEntity(unit_mg).setDescription("200 mg").build();
                hydroxychloroquine.getDosages().add(dosage_hydroxychloroquine);

                /* methylprednisolone [Medrol] */

                DosageEntity dosage_methylprednisolonMedrol4 = DosageEntity.builder().setName("4")
                                .setUnitMeasureEntity(unit_mg).setDescription("4 mg").build();
                DosageEntity dosage_methylprednisolonMedrol16 = DosageEntity.builder().setName("16")
                                .setUnitMeasureEntity(unit_mg).setDescription("16 mg").build();
                methylprednisolonMedrol.getDosages().add(dosage_methylprednisolonMedrol4);
                methylprednisolonMedrol.getDosages().add(dosage_methylprednisolonMedrol16);

                /* prednisone */
                DosageEntity dosage_prednisone15 = DosageEntity.builder().setName("1.5").setUnitMeasureEntity(unit_mg)
                                .setDescription("1.5 mg").build();
                DosageEntity dosage_prednisone50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_mg)
                                .setDescription("50 mg").build();
                prednisone.getDosages().add(dosage_prednisone15);
                prednisone.getDosages().add(dosage_prednisone50);

                /* sulfasalazine */
                DosageEntity dosage_sulfasalazine = DosageEntity.builder().setName("500").setUnitMeasureEntity(unit_mg)
                                .setDescription("500 mg").build();
                sulfasalazine.getDosages().add(dosage_sulfasalazine);

                /* omalizumab */
                DosageEntity dosage_omalizumab7505 = DosageEntity.builder().setName("75/0.5")
                                .setUnitMeasureEntity(unit_mg).setDescription("75/0.5 mg injectable").build();
                DosageEntity dosage_omalizumab1501 = DosageEntity.builder().setName("150/1")
                                .setUnitMeasureEntity(unit_ui_ml).setDescription("150/1 mL injectable").build();
                omalizumab.getDosages().add(dosage_omalizumab7505);
                omalizumab.getDosages().add(dosage_omalizumab1501);

                /* Adalimumab */
                DosageEntity dosage_Adalimumab10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg injectable").build();
                DosageEntity dosage_Adalimumab20 = DosageEntity.builder().setName("20").setUnitMeasureEntity(unit_mg)
                                .setDescription("20 mg injectable").build();
                DosageEntity dosage_Adalimumab40 = DosageEntity.builder().setName("40").setUnitMeasureEntity(unit_mg)
                                .setDescription("40 mg injectable").build();
                DosageEntity dosage_Adalimumab80 = DosageEntity.builder().setName("80").setUnitMeasureEntity(unit_mg)
                                .setDescription("80 mg injectable").build();
                adalimumab.getDosages().add(dosage_Adalimumab10);
                adalimumab.getDosages().add(dosage_Adalimumab20);
                adalimumab.getDosages().add(dosage_Adalimumab40);
                adalimumab.getDosages().add(dosage_Adalimumab80);

                /* Cromoglycate sodique */
                DosageEntity dosage_Cromoglycatesodique10 = DosageEntity.builder().setName("1%")
                                .setUnitMeasureEntity(unit_mg).setDescription("1% nébul.").build();
                cromoglycatesodique.getDosages().add(dosage_Cromoglycatesodique10);

                /* Cyclophosphamide */
                DosageEntity dosage_Cyclophosphamide25 = DosageEntity.builder().setName("25")
                                .setUnitMeasureEntity(unit_mg).setDescription("25 mg injectable").build();
                DosageEntity dosage_Cyclophosphamide50 = DosageEntity.builder().setName("50")
                                .setUnitMeasureEntity(unit_mg).setDescription("50 mg injectable").build();
                cyclophosphamide.getDosages().add(dosage_Cyclophosphamide25);
                cyclophosphamide.getDosages().add(dosage_Cyclophosphamide50);

                /* Cyclosporine */
                DosageEntity dosage_Cyclosporine25 = DosageEntity.builder().setName("25").setUnitMeasureEntity(unit_mg)
                                .setDescription("25 mg").build();
                DosageEntity dosage_Cyclosporine50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_mg)
                                .setDescription("50 mg").build();
                DosageEntity dosage_Cyclosporine100 = DosageEntity.builder().setName("100")
                                .setUnitMeasureEntity(unit_mg).setDescription("100 mg").build();
                cyclosporine.getDosages().add(dosage_Cyclosporine25);
                cyclosporine.getDosages().add(dosage_Cyclosporine50);
                cyclosporine.getDosages().add(dosage_Cyclosporine100);

                /* Étanercept */
                DosageEntity dosage_etanercept25 = DosageEntity.builder().setName("25").setUnitMeasureEntity(unit_mg)
                                .setDescription("25 mg injectable").build();
                DosageEntity dosage_etanercept50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_mg)
                                .setDescription("50 mg injectable").build();
                etanercept.getDosages().add(dosage_etanercept25);
                etanercept.getDosages().add(dosage_etanercept50);

                /* Inflixumab */
                DosageEntity dosage_Inflixumab100 = DosageEntity.builder().setName("100").setUnitMeasureEntity(unit_mg)
                                .setDescription("100 mg injectable").build();
                Inflixumab.getDosages().add(dosage_Inflixumab100);

                /* Methotrexate */
                DosageEntity dosage_Methotrexate25 = DosageEntity.builder().setName("2.5").setUnitMeasureEntity(unit_mg)
                                .setDescription("2.5 mg").build();
                DosageEntity dosage_Methotrexate10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                methotrexate.getDosages().add(dosage_Methotrexate25);
                methotrexate.getDosages().add(dosage_Methotrexate10);

                /* Mofétilmycophénolate */

                DosageEntity dosage_mofetilmycophenolate250 = DosageEntity.builder().setName("250")
                                .setUnitMeasureEntity(unit_mg).setDescription("250 mg").build();
                DosageEntity dosage_mofetilmycophenolate500 = DosageEntity.builder().setName("500")
                                .setUnitMeasureEntity(unit_mg).setDescription("500 mg").build();
                mofetilmycophenolate.getDosages().add(dosage_mofetilmycophenolate250);
                mofetilmycophenolate.getDosages().add(dosage_mofetilmycophenolate500);

                /* Sirolimus */
                DosageEntity dosage_Sirolimus1 = DosageEntity.builder().setName("1").setUnitMeasureEntity(unit_mg)
                                .setDescription("1 mg").build();
                DosageEntity dosage_Sirolimus2 = DosageEntity.builder().setName("2").setUnitMeasureEntity(unit_mg)
                                .setDescription("2 mg").build();
                DosageEntity dosage_Sirolimus5 = DosageEntity.builder().setName("5").setUnitMeasureEntity(unit_mg)
                                .setDescription("5 mg").build();
                sirolimus.getDosages().add(dosage_Sirolimus1);
                sirolimus.getDosages().add(dosage_Sirolimus2);
                sirolimus.getDosages().add(dosage_Sirolimus5);

                /* Tacrolimus */
                DosageEntity dosage_Tacrolimus05 = DosageEntity.builder().setName("0.5").setUnitMeasureEntity(unit_mg)
                                .setDescription("0.5 mg").build();
                DosageEntity dosage_Tacrolimus1 = DosageEntity.builder().setName("1").setUnitMeasureEntity(unit_mg)
                                .setDescription("1 mg").build();
                DosageEntity dosage_Tacrolimus5 = DosageEntity.builder().setName("5").setUnitMeasureEntity(unit_mg)
                                .setDescription("5 mg").build();
                tacrolimus.getDosages().add(dosage_Tacrolimus05);
                tacrolimus.getDosages().add(dosage_Tacrolimus1);
                tacrolimus.getDosages().add(dosage_Tacrolimus5);

                /* Montélukast */
                DosageEntity dosageMontelukast4 = DosageEntity.builder().setName("4").setUnitMeasureEntity(unit_mg)
                                .setDescription("4 mg").build();
                DosageEntity dosageMontelukast5 = DosageEntity.builder().setName("5").setUnitMeasureEntity(unit_mg)
                                .setDescription("5 mg").build();
                DosageEntity dosageMontelukast10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                montelukast.getDosages().add(dosageMontelukast4);
                montelukast.getDosages().add(dosageMontelukast5);
                montelukast.getDosages().add(dosageMontelukast10);

                /* Zafirlukast */
                DosageEntity dosage_Zafirlukast20 = DosageEntity.builder().setName("20").setUnitMeasureEntity(unit_mg)
                                .setDescription("20 mg").build();
                zafirlukast.getDosages().add(dosage_Zafirlukast20);

                /* Acidetranexamique */
                DosageEntity dosage_Acidetranexamique500 = DosageEntity.builder().setName("500")
                                .setUnitMeasureEntity(unit_mg).setDescription("500 mg").build();
                acidetranexamique.getDosages().add(dosage_Acidetranexamique500);

                /* Epinephrine */
                DosageEntity dosage_Epinephrine015 = DosageEntity.builder().setName("0.15")
                                .setUnitMeasureEntity(unit_mg).setDescription("0.15 mg").build();
                DosageEntity dosage_Epinephrine03 = DosageEntity.builder().setName("0.3").setUnitMeasureEntity(unit_mg)
                                .setDescription("0.3 mg").build();
                epinephrine.getDosages().add(dosage_Epinephrine015);
                epinephrine.getDosages().add(dosage_Epinephrine03);

                /* Héparine */
                DosageEntity dosageHeparine2 = DosageEntity.builder().setName("2").setUnitMeasureEntity(unit_ui_ml)
                                .setDescription("2 injectable").build();
                DosageEntity dosageHeparine10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_ui_ml)
                                .setDescription("10 injectable").build();
                DosageEntity dosageHeparine40 = DosageEntity.builder().setName("40").setUnitMeasureEntity(unit_ui_ml)
                                .setDescription("40 injectable").build();
                DosageEntity dosageHeparine50 = DosageEntity.builder().setName("50").setUnitMeasureEntity(unit_ui_ml)
                                .setDescription("50 injectable").build();
                DosageEntity dosageHeparine100 = DosageEntity.builder().setName("100").setUnitMeasureEntity(unit_ui_ml)
                                .setDescription("100 injectable").build();
                DosageEntity dosageHeparine1000 = DosageEntity.builder().setName("1000")
                                .setUnitMeasureEntity(unit_ui_ml).setDescription("1000 injectable").build();
                heparine.getDosages().add(dosageHeparine2);
                heparine.getDosages().add(dosageHeparine10);
                heparine.getDosages().add(dosageHeparine40);
                heparine.getDosages().add(dosageHeparine50);
                heparine.getDosages().add(dosageHeparine100);
                heparine.getDosages().add(dosageHeparine1000);

                /* Nifédipine */
                DosageEntity dosageNifedipine5 = DosageEntity.builder().setName("5").setUnitMeasureEntity(unit_mg)
                                .setDescription("5 mg").build();
                DosageEntity dosageNifedipine10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                DosageEntity dosageNifedipine20 = DosageEntity.builder().setName("20").setUnitMeasureEntity(unit_mg)
                                .setDescription("20 mg").build();
                DosageEntity dosageNifedipine30 = DosageEntity.builder().setName("30").setUnitMeasureEntity(unit_mg)
                                .setDescription("30 mg").build();
                DosageEntity dosageNifedipine60 = DosageEntity.builder().setName("60").setUnitMeasureEntity(unit_mg)
                                .setDescription("60 mg").build();
                nifedipine.getDosages().add(dosageNifedipine5);
                nifedipine.getDosages().add(dosageNifedipine10);
                nifedipine.getDosages().add(dosageNifedipine20);
                nifedipine.getDosages().add(dosageNifedipine30);
                nifedipine.getDosages().add(dosageNifedipine60);

                /* Olopatadine */

                DosageEntity dosage_Olopatadine01 = DosageEntity.builder().setName("0.1")
                                .setUnitMeasureEntity(unit_percent).setDescription("0.1 %").build();
                DosageEntity dosage_Olopatadine02 = DosageEntity.builder().setName("0.2")
                                .setUnitMeasureEntity(unit_percent).setDescription("0.2 %").build();
                DosageEntity dosage_Olopatadine07 = DosageEntity.builder().setName("0.7")
                                .setUnitMeasureEntity(unit_percent).setDescription("0.7 %").build();
                olopatadine.getDosages().add(dosage_Olopatadine01);
                olopatadine.getDosages().add(dosage_Olopatadine02);
                olopatadine.getDosages().add(dosage_Olopatadine07);

                /* Théophylline */
                DosageEntity dosageTheophylline300 = DosageEntity.builder().setName("300")
                                .setUnitMeasureEntity(unit_mg).setDescription("300 mg").build();
                theophylline.getDosages().add(dosageTheophylline300);

                /* Warfarine */

                DosageEntity dosage_Warfarine1 = DosageEntity.builder().setName("1").setUnitMeasureEntity(unit_mg)
                                .setDescription("1 mg").build();
                DosageEntity dosage_Warfarine2 = DosageEntity.builder().setName("2").setUnitMeasureEntity(unit_mg)
                                .setDescription("2 mg").build();
                DosageEntity dosage_Warfarine25 = DosageEntity.builder().setName("2.5").setUnitMeasureEntity(unit_mg)
                                .setDescription("2.5 mg").build();
                DosageEntity dosage_Warfarine10 = DosageEntity.builder().setName("10").setUnitMeasureEntity(unit_mg)
                                .setDescription("10 mg").build();
                warfarine.getDosages().add(dosage_Warfarine1);
                warfarine.getDosages().add(dosage_Warfarine2);
                warfarine.getDosages().add(dosage_Warfarine25);
                warfarine.getDosages().add(dosage_Warfarine10);

                /* Drugs */
                drugDal.save(bilastine);
                // Commenter la cetirizine car ajouter par le AddDataToDB
                // cetirizine = drugDal.save(cetirizine);
                drugDal.save(chlorpheniramine);
                drugDal.save(cyproheptadine);
                drugDal.save(desloratadine);
                drugDal.save(diphenhydramine);
                drugDal.save(fexofenadine);
                drugDal.save(hydroxyzine);
                drugDal.save(loratadine);
                drugDal.save(nortriptyline);
                drugDal.save(panectyl);
                drugDal.save(promethazine);
                drugDal.save(rupatadine);
                drugDal.save(doxepine);
                drugDal.save(cimetidine);
                drugDal.save(famotidine);
                drugDal.save(ranitidine);
                drugDal.save(colchicine);
                drugDal.save(dapsone);
                drugDal.save(dexamethasone);
                drugDal.save(fludrocortisone);
                drugDal.save(hydroxychloroquine);
                drugDal.save(methylprednisolonMedrol);
                drugDal.save(prednisone);
                drugDal.save(sulfasalazine);
                drugDal.save(omalizumab);
                drugDal.save(adalimumab);
                drugDal.save(cromoglycatesodique);
                drugDal.save(cyclophosphamide);
                drugDal.save(cyclosporine);
                drugDal.save(etanercept);
                drugDal.save(Inflixumab);
                drugDal.save(methotrexate);
                drugDal.save(mofetilmycophenolate);
                drugDal.save(sirolimus);
                drugDal.save(tacrolimus);
                drugDal.save(montelukast);
                drugDal.save(zafirlukast);
                drugDal.save(acidetranexamique);
                drugDal.save(epinephrine);
                drugDal.save(heparine);
                drugDal.save(nifedipine);
                drugDal.save(olopatadine);
                drugDal.save(theophylline);
                drugDal.save(prednisolone);
        }

        @Test
        public void addQuestionTypeToDatabase() {
                QuestionTypeEntity calculated = QuestionTypeEntity.builder().setName("Calculated").setTypeCode(0)
                                .build();
                QuestionTypeEntity singleChoice = QuestionTypeEntity.builder().setName("Single choice").setTypeCode(1)
                                .build();
                QuestionTypeEntity multipleChoices = QuestionTypeEntity.builder().setName("Multiple choices")
                                .setTypeCode(2).build();
                QuestionTypeEntity inValue = QuestionTypeEntity.builder().setName("Int value (E.g. Hear rate)")
                                .setTypeCode(3).build();
                QuestionTypeEntity intRange = QuestionTypeEntity.builder().setName(
                                "Int range value (E.g. Blood pressure with diastolic and systolic values, respiration rate per minute)")
                                .setTypeCode(4).build();
                QuestionTypeEntity floatValue = QuestionTypeEntity.builder().setName("Float value (E.g. Temperature)")
                                .setTypeCode(5).build();
                QuestionTypeEntity floatRange = QuestionTypeEntity.builder().setName("Float range value").setTypeCode(6)
                                .build();
                QuestionTypeEntity stringValue = QuestionTypeEntity.builder().setName("String value").setTypeCode(7)
                                .build();
                QuestionTypeEntity dateValue = QuestionTypeEntity.builder().setName("Date value").setTypeCode(8)
                                .build();
                QuestionTypeEntity dateRange = QuestionTypeEntity.builder().setName("Date range value").setTypeCode(9)
                                .build();

                if(questionTypeDal.findByName(calculated.getName()) == null){ questionTypeDal.save(calculated); }
                if(questionTypeDal.findByName(singleChoice.getName()) == null){questionTypeDal.save(singleChoice); }
                if(questionTypeDal.findByName(multipleChoices.getName()) == null){questionTypeDal.save(multipleChoices); }
                if(questionTypeDal.findByName(inValue.getName()) == null){questionTypeDal.save(inValue); }
                if(questionTypeDal.findByName(intRange.getName()) == null){questionTypeDal.save(intRange); }
                if(questionTypeDal.findByName(floatValue.getName()) == null){questionTypeDal.save(floatValue); }
                if(questionTypeDal.findByName(floatRange.getName()) == null){questionTypeDal.save(floatRange); }
                if(questionTypeDal.findByName(stringValue.getName()) == null){questionTypeDal.save(stringValue); }
                if(questionTypeDal.findByName(dateValue.getName()) == null){questionTypeDal.save(dateValue); }
                if(questionTypeDal.findByName(dateRange.getName()) == null){questionTypeDal.save(dateRange); }
        }

        @Test
        public void addUasSurveyToDatabase() {
                AddFrequencyToDatabase();
                addQuestionTypeToDatabase();

                // Survey & Questions
                SurveyEntity UAS7 = SurveyEntity.builder().setShortName("UAS7")
                        .setName("Indice d’activité de l’urticaire")
                        .setFrequency(frequencyDal.findByName("jour"))
                        .setDelais(14)
                        .build();
                UAS7 = surveyDal.save(UAS7);

                QuestionEntity question1 = QuestionEntity.builder().setSurvey(UAS7)
                                .setText("Nombre de plaques d’urticaire").setName("Nombre de plaques d’urticaire")
                                .build();
                QuestionEntity question2 = QuestionEntity.builder().setSurvey(UAS7)
                                .setText("Intensité des démangeaisons (prurit)")
                                .setName("Intensité des démangeaisons (prurit)").build();
                QuestionEntity question3 = QuestionEntity.builder().setSurvey(UAS7)
                                .setText("Indice d’activité de l’urticaire quotidien")
                                .setName("Indice d’activité de l’urticaire quotidien").build();

                question1 = questionDal.save(question1);
                question2 = questionDal.save(question2);
                question3 = questionDal.save(question3);

                List<QuestionEntity> questions = new ArrayList<>();
                questions.add(question1);
                questions.add(question2);
                questions.add(question3);
                UAS7.setQuestions(questions);

                // Survey Version
                Timestamp yesterday = Timestamp.from(new Date().toInstant().minus(1, ChronoUnit.DAYS));
                Timestamp today = Timestamp.from(new Date().toInstant());

                SurveyVersionEntity surveyVersionOld = SurveyVersionEntity.builder().setSurvey(UAS7)
                        .setSurvey_version(1).setDate(yesterday)
                        .setFrequency(frequencyDal.findByName("jour"))
                        .setName(UAS7.getName()).setShortName(UAS7.getShortName())
                        .setFr(UAS7.getName()).setEn("Urticaria Activity Score")
                        .setDelais(14)
                        .build();
                SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(UAS7)
                        .setSurvey_version(2).setDate(today)
                        .setFrequency(frequencyDal.findByName("jour"))
                        .setDelais(14)
                        .setName(UAS7.getName()).setShortName(UAS7.getShortName())
                        .setFr(UAS7.getName()).setEn("Urticaria Activity Score")
                        .build();

                surveyVersionDal.save(surveyVersionOld);
                surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);

                // Question Version & Possible Answers (Answer Choice)
                QuestionVersionEntity questionVersion1 = QuestionVersionEntity.builder()
                        .setQuestion_order(1)
                        .setVerificationOrder(1)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Nombre de plaques d’urticaire")
                        .setFormattedText("Nombre de plaques d’urticaire").setName("")
                        .setInfo("Nombre des plaques tel que défini ci-dessous. Appuyez la case appropriée, qui définit le mieux votre situation.")
                        .setFormattedInfo("Nombre des plaques tel que défini ci-dessous. Appuyez la case appropriée, qui définit le mieux votre situation.")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question1).build();

                QuestionVersionEntity questionVersion2 = QuestionVersionEntity.builder()
                        .setQuestion_order(2)
                        .setVerificationOrder(2)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Intensité des démangeaisons (prurit)")
                        .setFormattedText("Intensité des démangeaisons (prurit)").setName("")
                        .setInfo("Sévérité des démangeaisons tel que défini ci-dessous. Appuyez la case appropriée, qui définit le mieux votre situation.")
                        .setFormattedInfo("Sévérité des démangeaisons tel que défini ci-dessous. Appuyez la case appropriée, qui définit le mieux votre situation.")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question2).build();

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion2 = questionVersionDal.save(questionVersion2);

                QuestionVersionTraductionEntity questionVersionTraduction1 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion1.getId())
                        .setLanguage("en")
                        .setText("Number of Wheals (last 24 h)")
                        .setFormattedText("Number of Wheals (last 24 h)")
                        .setInfo("Wheals number as defined below. Tap the appropriate box, which best defines your situation.")
                        .setFormattedInfo("Wheals number as defined below. Tap the appropriate box, which best defines your situation.")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction2 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion2.getId())
                        .setLanguage("en")
                        .setText("Intensity of pruritus (last 24 h)")
                        .setFormattedText("Intensity of pruritus (last 24 h)")
                        .setInfo("Pruritus intensity as defined below. Tap the appropriate box, which best defines your situation.")
                        .setFormattedInfo("Pruritus intensity as defined below. Tap the appropriate box, which best defines your situation.")
                        .setName("")
                        .build();

                questionVersionTraductionDal.save(questionVersionTraduction1);
                questionVersionTraductionDal.save(questionVersionTraduction2);

                QuestionVersionEntity questionVersion3 = QuestionVersionEntity.builder()
                        .setQuestion_order(3)
                        .setVerificationOrder(3)
                        .setQuestionType(questionTypeDal.findByTypeCode(0))
                        .setText("Indice d’activité de l’urticaire quotidien")
                        .setFormattedText("Indice d’activité de l’urticaire quotidien")
                        .setInfo("Somme du score des plaques et des démangeaisons quotidiennes")
                        .setFormattedInfo("Somme du score des plaques et des démangeaisons quotidiennes")
                        .setCommandClass("com.base.server.surveyVersion.business.surveyVersion.Uas7v1SurveyCommand")
                        .setIsHidden(true)
                        .setName("").setQuestion(question3).setSurveyVersionEntity(surveyVersionLatest).build();

                AnswerChoiceEntity answerChoice1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("0 - Aucune").setFormattedText("0 - Aucune")
                        .setName("").setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity answerChoice2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(1).setText("1 - Moins de 20").setFormattedText("1 - Moins de 20")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1).build();

                AnswerChoiceEntity answerChoice3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                        .setValue(2).setText("2 - Entre 20 et 50").setFormattedText("2 - Entre 20 et 50")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1).build();

                AnswerChoiceEntity answerChoice4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                        .setValue(3).setText("3 - Plus de 50").setFormattedText("3 - Pllus de 50")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1).build();

                AnswerChoiceEntity answerChoice5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                                .setValue(0).setText("0 - Aucune").setFormattedText("0 - Aucune").setInfo("Aucune")
                                .setFormattedInfo("Aucune").setName("").setQuestionVersionEntity(questionVersion2)
                                .build();

                AnswerChoiceEntity answerChoice6 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                                .setValue(1).setText("1 - Légères").setFormattedText("1 - Légères")
                                .setInfo("Pésentes, mais supportables et ne constituant pas une gêne")
                                .setFormattedInfo("Pésentes, mais supportables et ne constituant pas une gêne").setName("")
                                .setQuestionVersionEntity(questionVersion2).build();

                AnswerChoiceEntity answerChoice7 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                                .setValue(2).setText("2 - Modérées").setFormattedText("2 - Modérées")
                                .setInfo("Gênantes, mais ne nuisent pas aux activités quotidiennes ou au sommeil")
                                .setFormattedInfo("Gênantes, mais ne nuisent pas aux activités quotidiennes ou au sommeil")
                                .setName("").setQuestionVersionEntity(questionVersion2).build();

                AnswerChoiceEntity answerChoice8 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                                .setValue(3).setText("3 - Intenses").setFormattedText("3 - Intenses")
                                .setInfo("Graves, suffisamment gênantes pour nuire aux activités quotidiennes ou au sommeil")
                                .setFormattedInfo("Graves, suffisamment gênantes pour nuire aux activités quotidiennes ou au sommeil")
                                .setName("").setQuestionVersionEntity(questionVersion2).build();


                answerChoice1 = answerChoiceDal.save(answerChoice1);
                answerChoice2 = answerChoiceDal.save(answerChoice2);
                answerChoice3 = answerChoiceDal.save(answerChoice3);
                answerChoice4 = answerChoiceDal.save(answerChoice4);
                answerChoice5 = answerChoiceDal.save(answerChoice5);
                answerChoice6 = answerChoiceDal.save(answerChoice6);
                answerChoice7 = answerChoiceDal.save(answerChoice7);
                answerChoice8 = answerChoiceDal.save(answerChoice8);


                //Translation
                AnswerChoiceTraductionEntity answerChoiceTraduction1 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("0 - None")
                        .setFormattedText("0 - None")
                        .setAnswerChoiceId(answerChoice1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity answerChoiceTraduction2 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("1 - Less than 20")
                        .setFormattedText("1 - Less than 20")
                        .setAnswerChoiceId(answerChoice2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity answerChoiceTraduction3 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("2 - Between 20 and 50")
                        .setFormattedText("2 - Between 20 and 50")
                        .setAnswerChoiceId(answerChoice3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity answerChoiceTraduction4 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("3 - More than 50")
                        .setFormattedText("3 - More than 50")
                        .setAnswerChoiceId(answerChoice4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity answerChoiceTraduction5 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("0 - None")
                        .setFormattedText("3 - None")
                        .setInfo("Absent")
                        .setFormattedInfo("Absent")
                        .setAnswerChoiceId(answerChoice5.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity answerChoiceTraduction6 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("1 - Mild")
                        .setFormattedText("1 - Mild")
                        .setInfo("Present but not annoying or troublesome")
                        .setFormattedInfo("Present but not annoying or troublesome")
                        .setAnswerChoiceId(answerChoice6.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity answerChoiceTraduction7 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("2 - Moderate")
                        .setFormattedText("1 - Moderate")
                        .setInfo("Not interfere with normal daily activity or sleep")
                        .setFormattedInfo("Not interfere with normal daily activity or sleep")
                        .setAnswerChoiceId(answerChoice7.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity answerChoiceTraduction8 = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("3 - Intense")
                        .setFormattedText("3 - Intense")
                        .setInfo("Severe pruritus, which is sufficiently troublesome to interfere with normal daily activity or sleep")
                        .setFormattedInfo("Severe pruritus, which is sufficiently troublesome to interfere with normal daily activity or sleep")
                        .setAnswerChoiceId(answerChoice8.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(answerChoiceTraduction1);
                answerChoiceTraductionDal.save(answerChoiceTraduction2);
                answerChoiceTraductionDal.save(answerChoiceTraduction3);
                answerChoiceTraductionDal.save(answerChoiceTraduction4);
                answerChoiceTraductionDal.save(answerChoiceTraduction5);
                answerChoiceTraductionDal.save(answerChoiceTraduction6);
                answerChoiceTraductionDal.save(answerChoiceTraduction7);
                answerChoiceTraductionDal.save(answerChoiceTraduction8);

                List<AnswerChoiceEntity> possibleAnswers1 = new ArrayList<>();
                possibleAnswers1.add(answerChoice1);
                possibleAnswers1.add(answerChoice2);
                possibleAnswers1.add(answerChoice3);
                possibleAnswers1.add(answerChoice4);

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion1.setPossibleAnswers(possibleAnswers1);

                List<AnswerChoiceEntity> possibleAnswers2 = new ArrayList<>();
                possibleAnswers1.add(answerChoice5);
                possibleAnswers1.add(answerChoice6);
                possibleAnswers1.add(answerChoice7);
                possibleAnswers1.add(answerChoice8);

                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion2.setPossibleAnswers(possibleAnswers2);

                questionVersionDal.save(questionVersion3);
        }

        @Test
        public void addAasSurveyToDatabase() {
                AddFrequencyToDatabase();
                addQuestionTypeToDatabase();

                // Survey & Questions
                SurveyEntity AAS7 = SurveyEntity.builder().setShortName("AAS7")
                        .setName("Score d'activité de l'angio-oedème")
                        .setFrequency(frequencyDal.findByName("jour"))
                        .setDelais(14)
                        .build();
                AAS7 = surveyDal.save(AAS7);

                QuestionEntity question1 = QuestionEntity.builder().setSurvey(AAS7)
                        .setText("Avez-vous eu un gonflement pendant ladernières 24 heures ?")
                        .setName("").build();
                QuestionEntity question2 = QuestionEntity.builder().setSurvey(AAS7)
                        .setText("Durant quelle(s) période(s) de la journée cet épisode d'enflure a-t-il été présent?")
                        .setName("").build();
                QuestionEntity question3 = QuestionEntity.builder().setSurvey(AAS7)
                        .setText("Décrivez la sévérité / inconfort physique causé par cet épisode")
                        .setName("").build();
                QuestionEntity question4 = QuestionEntity.builder().setSurvey(AAS7)
                        .setText("Étiez vous ou êtes-vous capable de faire vos activités quotidiennes malgré l'enflure présente?")
                        .setName("").build();
                QuestionEntity question5 = QuestionEntity.builder().setSurvey(AAS7)
                        .setText("Est-ce que votre apparence physique est ou a été affectée par cet épisode d'enflure?")
                        .setName("").build();
                QuestionEntity question6 = QuestionEntity.builder().setSurvey(AAS7)
                        .setText("Comment décririez-vous la sévérité de cet épisode d'enflure, dans l'ensemble?")
                        .setName("").build();

                question1 = questionDal.save(question1);
                question2 = questionDal.save(question2);
                question3 = questionDal.save(question3);
                question4 = questionDal.save(question4);
                question5 = questionDal.save(question5);
                question6 = questionDal.save(question6);

                List<QuestionEntity> questions = new ArrayList<>();
                questions.add(question1);
                questions.add(question2);
                questions.add(question3);
                questions.add(question4);
                questions.add(question5);
                questions.add(question6);
                AAS7.setQuestions(questions);

                // Survey Version
                Timestamp today = Timestamp.from(new Date().toInstant());

                SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(AAS7)
                        .setSurvey_version(2).setDate(today)
                        .setFrequency(frequencyDal.findByName("jour"))
                        .setDelais(14)
                        .setName(AAS7.getName()).setShortName(AAS7.getShortName())
                        .setFr(AAS7.getName()).setEn("Angioedema Activity Score")
                        .build();

                surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);

                // Question Version & Possible Answers (Answer Choice)
                QuestionVersionEntity questionVersion1 = QuestionVersionEntity.builder()
                        .setQuestion_order(1)
                        .setVerificationOrder(1)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Avez-vous eu un gonflement pendant ladernières 24 heures ?")
                        .setFormattedText("Avez-vous eu un gonflement pendant ladernières 24 heures ?")
                        .setNoAnswerEndSurvey(true)
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question1).build();

                QuestionVersionEntity questionVersion2 = QuestionVersionEntity.builder()
                        .setQuestion_order(2)
                        .setVerificationOrder(2)
                        .setQuestionType(questionTypeDal.findByTypeCode(2))
                        .setText("Durant quelle(s) période(s) de la journée cet épisode d'enflure a-t-il été présent?")
                        .setFormattedText("Durant quelle(s) période(s) de la journée cet épisode d'enflure a-t-il été présent?")
                        .setInfo("(inscrire toutes les heures applicables)")
                        .setFormattedInfo("(inscrire toutes les heures applicables)")
                        .setNoAnswerEndSurvey(true)
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question2).build();

                QuestionVersionEntity questionVersion3 = QuestionVersionEntity.builder()
                        .setQuestion_order(3)
                        .setVerificationOrder(3)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Décrivez la sévérité / inconfort physique causé par cet épisode")
                        .setFormattedText("Décrivez la sévérité / inconfort physique causé par cet épisode")
                        .setInfo("(douleur, sensation de brûlure, démangeaison, etc…)")
                        .setFormattedInfo("(douleur, sensation de brûlure, démangeaison, etc…)")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question3).build();

                QuestionVersionEntity questionVersion4 = QuestionVersionEntity.builder()
                .setQuestion_order(4)
                        .setVerificationOrder(4)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Étiez vous ou êtes-vous capable de faire vos activités quotidiennes malgré l'enflure présente?")
                        .setFormattedText("Étiez vous ou êtes-vous capable de faire vos activités quotidiennes malgré l'enflure présente?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question4).build();

                QuestionVersionEntity questionVersion5 = QuestionVersionEntity.builder()
                        .setQuestion_order(5)
                        .setVerificationOrder(5)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Est-ce que votre apparence physique est ou a été affectée par cet épisode d'enflure?")
                        .setFormattedText("Est-ce que votre apparence physique est ou a été affectée par cet épisode d'enflure?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question5).build();

                QuestionVersionEntity questionVersion6 = QuestionVersionEntity.builder()
                .setQuestion_order(6)
                        .setVerificationOrder(6)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Comment décririez-vous la sévérité de cet épisode d'enflure, dans l'ensemble?")
                        .setFormattedText("Comment décririez-vous la sévérité de cet épisode d'enflure, dans l'ensemble?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question6).build();

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion3 = questionVersionDal.save(questionVersion3);
                questionVersion4 = questionVersionDal.save(questionVersion4);
                questionVersion5 = questionVersionDal.save(questionVersion5);
                questionVersion6 = questionVersionDal.save(questionVersion6);


                QuestionVersionTraductionEntity questionVersionTraduction1 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion1.getId())
                        .setLanguage("en")
                        .setText("Did you have any swelling during the past 24 hours?")
                        .setFormattedText("Did you have any swelling during the past 24 hours?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction2 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion2.getId())
                        .setLanguage("en")
                        .setText("At what time(s) of the day was the swelling present?")
                        .setFormattedText("At what time(s) of the day was the swelling present?")
                        .setInfo("(select all times that apply)")
                        .setFormattedInfo("(select all times that apply)")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction3 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion3.getId())
                        .setLanguage("en")
                        .setText("How bad were or are the physical ailments caused by the swelling?")
                        .setFormattedText("How bad were or are the physical ailments caused by the swelling?")
                        .setInfo("(e.g. pain, burning, itching)")
                        .setFormattedInfo("(e.g. pain, burning, itching)")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction4 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion4.getId())
                        .setLanguage("en")
                        .setText("Were or are you able to carry out your everyday activities despite the swelling?")
                        .setFormattedText("Were or are you able to carry out your everyday activities despite the swelling?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction5 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion5.getId())
                        .setLanguage("en")
                        .setText("Do you feel or have you felt negatively influenced by the swelling in terms of your appearance?")
                        .setFormattedText("Do you feel or have you felt negatively influenced by the swelling in terms of your appearance?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction6 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion6.getId())
                        .setLanguage("en")
                        .setText("In general, how severe would you the swelling?")
                        .setFormattedText("In general, how severe would you the swelling?")
                        .setName("")
                        .build();

                questionVersionTraductionDal.save(questionVersionTraduction1);
                questionVersionTraductionDal.save(questionVersionTraduction2);
                questionVersionTraductionDal.save(questionVersionTraduction3);
                questionVersionTraductionDal.save(questionVersionTraduction4);
                questionVersionTraductionDal.save(questionVersionTraduction5);
                questionVersionTraductionDal.save(questionVersionTraduction6);

                AnswerChoiceEntity Q1A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Oui").setFormattedText("Oui").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Non").setFormattedText("Non").setInfo("")
                        .setFormattedInfo("")
                        .setEndSurvey(true)
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                Q1A1 = answerChoiceDal.save(Q1A1);
                Q1A2 = answerChoiceDal.save(Q1A2);

                //Traduction Q1
                AnswerChoiceTraductionEntity Q1A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setAnswerChoiceId(Q1A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q1A2.getId())
                        .setName("")
                        .build();
                //



                AnswerChoiceEntity Q2A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Minuit - 8 am").setFormattedText("Minuit - 8 am").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("8 am - 4 pm").setFormattedText("8 am - 4 pm").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("4 pm - minuit").setFormattedText("4 pm - minuit").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                Q2A1 = answerChoiceDal.save(Q2A1);
                Q2A2 = answerChoiceDal.save(Q2A2);
                Q2A3 = answerChoiceDal.save(Q2A3);

                //Traduction Q2
                AnswerChoiceTraductionEntity Q2A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("0:00 - 8:00")
                        .setFormattedText("0:00 - 8:00")
                        .setAnswerChoiceId(Q2A1.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q2A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("8:00 - 16:00")
                        .setFormattedText("8:00 - 16:00")
                        .setAnswerChoiceId(Q2A2.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q2A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("16:00 - 24:00")
                        .setFormattedText("16:00 - 24:00")
                        .setAnswerChoiceId(Q2A3.getId())
                        .setName("")
                        .build();
                //



                AnswerChoiceEntity Q3A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Aucun inconfort").setFormattedText("Aucun inconfort").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Léger inconfort").setFormattedText("Léger inconfort").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Inconfort modéré").setFormattedText("Inconfort modéré").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Inconfort sévère").setFormattedText("Inconfort sévère").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                Q3A1 = answerChoiceDal.save(Q3A1);
                Q3A2 = answerChoiceDal.save(Q3A2);
                Q3A3 = answerChoiceDal.save(Q3A3);
                Q3A4 = answerChoiceDal.save(Q3A4);

                //Traduction Q3
                AnswerChoiceTraductionEntity Q3A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("None")
                        .setFormattedText("None")
                        .setAnswerChoiceId(Q3A1.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q3A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Slight")
                        .setFormattedText("Slight")
                        .setAnswerChoiceId(Q3A2.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q3A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Moderate")
                        .setFormattedText("Moderate")
                        .setAnswerChoiceId(Q3A3.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q3A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Severe")
                        .setFormattedText("Severe")
                        .setAnswerChoiceId(Q3A4.getId())
                        .setName("")
                        .build();
                //



                AnswerChoiceEntity Q4A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Aucune restriction").setFormattedText("Aucune restriction").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();
                AnswerChoiceEntity Q4A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Légère restriction").setFormattedText("Légère restriction").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();
                AnswerChoiceEntity Q4A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Sévère restriction").setFormattedText("Sévère restriction").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();
                AnswerChoiceEntity Q4A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Aucune activité possible").setFormattedText("Aucune activité possible").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                Q4A1 = answerChoiceDal.save(Q4A1);
                Q4A2 = answerChoiceDal.save(Q4A2);
                Q4A3 = answerChoiceDal.save(Q4A3);
                Q4A4 = answerChoiceDal.save(Q4A4);

                //Traduction Q4
                AnswerChoiceTraductionEntity Q4A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No restrictions")
                        .setFormattedText("No restrictions")
                        .setAnswerChoiceId(Q4A1.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q4A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Some restrictions")
                        .setFormattedText("Some restrictions")
                        .setAnswerChoiceId(Q4A2.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q4A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Severe restrictions")
                        .setFormattedText("Severe restrictions")
                        .setAnswerChoiceId(Q4A3.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q4A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Activities not possible")
                        .setFormattedText("Activities not possible")
                        .setAnswerChoiceId(Q4A4.getId())
                        .setName("")
                        .build();
                //



                AnswerChoiceEntity Q5A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Non").setFormattedText("Non").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion5)
                        .build();
                AnswerChoiceEntity Q5A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Légèrement").setFormattedText("Légèrement").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion5)
                        .build();
                AnswerChoiceEntity Q5A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Modérément").setFormattedText("Modérément").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion5)
                        .build();
                AnswerChoiceEntity Q5A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Sévèrement").setFormattedText("Sévèrement").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion5)
                        .build();

                Q5A1 = answerChoiceDal.save(Q5A1);
                Q5A2 = answerChoiceDal.save(Q5A2);
                Q5A3 = answerChoiceDal.save(Q5A3);
                Q5A4 = answerChoiceDal.save(Q5A4);

                //Traduction Q5
                AnswerChoiceTraductionEntity Q5A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q5A1.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q5A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Slightly")
                        .setFormattedText("Slightly")
                        .setAnswerChoiceId(Q5A2.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q5A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Moderately")
                        .setFormattedText("Moderately")
                        .setAnswerChoiceId(Q5A3.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q5A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Severely")
                        .setFormattedText("Severely")
                        .setAnswerChoiceId(Q5A4.getId())
                        .setName("")
                        .build();
                //



                AnswerChoiceEntity Q6A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Négligeable").setFormattedText("Négligeable").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion6)
                        .build();
                AnswerChoiceEntity Q6A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Légère").setFormattedText("Légère").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion6)
                        .build();
                AnswerChoiceEntity Q6A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Modérée").setFormattedText("Modérée").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion6)
                        .build();
                AnswerChoiceEntity Q6A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Sévère").setFormattedText("Sévère").setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion6)
                        .build();

                Q6A1 = answerChoiceDal.save(Q6A1);
                Q6A2 = answerChoiceDal.save(Q6A2);
                Q6A3 = answerChoiceDal.save(Q6A3);
                Q6A4 = answerChoiceDal.save(Q6A4);

                //Traduction Q6
                AnswerChoiceTraductionEntity Q6A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Negligible")
                        .setFormattedText("Negligible")
                        .setAnswerChoiceId(Q6A1.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q6A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Slight")
                        .setFormattedText("Slight")
                        .setAnswerChoiceId(Q6A2.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q6A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Moderate")
                        .setFormattedText("Moderate")
                        .setAnswerChoiceId(Q6A3.getId())
                        .setName("")
                        .build();
                AnswerChoiceTraductionEntity Q6A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Severe")
                        .setFormattedText("Severe")
                        .setAnswerChoiceId(Q6A4.getId())
                        .setName("")
                        .build();
                //

                answerChoiceTraductionDal.save(Q1A1Traduction);
                answerChoiceTraductionDal.save(Q1A2Traduction);

                answerChoiceTraductionDal.save(Q2A1Traduction);
                answerChoiceTraductionDal.save(Q2A2Traduction);
                answerChoiceTraductionDal.save(Q2A3Traduction);

                answerChoiceTraductionDal.save(Q3A1Traduction);
                answerChoiceTraductionDal.save(Q3A2Traduction);
                answerChoiceTraductionDal.save(Q3A3Traduction);
                answerChoiceTraductionDal.save(Q3A4Traduction);

                answerChoiceTraductionDal.save(Q4A1Traduction);
                answerChoiceTraductionDal.save(Q4A2Traduction);
                answerChoiceTraductionDal.save(Q4A3Traduction);
                answerChoiceTraductionDal.save(Q4A4Traduction);

                answerChoiceTraductionDal.save(Q5A1Traduction);
                answerChoiceTraductionDal.save(Q5A2Traduction);
                answerChoiceTraductionDal.save(Q5A3Traduction);
                answerChoiceTraductionDal.save(Q5A4Traduction);

                answerChoiceTraductionDal.save(Q6A1Traduction);
                answerChoiceTraductionDal.save(Q6A2Traduction);
                answerChoiceTraductionDal.save(Q6A3Traduction);
                answerChoiceTraductionDal.save(Q6A4Traduction);


                List<AnswerChoiceEntity> possibleAnswersQ1 = new ArrayList<>();
                possibleAnswersQ1.add(Q1A1);
                possibleAnswersQ1.add(Q1A2);

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion1.setPossibleAnswers(possibleAnswersQ1);

                List<AnswerChoiceEntity> possibleAnswersQ2 = new ArrayList<>();
                possibleAnswersQ2.add(Q2A1);
                possibleAnswersQ2.add(Q2A2);
                possibleAnswersQ2.add(Q2A3);

                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion2.setPossibleAnswers(possibleAnswersQ2);


                List<AnswerChoiceEntity> possibleAnswersQ3 = new ArrayList<>();
                possibleAnswersQ3.add(Q3A1);
                possibleAnswersQ3.add(Q3A2);
                possibleAnswersQ3.add(Q3A3);
                possibleAnswersQ3.add(Q3A4);

                questionVersion3 = questionVersionDal.save(questionVersion3);
                questionVersion3.setPossibleAnswers(possibleAnswersQ3);


                List<AnswerChoiceEntity> possibleAnswersQ4 = new ArrayList<>();
                possibleAnswersQ4.add(Q4A1);
                possibleAnswersQ4.add(Q4A2);
                possibleAnswersQ4.add(Q4A3);
                possibleAnswersQ4.add(Q4A4);

                questionVersion4 = questionVersionDal.save(questionVersion4);
                questionVersion4.setPossibleAnswers(possibleAnswersQ4);


                List<AnswerChoiceEntity> possibleAnswersQ5 = new ArrayList<>();
                possibleAnswersQ5.add(Q5A1);
                possibleAnswersQ5.add(Q5A2);
                possibleAnswersQ5.add(Q5A3);
                possibleAnswersQ5.add(Q5A4);

                questionVersion5 = questionVersionDal.save(questionVersion5);
                questionVersion5.setPossibleAnswers(possibleAnswersQ5);


                List<AnswerChoiceEntity> possibleAnswersQ6 = new ArrayList<>();
                possibleAnswersQ6.add(Q6A1);
                possibleAnswersQ6.add(Q6A2);
                possibleAnswersQ6.add(Q6A3);
                possibleAnswersQ6.add(Q6A4);

                questionVersion6 = questionVersionDal.save(questionVersion6);
                questionVersion6.setPossibleAnswers(possibleAnswersQ6);
        }

        @Test
        public void addUctSurveyToDatabase() {
                AddFrequencyToDatabase();
                addQuestionTypeToDatabase();

                // Survey & Questions
                SurveyEntity UCT = SurveyEntity.builder().setShortName("UCT")
                        .setName("Test de contrôle de l’urticaire")
                        .setFrequency(frequencyDal.findByName("mois"))
                        .setDelais(7)
                        .build();
                UCT = surveyDal.save(UCT);

                QuestionEntity question1 = QuestionEntity.builder().setSurvey(UCT)
                        .setText("Dans quelle mesure avez-vous été gêné physiquement par votre urticaire (démangeaisons, plaques rouges et/ou oedèmes) durant les 4 dernières semaines?").setName("")
                        .build();
                QuestionEntity question2 = QuestionEntity.builder().setSurvey(UCT)
                        .setText("Dans quelle mesure votre qualité de vie a-t-elle été altérée par votre urticaire ces 4 dernières semaines?")
                        .setName("").build();
                QuestionEntity question3 = QuestionEntity.builder().setSurvey(UCT)
                        .setText("Au cours des 4 dernières semaines, combien de fois le traitement de votre urticaire n’a-t-il pas été suffisant pour contrôler vos symptômes?")
                        .setName("").build();
                QuestionEntity question4 = QuestionEntity.builder().setSurvey(UCT)
                        .setText("Au total, comment estimez-vous que votre urticaire a été contrôlée au cours de ces 4 dernières semaines ?")
                        .setName("").build();

                question1 = questionDal.save(question1);
                question2 = questionDal.save(question2);
                question3 = questionDal.save(question3);
                question4 = questionDal.save(question4);

                List<QuestionEntity> questions = new ArrayList<>();
                questions.add(question1);
                questions.add(question2);
                questions.add(question3);
                questions.add(question4);
                UCT.setQuestions(questions);

                // Survey Version
                Timestamp today = Timestamp.from(new Date().toInstant());

                SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(UCT)
                        .setSurvey_version(1)
                        .setFrequency(frequencyDal.findByName("mois"))
                        .setDelais(7)
                        .setDate(today)
                        .setName(UCT.getName()).setShortName(UCT.getShortName())
                        .setFr(UCT.getName()).setEn("Urticaria Control Test")
                        .build();

                surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);

                // Question Version & Possible Answers (Answer Choice)
                QuestionVersionEntity questionVersion1 = QuestionVersionEntity.builder()
                        .setQuestion_order(1)
                        .setVerificationOrder(1)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Dans quelle mesure avez-vous été gêné physiquement par votre urticaire (démangeaisons, plaques rouges et/ou oedèmes) durant les 4 dernières semaines ?")
                        .setFormattedText("Dans quelle mesure avez-vous été gêné physiquement par votre urticaire (démangeaisons, plaques rouges et/ou oedèmes) durant les 4 dernières semaines ?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question1).build();

                QuestionVersionEntity questionVersion2 = QuestionVersionEntity.builder()
                        .setQuestion_order(2)
                        .setVerificationOrder(2)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Dans quelle mesure votre qualité de vie a-t-elle été altérée par votre urticaire ces 4 dernières semaines ?")
                        .setFormattedText("Dans quelle mesure votre qualité de vie a-t-elle été altérée par votre urticaire ces 4 dernières semaines ?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question2).build();

                QuestionVersionEntity questionVersion3 = QuestionVersionEntity.builder()
                        .setQuestion_order(3)
                        .setVerificationOrder(3)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Au cours des 4 dernières semaines, combien de fois le traitement de votre urticaire n’a-t-il pas été suffisant pour contrôler vos symptômes ?")
                        .setFormattedText("Au cours des 4 dernières semaines, combien de fois le traitement de votre urticaire n’a-t-il pas été suffisant pour contrôler vos symptômes ?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question3).build();

                QuestionVersionEntity questionVersion4 = QuestionVersionEntity.builder()
                        .setQuestion_order(4)
                        .setVerificationOrder(4)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Au total, comment estimez-vous que votre urticaire a été contrôlée au cours de ces 4 dernières semaines ?")
                        .setFormattedText("Au total, comment estimez-vous que votre urticaire a été contrôlée au cours de ces 4 dernières semaines ?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question4).build();

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion3 = questionVersionDal.save(questionVersion3);
                questionVersion4 = questionVersionDal.save(questionVersion4);

                //Traduction
                QuestionVersionTraductionEntity questionVersionTraduction1 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion1.getId())
                        .setLanguage("en")
                        .setText("How much have you suffered from the physical symptoms of the urticaria in the last four weeks?")
                        .setFormattedText("How much have you suffered from the physical symptoms of the urticaria in the last four weeks?")
                        .setInfo("(itch, hives (welts) and/or swelling)")
                        .setFormattedInfo("(itch, hives (welts) and/or swelling)")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction2 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion2.getId())
                        .setLanguage("en")
                        .setText("How much was your quality of life affected by the urticaria in the last 4 weeks?")
                        .setFormattedText("How much was your quality of life affected by the urticaria in the last 4 weeks?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction3 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion3.getId())
                        .setLanguage("en")
                        .setText("How often was the treatment for your urticaria in the last 4 weeks not enough tcontrol your urticaria symptoms?")
                        .setFormattedText("How often was the treatment for your urticaria in the last 4 weeks not enough tcontrol your urticaria symptoms?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction4 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion4.getId())
                        .setLanguage("en")
                        .setText("Overall, how well have you had your urticaria under control in the last 4 weeks?")
                        .setFormattedText("Overall, how well have you had your urticaria under control in the last 4 weeks?")
                        .setName("")
                        .build();

                questionVersionTraductionDal.save(questionVersionTraduction1);
                questionVersionTraductionDal.save(questionVersionTraduction2);
                questionVersionTraductionDal.save(questionVersionTraduction3);
                questionVersionTraductionDal.save(questionVersionTraduction4);

                AnswerChoiceEntity Q1A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Très fortement").setFormattedText("Très fortement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();


                AnswerChoiceEntity Q1A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Fortement").setFormattedText("Fortement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Moyennement").setFormattedText("Moyennement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Peu").setFormattedText("Peu")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Pas du tout").setFormattedText("Pas du tout")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                Q1A1 = answerChoiceDal.save(Q1A1);
                Q1A2 = answerChoiceDal.save(Q1A2);
                Q1A3 = answerChoiceDal.save(Q1A3);
                Q1A4 = answerChoiceDal.save(Q1A4);
                Q1A5 = answerChoiceDal.save(Q1A5);

                //Traduction
                AnswerChoiceTraductionEntity Q1A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very much")
                        .setFormattedText("Very much")
                        .setAnswerChoiceId(Q1A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Much")
                        .setFormattedText("Much")
                        .setAnswerChoiceId(Q1A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Somewhat")
                        .setFormattedText("Somewhat")
                        .setAnswerChoiceId(Q1A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("A little")
                        .setFormattedText("A little")
                        .setAnswerChoiceId(Q1A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q1A5.getId())
                        .setName("")
                        .build();
                //End



                AnswerChoiceEntity Q2A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Très fortement").setFormattedText("Très fortement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Fortement").setFormattedText("Fortement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Moyennement").setFormattedText("Moyennement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Peu").setFormattedText("Peu")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Pas du tout").setFormattedText("Pas du tout")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                Q2A1 = answerChoiceDal.save(Q2A1);
                Q2A2 = answerChoiceDal.save(Q2A2);
                Q2A3 = answerChoiceDal.save(Q2A3);
                Q2A4 = answerChoiceDal.save(Q2A4);
                Q2A5 = answerChoiceDal.save(Q2A5);

                //Traduction
                AnswerChoiceTraductionEntity Q2A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very much")
                        .setFormattedText("Very much")
                        .setAnswerChoiceId(Q2A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Much")
                        .setFormattedText("Much")
                        .setAnswerChoiceId(Q2A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Somewhat")
                        .setFormattedText("Somewhat")
                        .setAnswerChoiceId(Q2A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("A little")
                        .setFormattedText("A little")
                        .setAnswerChoiceId(Q2A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q2A5.getId())
                        .setName("")
                        .build();
                //End



                AnswerChoiceEntity Q3A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Très souvent").setFormattedText("Très souvent")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Souvent").setFormattedText("Souvent")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Occasionnnellement").setFormattedText("Occasionnnellement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Rarement").setFormattedText("Rarement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Jamais").setFormattedText("Jamais")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                Q3A1 = answerChoiceDal.save(Q3A1);
                Q3A2 = answerChoiceDal.save(Q3A2);
                Q3A3 = answerChoiceDal.save(Q3A3);
                Q3A4 = answerChoiceDal.save(Q3A4);
                Q3A5 = answerChoiceDal.save(Q3A5);

                //Traduction
                AnswerChoiceTraductionEntity Q3A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very often")
                        .setFormattedText("Very often")
                        .setAnswerChoiceId(Q3A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Often")
                        .setFormattedText("Often")
                        .setAnswerChoiceId(Q3A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Sometimes")
                        .setFormattedText("Sometimes")
                        .setAnswerChoiceId(Q3A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Seldom")
                        .setFormattedText("Seldom")
                        .setAnswerChoiceId(Q3A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q3A5.getId())
                        .setName("")
                        .build();
                //End



                AnswerChoiceEntity Q4A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Pas du tout").setFormattedText("Pas du tout")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Peu").setFormattedText("Peu")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Moyennement").setFormattedText("Moyennement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Bien").setFormattedText("Bien")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Complétement").setFormattedText("Complétement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                Q4A1 = answerChoiceDal.save(Q4A1);
                Q4A2 = answerChoiceDal.save(Q4A2);
                Q4A3 = answerChoiceDal.save(Q4A3);
                Q4A4 = answerChoiceDal.save(Q4A4);
                Q4A5 = answerChoiceDal.save(Q4A5);

                //Traduction
                AnswerChoiceTraductionEntity Q4A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q4A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("A little")
                        .setFormattedText("A little")
                        .setAnswerChoiceId(Q4A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Somewhat")
                        .setFormattedText("Somewhat")
                        .setAnswerChoiceId(Q4A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Well")
                        .setFormattedText("Well")
                        .setAnswerChoiceId(Q4A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very well")
                        .setFormattedText("Very well")
                        .setAnswerChoiceId(Q4A5.getId())
                        .setName("")
                        .build();
                //End

                answerChoiceTraductionDal.save(Q1A1Traduction);
                answerChoiceTraductionDal.save(Q1A2Traduction);
                answerChoiceTraductionDal.save(Q1A3Traduction);
                answerChoiceTraductionDal.save(Q1A4Traduction);
                answerChoiceTraductionDal.save(Q1A5Traduction);

                answerChoiceTraductionDal.save(Q2A1Traduction);
                answerChoiceTraductionDal.save(Q2A2Traduction);
                answerChoiceTraductionDal.save(Q2A3Traduction);
                answerChoiceTraductionDal.save(Q2A4Traduction);
                answerChoiceTraductionDal.save(Q2A5Traduction);

                answerChoiceTraductionDal.save(Q3A1Traduction);
                answerChoiceTraductionDal.save(Q3A2Traduction);
                answerChoiceTraductionDal.save(Q3A3Traduction);
                answerChoiceTraductionDal.save(Q3A4Traduction);
                answerChoiceTraductionDal.save(Q3A5Traduction);

                answerChoiceTraductionDal.save(Q4A1Traduction);
                answerChoiceTraductionDal.save(Q4A2Traduction);
                answerChoiceTraductionDal.save(Q4A3Traduction);
                answerChoiceTraductionDal.save(Q4A4Traduction);
                answerChoiceTraductionDal.save(Q4A5Traduction);

                List<AnswerChoiceEntity> possibleAnswersQ1 = new ArrayList<>();
                possibleAnswersQ1.add(Q1A1);
                possibleAnswersQ1.add(Q1A2);
                possibleAnswersQ1.add(Q1A3);
                possibleAnswersQ1.add(Q1A4);
                possibleAnswersQ1.add(Q1A5);

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion1.setPossibleAnswers(possibleAnswersQ1);

                List<AnswerChoiceEntity> possibleAnswersQ2 = new ArrayList<>();
                possibleAnswersQ2.add(Q2A1);
                possibleAnswersQ2.add(Q2A2);
                possibleAnswersQ2.add(Q2A3);
                possibleAnswersQ2.add(Q2A4);
                possibleAnswersQ2.add(Q2A5);

                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion2.setPossibleAnswers(possibleAnswersQ2);

                List<AnswerChoiceEntity> possibleAnswersQ3 = new ArrayList<>();
                possibleAnswersQ3.add(Q3A1);
                possibleAnswersQ3.add(Q3A2);
                possibleAnswersQ3.add(Q3A3);
                possibleAnswersQ3.add(Q3A4);
                possibleAnswersQ3.add(Q3A5);

                questionVersion3 = questionVersionDal.save(questionVersion3);
                questionVersion3.setPossibleAnswers(possibleAnswersQ3);


                List<AnswerChoiceEntity> possibleAnswersQ4 = new ArrayList<>();
                possibleAnswersQ4.add(Q4A1);
                possibleAnswersQ4.add(Q4A2);
                possibleAnswersQ4.add(Q4A3);
                possibleAnswersQ4.add(Q4A4);
                possibleAnswersQ4.add(Q4A5);

                questionVersion4 = questionVersionDal.save(questionVersion4);
                questionVersion4.setPossibleAnswers(possibleAnswersQ4);
        }

        @Test
        public void addAectSurveyToDatabase() {
                AddFrequencyToDatabase();
                addQuestionTypeToDatabase();

                // Survey & Questions
                SurveyEntity AECT = SurveyEntity.builder().setShortName("AECT")
                        .setName("Test de contrôle de l’angioedème")
                        .setFrequency(frequencyDal.findByName("mois"))
                        .setDelais(7)
                        .build();
                AECT = surveyDal.save(AECT);

                QuestionEntity question1 = QuestionEntity.builder().setSurvey(AECT)
                        .setText("Durant les 4 dernières semaines, combien de fois s’est manifesté votre angioedème?").setName("")
                        .build();
                QuestionEntity question2 = QuestionEntity.builder().setSurvey(AECT)
                        .setText("Durant les 4 dernières semaines, à quel point votre qualité de vie en a-t-elle été perturbée?")
                        .setName("").build();
                QuestionEntity question3 = QuestionEntity.builder().setSurvey(AECT)
                        .setText("Durant les 4 dernières semaines, à quel point l’imprévisibilité de l’angioedème vous a-t- elle dérangé(e)?")
                        .setName("").build();
                QuestionEntity question4 = QuestionEntity.builder().setSurvey(AECT)
                        .setText("Durant les 4 dernières semaines, votre angioedème a-t-il été contrôlé par vos médicaments?")
                        .setName("").build();

                question1 = questionDal.save(question1);
                question2 = questionDal.save(question2);
                question3 = questionDal.save(question3);
                question4 = questionDal.save(question4);

                List<QuestionEntity> questions = new ArrayList<>();
                questions.add(question1);
                questions.add(question2);
                questions.add(question3);
                questions.add(question4);
                AECT.setQuestions(questions);

                // Survey Version
                Timestamp today = Timestamp.from(new Date().toInstant());

                SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(AECT)
                        .setSurvey_version(1)
                        .setFrequency(frequencyDal.findByName("mois"))
                        .setDelais(7)
                        .setDate(today)
                        .setName(AECT.getName()).setShortName(AECT.getShortName())
                        .setFr(AECT.getName()).setEn("Angioedema Control Test")
                        .build();

                surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);

                // Question Version & Possible Answers (Answer Choice)
                QuestionVersionEntity questionVersion1 = QuestionVersionEntity.builder()
                        .setQuestion_order(1)
                        .setVerificationOrder(1)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Durant les 4 dernières semaines, combien de fois s’est manifesté votre angioedème?")
                        .setFormattedText("Durant les 4 dernières semaines, combien de fois s’est manifesté votre angioedème?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question1).build();

                QuestionVersionEntity questionVersion2 = QuestionVersionEntity.builder()
                        .setQuestion_order(2)
                        .setVerificationOrder(2)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Durant les 4 dernières semaines, à quel point votre qualité de vie en a-t-elle été perturbée?")
                        .setFormattedText("Durant les 4 dernières semaines, à quel point votre qualité de vie en a-t-elle été perturbée?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question2).build();

                QuestionVersionEntity questionVersion3 = QuestionVersionEntity.builder()
                        .setQuestion_order(3)
                        .setVerificationOrder(3)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Durant les 4 dernières semaines, à quel point l’imprévisibilité de l’angioedème vous a-t- elle dérangé(e)?")
                        .setFormattedText("Durant les 4 dernières semaines, à quel point l’imprévisibilité de l’angioedème vous a-t- elle dérangé(e)?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question3).build();

                QuestionVersionEntity questionVersion4 = QuestionVersionEntity.builder()
                        .setQuestion_order(4)
                        .setVerificationOrder(4)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Durant les 4 dernières semaines, votre angioedème a-t-il été contrôlé par vos médicaments?")
                        .setFormattedText("Durant les 4 dernières semaines, votre angioedème a-t-il été contrôlé par vos médicaments?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question4).build();

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion3 = questionVersionDal.save(questionVersion3);
                questionVersion4 = questionVersionDal.save(questionVersion4);

                //Traduction
                QuestionVersionTraductionEntity questionVersionTraduction1 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion1.getId())
                        .setLanguage("en")
                        .setText("In the last 4 weeks, how often have you had angioedema?")
                        .setFormattedText("In the last 4 weeks, how often have you had angioedema?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction2 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion2.getId())
                        .setLanguage("en")
                        .setText("In the last 4 weeks, how much has your quality of life been affected by angicedema?")
                        .setFormattedText("In the last 4 weeks, how much has your quality of life been affected by angicedema?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction3 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion3.getId())
                        .setLanguage("en")
                        .setText("In the last 4 weeks, how much has the unpredictability of your angioedema bothered you?")
                        .setFormattedText("In the last 4 weeks, how much has the unpredictability of your angioedema bothered you?")
                        .setName("")
                        .build();

                QuestionVersionTraductionEntity questionVersionTraduction4 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion4.getId())
                        .setLanguage("en")
                        .setText("In the last 4 weeks, how well has your angioedema been controlled by your therapy?")
                        .setFormattedText("In the last 4 weeks, how well has your angioedema been controlled by your therapy?")
                        .setName("")
                        .build();

                questionVersionTraductionDal.save(questionVersionTraduction1);
                questionVersionTraductionDal.save(questionVersionTraduction2);
                questionVersionTraductionDal.save(questionVersionTraduction3);
                questionVersionTraductionDal.save(questionVersionTraduction4);

                AnswerChoiceEntity Q1A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Très souvent").setFormattedText("Très souvent")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Souvent").setFormattedText("Souvent")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Quelques fois").setFormattedText("Quelques fois")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Rarement").setFormattedText("Rarement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                AnswerChoiceEntity Q1A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Jamais").setFormattedText("Jamais")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion1)
                        .build();

                Q1A1 = answerChoiceDal.save(Q1A1);
                Q1A2 = answerChoiceDal.save(Q1A2);
                Q1A3 = answerChoiceDal.save(Q1A3);
                Q1A4 = answerChoiceDal.save(Q1A4);
                Q1A5 = answerChoiceDal.save(Q1A5);

                //Traduction
                AnswerChoiceTraductionEntity Q1A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very often")
                        .setFormattedText("Very often")
                        .setAnswerChoiceId(Q1A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Often")
                        .setFormattedText("Often")
                        .setAnswerChoiceId(Q1A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Sometimes")
                        .setFormattedText("Sometimes")
                        .setAnswerChoiceId(Q1A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Seldom")
                        .setFormattedText("Seldom")
                        .setAnswerChoiceId(Q1A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q1A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q1A5.getId())
                        .setName("")
                        .build();


                answerChoiceTraductionDal.save(Q1A1Traduction);
                answerChoiceTraductionDal.save(Q1A2Traduction);
                answerChoiceTraductionDal.save(Q1A3Traduction);
                answerChoiceTraductionDal.save(Q1A4Traduction);
                answerChoiceTraductionDal.save(Q1A5Traduction);

                AnswerChoiceEntity Q2A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Énormément").setFormattedText("Énormément")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Beaucoup").setFormattedText("Beaucoup")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Quelque peu").setFormattedText("Quelque peu")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Rarement").setFormattedText("Rarement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Jamais").setFormattedText("Jamais")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                Q2A1 = answerChoiceDal.save(Q2A1);
                Q2A2 = answerChoiceDal.save(Q2A2);
                Q2A3 = answerChoiceDal.save(Q2A3);
                Q2A4 = answerChoiceDal.save(Q2A4);
                Q2A5 = answerChoiceDal.save(Q2A5);

                //Traduction
                AnswerChoiceTraductionEntity Q2A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very much")
                        .setFormattedText("Very much")
                        .setAnswerChoiceId(Q2A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Much")
                        .setFormattedText("Much")
                        .setAnswerChoiceId(Q2A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Somewhat")
                        .setFormattedText("Somewhat")
                        .setAnswerChoiceId(Q2A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("A little")
                        .setFormattedText("A little")
                        .setAnswerChoiceId(Q2A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q2A5.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q2A1Traduction);
                answerChoiceTraductionDal.save(Q2A2Traduction);
                answerChoiceTraductionDal.save(Q2A3Traduction);
                answerChoiceTraductionDal.save(Q2A4Traduction);
                answerChoiceTraductionDal.save(Q2A5Traduction);

                AnswerChoiceEntity Q3A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Énormément").setFormattedText("Énormément")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Beaucoup").setFormattedText("Beaucoup")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Quelque peu").setFormattedText("Quelque peu")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Rarement").setFormattedText("Rarement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Jamais").setFormattedText("Jamais")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                Q3A1 = answerChoiceDal.save(Q3A1);
                Q3A2 = answerChoiceDal.save(Q3A2);
                Q3A3 = answerChoiceDal.save(Q3A3);
                Q3A4 = answerChoiceDal.save(Q3A4);
                Q3A5 = answerChoiceDal.save(Q3A5);

                //Traduction
                AnswerChoiceTraductionEntity Q3A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very much")
                        .setFormattedText("Very much")
                        .setAnswerChoiceId(Q3A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Much")
                        .setFormattedText("Much")
                        .setAnswerChoiceId(Q3A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Somewhat")
                        .setFormattedText("Somewhat")
                        .setAnswerChoiceId(Q3A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("A little")
                        .setFormattedText("A little")
                        .setAnswerChoiceId(Q3A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q3A5.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q3A1Traduction);
                answerChoiceTraductionDal.save(Q3A2Traduction);
                answerChoiceTraductionDal.save(Q3A3Traduction);
                answerChoiceTraductionDal.save(Q3A4Traduction);
                answerChoiceTraductionDal.save(Q3A5Traduction);

                AnswerChoiceEntity Q4A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Pas du tout").setFormattedText("Pas du tout")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A2 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(2)
                        .setValue(0).setText("Rarement").setFormattedText("Rarement")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A3 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(3)
                        .setValue(0).setText("Quelque peu").setFormattedText("Quelque peu")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A4 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(4)
                        .setValue(0).setText("Bien").setFormattedText("Bien")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                AnswerChoiceEntity Q4A5 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(5)
                        .setValue(0).setText("Très bien").setFormattedText("Très bien")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion4)
                        .build();

                Q4A1 = answerChoiceDal.save(Q4A1);
                Q4A2 = answerChoiceDal.save(Q4A2);
                Q4A3 = answerChoiceDal.save(Q4A3);
                Q4A4 = answerChoiceDal.save(Q4A4);
                Q4A5 = answerChoiceDal.save(Q4A5);

                //Traduction
                AnswerChoiceTraductionEntity Q4A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Not at all")
                        .setFormattedText("Not at all")
                        .setAnswerChoiceId(Q4A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("A little")
                        .setFormattedText("A little")
                        .setAnswerChoiceId(Q4A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Somewhat")
                        .setFormattedText("Somewhat")
                        .setAnswerChoiceId(Q4A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Well")
                        .setFormattedText("Well")
                        .setAnswerChoiceId(Q4A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q4A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Very well")
                        .setFormattedText("Very well")
                        .setAnswerChoiceId(Q4A5.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q4A1Traduction);
                answerChoiceTraductionDal.save(Q4A2Traduction);
                answerChoiceTraductionDal.save(Q4A3Traduction);
                answerChoiceTraductionDal.save(Q4A4Traduction);
                answerChoiceTraductionDal.save(Q4A5Traduction);

                List<AnswerChoiceEntity> possibleAnswersQ1 = new ArrayList<>();
                possibleAnswersQ1.add(Q1A1);
                possibleAnswersQ1.add(Q1A2);
                possibleAnswersQ1.add(Q1A3);
                possibleAnswersQ1.add(Q1A4);
                possibleAnswersQ1.add(Q1A5);

                questionVersion1 = questionVersionDal.save(questionVersion1);
                questionVersion1.setPossibleAnswers(possibleAnswersQ1);


                List<AnswerChoiceEntity> possibleAnswersQ2 = new ArrayList<>();
                possibleAnswersQ2.add(Q2A1);
                possibleAnswersQ2.add(Q2A2);
                possibleAnswersQ2.add(Q2A3);
                possibleAnswersQ2.add(Q2A4);
                possibleAnswersQ2.add(Q2A5);

                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion2.setPossibleAnswers(possibleAnswersQ2);

                List<AnswerChoiceEntity> possibleAnswersQ3 = new ArrayList<>();
                possibleAnswersQ3.add(Q3A1);
                possibleAnswersQ3.add(Q3A2);
                possibleAnswersQ3.add(Q3A3);
                possibleAnswersQ3.add(Q3A4);
                possibleAnswersQ3.add(Q3A5);

                questionVersion3 = questionVersionDal.save(questionVersion3);
                questionVersion3.setPossibleAnswers(possibleAnswersQ3);

                List<AnswerChoiceEntity> possibleAnswersQ4 = new ArrayList<>();
                possibleAnswersQ4.add(Q4A1);
                possibleAnswersQ4.add(Q4A2);
                possibleAnswersQ4.add(Q4A3);
                possibleAnswersQ4.add(Q4A4);
                possibleAnswersQ4.add(Q4A5);

                questionVersion4 = questionVersionDal.save(questionVersion4);
                questionVersion4.setPossibleAnswers(possibleAnswersQ4);
        }

        @Test
        public void addCuQolSurveyToDatabase() {
                AddFrequencyToDatabase();
                addQuestionTypeToDatabase();

                SurveyEntity CUQOL = SurveyEntity.builder().setShortName("CUQOL")
                        .setName("Qualité de vie - Urticaire")
                        .setFrequency(frequencyDal.findByName("année"))
                        .setDelais(7)
                        .build();
                CUQOL = surveyDal.save(CUQOL);

                // Survey Version
                Timestamp today = Timestamp.from(new Date().toInstant());
                SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(CUQOL)
                        .setSurvey_version(2).setDate(today).setName(CUQOL.getName())
                        .setName(CUQOL.getName()).setShortName(CUQOL.getShortName())
                        .setFr(CUQOL.getName()).setEn("Chronic Urticaria Quality of Life")
                        .setFrequency(frequencyDal.findByName("année"))
                        .setDelais(7)
                        .build();

                surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);

                String part1 = "À quel point avez-vous été dérangé par les symptômes suivants au cours des 15 derniers jours ?";
                String[] allAnswerTextes = new String[]{"Pas du tout", "Un peu", "Moyennement", "Beaucoup", "Énormément"};

                String[] part1Textes = new String[]{
                        "1. Démangeaison (prurit)",
                        "2. Plaques rouges",
                        "3. Gonflement des yeux",
                        "4. Gonflement des lèvres"};

                int questionVersionIndex = 0;
                questionVersionIndex = createQuestion(part1Textes, CUQOL, part1, surveyVersionLatest, questionVersionIndex, allAnswerTextes);

                String part2 = "Au cours des 15 derniers jours, indiquez si l’urticaire vous a limité dans les domaines suivants :";
                String[] part2Textes = new String[]{
                        "5. Travail",
                        "6. Activité physique",
                        "7. Sommeil",
                        "8. Loisirs",
                        "9. Relations interpersonnelles",
                        "10. Alimentation"};

                questionVersionIndex = createQuestion(part2Textes, CUQOL, part2, surveyVersionLatest, questionVersionIndex, allAnswerTextes);

                String part3 = "Les questions suivantes visent à mieux comprendre les difficultés et les problèmes pouvant être liés à l’urticaire durant les 15 derniers jours ?";
                String[] part3Textes = new String[]{
                        "11. Avez-vous de la difficulté à vous endormir ?",
                        "12. Vous réveillez-vous durant la nuit ?",
                        "13. Êtes-vous fatigué durant la journée parce que vous avez mal dormi la nuit ?",
                        "14. Avez-vous de la difficulté à vous concentrer?",
                        "15. Vous sentez-vous nerveux?",
                        "16. Vous sentez-vous déprimé ?",
                        "17. Devez-vous vous limiter dans le choix des aliments ?",
                        "18. Les rougeurs ou gonflements dues à l’urticaire apparaissant sur votre corps vous gênent-elles ?",
                        "19. Êtes-vous gêné de fréquenter des endroits publics ?",
                        "20. Avez-vous des problèmes à utiliser des produits cosmétiques(ex. parfums, crèmes, lotions, bain moussant, maquillage)?",
                        "21. Vous restreignezvous dans le choix des vêtements ?",
                        "22. L’urticaire limite-telle votre participation aux activités sportives?",
                        "23. Les effets secondaires des médicaments employés pour l’urticaire vous dérangent-ils?"};

                createQuestion(part3Textes, CUQOL, part3, surveyVersionLatest, questionVersionIndex, allAnswerTextes);

                //Traduction
                int questionVersionTraductionIndex = 1;
                List<QuestionVersionEntity> allQuestionVersion = questionVersionDal.getAllQuestionVersionBySurveyVersion(surveyVersionLatest.getId());

                String[] allAnswerTraduction = new String[]{
                        "not at all",
                        "a little",
                        "rather",
                        "a lot",
                        "very much"
                };


                String part1TextTraduction = "In the past 14 days how much were you troubled by the following symptoms?";
                String[] allQuestionTextTraduction1 = new String[]{
                        "1. Itching",
                        "2. Wheals",
                        "3. Swelling of your eyes",
                        "4. Swelling of your lips"
                };
                questionVersionTraductionIndex = createQuestionVersionTraduction(allQuestionVersion, questionVersionTraductionIndex, allQuestionTextTraduction1, allAnswerTraduction, part1TextTraduction, "en", surveyVersionLatest);


                String part2TextTraduction = "Indicate how often you were limited by your hives (urticaria) in the past 14 days in the following areas of daily life";
                String[] allQuestionTextTraduction2 = new String[]{
                        "5. Work",
                        "6. Physical activities",
                        "7. Sleep",
                        "8. Free time",
                        "9. Social relationships",
                        "10. Eating"
                };
                questionVersionTraductionIndex = createQuestionVersionTraduction(allQuestionVersion, questionVersionTraductionIndex, allQuestionTextTraduction2, allAnswerTraduction, part2TextTraduction, "en", surveyVersionLatest);


                String part3TextTraduction = "In the following questions, we would like to know more about the difficulties and problems that could be related to your hives (urticaria) (regarding the past 14 days)";
                String[] allQuestionTextTraduction3 = new String[]{
                        "11. Do you have difficulties falling asleep?",
                        "12. Do you wake up at night?",
                        "13. Are you tired during the day because you did not sleep well at night?",
                        "14. Do you have difficulties concentrating?",
                        "15. Do you feel nervous?",
                        "16. Do you feel miserable?",
                        "17. Do you have to limit your food choices?",
                        "18. Are you bothered by the symptoms of hives (urticaria) that appear on your body?",
                        "19. Are you embarrassed to go to public places?",
                        "20. Is it a problem for you to use cosmetics (e.g. perfumes, creams, lotions, bubblebath, make up)?",
                        "21. Do you have to limit your clothing choices?",
                        "22. Are your sports activities limited because of your hives (urticaria)?",
                        "23. Do you suffer side-effects from the medications you take for hives (urticaria)?"
                };
                createQuestionVersionTraduction(allQuestionVersion, questionVersionTraductionIndex, allQuestionTextTraduction3, allAnswerTraduction, part3TextTraduction, "en", surveyVersionLatest);
        }

        @Test
        public void addAeQolSurveyToDatabase(){
                AddFrequencyToDatabase();
                addQuestionTypeToDatabase();

                SurveyEntity AEQOL = SurveyEntity.builder().setShortName("AEQOL")
                        .setName("Qualité de vie - Angiœdèmes")
                        .setFrequency(frequencyDal.findByName("année"))
                        .setDelais(7)
                        .build();
                AEQOL = surveyDal.save(AEQOL);

                // Survey Version
                Timestamp today = Timestamp.from(new Date().toInstant());
                SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(AEQOL)
                        .setSurvey_version(2).setDate(today)
                        .setName(AEQOL.getName()).setShortName(AEQOL.getShortName())
                        .setFr(AEQOL.getName()).setEn("Angioedema Quallity of Life")
                        .setFrequency(frequencyDal.findByName("année"))
                        .setDelais(7)
                        .build();

                surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);
                String[] allAnswerTextes = new String[]{"Jamais", "Rarement", "Parfois", "Souvent", "Très souvent"};


                String part1 = "Pour les domaines de votre vie quotidienne énumérés ci-dessous, indiquez à quelle fréquence au cours des 4 dernières semaines vous avez été limité en raison d’épisodes de gonflement.";
                String[] part1Textes = new String[]{
                        "1. Travail",
                        "2. Activité physique",
                        "3. Passe-temps/Loisirs",
                        "4. Relations avec les autres",
                        "5. Alimentation"};

                int questionVersionIndex = 0;
                questionVersionIndex = createQuestion(part1Textes, AEQOL, part1, surveyVersionLatest, questionVersionIndex, allAnswerTextes);

                String part2 = "Pour les questions suivantes, nous désirons avoir plus de détails sur les problèmes et les difficultés qui peuvent être associés à vos épisodes récurrents de gonflement (Pensez à vos 4 dernières semaines)";
                String[] part2Textes = new String[]{
                        "6. Avez-vous de la difficulté à vous endormir?",
                        "7. Vous réveillez-vous durant la nuit?",
                        "8. Êtes-vous fatigué durant la journée parce que vous n’avez pas bien dormi?",
                        "9. Avez-vous des problèmes de concentration?",
                        "10.Vous sentez-vous déprimé?",
                        "11. Est-ce que vous devez vous limiter dans vos choix de boissons ou de nourriture?",
                        "12. Est-ce que vos épisodes de gonflement vous semblent un fardeau?",
                        "13. Avez-vous toujours peur qu’un épisode de gonflement arrive soudainement?",
                        "14. Avez-vous peur que la fréquence des épisodes de gonflement augmente?",
                        "15. Est-ce que vous êtes gêné de fréquenter les endroits publics à cause de vos épisodes de gonflement ?",
                        "16. Est-ce que vos épisodes vous gênent ou vous rendent plus conscient de votre état?",
                        "17. Avez-vous peur que les medicaments que vous prenez pour traiter les épisodes aient des effets negatifs à long terme?"};

                createQuestion(part2Textes, AEQOL, part2, surveyVersionLatest, questionVersionIndex, allAnswerTextes);

                int questionVersionTraductionIndex = 1;
                List<QuestionVersionEntity> allQuestionVersion = questionVersionDal.getAllQuestionVersionBySurveyVersion(surveyVersionLatest.getId());

                String[] allAnswerTraduction = new String[]{
                        "Never",
                        "Rarely",
                        "Occasionally",
                        "Often",
                        "Very Often"
                };


                String part1TextTraduction = "Specify how often, over the past 4 weeks, the recurring swelling (angioedema) has restricted you in the following areas of daily life. (It is not necessary for swelling to have actually occurred during this time.)";
                String[] allQuestionTextTraduction1 = new String[]{
                        "1. Work",
                        "2. Physical activity",
                        "3. Leisure",
                        "4. Social relationships",
                        "5. Nutrition",
                };
                questionVersionTraductionIndex = createQuestionVersionTraduction(allQuestionVersion, questionVersionTraductionIndex, allQuestionTextTraduction1, allAnswerTraduction, part1TextTraduction, "en", surveyVersionLatest);


                String part2TextTraduction = "The following questions focus in more detail on the difficulties and problems that can be associated with your recurring swelling (angioedema) (regarding the last 4 weeks).";
                String[] allQuestionTextTraduction2 = new String[]{
                        "6. Do you have difficulty falling asleep?",
                        "7. Do you wake up at night?",
                        "8. Are you tired during the day because you don't sleep well at night?",
                        "9. Do you have trouble concentrating?",
                        "10. Do you feel depressed?",
                        "11. Do you have to restrict your choices of food or drink?",
                        "12. Is the swelling, which occurs on your body due to the disease, a burden to you?",
                        "13. Are you afraid some swelling might occur suddenly?",
                        "14. Are you afraid the frequency of the swelling could increase?",
                        "15. Are you ashamed to go to public places due to the recurring swelling?",
                        "16. Are you embarrassed or self- conscious due to the recurring swelling?",
                        "17. Are you afraid that the treatment of the recurrent swelling could have long-term negative consequences for you?",
                };
                createQuestionVersionTraduction(allQuestionVersion, questionVersionTraductionIndex, allQuestionTextTraduction2, allAnswerTraduction, part2TextTraduction, "en", surveyVersionLatest);
        }

        @Test public void addMedicalHistoryToDatabase() {
                AddFrequencyToDatabase();
                addQuestionTypeToDatabase();

                // Survey & Questions
                SurveyEntity HM = SurveyEntity.builder().setShortName("HM")
                        .setName("Historique médicale")
                        .setFrequency(frequencyDal.findByName("unique"))
                        .setDelais(1)
                        .build();
                HM = surveyDal.save(HM);

                QuestionEntity question1 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Quand avez-vous eu des symptômes d’urticaire pour la première fois? ").setName("")
                        .build();

                QuestionEntity question2 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Qu’est-ce qui s’applique à votre urticaire?").setName("")
                        .build();

                QuestionEntity question3 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Si votre urticaire est à la fois spontanée et inductible, lequel des deux mécanismes est prédominant?").setName("")
                        .build();

                QuestionEntity question4 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Laquelle de ces descriptions correspond aux symptômes de votre urticaire?").setName("")
                        .build();

                QuestionEntity question5 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Votre urticaire est-elle accompagnée de démangeaison, et si oui, quelle en était l’intensité durant les 7 derniers jours?").setName("")
                        .build();

                QuestionEntity question6 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Est-ce-que votre urticaire s’accompagne de sensation de brûlure plus que de démangeaison?").setName("")
                        .build();

                QuestionEntity question7 = QuestionEntity.builder().setSurvey(HM)
                        .setText("L’urticaire laisse-t-elle des marques sur votre peau, comme des ecchymoses (bleus)?").setName("")
                        .build();

                QuestionEntity question8 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Combien une plaque d’urticaire (papules et rougeurs) prend-elle de temps avant de disparaitre complètement?").setName("")
                        .build();

                QuestionEntity question9 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Quelle est habituellement la taille ou grosseur de vos plaques d’urticaire (papules et rougeurs)?").setName("")
                        .build();

                QuestionEntity question10 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Sur quelle partie du corps apparaissent vos plaques d’urticaire (papules et rougeurs)?").setName("")
                        .build();

                QuestionEntity question11 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Si vous souffrez d’angioedème (ou enflure), quelle est la durée moyenne de chaque enflure, avant qu’elle ne soit complètement disparue?").setName("")
                        .build();

                QuestionEntity question12 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Sur quelles parties de votre corps se produit l’enflure (angioedème)?").setName("")
                        .build();

                QuestionEntity question13 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Si votre urticaire se manifeste par des plaques d’érythème (papules et rougeurs) et de l’angioedème (enflure), quel symptôme est apparu le premier?").setName("")
                        .build();

                QuestionEntity question14 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Si les deux symptômes (papules et enflure) ne sont pas arrivés en même temps, combien de temps après l’apparition du premier, le deuxième s’est-il produit?").setName("")
                        .build();

                QuestionEntity question15 = QuestionEntity.builder().setSurvey(HM)
                        .setText("En plus de vos symptômes d’urticaire, avez-vous souffert des symptômes suivants?").setName("")
                        .build();

                QuestionEntity question16 = QuestionEntity.builder().setSurvey(HM)
                        .setText("En plus de vos symptômes d’urticaire, avez-vous souffert des symptômes suivants?").setName("")
                        .build();

                QuestionEntity question17 = QuestionEntity.builder().setSurvey(HM)
                        .setText("En plus de vos symptômes d’urticaire, avez-vous souffert des symptômes suivants?").setName("")
                        .build();

                QuestionEntity question18 = QuestionEntity.builder().setSurvey(HM)
                        .setText("En même temps que l’urticaire, avez-vous déjà perdu connaissance, vous êtes-vous senti faible ou épuisé, ou avez-vous eu de la difficulté à respirer?").setName("")
                        .build();

                QuestionEntity question19 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Si vous avez des maladies autres que l’urticaire, veuillez s’il-vous-plait les nommer.").setName("")
                        .build();

                QuestionEntity question20 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Souffrez-vous de problèmes psychologiques ou de maladies mentales (ex : anxiété, dépression etc.)?").setName("")
                        .build();

                QuestionEntity question21 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Souffrez-vous d’infections chroniques (ex : hépatite)?").setName("")
                        .build();

                QuestionEntity question22 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Avez-vous des problèmes gastro-intestinaux (digestion, crampes, diarrhée…)?").setName("")
                        .build();

                QuestionEntity question23 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Prenez-vous des médicaments sur une base régulière (autres que les médicaments pour l’urticaire)?").setName("")
                        .build();

                QuestionEntity question24 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Les symptômes de votre urticaire sont-ils aggravés par la prise de médicaments anti-douleurs ou anti-inflammatoires?").setName("")
                        .build();

                QuestionEntity question25 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Avez-vous déjà utilisé des médicaments pour le traitement de votre urticaire").setName("")
                        .build();

                QuestionEntity question26 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Efficacité de ces traitements").setName("")
                        .build();

                QuestionEntity question27 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Effets secondaires du (des) traitement(s)").setName("")
                        .build();

                QuestionEntity question28 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Votre urticaire a-t-elle déjà été traitée par des approches autres que la médecine traditionnelle (ex : diètes, naturopathie, homéopathie, acupuncture, etc.)?").setName("")
                        .build();

                QuestionEntity question29 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Quel a été selon vous, au début, le facteur déclenchant de votre urticaire").setName("")
                        .build();

                QuestionEntity question30 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Parmi les facteurs suivants, lesquels semblent, d’après-vous, aggraver votre urticaire?").setName("")
                        .build();

                QuestionEntity question31 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Depuis le début des symptômes, l’urticaire a-t-elle été présente presque tous les jours?").setName("")
                        .build();

                QuestionEntity question32 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Quel est l’effet des menstruations sur votre urticaire?").setName("")
                        .build();

                QuestionEntity question33 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Avez-vous des parents proches (père, mère, enfants) qui ont eu de l’urticaire chronique (symptômes durant plus de six semaines)?").setName("")
                        .build();

                QuestionEntity question34 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Si oui, de quel type?").setName("")
                        .build();

                QuestionEntity question35 = QuestionEntity.builder().setSurvey(HM)
                        .setText("Souffrez-vous d’allergies, asthme, ou eczéma?").setName("")
                        .build();



                QuestionEntity[] allQuestion = new QuestionEntity[]{
                        question1, question2, question3, question4, question5,
                        question6, question7, question8, question9, question10,
                        question11, question12, question13, question14, question15,
                        question16, question17, question18, question19, question20,
                        question21, question22, question23, question24, question25,
                        question26, question27, question28, question29, question30,
                        question31, question32, question33, question34, question35,
                };

                for (QuestionEntity question : allQuestion) {
                        questionDal.save(question);
                }

                HM.setQuestions(Arrays.asList(allQuestion));

                // Survey Version
                Timestamp today = Timestamp.from(new Date().toInstant());

                SurveyVersionEntity surveyVersionLatest = SurveyVersionEntity.builder().setSurvey(HM)
                        .setSurvey_version(1)
                        .setFrequency(HM.getFrequency())
                        .setDelais(1)
                        .setDate(today)
                        .setName(HM.getName()).setShortName(HM.getShortName())
                        .setFr(HM.getName()).setEn("Medical History")
                        .build();

                surveyVersionLatest = surveyVersionDal.save(surveyVersionLatest);


                String[] Q1Answers = new String[]{
                        "moins de 6 mois",
                        "6 mois à 1 an",
                        "1 à 5 ans",
                        "plus de 5 ans"
                };
                createQuestionVersion(question1, "", surveyVersionLatest, 1, 1, Q1Answers, 1);

                //Traduction Question1
                String Question1Traduction = "When did you experience symptoms of your urticaria for the first time?";
                String[] Q1AnswersTraduction = new String[]{
                        "Less than 6 months",
                        "6 months to 1 year",
                        "1 to 5 years",
                        "More than 5 years",
                };
                createQuestionVersionTraduction(null, 1, new String[]{""}, Q1AnswersTraduction, Question1Traduction, "en", surveyVersionLatest);

                QuestionVersionEntity questionVersion2 = QuestionVersionEntity.builder()
                        .setQuestion_order(2)
                        .setVerificationOrder(2)
                        .setQuestionType(questionTypeDal.findByTypeCode(2))
                        .setText("Qu’est-ce qui s’applique à votre urticaire?")
                        .setFormattedText("Qu’est-ce qui s’applique à votre urticaire?")
                        .setInfo("(vous pouvez cocher plus d’une réponse)")
                        .setFormattedInfo("(vous pouvez cocher plus d’une réponse)")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question2).build();
                questionVersion2 = questionVersionDal.save(questionVersion2);

                QuestionVersionEntity questionVersion3 = QuestionVersionEntity.builder()
                        .setQuestion_order(0)
                        .setVerificationOrder(3)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Si votre urticaire est à la fois spontanée et inductible, lequel des deux mécanismes est prédominant?")
                        .setFormattedText("Si votre urticaire est à la fois spontanée et inductible, lequel des deux mécanismes est prédominant?")
                        .setIsSubQuestion(true)
                        .setSubQuestionOrder(3)
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question3).build();
                questionVersion3 = questionVersionDal.save(questionVersion3);

                AnswerChoiceEntity Q3A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Spontané")
                        .setFormattedText("Spontané")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(0).setText("Induit")
                        .setFormattedText("Induit")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                AnswerChoiceEntity Q3A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(0).setText("Je ne sais pas")
                        .setFormattedText("Je ne sais pas")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion3)
                        .build();

                Q3A1 = answerChoiceDal.save(Q3A1);
                Q3A2 = answerChoiceDal.save(Q3A2);
                Q3A3 = answerChoiceDal.save(Q3A3);

                //Traduction question 3
                QuestionVersionTraductionEntity questionVersionTraduction3 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion3.getId())
                        .setLanguage("en")
                        .setText("If your urticaria symptoms are spontaneous as well as specifically induced, which one of the two mechanisms is predominant?")
                        .setFormattedText("If your urticaria symptoms are spontaneous as well as specifically induced, which one of the two mechanisms is predominant?")
                        .setName("")
                        .build();

                questionVersionTraductionDal.save(questionVersionTraduction3);

                AnswerChoiceTraductionEntity Q3A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Induced")
                        .setFormattedText("Induced")
                        .setAnswerChoiceId(Q3A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Spontaneous")
                        .setFormattedText("Spontaneous")
                        .setAnswerChoiceId(Q3A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q3A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("I don’t know")
                        .setFormattedText("I don’t know")
                        .setAnswerChoiceId(Q3A3.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q3A1Traduction);
                answerChoiceTraductionDal.save(Q3A2Traduction);
                answerChoiceTraductionDal.save(Q3A3Traduction);

                List<AnswerChoiceEntity> possibleAnswersQ3 = new ArrayList<>();
                possibleAnswersQ3.add(Q3A1);
                possibleAnswersQ3.add(Q3A2);
                possibleAnswersQ3.add(Q3A3);
                questionVersion3 = questionVersionDal.save(questionVersion3);
                questionVersion3.setPossibleAnswers(possibleAnswersQ3);

                SubQuestionConditionEntity subQuestionConditionEntityQ2C1 = SubQuestionConditionEntity.builder()
                        .setName("")
                        .setSurveyVersion(surveyVersionLatest.getId())
                        .setQuestionVersion(questionVersion2.getId())
                        .setSubQuestionVersion(questionVersion3.getId())
                        .setSubQuestionOrder(1)
                        .build();
                subQuestionConditionEntityQ2C1 = subQuestionConditionDal.save(subQuestionConditionEntityQ2C1);

                AnswerChoiceEntity Q2A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("mes symptômes surviennent spontanément, sans être déclenchés par des facteurs extérieurs (urticaire spontanée).")
                        .setFormattedText("mes symptômes surviennent spontanément, sans être déclenchés par des facteurs extérieurs (urticaire spontanée).")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setSubQuestionCondition(subQuestionConditionEntityQ2C1.getId())
                        .setQuestionVersionEntity(questionVersion2)
                        .build();

                AnswerChoiceEntity Q2A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(0).setText("mes symptômes peuvent être déclenchés par des facteurs physiques* tels la pression, la friction, le froid, l’exercice, la chaleur, le soleil ou autres… (urticaire inductible).")
                        .setFormattedText("mes symptômes peuvent être déclenchés par des facteurs physiques* tels la pression, la friction, le froid, l’exercice, la chaleur, le soleil ou autres… (urticaire inductible).")
                        .setInfo("").setFormattedInfo("")
                        .setName("")
                        .setSupplementInput(true)
                        .setSupplementInputText("*Quel(s) est (sont), dans ce cas, le(s) facteur(s) déclenchant(s)?")
                        .setSubQuestionCondition(subQuestionConditionEntityQ2C1.getId())
                        .setQuestionVersionEntity(questionVersion2)
                        .build();
                Q2A1 = answerChoiceDal.save(Q2A1);
                Q2A2 = answerChoiceDal.save(Q2A2);

               //Traduction question 2
                QuestionVersionTraductionEntity questionVersionTraduction2 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion2.getId())
                        .setLanguage("en")
                        .setText("What applies to your urticaria?")
                        .setFormattedText("What applies to your urticaria?")
                        .setInfo("(mark all applicable answers)")
                        .setFormattedInfo("(mark all applicable answers)")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction2);

                AnswerChoiceTraductionEntity Q2A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("My skin symptoms appear spontaneously, I cannot trigger them through specific stimuli")
                        .setFormattedText("My skin symptoms appear spontaneously, I cannot trigger them through specific stimuli")
                        .setInfo("(spontaneous urticaria)")
                        .setFormattedInfo("(spontaneous urticaria)")
                        .setAnswerChoiceId(Q2A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q2A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("My symptoms can be triggered through specific stimulations* like prolonged pressure, scratching, cold or heat contact, strenuous exercise, sun, and others… (inducible urticaria)")
                        .setFormattedText("My symptoms can be triggered through specific stimulations* like prolonged pressure, scratching, cold or heat contact, strenuous exercise, sun, and others… (inducible urticaria)")
                        .setAnswerChoiceId(Q2A2.getId())
                        .setSupplementInputText("* Which, in this case, are the triggering factors?")
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q2A1Traduction);
                answerChoiceTraductionDal.save(Q2A2Traduction);

                List<AnswerChoiceEntity> possibleAnswersQ2 = new ArrayList<>();
                possibleAnswersQ2.add(Q2A1);
                possibleAnswersQ2.add(Q2A2);
                questionVersion2 = questionVersionDal.save(questionVersion2);
                questionVersion2.setPossibleAnswers(possibleAnswersQ2);

                String[] Q3Answers = new String[]{
                        "Rougeurs et plaques (papules) semblables à des piqûres de moustiques, de grosseur variable et n’apparaissant pas toutes en même temps. Elles durent de quelques minutes à quelques heures et sont accompagnées de démangeaison.",
                        "Enflure ou angioedème : gonflement sans rougeur, parfois douloureux, de la peau ou des muqueuses (lèvres, langue, paupières, mains, pieds, etc…), d’une durée de quelques heures à quelques jours.",
                };
                String Q3Info = "(cochez les deux si les deux s’appliquent)";
                createQuestionVersion(question4, Q3Info, surveyVersionLatest, 3, 4, Q3Answers, 2);

                //Traduction
                String Question3Traduction = "Which of these descriptions correspond to your urticaria symptoms? (check both if necessary)";
                String[] Q3AnswersTraduction = new String[]{
                        "wheals (pinhead-sized, looking like insect bites, reddish, transient lesions of variable sizes, mostly itching, lasting minutes up to many hours)",
                        "angioedema (usually skin colored, sometimes painful, swelling of the skin or mucous membranes (eyelids, lips, tongue, hands, feet etc…), lasting several hours up to many days",
                };
                createQuestionVersionTraduction(null, 4, new String[]{""}, Q3AnswersTraduction, Question3Traduction, "en", surveyVersionLatest);

                String[] Q4Answers = new String[]{
                        "Aucune démangeaison.",
                        "Légère démangeaison (présente, mais ne me dérange pas)",
                        "Démangeaison modérée (dérange, mais n’interfère pas avec mes activités quotidiennes ni avec mon sommeil)",
                        "Démangeaison intense (perturbe mes activités quotidiennes et mon sommeil)",
                };
                createQuestionVersion(question5, "", surveyVersionLatest, 4, 5, Q4Answers, 1);

                //Traduction
                String Question4Traduction = "Is your urticaria accompanied by itching, and, if yes, of what intensity during the past 7 days?";
                String[] Q4AnswersTraduction = new String[]{
                        "No itch during past 7 days",
                        "Mild itch (present, but not annoying)",
                        "Moderate itch (troublesome, but does not interfere with normal daily activity or sleep)" ,
                        "Intense itch (considerably disrupts daily activity and sleep)"
                };
                createQuestionVersionTraduction(null, 5, new String[]{""}, Q4AnswersTraduction, Question4Traduction, "en", surveyVersionLatest);
                //
                String[] Q5Answers = new String[]{
                        "Oui",
                        "Non",
                };
                createQuestionVersion(question6, "", surveyVersionLatest, 5, 6, Q5Answers, 1);

                //Traduction
                String Question5Traduction = "Does your urticaria burn rather than itch?";
                String[] Q5AnswersTraduction = new String[]{
                        "Yes",
                        "No",
                };
                createQuestionVersionTraduction(null, 6, new String[]{""}, Q5AnswersTraduction, Question5Traduction, "en", surveyVersionLatest);

                String[] Q6Answers = new String[]{
                        "Oui",
                        "Non",
                };
                createQuestionVersion(question7, "", surveyVersionLatest, 6, 7, Q6Answers, 1);

                String Question6Traduction = "Does your urticaria leave bruises?";
                String[] Q6AnswersTraduction = new String[]{
                        "Yes",
                        "No",
                };
                createQuestionVersionTraduction(null, 7, new String[]{""}, Q6AnswersTraduction, Question6Traduction, "en", surveyVersionLatest);

                String[] Q7Answers = new String[]{
                        "moins d’une heure",
                        "1 à 6 heures",
                        "6 à 12 heures",
                        "12 à 24 heures",
                        "24 à 48 heures",
                        "48 à 72 heures",
                        "plus de 72 heures"
                };
                createQuestionVersion(question8, "", surveyVersionLatest, 7, 8, Q7Answers, 1);

                String Question7Traduction = "If you suffer from wheals, how long does a single wheal usually last until it completely disappears?";
                String[] Q7AnswersTraduction = new String[]{
                        "less than 1 hour",
                        "1 – 6 hours",
                        "6 – 12 hours",
                        "12 – 24 hours",
                        "24 – 48 hours",
                        "48 – 72 hours",
                        "more than 72 hours"
                };
                createQuestionVersionTraduction(null, 8, new String[]{""}, Q7AnswersTraduction, Question7Traduction, "en", surveyVersionLatest);

                String[] Q8Answers = new String[]{
                        "5mm ( ¼ pouce) ou moins",
                        "plus de 5mm (plus de ¼ pouce)"
                };
                createQuestionVersion(question9, "", surveyVersionLatest, 8, 9, Q8Answers, 1);

                String Question8Traduction = "If you suffer from wheals, how large do they get normally?";
                String[] Q8AnswersTraduction = new String[]{
                        "5 mm(¼ inch) or less",
                        "larger than 5 mm(¼ inch)"
                };
                createQuestionVersionTraduction(null, 9, new String[]{""}, Q8AnswersTraduction, Question8Traduction, "en", surveyVersionLatest);

                QuestionVersionEntity questionVersion9 = QuestionVersionEntity.builder()
                        .setQuestion_order(9)
                        .setVerificationOrder(10)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Sur quelle partie du corps apparaissent vos plaques d’urticaire (papules et rougeurs)?")
                        .setFormattedText("Sur quelle partie du corps apparaissent vos plaques d’urticaire (papules et rougeurs)?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question10).build();
                questionVersion9 = questionVersionDal.save(questionVersion9);

                AnswerChoiceEntity Q9A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Sur tout le corps")
                        .setFormattedText("Sur tout le corps")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion9)
                        .build();

                AnswerChoiceEntity Q9A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(0).setText("Sur des parties spécifiques du corp")
                        .setFormattedText("Sur des parties spécifiques du corp")
                        .setSupplementInput(true)
                        .setSupplementInputText("Nommez-les")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion9)
                        .build();

                Q9A1 = answerChoiceDal.save(Q9A1);
                Q9A2 = answerChoiceDal.save(Q9A2);

                //Traduction
                QuestionVersionTraductionEntity questionVersionTraduction9 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion9.getId())
                        .setLanguage("en")
                        .setText("If you suffer from wheals, where do they normally appear?")
                        .setFormattedText("If you suffer from wheals, where do they normally appear?")
                        .setName("")
                        .build();

                questionVersionTraductionDal.save(questionVersionTraduction9);

                AnswerChoiceTraductionEntity Q9A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("The entire body")
                        .setFormattedText("The entire body")
                        .setAnswerChoiceId(Q9A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q9A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Specific body parts")
                        .setFormattedText("Specific body parts")
                        .setSupplementInputText("namely:")
                        .setAnswerChoiceId(Q9A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q9A1Traduction);
                answerChoiceTraductionDal.save(Q9A2Traduction);

                List<AnswerChoiceEntity> possibleAnswersQ9 = new ArrayList<>();
                possibleAnswersQ9.add(Q3A1);
                possibleAnswersQ9.add(Q3A2);
                questionVersion9 = questionVersionDal.save(questionVersion9);
                questionVersion9.setPossibleAnswers(possibleAnswersQ9);

                String[] Q10Answers = new String[]{
                        "moins d’une heure",
                        "1 à 6 heures",
                        "6 à 12 heures",
                        "12 à 24 heures",
                        "24 à 48 heures",
                        "48 à 72 heures",
                        "plus de 72 heures"
                };
                createQuestionVersion(question11, "", surveyVersionLatest, 10, 11, Q10Answers, 1);

                String Question10Traduction = "If you suffer from angioedema, how long does single angioedema usually last until it completely disappears?";
                String[] Q10AnswersTraduction = new String[]{
                        "less than 1 hour ",
                        "1 – 6 hours",
                        "6 – 12 hours",
                        "12 – 24 hours ",
                        "24-48 hours",
                        "48 – 72 hours",
                        "more than 72 hours"
                };
                createQuestionVersionTraduction(null, 11, new String[]{""}, Q10AnswersTraduction, Question10Traduction, "en", surveyVersionLatest);

                QuestionVersionEntity questionVersion11 = QuestionVersionEntity.builder()
                        .setQuestion_order(11)
                        .setVerificationOrder(12)
                        .setQuestionType(questionTypeDal.findByTypeCode(2))
                        .setText("Sur quelles parties de votre corps se produit l’enflure (angioedème)?")
                        .setFormattedText("Sur quelles parties de votre corps se produit l’enflure (angioedème)?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question12).build();
                questionVersion11 = questionVersionDal.save(questionVersion11);

                AnswerChoiceEntity Q11A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Langue")
                        .setFormattedText("Langue")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(0).setText("Lèvres")
                        .setFormattedText("Lèvres")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A3 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(3)
                        .setValue(0).setText("Larynx")
                        .setFormattedText("larynx")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A4 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(4)
                        .setValue(0).setText("Mains")
                        .setFormattedText("Mains")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A5 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(5)
                        .setValue(0).setText("Paupières")
                        .setFormattedText("Paupières")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A6 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(6)
                        .setValue(0).setText("Parties génitale")
                        .setFormattedText("Parties génitale")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A7 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(7)
                        .setValue(0).setText("Pieds")
                        .setFormattedText("Pieds")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A8 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(8)
                        .setValue(0).setText("Reste du visage")
                        .setFormattedText("Reste du visage")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                AnswerChoiceEntity Q11A9 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(9)
                        .setValue(0).setText("Autres parties du corps")
                        .setFormattedText("Autres parties du corps")
                        .setSupplementInput(true)
                        .setSupplementInputText("Nommez-les")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion11)
                        .build();

                Q11A1 = answerChoiceDal.save(Q11A1);
                Q11A2 = answerChoiceDal.save(Q11A2);
                Q11A3 = answerChoiceDal.save(Q11A3);
                Q11A4 = answerChoiceDal.save(Q11A4);
                Q11A5 = answerChoiceDal.save(Q11A5);
                Q11A6 = answerChoiceDal.save(Q11A6);
                Q11A7 = answerChoiceDal.save(Q11A7);
                Q11A8 = answerChoiceDal.save(Q11A8);
                Q11A9 = answerChoiceDal.save(Q11A9);

                List<AnswerChoiceEntity> possibleAnswersQ11 = new ArrayList<>();
                possibleAnswersQ11.add(Q11A1);
                possibleAnswersQ11.add(Q11A2);
                possibleAnswersQ11.add(Q11A3);
                possibleAnswersQ11.add(Q11A4);
                possibleAnswersQ11.add(Q11A5);
                possibleAnswersQ11.add(Q11A6);
                possibleAnswersQ11.add(Q11A7);
                possibleAnswersQ11.add(Q11A8);
                possibleAnswersQ11.add(Q11A9);
                questionVersion11 = questionVersionDal.save(questionVersion11);
                questionVersion11.setPossibleAnswers(possibleAnswersQ11);

                QuestionVersionTraductionEntity questionVersionTraduction11 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion11.getId())
                        .setLanguage("en")
                        .setText("If you suffer from angioedema, on which body parts have they already appeared?")
                        .setFormattedText("If you suffer from angioedema, on which body parts have they already appeared?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction11);

                AnswerChoiceTraductionEntity Q11A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Tongue")
                        .setFormattedText("Tongue")
                        .setAnswerChoiceId(Q11A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Lips")
                        .setFormattedText("Lips")
                        .setAnswerChoiceId(Q11A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Larynx")
                        .setFormattedText("Larynx")
                        .setAnswerChoiceId(Q11A3.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A4Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Hands")
                        .setFormattedText("Hands")
                        .setAnswerChoiceId(Q11A4.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A5Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Eyelids")
                        .setFormattedText("Eyelids")
                        .setAnswerChoiceId(Q11A5.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A6Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Genitals")
                        .setFormattedText("Genitals")
                        .setAnswerChoiceId(Q11A6.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A7Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Feet")
                        .setFormattedText("Feet")
                        .setAnswerChoiceId(Q11A7.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A8Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Rest of the face")
                        .setFormattedText("Rest of the face")
                        .setAnswerChoiceId(Q11A8.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q11A9Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Other parts of the body")
                        .setFormattedText("Other parts of the body")
                        .setSupplementInputText("Namely:")
                        .setAnswerChoiceId(Q11A9.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q11A1Traduction);
                answerChoiceTraductionDal.save(Q11A2Traduction);
                answerChoiceTraductionDal.save(Q11A3Traduction);
                answerChoiceTraductionDal.save(Q11A4Traduction);
                answerChoiceTraductionDal.save(Q11A5Traduction);
                answerChoiceTraductionDal.save(Q11A6Traduction);
                answerChoiceTraductionDal.save(Q11A7Traduction);
                answerChoiceTraductionDal.save(Q11A8Traduction);
                answerChoiceTraductionDal.save(Q11A9Traduction);

                QuestionVersionEntity questionVersion12 = QuestionVersionEntity.builder()
                        .setQuestion_order(12)
                        .setVerificationOrder(13)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Si votre urticaire se manifeste par des plaques d’érythème (papules et rougeurs) et de l’angioedème (enflure), quel symptôme est apparu le premier?")
                        .setFormattedText("Si votre urticaire se manifeste par des plaques d’érythème (papules et rougeurs) et de l’angioedème (enflure), quel symptôme est apparu le premier?")
                        .setInfo("")
                        .setFormattedInfo("")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question13).build();
                questionVersion12 = questionVersionDal.save(questionVersion12);

                QuestionVersionEntity questionVersion13 = QuestionVersionEntity.builder()
                        .setQuestion_order(0)
                        .setVerificationOrder(14)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Si les deux symptômes (papules et enflure) ne sont pas arrivés en même temps, combien de temps après l’apparition du premier, le deuxième s’est-il produit?")
                        .setFormattedText("Si les deux symptômes (papules et enflure) ne sont pas arrivés en même temps, combien de temps après l’apparition du premier, le deuxième s’est-il produit?")
                        .setIsSubQuestion(true)
                        .setSubQuestionOrder(0)
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question14).build();
                questionVersion13 = questionVersionDal.save(questionVersion13);

                AnswerChoiceEntity Q13A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("3 mois ou moins")
                        .setFormattedText("3 mois ou moins")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion13)
                        .build();

                AnswerChoiceEntity Q13A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("De 4 à 12 moi")
                        .setFormattedText("De 4 à 12 moi")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion13)
                        .build();

                AnswerChoiceEntity Q13A3 = AnswerChoiceEntity.builder().setCode(3).setAnswer_order(3)
                        .setValue(3).setText("Après 1 an")
                        .setFormattedText("Après 1 an")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion13)
                        .build();

                Q13A1 = answerChoiceDal.save(Q13A1);
                Q13A2 = answerChoiceDal.save(Q13A2);
                Q13A3 = answerChoiceDal.save(Q13A3);

                List<AnswerChoiceEntity> possibleAnswersQ13 = new ArrayList<>();
                possibleAnswersQ13.add(Q13A1);
                possibleAnswersQ13.add(Q13A2);
                possibleAnswersQ13.add(Q13A3);
                questionVersion13 = questionVersionDal.save(questionVersion13);
                questionVersion13.setPossibleAnswers(possibleAnswersQ13);

                //Traduction Question 13
                QuestionVersionTraductionEntity questionVersionTraduction13 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion13.getId())
                        .setLanguage("en")
                        .setText("If the two symptoms (wheals and angioedema) did not appear together, how long after the occurrence of the first symptom , did the second one appear?")
                        .setFormattedText("If the two symptoms (wheals and angioedema) did not appear together, how long after the occurrence of the first symptom , did the second one appear?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction13);

                AnswerChoiceTraductionEntity Q13A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Within 3 months")
                        .setFormattedText("Within 3 months")
                        .setAnswerChoiceId(Q13A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q13A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("After 4 – 12 months")
                        .setFormattedText("After 4 – 12 months")
                        .setAnswerChoiceId(Q13A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q13A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("After more than 1 year")
                        .setFormattedText("After more than 1 year")
                        .setAnswerChoiceId(Q13A3.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q13A1Traduction);
                answerChoiceTraductionDal.save(Q13A2Traduction);
                answerChoiceTraductionDal.save(Q13A3Traduction);

                SubQuestionConditionEntity subQuestionConditionEntityQ12C1 = SubQuestionConditionEntity.builder()
                        .setName("")
                        .setSurveyVersion(surveyVersionLatest.getId())
                        .setQuestionVersion(questionVersion12.getId())
                        .setSubQuestionVersion(questionVersion13.getId())
                        .setSubQuestionOrder(1)
                        .build();
                subQuestionConditionEntityQ12C1 = subQuestionConditionDal.save(subQuestionConditionEntityQ12C1);

                AnswerChoiceEntity Q12A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(0).setText("Plaques d’érythème (papules et rougeurs)")
                        .setFormattedText("Plaques d’érythème (papules et rougeurs)")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion12)
                        .build();

                AnswerChoiceEntity Q12A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(0).setText("Angioedème (enflure)")
                        .setFormattedText("Angioedème (enflure)")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion12)
                        .build();

                AnswerChoiceEntity Q12A3 = AnswerChoiceEntity.builder().setCode(3).setAnswer_order(3)
                        .setValue(0).setText("Les deux en même temps")
                        .setFormattedText("Les deux en même temps")
                        .setSubQuestionCondition(subQuestionConditionEntityQ12C1.getId())
                        .setName("")
                        .setQuestionVersionEntity(questionVersion12)
                        .build();

                Q12A1 = answerChoiceDal.save(Q12A1);
                Q12A2 = answerChoiceDal.save(Q12A2);
                Q12A3 = answerChoiceDal.save(Q12A3);

                List<AnswerChoiceEntity> possibleAnswersQ12 = new ArrayList<>();
                possibleAnswersQ12.add(Q12A1);
                possibleAnswersQ12.add(Q12A2);
                possibleAnswersQ12.add(Q12A3);
                questionVersion12 = questionVersionDal.save(questionVersion12);
                questionVersion12.setPossibleAnswers(possibleAnswersQ12);

                //Traduction Question 12
                QuestionVersionTraductionEntity questionVersionTraduction12 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion12.getId())
                        .setLanguage("en")
                        .setText("If you suffer from wheals and angioedema, which symptom appeared first?")
                        .setFormattedText("If you suffer from wheals and angioedema, which symptom appeared first?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction12);

                AnswerChoiceTraductionEntity Q12A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Initially, only wheals")
                        .setFormattedText("Initially, only wheals")
                        .setAnswerChoiceId(Q12A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q12A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Initially, only angioedema")
                        .setFormattedText("Initially, only angioedema")
                        .setAnswerChoiceId(Q12A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q12A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Both symptoms appeared from the beginning")
                        .setFormattedText("Both symptoms appeared from the beginning")
                        .setAnswerChoiceId(Q12A3.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q12A1Traduction);
                answerChoiceTraductionDal.save(Q12A2Traduction);
                answerChoiceTraductionDal.save(Q12A3Traduction);

                String[] Q14Part1Answers = new String[]{
                        "Oui",
                        "Non"
                };
                createQuestionVersion(question15, "Fièvre inexpliquée récidivante", surveyVersionLatest, 13, 15, Q14Part1Answers, 1);

                //Traduction
                String Question14TraductionPart1 = "In addition to wheals and angioedema, did you suffer from the symptoms listed below?";
                String[] Q14AnswersTraductionPart1 = new String[]{
                        "Yes ",
                        "No"
                };
                createQuestionVersionTraduction(null, 15, new String[]{"Recurrent unexplained fever"}, Q14AnswersTraductionPart1, Question14TraductionPart1, "en", surveyVersionLatest);
                //


                String[] Q14Part2Answers = new String[]{
                        "Oui",
                        "Non"
                };
                createQuestionVersion(question16, "Douleurs musculo-squelettiques", surveyVersionLatest, 14, 16, Q14Part2Answers, 1);

                //Traduction
                String Question14TraductionPart2 = "In addition to wheals and angioedema, did you suffer from the symptoms listed below?";
                String[] Q14AnswersTraductionPart2 = new String[]{
                        "Yes ",
                        "No"
                };
                createQuestionVersionTraduction(null, 16, new String[]{"Joint, bone, muscle pain"}, Q14AnswersTraductionPart2, Question14TraductionPart2, "en", surveyVersionLatest);

                String[] Q14Part3Answers = new String[]{
                        "Oui",
                        "Non"
                };
                createQuestionVersion(question17, "Malaises généralisés", surveyVersionLatest, 15, 17, Q14Part3Answers, 1);

                String Question14TraductionPart3 = "In addition to wheals and angioedema, did you suffer from the symptoms listed below?";
                String[] Q14AnswersTraductionPart3 = new String[]{
                        "Yes ",
                        "No"
                };
                createQuestionVersionTraduction(null, 17, new String[]{"General recurrent discomfort"}, Q14AnswersTraductionPart3, Question14TraductionPart3, "en", surveyVersionLatest);

                QuestionVersionEntity questionVersion15 = QuestionVersionEntity.builder()
                        .setQuestion_order(16)
                        .setVerificationOrder(18)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("En même temps que l’urticaire, avez-vous déjà perdu connaissance, vous êtes-vous senti faible ou épuisé, ou avez-vous eu de la difficulté à respirer?")
                        .setFormattedText("En même temps que l’urticaire, avez-vous déjà perdu connaissance, vous êtes-vous senti faible ou épuisé, ou avez-vous eu de la difficulté à respirer?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question18).build();
                questionVersion15 = questionVersionDal.save(questionVersion15);

                AnswerChoiceEntity Q15A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion15)
                        .build();

                AnswerChoiceEntity Q15A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, précisez (nature et fréquence)")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion15)
                        .build();

                Q15A1 = answerChoiceDal.save(Q15A1);
                Q15A2 = answerChoiceDal.save(Q15A2);

                List<AnswerChoiceEntity> possibleAnswersQ15 = new ArrayList<>();
                possibleAnswersQ15.add(Q15A1);
                possibleAnswersQ15.add(Q15A2);
                questionVersion15 = questionVersionDal.save(questionVersion15);
                questionVersion15.setPossibleAnswers(possibleAnswersQ15);

                QuestionVersionTraductionEntity questionVersionTraduction15 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion15.getId())
                        .setLanguage("en")
                        .setText("Have you ever collapsed, felt faint or breathless with urticaria?")
                        .setFormattedText("Have you ever collapsed, felt faint or breathless with urticaria?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction15);

                AnswerChoiceTraductionEntity Q15A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, nature and frequency:")
                        .setAnswerChoiceId(Q15A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q15A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q15A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q15A1Traduction);
                answerChoiceTraductionDal.save(Q15A2Traduction);

                String[] Q16Answers = new String[]{
                        "",
                };
                createQuestionVersion(question19, "", surveyVersionLatest, 17, 19, Q16Answers, 7);

                String Question16Traduction = "Which other diseases, besides urticaria, do you have?";
                String[] Q16AnswersTraduction = new String[]{
                        ""
                };
                createQuestionVersionTraduction(null, 19, new String[]{""}, Q16AnswersTraduction, Question16Traduction, "en", surveyVersionLatest);
                //

                QuestionVersionEntity questionVersion17 = QuestionVersionEntity.builder()
                        .setQuestion_order(18)
                        .setVerificationOrder(20)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Souffrez-vous de problèmes psychologiques ou de maladies mentales?")
                        .setFormattedText("Souffrez-vous de problèmes psychologiques ou de maladies mentales?")
                        .setInfo("(ex : anxiété, dépression etc.)")
                        .setFormattedInfo("(ex : anxiété, dépression etc.)")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question20).build();
                questionVersion17 = questionVersionDal.save(questionVersion17);

                AnswerChoiceEntity Q17A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Nommez les")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion17)
                        .build();

                AnswerChoiceEntity Q17A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion17)
                        .build();

                Q17A1 = answerChoiceDal.save(Q17A1);
                Q17A2 = answerChoiceDal.save(Q17A2);

                List<AnswerChoiceEntity> possibleAnswersQ17 = new ArrayList<>();
                possibleAnswersQ17.add(Q17A1);
                possibleAnswersQ17.add(Q17A2);
                questionVersion17 = questionVersionDal.save(questionVersion17);
                questionVersion17.setPossibleAnswers(possibleAnswersQ17);

                QuestionVersionTraductionEntity questionVersionTraduction17 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion17.getId())
                        .setLanguage("en")
                        .setText("Do you suffer from psychological problems or mental illness (for example: depression, anxiety disorders)")
                        .setFormattedText("Do you suffer from psychological problems or mental illness (for example: depression, anxiety disorders)")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction17);

                AnswerChoiceTraductionEntity Q17A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, specify:")
                        .setAnswerChoiceId(Q17A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q17A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q17A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q17A1Traduction);
                answerChoiceTraductionDal.save(Q17A2Traduction);

                QuestionVersionEntity questionVersion18 = QuestionVersionEntity.builder()
                        .setQuestion_order(19)
                        .setVerificationOrder(21)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Souffrez-vous d’infections chroniques?")
                        .setFormattedText("Souffrez-vous d’infections chroniques?")
                        .setInfo("(ex : hépatite)")
                        .setFormattedInfo("(ex : hépatite)")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question21).build();
                questionVersion18 = questionVersionDal.save(questionVersion18);

                AnswerChoiceEntity Q18A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, lesquelles?")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion18)
                        .build();

                AnswerChoiceEntity Q18A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion18)
                        .build();

                Q18A1 = answerChoiceDal.save(Q18A1);
                Q18A2 = answerChoiceDal.save(Q18A2);

                List<AnswerChoiceEntity> possibleAnswersQ18 = new ArrayList<>();
                possibleAnswersQ18.add(Q18A1);
                possibleAnswersQ18.add(Q18A2);
                questionVersion18 = questionVersionDal.save(questionVersion18);
                questionVersion18.setPossibleAnswers(possibleAnswersQ18);

                QuestionVersionTraductionEntity questionVersionTraduction18 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion18.getId())
                        .setLanguage("en")
                        .setText("Do you suffer from chronic infections?")
                        .setFormattedText("Do you suffer from chronic infections?")
                        .setInfo("(e.g. hepatitis)")
                        .setFormattedInfo("(e.g. hepatitis)")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction18);

                AnswerChoiceTraductionEntity Q18A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, which ones?")
                        .setAnswerChoiceId(Q18A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q18A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q18A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q18A1Traduction);
                answerChoiceTraductionDal.save(Q18A2Traduction);

                QuestionVersionEntity questionVersion19 = QuestionVersionEntity.builder()
                        .setQuestion_order(20)
                        .setVerificationOrder(22)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Avez-vous des problèmes gastro-intestinaux?")
                        .setFormattedText("Avez-vous des problèmes gastro-intestinaux?")
                        .setInfo("(digestion, crampes, diarrhée…)")
                        .setFormattedInfo("(digestion, crampes, diarrhée…)")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question22).build();
                questionVersion19 = questionVersionDal.save(questionVersion19);

                AnswerChoiceEntity Q19A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, spécifiez")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion19)
                        .build();

                AnswerChoiceEntity Q19A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion19)
                        .build();

                Q19A1 = answerChoiceDal.save(Q19A1);
                Q19A2 = answerChoiceDal.save(Q19A2);

                List<AnswerChoiceEntity> possibleAnswersQ19 = new ArrayList<>();
                possibleAnswersQ19.add(Q19A1);
                possibleAnswersQ19.add(Q19A2);
                questionVersion19 = questionVersionDal.save(questionVersion19);
                questionVersion19.setPossibleAnswers(possibleAnswersQ19);

                //Traduction
                QuestionVersionTraductionEntity questionVersionTraduction19 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion19.getId())
                        .setLanguage("en")
                        .setText("Do you suffer from gastrointestinal complaints?")
                        .setFormattedText("Do you suffer from gastrointestinal complaints?")
                        .setInfo("(GI symptoms, cramps, diarrhea…)")
                        .setFormattedInfo("(GI symptoms, cramps, diarrhea…)")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction19);

                AnswerChoiceTraductionEntity Q19A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, specify:")
                        .setAnswerChoiceId(Q19A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q19A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q19A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q19A1Traduction);
                answerChoiceTraductionDal.save(Q19A2Traduction);

                QuestionVersionEntity questionVersion20 = QuestionVersionEntity.builder()
                        .setQuestion_order(21)
                        .setVerificationOrder(23)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Prenez-vous des médicaments sur une base régulière (autres que les médicaments pour l’urticaire)?")
                        .setFormattedText("Prenez-vous des médicaments sur une base régulière (autres que les médicaments pour l’urticaire)?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question23).build();
                questionVersion20 = questionVersionDal.save(questionVersion20);

                AnswerChoiceEntity Q20A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, lesquels?")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion20)
                        .build();

                AnswerChoiceEntity Q20A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion20)
                        .build();

                Q20A1 = answerChoiceDal.save(Q20A1);
                Q20A2 = answerChoiceDal.save(Q20A2);

                List<AnswerChoiceEntity> possibleAnswersQ20 = new ArrayList<>();
                possibleAnswersQ20.add(Q20A1);
                possibleAnswersQ20.add(Q20A2);
                questionVersion20 = questionVersionDal.save(questionVersion20);
                questionVersion20.setPossibleAnswers(possibleAnswersQ20);

                QuestionVersionTraductionEntity questionVersionTraduction20 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion20.getId())
                        .setLanguage("en")
                        .setText("Do you take medication on a regular basis?")
                        .setFormattedText("Do you take medication on a regular basis?")
                        .setInfo("(apart from medication for urticaria)")
                        .setFormattedInfo("(apart from medication for urticaria)")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction20);

                AnswerChoiceTraductionEntity Q20A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, which medication?")
                        .setAnswerChoiceId(Q20A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q20A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q20A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q20A1Traduction);
                answerChoiceTraductionDal.save(Q20A2Traduction);

                String[] Q21Answers = new String[]{
                        "Oui",
                        "Non"
                };
                createQuestionVersion(question24, "", surveyVersionLatest, 22, 24, Q21Answers, 1);

                //Traduction
                String Question21Traduction = "Are the symptoms of your urticaria getting worse after you take pain relievers or anti-inflammatory drugs?";
                String[] Q21AnswersTraduction = new String[]{
                        "Yes",
                        "No"
                };
                createQuestionVersionTraduction(null, 24, new String[]{""}, Q21AnswersTraduction, Question21Traduction, "en", surveyVersionLatest);
                //


                QuestionVersionEntity questionVersion22 = QuestionVersionEntity.builder()
                        .setQuestion_order(23)
                        .setVerificationOrder(25)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Avez-vous déjà utilisé des médicaments pour le traitement de votre urticaire?")
                        .setFormattedText("Avez-vous déjà utilisé des médicaments pour le traitement de votre urticaire?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question25).build();
                questionVersion22 = questionVersionDal.save(questionVersion22);

                QuestionVersionEntity questionVersion23 = QuestionVersionEntity.builder()
                        .setQuestion_order(0)
                        .setVerificationOrder(26)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setIsSubQuestion(true)
                        .setSubQuestionOrder(0)
                        .setText("Efficacité de ces traitements")
                        .setFormattedText("Efficacité de ces traitements")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question26).build();
                questionVersion23 = questionVersionDal.save(questionVersion23);

                AnswerChoiceEntity Q23A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Très efficace (symptômes réduits de 90% et plus)")
                        .setFormattedText("Très efficace (symptômes réduits de 90% et plus)")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion23)
                        .build();

                AnswerChoiceEntity Q23A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Efficacité moyenne (symptômes réduits, mais de moins de 90%)")
                        .setFormattedText("Efficacité moyenne (symptômes réduits, mais de moins de 90%)")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion23)
                        .build();

                AnswerChoiceEntity Q23A3 = AnswerChoiceEntity.builder().setCode(3).setAnswer_order(3)
                        .setValue(3).setText("Non efficace")
                        .setFormattedText("Non efficace")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion23)
                        .build();

                Q23A1 = answerChoiceDal.save(Q23A1);
                Q23A2 = answerChoiceDal.save(Q23A2);
                Q23A3 = answerChoiceDal.save(Q23A3);

                List<AnswerChoiceEntity> possibleAnswersQ23 = new ArrayList<>();
                possibleAnswersQ23.add(Q23A1);
                possibleAnswersQ23.add(Q23A2);
                possibleAnswersQ23.add(Q23A3);
                questionVersion23 = questionVersionDal.save(questionVersion23);
                questionVersion23.setPossibleAnswers(possibleAnswersQ23);

                //Traduction question23
                QuestionVersionTraductionEntity questionVersionTraduction23 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion23.getId())
                        .setLanguage("en")
                        .setText("Success of treatments")
                        .setFormattedText("Success of treatments")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction23);

                AnswerChoiceTraductionEntity Q23A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Good success")
                        .setFormattedText("Good success")
                        .setInfo("(reduction of symptoms by at least 90%)")
                        .setFormattedInfo("(reduction of symptoms by at least 90%)")
                        .setAnswerChoiceId(Q23A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q23A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Partial success")
                        .setFormattedText("Partial success")
                        .setInfo("(reduction of symptoms, but by less than 90%)")
                        .setFormattedInfo("(reduction of symptoms, but by less than 90%)")
                        .setAnswerChoiceId(Q23A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q23A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No reduction of symptoms")
                        .setFormattedText("No reduction of symptoms")
                        .setAnswerChoiceId(Q23A3.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q23A1Traduction);
                answerChoiceTraductionDal.save(Q23A2Traduction);
                answerChoiceTraductionDal.save(Q23A3Traduction);

                SubQuestionConditionEntity subQuestionConditionEntityQ22C1 = SubQuestionConditionEntity.builder()
                        .setName("")
                        .setSurveyVersion(surveyVersionLatest.getId())
                        .setQuestionVersion(questionVersion22.getId())
                        .setSubQuestionVersion(questionVersion23.getId())
                        .setSubQuestionOrder(1)
                        .build();
                subQuestionConditionEntityQ22C1 = subQuestionConditionDal.save(subQuestionConditionEntityQ22C1);

                AnswerChoiceEntity Q22A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, nommez les")
                        .setSubQuestionCondition(subQuestionConditionEntityQ22C1.getId())
                        .setName("")
                        .setQuestionVersionEntity(questionVersion22)
                        .build();

                AnswerChoiceEntity Q22A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion22)
                        .build();

                Q22A1 = answerChoiceDal.save(Q22A1);
                Q22A2 = answerChoiceDal.save(Q22A2);

                List<AnswerChoiceEntity> possibleAnswersQ22 = new ArrayList<>();
                possibleAnswersQ22.add(Q20A1);
                possibleAnswersQ22.add(Q20A2);
                questionVersion22 = questionVersionDal.save(questionVersion22);
                questionVersion22.setPossibleAnswers(possibleAnswersQ22);

                QuestionVersionTraductionEntity questionVersionTraduction22 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion22.getId())
                        .setLanguage("en")
                        .setText("Has your urticaria been treated differently with medications before?")
                        .setFormattedText("Has your urticaria been treated differently with medications before?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction22);

                AnswerChoiceTraductionEntity Q22A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, which medications?")
                        .setAnswerChoiceId(Q22A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q22A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q22A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q22A1Traduction);
                answerChoiceTraductionDal.save(Q22A2Traduction);

                QuestionVersionEntity questionVersion24 = QuestionVersionEntity.builder()
                        .setQuestion_order(0)
                        .setVerificationOrder(27)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setIsSubQuestion(true)
                        .setSubQuestionOrder(0)
                        .setText("Effets secondaires du (des) traitement(s)")
                        .setFormattedText("Effets secondaires du (des) traitement(s)")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question27).build();
                questionVersion24 = questionVersionDal.save(questionVersion24);

                SubQuestionConditionEntity subQuestionConditionEntityQ22C2 = SubQuestionConditionEntity.builder()
                        .setName("")
                        .setSurveyVersion(surveyVersionLatest.getId())
                        .setQuestionVersion(questionVersion23.getId())
                        .setSubQuestionVersion(questionVersion24.getId())
                        .setSubQuestionOrder(1)
                        .build();
                subQuestionConditionDal.save(subQuestionConditionEntityQ22C2);

                AnswerChoiceEntity Q24A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, lesquels")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion24)
                        .build();

                AnswerChoiceEntity Q24A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion24)
                        .build();

                Q24A1 = answerChoiceDal.save(Q24A1);
                Q24A2 = answerChoiceDal.save(Q24A2);

                List<AnswerChoiceEntity> possibleAnswersQ24 = new ArrayList<>();
                possibleAnswersQ24.add(Q24A1);
                possibleAnswersQ24.add(Q24A2);
                questionVersion24 = questionVersionDal.save(questionVersion24);
                questionVersion24.setPossibleAnswers(possibleAnswersQ24);

                QuestionVersionTraductionEntity questionVersionTraduction24 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion24.getId())
                        .setLanguage("en")
                        .setText("Side effects of the treatment(s)?")
                        .setFormattedText("Side effects of the treatment(s)?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction24);

                AnswerChoiceTraductionEntity Q24A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, which ones?")
                        .setAnswerChoiceId(Q24A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q24A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q24A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q24A1Traduction);
                answerChoiceTraductionDal.save(Q24A2Traduction);

                QuestionVersionEntity questionVersion25 = QuestionVersionEntity.builder()
                        .setQuestion_order(24)
                        .setVerificationOrder(28)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Votre urticaire a-t-elle déjà été traitée par des approches autres que la médecine traditionnelle?")
                        .setFormattedText("Votre urticaire a-t-elle déjà été traitée par des approches autres que la médecine traditionnelle?")
                        .setInfo("(ex : diètes, naturopathie, homéopathie, acupuncture, etc.)")
                        .setFormattedInfo("(ex : diètes, naturopathie, homéopathie, acupuncture, etc.)")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question28).build();
                questionVersion25 = questionVersionDal.save(questionVersion25);

                AnswerChoiceEntity Q25A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, lesquelles")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion25)
                        .build();

                AnswerChoiceEntity Q25A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion25)
                        .build();

                Q25A1 = answerChoiceDal.save(Q25A1);
                Q25A2 = answerChoiceDal.save(Q25A2);

                List<AnswerChoiceEntity> possibleAnswersQ25 = new ArrayList<>();
                possibleAnswersQ25.add(Q25A1);
                possibleAnswersQ25.add(Q25A2);
                questionVersion25 = questionVersionDal.save(questionVersion25);
                questionVersion25.setPossibleAnswers(possibleAnswersQ25);

                QuestionVersionTraductionEntity questionVersionTraduction25 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion25.getId())
                        .setLanguage("en")
                        .setText("Has your urticaria ever been treated with something else than medications?")
                        .setFormattedText("Has your urticaria ever been treated with something else than medications?")
                        .setInfo("(diets, naturopathic treatments, acupuncture, and others…)")
                        .setFormattedInfo("(diets, naturopathic treatments, acupuncture, and others…)")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction25);

                AnswerChoiceTraductionEntity Q25A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, specify?")
                        .setAnswerChoiceId(Q25A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q25A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q25A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q25A1Traduction);
                answerChoiceTraductionDal.save(Q25A2Traduction);

                String[] Q26Answers = new String[]{
                        "Infection",
                        "Médicaments",
                        "Aliments",
                        "Stress",
                        "Piqûres d’insectes",
                        "Autres déclencheurs",
                        "Je n’ai aucune idée de ce qui a causé mon urticaire"
                };
                createQuestionVersion(question29, "", surveyVersionLatest, 25, 29, Q26Answers, 1);

                String Question26Traduction = "What, do you believe, initially cause your urticaria?";
                String[] Q26AnswersTraduction = new String[]{
                        "Infection",
                        "Medication",
                        "Food",
                        "Stress" ,
                        "Insect bites" ,
                        "Other triggers" ,
                        "I ignore what could have caused my urticaria"
                };
                createQuestionVersionTraduction(null, 29, new String[]{""}, Q26AnswersTraduction, Question26Traduction, "en", surveyVersionLatest);

                String[] Q27Answers = new String[]{
                        "Infection",
                        "Médicaments",
                        "Aliments",
                        "Stress",
                        "Piqûres d’insectes",
                        "Autres facteurs",
                        "J’ignore ce qui aggrave mon urticaire"
                };
                createQuestionVersion(question30, "", surveyVersionLatest, 26, 30, Q27Answers, 2);

                //Traduction
                String Question27Traduction = "Which of the following factors can, you believe, make your urticaria worse?";
                String[] Q27AnswersTraduction = new String[]{
                        "Infection",
                        "Medication",
                        "Food",
                        "Stress",
                        "Insect bites",
                        "Other factors",
                        "I don’t know what can make my urticaria worse"
                };
                createQuestionVersionTraduction(null, 30, new String[]{""}, Q27AnswersTraduction, Question27Traduction, "en", surveyVersionLatest);

                String[] Q28Answers = new String[]{
                        "Oui, j’ai eu sans arrêt des symptômes d’urticaire depuis le début de la maladie",
                        "Non, j’ai eu parfois des périodes de plus de deux semaines sans symptômes d’urticaire"
                };
                createQuestionVersion(question31, "", surveyVersionLatest, 27, 31, Q28Answers, 1);

                String Question28Traduction = "Since the beginning of your urticaria, have you had almost continuous symptoms?";
                String[] Q28AnswersTraduction = new String[]{
                        "Yes, I have had continuous urticaria symptoms since the beginning of illness",
                        "No, there has been longer periods (at least 2 weeks) without urticaria symptoms",
                };
                createQuestionVersionTraduction(null, 31, new String[]{""}, Q28AnswersTraduction, Question28Traduction, "en", surveyVersionLatest);

                String[] Q29Answers = new String[]{
                        "Mon urticaire s’améliore",
                        "Mon urticaire s’aggrave",
                        "Aucun changement de mon urticaire",
                        "La question ne s’applique pas à moi"
                };
                createQuestionVersion(question32, "", surveyVersionLatest, 28, 32, Q29Answers, 1);

                String Question29Traduction = "What effect have menstruations on your urticaria symptoms?";
                String[] Q29AnswersTraduction = new String[]{
                        "My urticaria improves",
                        "My urticaria worsens",
                        "My urticaria doesn’t change",
                        "Question doesn’t apply to me"
                };
                createQuestionVersionTraduction(null, 32, new String[]{""}, Q29AnswersTraduction, Question29Traduction, "en", surveyVersionLatest);

                QuestionVersionEntity questionVersion30 = QuestionVersionEntity.builder()
                        .setQuestion_order(29)
                        .setVerificationOrder(33)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Avez-vous des parents proches (père, mère, enfants) qui ont eu de l’urticaire chronique (symptômes durant plus de six semaines)?")
                        .setFormattedText("Avez-vous des parents proches (père, mère, enfants) qui ont eu de l’urticaire chronique (symptômes durant plus de six semaines)?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question33).build();
                questionVersion30 = questionVersionDal.save(questionVersion30);

                QuestionVersionEntity questionVersion31 = QuestionVersionEntity.builder()
                        .setQuestion_order(0)
                        .setVerificationOrder(34)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setIsSubQuestion(true)
                        .setSubQuestionOrder(0)
                        .setText("Si oui, de quel type")
                        .setFormattedText("Si oui, de quel type")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question34).build();
                questionVersion31 = questionVersionDal.save(questionVersion31);

                AnswerChoiceEntity Q31A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Symptômes spontanés")
                        .setFormattedText("Symptômes inductibles")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion31)
                        .build();

                AnswerChoiceEntity Q31A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Symptômes inductibles")
                        .setFormattedText("Symptômes inductibles")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion31)
                        .build();

                AnswerChoiceEntity Q31A3 = AnswerChoiceEntity.builder().setCode(3).setAnswer_order(3)
                        .setValue(3).setText("Je ne sais pas")
                        .setFormattedText("Je ne sais pas")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion31)
                        .build();

                Q31A1 = answerChoiceDal.save(Q31A1);
                Q31A2 = answerChoiceDal.save(Q31A2);
                Q31A3 = answerChoiceDal.save(Q31A3);

                List<AnswerChoiceEntity> possibleAnswersQ31 = new ArrayList<>();
                possibleAnswersQ31.add(Q31A1);
                possibleAnswersQ31.add(Q31A2);
                possibleAnswersQ31.add(Q31A3);
                questionVersion31 = questionVersionDal.save(questionVersion31);
                questionVersion31.setPossibleAnswers(possibleAnswersQ31);

                QuestionVersionTraductionEntity questionVersionTraduction31 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion31.getId())
                        .setLanguage("en")
                        .setText("If yes, which type?")
                        .setFormattedText("If yes, which type?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction31);

                AnswerChoiceTraductionEntity Q31A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Spontaneously appearing symptoms")
                        .setFormattedText("Spontaneously appearing symptoms")
                        .setAnswerChoiceId(Q31A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q31A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Inducible skin symptoms")
                        .setFormattedText("Inducible skin symptoms")
                        .setAnswerChoiceId(Q31A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q31A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("I don’t know")
                        .setFormattedText("I don’t know")
                        .setAnswerChoiceId(Q31A3.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q31A1Traduction);
                answerChoiceTraductionDal.save(Q31A2Traduction);
                answerChoiceTraductionDal.save(Q31A3Traduction);

                SubQuestionConditionEntity subQuestionConditionEntityQ30C1 = SubQuestionConditionEntity.builder()
                        .setName("")
                        .setSurveyVersion(surveyVersionLatest.getId())
                        .setQuestionVersion(questionVersion30.getId())
                        .setSubQuestionVersion(questionVersion31.getId())
                        .setSubQuestionOrder(1)
                        .build();
                subQuestionConditionEntityQ30C1 = subQuestionConditionDal.save(subQuestionConditionEntityQ30C1);

                AnswerChoiceEntity Q30A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSubQuestionCondition(subQuestionConditionEntityQ30C1.getId())
                        .setName("")
                        .setQuestionVersionEntity(questionVersion30)
                        .build();

                AnswerChoiceEntity Q30A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion30)
                        .build();

                AnswerChoiceEntity Q30A3 = AnswerChoiceEntity.builder().setCode(3).setAnswer_order(3)
                        .setValue(3).setText("Je ne sais pas")
                        .setFormattedText("Je ne sais pas")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion30)
                        .build();

                Q30A1 = answerChoiceDal.save(Q30A1);
                Q30A2 = answerChoiceDal.save(Q30A2);
                Q30A3 = answerChoiceDal.save(Q30A3);

                List<AnswerChoiceEntity> possibleAnswersQ30 = new ArrayList<>();
                possibleAnswersQ30.add(Q30A1);
                possibleAnswersQ30.add(Q30A2);
                possibleAnswersQ30.add(Q30A3);
                questionVersion30 = questionVersionDal.save(questionVersion30);
                questionVersion30.setPossibleAnswers(possibleAnswersQ30);

                //Traduction question 30
                QuestionVersionTraductionEntity questionVersionTraduction30 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion30.getId())
                        .setLanguage("en")
                        .setText("Do you have first degree relatives (parents or own children) who have or have had urticaria for more than 6 weeks?")
                        .setFormattedText("Do you have first degree relatives (parents or own children) who have or have had urticaria for more than 6 weeks?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction30);

                AnswerChoiceTraductionEntity Q30A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setAnswerChoiceId(Q30A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q30A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q30A2.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q30A3Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("I don’t know")
                        .setFormattedText("I don’t know")
                        .setAnswerChoiceId(Q30A3.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q30A1Traduction);
                answerChoiceTraductionDal.save(Q30A2Traduction);
                answerChoiceTraductionDal.save(Q30A3Traduction);

                QuestionVersionEntity questionVersion32 = QuestionVersionEntity.builder()
                        .setQuestion_order(30)
                        .setVerificationOrder(35)
                        .setQuestionType(questionTypeDal.findByTypeCode(1))
                        .setText("Souffrez-vous d’allergies, asthme, ou eczéma?")
                        .setFormattedText("Souffrez-vous d’allergies, asthme, ou eczéma?")
                        .setName("")
                        .setSurveyVersionEntity(surveyVersionLatest).setQuestion(question35).build();
                questionVersion32 = questionVersionDal.save(questionVersion32);

                AnswerChoiceEntity Q32A1 = AnswerChoiceEntity.builder().setCode(1).setAnswer_order(1)
                        .setValue(1).setText("Oui")
                        .setFormattedText("Oui")
                        .setSupplementInput(true)
                        .setSupplementInputText("Si oui, précisez")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion32)
                        .build();

                AnswerChoiceEntity Q32A2 = AnswerChoiceEntity.builder().setCode(2).setAnswer_order(2)
                        .setValue(2).setText("Non")
                        .setFormattedText("Non")
                        .setName("")
                        .setQuestionVersionEntity(questionVersion32)
                        .build();

                Q32A1 = answerChoiceDal.save(Q32A1);
                Q32A2 = answerChoiceDal.save(Q32A2);

                List<AnswerChoiceEntity> possibleAnswersQ32 = new ArrayList<>();
                possibleAnswersQ32.add(Q32A1);
                possibleAnswersQ32.add(Q32A2);
                questionVersion32 = questionVersionDal.save(questionVersion32);
                questionVersion32.setPossibleAnswers(possibleAnswersQ32);

                //Tradution question 32
                QuestionVersionTraductionEntity questionVersionTraduction32 = QuestionVersionTraductionEntity.builder()
                        .setQuestionVersionId(questionVersion32.getId())
                        .setLanguage("en")
                        .setText("Do you suffer from allergies, asthma, or atopic dermatitis?")
                        .setFormattedText("Do you suffer from allergies, asthma, or atopic dermatitis?")
                        .setName("")
                        .build();
                questionVersionTraductionDal.save(questionVersionTraduction32);

                AnswerChoiceTraductionEntity Q32A1Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("Yes")
                        .setFormattedText("Yes")
                        .setSupplementInputText("If yes, specify:")
                        .setAnswerChoiceId(Q32A1.getId())
                        .setName("")
                        .build();

                AnswerChoiceTraductionEntity Q32A2Traduction = AnswerChoiceTraductionEntity.builder()
                        .setLanguage("en")
                        .setText("No")
                        .setFormattedText("No")
                        .setAnswerChoiceId(Q32A2.getId())
                        .setName("")
                        .build();

                answerChoiceTraductionDal.save(Q32A1Traduction);
                answerChoiceTraductionDal.save(Q32A2Traduction);
        }

        public int createQuestion(String[] listOfItems, SurveyEntity survey, String text, SurveyVersionEntity surveyVersion, int questionVersionIndex, String[] allAnswerTextes){
                List<QuestionEntity> listOfQuestionsForSurvey = new ArrayList<>();
                for (String info : listOfItems)
                {
                        QuestionEntity newQuestion = QuestionEntity.builder().setSurvey(survey)
                                .setText(text).setName("")
                                .build();
                        newQuestion = questionDal.save(newQuestion);
                        listOfQuestionsForSurvey.add(newQuestion);
                        questionVersionIndex++;
                        createQuestionVersion(newQuestion, info ,surveyVersion, questionVersionIndex, questionVersionIndex, allAnswerTextes, 1);
                }
                survey.setQuestions(listOfQuestionsForSurvey);
                return questionVersionIndex;
        }

        public void createQuestionVersion(QuestionEntity question, String info, SurveyVersionEntity surveyVersion, int order, int verification_order, String[] allAnswerTextes, int questionTypeCode){
                QuestionVersionEntity newQuestionVesion = QuestionVersionEntity.builder()
                        .setQuestion_order(order)
                        .setVerificationOrder(verification_order)
                        .setQuestionType(questionTypeDal.findByTypeCode(questionTypeCode))
                        .setText(question.getText())
                        .setFormattedText(question.getText())
                        .setInfo(info)
                        .setFormattedInfo(info)
                        .setName("")
                        .setSurveyVersionEntity(surveyVersion).setQuestion(question).build();

                newQuestionVesion = questionVersionDal.save(newQuestionVesion);
                List<AnswerChoiceEntity> possibleAnswers = createAnswersChoices(newQuestionVesion, allAnswerTextes);

                newQuestionVesion = questionVersionDal.save(newQuestionVesion);
                newQuestionVesion.setPossibleAnswers(possibleAnswers);
        }

        public List<AnswerChoiceEntity> createAnswersChoices(QuestionVersionEntity questionVersion, String[] allAnswerTextes){
                List<AnswerChoiceEntity> possibleAnswers = new ArrayList<>();
                for (int i = 0; i < allAnswerTextes.length; i++) {
                        String text = allAnswerTextes[i];
                        AnswerChoiceEntity newAnswerChoice = AnswerChoiceEntity.builder().setAnswer_order(i+1)
                                .setValue(0).setText(text).setFormattedText(text).setInfo("")
                                .setFormattedInfo("")
                                .setName("")
                                .setQuestionVersionEntity(questionVersion)
                                .build();
                        newAnswerChoice = answerChoiceDal.save(newAnswerChoice);
                        possibleAnswers.add(newAnswerChoice);
                }
                return possibleAnswers;
        }

        public int createQuestionVersionTraduction(List<QuestionVersionEntity> allQuestionVersion, int index, String[] allQuestionTextTraduction, String[] allAnswerTraduction, String partText, String languageShort, SurveyVersionEntity surveyVersion){
                for (String text : allQuestionTextTraduction) {
                        int questionVersionTraductionIndex = index;

                        if(allQuestionVersion == null){
                                allQuestionVersion = questionVersionDal.getAllQuestionVersionBySurveyVersion(surveyVersion.getId());
                        }

                        QuestionVersionEntity currentQuestion = allQuestionVersion.stream().filter(qv -> qv.getVerificationOrder() == questionVersionTraductionIndex).findFirst().get();
                        QuestionVersionTraductionEntity questionVersionTraduction = QuestionVersionTraductionEntity.builder()
                                .setQuestionVersionId(currentQuestion.getId())
                                .setLanguage(languageShort)
                                .setText(partText)
                                .setFormattedText(partText)
                                .setInfo(text)
                                .setFormattedInfo(text)
                                .setName("")
                                .build();

                        if("".equals(text)){
                                questionVersionTraduction.setInfo(null);
                                questionVersionTraduction.setFormattedInfo(null);
                        }

                        questionVersionTraductionDal.save(questionVersionTraduction);

                        int answerIndex = 1;
                        for (String answer: allAnswerTraduction){
                                List<AnswerChoiceEntity> allAnswerChoice = answerChoiceDal.findAllByQuestionId(currentQuestion.getId());
                                int finalAnswerIndex = answerIndex;
                                AnswerChoiceEntity currentAnswer = allAnswerChoice.stream().filter(a -> a.getAnswer_order() == finalAnswerIndex).findFirst().get();

                                AnswerChoiceTraductionEntity answerTraduction = AnswerChoiceTraductionEntity.builder()
                                        .setLanguage(languageShort)
                                        .setText(answer)
                                        .setFormattedText(answer)
                                        .setAnswerChoiceId(currentAnswer.getId())
                                        .setName("")
                                        .build();

                                answerChoiceTraductionDal.save(answerTraduction);
                                answerIndex++;
                        }
                        index++;
                }
                return index;
        }
}


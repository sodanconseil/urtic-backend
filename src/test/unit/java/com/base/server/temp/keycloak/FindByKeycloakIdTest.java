package unit.java.com.base.server.temp.keycloak;


import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import com.base.server.BaseKeycloakStarter;
import com.base.server.dosage.persistence.jpa.DosageDal;
import com.base.server.dosage.persistence.jpa.DosageEntity;
import com.base.server.drug.persistence.jpa.DrugDal;
import com.base.server.drug.persistence.jpa.DrugEntity;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.frequency.persistence.jpa.FrequencyDal;
import com.base.server.frequency.persistence.jpa.FrequencyEntity;
import com.base.server.generic.presentation.JwtTokenParser;
import com.base.server.generic.presentation.JwtTokenParser.JwtToken;
import com.base.server.prescription.business.Prescription;
import com.base.server.prescription.persistence.jpa.PrescriptionDal;
import com.base.server.prescription.persistence.jpa.PrescriptionEntity;
import com.base.server.unit.persistence.jpa.UnitDal;
import com.base.server.unit.persistence.jpa.UnitEntity;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.persistence.jpa.UserDal;
import com.base.server.user.persistence.jpa.UserEntity;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest
public class FindByKeycloakIdTest {
    
    @Autowired
    UserDal userDal;

    @Test
    public void testFindByKeycloakId(){
        UserEntity userEntity = UserEntity.builder().setName("damn").setKeycloakId("1234567").setFirstName("jonathan").build();
        userEntity = userDal.save(userEntity);
        Optional<UserEntity> maybeFound = userDal.findByKeycloakId("1234567");
        System.out.println();
    }
}

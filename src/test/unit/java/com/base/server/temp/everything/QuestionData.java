package unit.java.com.base.server.temp.everything;

import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.surveyVersion.persistance.jpa.SurveyVersionEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder(setterPrefix = "set")
public class QuestionData {
    private SurveyEntity survey;

    private SurveyVersionEntity surveyVersion;

    private int order;

    private int verificationOrder;

    private String labelFr;

    private String labelEn;

    private String infoFr;

    private String infoEn;

    private List<String> choiceLabelsFr;

    private List<String> choiceLabelsEn;

    private List<String> supplementInputLabelsFr;

    private List<String> supplementInputLabelsEn;
}

package unit.java.com.base.server.temp.User;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import com.base.server.BaseKeycloakStarter;
import com.base.server.ethnicity.business.Ethnicity;
import com.base.server.ethnicity.business.interfaces.EthnicityBusiness;
import com.base.server.ethnicity.mappers.EthnicityDtoMapper;
import com.base.server.ethnicity.persistence.jpa.EthnicityDal;
import com.base.server.ethnicity.persistence.jpa.EthnicityEntity;
import com.base.server.user.business.User;
import com.base.server.user.business.interfaces.UserBusiness;
import com.base.server.user.mappers.UserEntityMapper;
import com.base.server.user.persistence.jpa.UserDal;
import com.base.server.user.persistence.jpa.UserEntity;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest
public class BusinessTest {
    

    @Autowired 
    UserBusiness userBusiness;

    @Test
    public void testUserBusiness() {
        String userId = "toto";

        Ethnicity caucasian = Ethnicity.builder().setName("caucasian").build();
        Ethnicity indian = Ethnicity.builder().setName("indian").build();
        User jonathan = User.builder().setName("Jonathan").build();

        jonathan.getEthnicities().add(caucasian);

        User jonathanSaved = userBusiness.save(jonathan, userId);
        String id = jonathanSaved.getId();

        User temp = userBusiness.get(id);

        jonathanSaved.getEthnicities().add(indian);

        jonathanSaved = userBusiness.save(jonathanSaved, userId);

        temp = userBusiness.get(id);
        
        System.out.println();
    }
}

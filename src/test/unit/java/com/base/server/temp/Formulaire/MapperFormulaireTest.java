package unit.java.com.base.server.temp.Formulaire;

import com.base.server.BaseKeycloakStarter;
import com.base.server.Question.Business.Question;
import com.base.server.Question.mappers.QuestionEntityMapper;
import com.base.server.Question.persistance.jpa.QuestionEntity;
import com.base.server.survey.business.Survey;
import com.base.server.survey.mappers.SurveyEntityMapper;
import com.base.server.survey.mappers.SurveyMapper;
import com.base.server.survey.persistance.jpa.SurveyEntity;
import com.base.server.survey.presentation.dtos.SurveyRequestDto;
import com.base.server.survey.presentation.dtos.SurveyResponseDto;
import com.base.server.userAnswer.business.UserAnswer;
import com.base.server.userAnswer.mappers.UserAnswerDtoMapper;
import com.base.server.userAnswer.mappers.UserAnswerEntityMapper;
import com.base.server.userAnswer.persistance.jpa.UserAnswerDal;
import com.base.server.userAnswer.persistance.jpa.UserAnswerEntity;
import com.base.server.userQuestion.Business.UserQuestion;
import com.base.server.userQuestion.mappers.UserQuestionDtoMapper;
import com.base.server.userQuestion.mappers.UserQuestionEntityMapper;
import com.base.server.userQuestion.persistance.jpa.UserQuestionDal;
import com.base.server.userQuestion.persistance.jpa.UserQuestionEntity;
import com.base.server.userQuestion.presentation.dtos.UserQuestionRequestDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = { BaseKeycloakStarter.class })
@SpringBootTest
public class MapperFormulaireTest {

    @Autowired
    QuestionEntityMapper questionEntityMapper;

    @Autowired
    SurveyEntityMapper surveyEntityMapper;


    @Autowired
    SurveyMapper surveyMapper;


    @Autowired
    UserQuestionDtoMapper userQuestionDtoMapper;

    @Autowired
    UserQuestionEntityMapper userQuestionEntityMapper;

    @Autowired
    UserQuestionDal userQuestionDal;

    @Autowired
    UserAnswerDal userAnswerDal;

    @Autowired
    UserAnswerEntityMapper userAnswerEntityMapper;


    @Test
    public void TestQuestion() {
        QuestionEntity questionEntity = QuestionEntity.builder().setName("Combien de medicaments").setText("Description des medicaments").build();
        Question question = this.questionEntityMapper.entityToBusinessObject(questionEntity);
        System.out.println();
    }

    @Test
    public void TestSurvey() {
        QuestionEntity questionEntity = QuestionEntity.builder().setName("Combien de medicaments").setText("Description des medicaments").build();
        Question question = this.questionEntityMapper.entityToBusinessObject(questionEntity);

      //  SurveyEntity surveyEntity = SurveyEntity.builder().setName("USAC57").setShortName("USAC").build();
       // Survey survey = this.surveyEntityMapper.entityToBusinessObject(surveyEntity);
       // survey.getQuestions().add(question);

        System.out.println();

    }

    @Test
    public void TestMapperDto(){
        SurveyRequestDto surveyRequestDto = SurveyRequestDto.builder().setName("request").build();
        SurveyResponseDto surveyResponseDto = this.surveyMapper.requestDtoToResponseDto(surveyRequestDto);

        System.out.println();
    }

}
